package com.zoylo.validator;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.lang.annotation.ElementType;
import java.lang.annotation.RetentionPolicy;
import javax.validation.Constraint;
import javax.validation.Payload;

import com.zoylo.gateway.web.rest.errors.CustomValidationConstant;

@Documented
@Constraint(validatedBy = PhoneValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface MobileNumber {
	String message() default CustomValidationConstant.MOBILE_LENGTH;
    
    Class<?>[] groups() default {};
     
    Class<? extends Payload>[] payload() default {};
}
