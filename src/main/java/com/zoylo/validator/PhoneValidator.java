package com.zoylo.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PhoneValidator implements ConstraintValidator<MobileNumber, String> {
	public static final String INDIA_MOBILE_CODE = "+91";
	/**
	 * this method will use further
	 */
	@Override
	public void initialize(MobileNumber paramA) {
	}

	@Override
	public boolean isValid(String phoneNo, ConstraintValidatorContext ctx) {
		if (phoneNo == null) {
			return false;
		}
		if(phoneNo.startsWith(INDIA_MOBILE_CODE)) {
			phoneNo = phoneNo.substring(3, phoneNo.length());
		}
		// validate phone numbers of format "1234567890"
		if (phoneNo.matches("-?\\\\d+(.\\\\d+)?") || phoneNo.matches("[5-9]{1}[0-9]{9}"))
			return true;
		else
			return false;
	}
}
