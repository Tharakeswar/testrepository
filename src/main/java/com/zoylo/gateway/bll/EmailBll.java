package com.zoylo.gateway.bll;

import java.io.IOException;

import freemarker.template.TemplateException;

public interface EmailBll {
	
	String verificationLinkTemplate(String name,String verificationLink) throws TemplateException, IOException;

	String ghostEmailVerification(String name) throws TemplateException, IOException;

	String accountActive(String name) throws TemplateException, IOException;

	String passwordChange(String name) throws TemplateException, IOException;

	String resetPassword(String name,String token) throws TemplateException, IOException;

	String generateOTP(String name,int otp) throws TemplateException, IOException;

	String passwordChangedSuccessfully(String name) throws TemplateException, IOException;

}

