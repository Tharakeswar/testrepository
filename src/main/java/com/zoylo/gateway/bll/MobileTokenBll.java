package com.zoylo.gateway.bll;

import java.util.Map;

import com.zoylo.gateway.model.MobileVerificationVM;

/**
 * @author Mehraj Malik
 * @version 1.0
 *
 */
public interface MobileTokenBll {

	public Map<String, String> getMobileToken(MobileVerificationVM verificationVM);

}
