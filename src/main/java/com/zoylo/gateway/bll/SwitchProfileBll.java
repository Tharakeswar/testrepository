package com.zoylo.gateway.bll;


import com.zoylo.gateway.model.ZoyloAuthVM;
import com.zoylo.gateway.security.CustomUserDetails;

public interface SwitchProfileBll {
	public String switchProfile(CustomUserDetails customUserDetails, String authority, String serviceProviderId);

	public boolean isAuthorized(String currentUserId, String serviceProviderId, String permissionCode);

	public ZoyloAuthVM getServiceProviderUserRole(String userId, String serviceProviderId, String providerType);
}
