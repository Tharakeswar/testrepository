package com.zoylo.gateway.bll.impl;

import static com.zoylo.gateway.service.util.Constants.CHANGE_PD_MESSAGE;

import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.amazonaws.services.simpleemail.model.SendEmailResult;
import com.zoylo.core.emailservice.EmailSEService;
import com.zoylo.core.emailservice.EmailTemplate;
import com.zoylo.core.smsservice.SMSService;
import com.zoylo.core.smsservice.SendSmsVM;
import com.zoylo.core.smsservice.SmsServiceResModel;
import com.zoylo.gateway.bll.AuditLogService;
import com.zoylo.gateway.bll.EmailBll;
import com.zoylo.gateway.bll.NotificationBll;
import com.zoylo.gateway.bll.UserBll;
import com.zoylo.gateway.bll.UserProfileBll;
import com.zoylo.gateway.domain.User;
import com.zoylo.gateway.domain.history.UserHistory;
import com.zoylo.gateway.model.ChangePasswordVM;
import com.zoylo.gateway.model.PasswordErrorVM;
import com.zoylo.gateway.repository.CustomRepository;
import com.zoylo.gateway.repository.UserRepository;
import com.zoylo.gateway.service.util.Constants;
import com.zoylo.gateway.web.rest.client.AdminRestClient;
import com.zoylo.gateway.web.rest.errors.CustomExceptionCode;
import com.zoylo.gateway.web.rest.errors.CustomParameterizedException;
import com.zoylo.gateway.web.rest.errors.RegisteredException;
import com.zoylo.gateway.web.rest.util.PasswordEncryption;

@Service
public class UserProfileBllImpl implements UserProfileBll {
	private final Logger log = LoggerFactory.getLogger(UserProfileBllImpl.class);
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private AuditLogService auditLogService;

	@Autowired
	private CustomRepository customRepository;

	@Value("${app.accessKey}")
	private String accesskey;

	@Value("${app.secretKey}")
	private String key;

	@Value("${app.emailFrom}")
	private String emailFrom;

	@Value("${app.registrationPort}")
	private String registrationPort;

	@Value("${app.change-password-subject}")
	private String subject;

	@Value("${app.change-password-body}")
	private String body;
	
	@Value("${app.sms.apiurl}")
	private String apiUrl;
	
	@Value("${app.sms.authkey}")
	private String authkey;
	
	@Value("${app.sms.country}")
	private String country;
	
	@Value("${app.sms.sender}")
	private String sender;
	
	@Value("${app.sms.route}")
	private String route;

	@Autowired
	private AdminRestClient adminRestClient;
	
	@Autowired
	private NotificationBll notificationBll;

	@Value("${app.mobileNotificationAllowed}")
	private Boolean mobileNotificationAllowed;
	
	@Value("${app.emailNotificationAllowed}")
	private Boolean emailNotificationAllowed;
	@Autowired
	private UserBll userBll;
	
	@Autowired
	private EmailBll emailBll;
	
	private static final String STATUS_CODE = "S";
	private static final String MESSAGE_MODE_CODE = "S";
	private static final String EMAIL_MODE_CODE = "E";

	private final PasswordEncoder passwordEncoder;

	public UserProfileBllImpl(UserRepository userRepository, PasswordEncoder passwordEncoder) {
		this.userRepository = userRepository;
		this.passwordEncoder = passwordEncoder;
	}

	/**
	 * @author Devendra.Kumar
	 * @description To change password
	 * @param userId
	 * @param changePasswordVM
	 * @exception CustomParameterizedException,MongoException
	 */
	public PasswordErrorVM changePassword(String userId, ChangePasswordVM changePasswordVM) {
		log.info("In Change Password of UserService ");
		
		try {
			PasswordErrorVM errorVM = new PasswordErrorVM();
			// encrypt password in sha256 first
			String oldPassword = PasswordEncryption.getEncryptedPassword(changePasswordVM.getOldPassword());
			String newPassword = PasswordEncryption.getEncryptedPassword(changePasswordVM.getNewPassword());
			changePasswordVM.setOldPassword(oldPassword);
			changePasswordVM.setNewPassword(newPassword);
			// check old and new password
			if (!changePasswordVM.getOldPassword().equals(changePasswordVM.getNewPassword())) {
				User user = userRepository.findOne(userId);
				if (Optional.ofNullable(user).isPresent()) {
					// to maintain history
					UserHistory history = new UserHistory();
					modelMapper.map(user, history);
					history.setId(null);
					history.setUserId(user.getId());
					if (passwordEncoder.matches(changePasswordVM.getOldPassword(), user.getEncryptedPassword())) {

						user.setEncryptedPassword(passwordEncoder.encode(changePasswordVM.getNewPassword()));
						userRepository.save(user);
						// to save history
						auditLogService.auditActionLog(Constants.CHANGE_PD, Constants.ZOYLO_USER);
						customRepository.save(history, Constants.ZOYLO_USER_HISTORY);
						// send email
						EmailSEService emailSEService = new EmailSEService();
						EmailTemplate emailTemplate = new EmailTemplate();
						emailTemplate.setAccessKey(accesskey);
						emailTemplate.setSecretKey(key);
						emailTemplate.setEmailFrom(emailFrom);
						emailTemplate.setSendTo(user.getEmailInfo().getEmailAddress());
						emailTemplate.setSubject(subject);
						//done
						emailTemplate.setBody(emailBll.passwordChange(user.getFirstName()));
						String messageId = "";
						// send emails only if notification is allowed and email address is verified
						if(emailNotificationAllowed && user.getEmailInfo().isVerifiedFlag()) {
							SendEmailResult sendEmailResult = emailSEService.sendEmail(emailTemplate);
							messageId = sendEmailResult.getMessageId();
						}
						// send notification for e-mail
						/*try {
							ZoyloNotificationVM zoyloNotificationVM = new ZoyloNotificationVM();
							RecipientInfo recipientInfo = new RecipientInfo();
							MessageInfo messageInfo = new MessageInfo();

							recipientInfo.setRecipientFirstName(user.getFirstName());
							recipientInfo.setRecipientLastName(user.getLastName());
							recipientInfo.setRecipientZoyloId(user.getZoyloId());
							recipientInfo.setRecipientId(user.getId());
							recipientInfo.setRecipientEmail(emailTemplate.getSendTo());

							messageInfo.setMessageText(emailTemplate.getBody());

							List<DeliveryTrail> deliveryTrail = new ArrayList<>();
							DeliveryTrail trail = new DeliveryTrail();
							trail.setDeliveryAttemptedAt(new Date());
							trail.setDeliveryStatusCode(STATUS_CODE);
							deliveryTrail.add(trail);

							zoyloNotificationVM.setNotificationMessageId(emailMessageId);
							zoyloNotificationVM.setNotificationModeCode(EMAIL_MODE_CODE);
							zoyloNotificationVM.setNotificationStatusCode(STATUS_CODE);
							zoyloNotificationVM.setRecipientInfo(recipientInfo);
							zoyloNotificationVM.setRetryCounter(1);
							zoyloNotificationVM.setMessageInfo(messageInfo);
							zoyloNotificationVM.setDeliveryTrail(deliveryTrail);

							adminRestClient.save(zoyloNotificationVM);
						} catch (Exception exception) {
							log.info("unable to save notification email");
						}*/
						notificationBll.saveNotificationAfterEmail(user, emailTemplate, messageId);
						// send sms
						SMSService smsService = new SMSService();
						SendSmsVM sendSmsVM = new SendSmsVM();
						sendSmsVM.setMessage(CHANGE_PD_MESSAGE);
						sendSmsVM.setNumber(user.getPhoneInfo().getPhoneNumber());
						sendSmsVM.setSecretKey(key);
						sendSmsVM.setAccesskey(accesskey);
						sendSmsVM.setApiUrl(apiUrl);
						sendSmsVM.setCountry(country);
						sendSmsVM.setAuthkey(authkey);
						sendSmsVM.setRoute(route);
						sendSmsVM.setSender(sender);
						String smsMessageId = "";
						if (mobileNotificationAllowed){
							SmsServiceResModel smsServiceResModel = smsService.sendSMSMessage(sendSmsVM);
							smsMessageId = smsServiceResModel.getRequestId();
						}

						/*try {
							ZoyloNotificationVM zoyloNotification = new ZoyloNotificationVM();
							RecipientInfo recipientPhoneInfo = new RecipientInfo();
							MessageInfo messagePhoneInfo = new MessageInfo();
							recipientPhoneInfo.setRecipientFirstName(user.getFirstName());
							recipientPhoneInfo.setRecipientLastName(user.getLastName());
							recipientPhoneInfo.setRecipientZoyloId(user.getZoyloId());
							recipientPhoneInfo.setRecipientId(user.getId());
							recipientPhoneInfo.setRecipientPhone(sendSmsVM.getNumber());

							messagePhoneInfo.setMessageText(sendSmsVM.getMessage());
							List<DeliveryTrail> deliveryTrails = new ArrayList<>();
							DeliveryTrail trails = new DeliveryTrail();
							trails.setDeliveryAttemptedAt(new Date());
							trails.setDeliveryStatusCode(STATUS_CODE);
							deliveryTrails.add(trails);

							zoyloNotification.setDeliveryTrail(deliveryTrails);
							zoyloNotification.setNotificationMessageId(messageId);
							zoyloNotification.setNotificationModeCode(MESSAGE_MODE_CODE);
							zoyloNotification.setNotificationStatusCode(STATUS_CODE);
							zoyloNotification.setRecipientInfo(recipientPhoneInfo);
							zoyloNotification.setRetryCounter(1);
							zoyloNotification.setMessageInfo(messagePhoneInfo);

							adminRestClient.save(zoyloNotification);
						} catch (Exception exception) {
							log.info("unable to save notification sms");
						}*/

						notificationBll.saveNotificationAfterSMS(user, sendSmsVM, smsMessageId);
						return null;
					} else {
						log.info("old password incorrect");
						errorVM.setErrorCode(CustomExceptionCode.PASSWORD_INCORRECT.getErrCode());
						errorVM.setErrorMsg(CustomExceptionCode.PASSWORD_INCORRECT.getErrMsg());
						return errorVM;
					}
				} else {
					log.info("User not exist  {}", userId);
					errorVM.setErrorCode(CustomExceptionCode.USER_NOT_FOUND.getErrCode());
					errorVM.setErrorMsg(CustomExceptionCode.USER_NOT_FOUND.getErrMsg());
					return errorVM;
				}
			} else {
				// old and new password should not be same.
				log.info("Old and new Password should not be same");
				errorVM.setErrorCode(CustomExceptionCode.OLD_AND_NEW_PASSWORD_SAME_EXCEPTION.getErrCode());
				errorVM.setErrorMsg(CustomExceptionCode.OLD_AND_NEW_PASSWORD_SAME_EXCEPTION.getErrMsg());
				return errorVM;
			}
		} catch (Exception exception) {

			throw new CustomParameterizedException(RegisteredException.USER_PROFILE_EXCEPTION.getException(),
					CustomExceptionCode.CHANGE_PASSWORD_ERROR.getErrMsg(), exception,
					CustomExceptionCode.CHANGE_PASSWORD_ERROR.getErrCode());

		}
	}
}
