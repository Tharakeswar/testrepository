package com.zoylo.gateway.bll.impl;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.zoylo.gateway.bll.EmailBll;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
@Service
public class EmailBllImpl implements EmailBll {

	private static final String TEMPLATE = "/templates/email";
	private static final String VERIFICATION_EMAIL = "VerificationEmail.html";
	private static final String ACCOUNT_ACTIVE = "AccountActive.html";
	private static final String GHOST_EMAIL_VERIFICATION = "GhostUserEmailVerification.html";
	private static final String PASSWORD_CHANGE = "PasswordChange.html";
	private static final String RESET_PASSWORD = "ResetPassword.html";
	private static final String CHANGE_PASSWORD_SUCCESSFULLY = "ChangePasswordSuccessfully.html";
	private static final String TOKEN = "token";
	private static final String OTP = "sixDigitOtp";
	private static final String GENERATE_OTP = "GenerateOTP.html";
	private static final String NAME = "name";
	private static final String VERIFICATION_LINK = "link";
	private static final String UTC = "UTF-8";

	/**
	 * @author damini.arora
	 * @param name,verificationLink
	 * @return String
	 */
	@Override
	public String verificationLinkTemplate(String name, String verificationLink) throws TemplateException, IOException {
		Configuration cfg = new Configuration();
		cfg.setClassForTemplateLoading(EmailBllImpl.class, TEMPLATE);
		cfg.setDefaultEncoding(UTC);
		cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
		Template template = cfg.getTemplate(VERIFICATION_EMAIL);
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(NAME, name);
		paramMap.put(VERIFICATION_LINK, verificationLink);
		Writer body = new StringWriter();
		template.process(paramMap, body);
		return body.toString();
	}

	/**
	 * @author damini.arora
	 * @param name
	 * @return String
	 */
	@Override
	public String ghostEmailVerification(String name) throws TemplateException, IOException {
		Configuration cfg = new Configuration();
		cfg.setClassForTemplateLoading(EmailBllImpl.class, TEMPLATE);
		cfg.setDefaultEncoding(UTC);
		cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
		Template template = cfg.getTemplate(GHOST_EMAIL_VERIFICATION);
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(NAME, name);
		Writer body = new StringWriter();
		template.process(paramMap, body);
		return body.toString();
	}

	/**
	 * @author damini.arora
	 * @param name
	 * @return String
	 */
	@Override
	public String accountActive(String name) throws TemplateException, IOException {
		Configuration cfg = new Configuration();
		cfg.setClassForTemplateLoading(EmailBllImpl.class, TEMPLATE);
		cfg.setDefaultEncoding(UTC);
		cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
		Template template = cfg.getTemplate(ACCOUNT_ACTIVE);
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(NAME, name);
		Writer body = new StringWriter();
		template.process(paramMap, body);
		return body.toString();
	}

	/**
	 * @author damini.arora
	 * @param name
	 * @return String
	 */
	@Override
	public String passwordChange(String name) throws TemplateException, IOException {
		Configuration cfg = new Configuration();
		cfg.setClassForTemplateLoading(EmailBllImpl.class, TEMPLATE);
		cfg.setDefaultEncoding(UTC);
		cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
		Template template = cfg.getTemplate(PASSWORD_CHANGE);
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(NAME, name);
		Writer body = new StringWriter();
		template.process(paramMap, body);
		return body.toString();
	}

	/**
	 * @author damini.arora
	 * @param name,token
	 * @return String
	 */
	@Override
	public String resetPassword(String name, String token) throws TemplateException, IOException {
		Configuration cfg = new Configuration();
		cfg.setClassForTemplateLoading(EmailBllImpl.class, TEMPLATE);
		cfg.setDefaultEncoding(UTC);
		cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
		Template template = cfg.getTemplate(RESET_PASSWORD);
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(NAME, name);
		paramMap.put(TOKEN, token);
		Writer body = new StringWriter();
		template.process(paramMap, body);
		return body.toString();
	}

	@Override
	public String generateOTP(String name, int otp) throws TemplateException, IOException {
		Configuration cfg = new Configuration();
		cfg.setClassForTemplateLoading(EmailBllImpl.class, TEMPLATE);
		cfg.setDefaultEncoding(UTC);
		cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
		Template template = cfg.getTemplate(GENERATE_OTP);
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put(NAME, name);
		paramMap.put(OTP, otp);
		Writer body = new StringWriter();
		template.process(paramMap, body);
		return body.toString();
	}

	/**
	 * @author damini.arora
	 * @param name
	 * @return String
	 */
	@Override
	public String passwordChangedSuccessfully(String name) throws TemplateException, IOException {
		Configuration cfg = new Configuration();
		cfg.setClassForTemplateLoading(EmailBllImpl.class, TEMPLATE);
		cfg.setDefaultEncoding(UTC);
		cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
		Template template = cfg.getTemplate(CHANGE_PASSWORD_SUCCESSFULLY);
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(NAME, name);
		Writer body = new StringWriter();
		template.process(paramMap, body);
		return body.toString();
	}
}
