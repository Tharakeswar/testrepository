package com.zoylo.gateway.bll.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mongodb.MongoException;
import com.zoylo.gateway.bll.AuditLogService;
import com.zoylo.gateway.bll.RoleBll;
import com.zoylo.gateway.domain.GrantedPermissionData;
import com.zoylo.gateway.domain.GrantedPermissionsList;
import com.zoylo.gateway.domain.Language;
import com.zoylo.gateway.domain.Permission;
import com.zoylo.gateway.domain.PermissionData;
import com.zoylo.gateway.domain.PermissionList;
import com.zoylo.gateway.domain.Role;
import com.zoylo.gateway.domain.RoleData;
import com.zoylo.gateway.domain.RoleHistory;
import com.zoylo.gateway.domain.ZoyloAuthCategory;
import com.zoylo.gateway.model.PageInfoVM;
import com.zoylo.gateway.model.PageableVM;
import com.zoylo.gateway.model.RoleEditVM;
import com.zoylo.gateway.model.RoleVM;
import com.zoylo.gateway.model.ZoyloPermissionVM;
import com.zoylo.gateway.model.ZoyloRoleVM;
import com.zoylo.gateway.mv.RoleMV;
import com.zoylo.gateway.repository.AuthCategoryRepository;
import com.zoylo.gateway.repository.CustomRepository;
import com.zoylo.gateway.repository.LanguageRepository;
import com.zoylo.gateway.repository.PermissionRepository;
import com.zoylo.gateway.repository.RoleRepository;
import com.zoylo.gateway.repository.ZoyloRoleCustomRepository;
import com.zoylo.gateway.web.rest.errors.CustomExceptionCode;
import com.zoylo.gateway.web.rest.errors.CustomParameterizedException;
import com.zoylo.gateway.web.rest.errors.RegisteredException;

/**
 * 
 * @author Diksha Gupta
 * @version 1.0
 * @description Class for User role management
 *
 */

@Service
public class RoleBllImpl implements RoleBll {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private ModelMapper modelMapper;
	@Autowired
	private LanguageRepository languageRepository;
	@Autowired
	private CustomRepository customRepository;
	@Autowired
	private AuditLogService auditLogService;
	@Autowired
	private AuthCategoryRepository authCategoryRepository;
	@Autowired
	private PermissionRepository permissionRepository;
	private boolean falseValue = false;
//	@Autowired
//	private MongoOperations mongoOperations;
	@Autowired
	private ZoyloRoleCustomRepository zoyloRoleCustomRepository;

	/**
	 * @author Diksha Gupta
	 * @description To get all the defined roles.
	 * @param
	 * @return List<RoleModel>
	 * @exception CustomParameterizedException
	 */
	@Override
	public List<RoleMV> getall() {
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		logger.info("Info: ---------- Get all Role service. ----------");
		try {
			List<Role> roleList = roleRepository.findAll();
			RoleMV roleModelArray[] = modelMapper.map(roleList, RoleMV[].class);
			List<RoleMV> roleModuleList = Arrays.asList(roleModelArray);
			logger.info("Info: ---------- Roles fetched successfully. ----------");
			return roleModuleList;
		} catch (MongoException exception) {
			logger.error("Error: ---------- All Roles not fetched ----------");
			logger.error(exception.getMessage());
			throw new CustomParameterizedException(RegisteredException.ROLE_EXCEPTION.getException(),
					CustomExceptionCode.DB_ERROR.getErrMsg(), exception, CustomExceptionCode.DB_ERROR.getErrCode());
		} catch (Exception exception) {
			logger.error("Error: ---------- All Roles not fetched ----------");
			logger.error(exception.getMessage());
			throw new CustomParameterizedException(RegisteredException.ROLE_EXCEPTION.getException(),
					CustomExceptionCode.DB_ERROR.getErrMsg(), exception, CustomExceptionCode.DB_ERROR.getErrCode());
		}
	}

	/**
	 * @author Diksha Gupta
	 * @description To get role by given role code.
	 * @param roleCode
	 * @return RoleMV
	 * @exception CustomParameterizedException
	 */
	@Override
	public List<RoleMV> getRole(String roleValue, String authCategoryCode) {
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		logger.info("Info: ---------- Role by code fetch service. ----------");
		try {
			List<Role> roles = new ArrayList<>();
			if (roleValue != null && !roleValue.isEmpty()) {
				Role role = roleRepository.findOne(roleValue);
				if (role != null) {
					roles.add(role);
				}
				if (roles.isEmpty()) {
					roles = roleRepository.findByRoleCode(roleValue);
				}
			} else {
				roles = roleRepository.findByAuthCategoryCode(authCategoryCode);
			}
			RoleMV[] roleMVArray = modelMapper.map(roles, RoleMV[].class);
			logger.info("Info: ---------- Role fetched successfully. ----------");
			return Arrays.asList(roleMVArray);
		} catch (MongoException exception) {
			logger.error("Error: ---------- Role not fetched for role code: {} ", roleValue);
			logger.error(exception.getMessage());
			throw new CustomParameterizedException(RegisteredException.ROLE_EXCEPTION.getException(),
					CustomExceptionCode.DB_ERROR.getErrMsg(), exception, CustomExceptionCode.DB_ERROR.getErrCode());
		} catch (Exception exception) {
			logger.error("Error: ---------- Role not fetched for role code: {}", roleValue);
			logger.error(exception.getMessage());
			throw new CustomParameterizedException(RegisteredException.ROLE_EXCEPTION.getException(),
					CustomExceptionCode.DB_ERROR.getErrMsg(), exception, CustomExceptionCode.DB_ERROR.getErrCode());
		}
	}

	/**
	 * @author Diksha Gupta
	 * @description To save role for given data
	 * @param roleModel
	 * @return Boolean
	 * @exception CustomParameterizedException
	 */
	@Override
	public RoleMV save(RoleVM roleModel, String languageCode) {
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		logger.info("Info: ---------- Role add service. ----------");
		try {
			if (roleModel.getRoleCode() != null) {
				roleModel.setRoleCode(roleModel.getRoleCode().toUpperCase());
			}
			Role role = roleRepository.findOneByRoleCode(roleModel.getRoleCode());
			if (role != null) {
				throw new CustomParameterizedException(RegisteredException.ROLE_EXCEPTION.getException(),
						CustomExceptionCode.ROLE_CODE_ERROR.getErrMsg(),
						CustomExceptionCode.ROLE_CODE_ERROR.getErrCode());
			}
			role = new Role();
			Language language = languageRepository.findOneByLanguageCode(languageCode);
			modelMapper.map(roleModel, role);
			if (roleModel.isActiveFlag() == true) {
				role.setActiveFromDate(new Date());
			} else {
				role.setActiveTillDate(new Date());
			}
			List<RoleData> roleDataList = roleModel.getRoleData();
			List<RoleData> roleDataListNew = new ArrayList<>();
			for (RoleData roleData : roleDataList) {
				RoleData roleDataNew = new RoleData();
				roleDataNew.setLanguageCode(language.getLanguageCode());
				roleDataNew.setLanguageId(language.getId());
				roleDataNew.setRoleDescription(roleData.getRoleDescription());
				roleDataNew.setRoleName(roleData.getRoleName());
				roleDataListNew.add(roleDataNew);
			}
			role.setRoleData(roleDataListNew);
			ZoyloAuthCategory zoyloAuthCategory = authCategoryRepository
					.findByAuthCategoryCode(roleModel.getAuthCategoryCode());
			if (zoyloAuthCategory == null) {
				throw new CustomParameterizedException(RegisteredException.ROLE_EXCEPTION.getException(),
						CustomExceptionCode.AUTH_CATEGORY_ERROR.getErrMsg(),
						CustomExceptionCode.AUTH_CATEGORY_ERROR.getErrCode());
			}
			role.setAuthCategoryCode(zoyloAuthCategory.getAuthCategoryCode());
			role.setAuthCategoryId(zoyloAuthCategory.getId());
			role.setAuthCategoryData(zoyloAuthCategory.getAuthCategoryData());
			List<GrantedPermissionsList> grantedPermissionsLists = new ArrayList<>();
			for (PermissionList list : roleModel.getPermissionsList()) {
				Permission permission = permissionRepository.findOneByPermissionCode(list.getPermissionCode());
				if (permission != null) {
					GrantedPermissionsList permissionsList = new GrantedPermissionsList();
					permissionsList.setActiveFlag(permission.isActiveFlag());
					permissionsList.setPermissionCode(permission.getPermissionCode());
					permissionsList.setPermissionId(permission.getId());
					List<GrantedPermissionData> dataList = new ArrayList<>();
					for (PermissionData data : permission.getPermissionData()) {
						GrantedPermissionData grantedPermissionData = new GrantedPermissionData();
						grantedPermissionData.setLanguageCode(data.getLanguageCode());
						grantedPermissionData.setLanguageCode(data.getLanguageId());
						grantedPermissionData.setPermissionName(data.getPermissionName());
						dataList.add(grantedPermissionData);
					}
					permissionsList.setPermissionData(dataList);
					
					permissionsList.setSystemDefined(permission.isSystemDefined());
					grantedPermissionsLists.add(permissionsList);
				}
			}
			role.setPermissionList(grantedPermissionsLists);
			role = roleRepository.save(role);
			RoleMV roleMV = new RoleMV();
			modelMapper.map(role, roleMV);

			logger.info("Info: ---------- Role saved successfully. ----------");
			return roleMV;
		} catch (MongoException exception) {
			logger.error("Error: ---------- Role not saved .----------");
			logger.error(exception.getMessage());
			throw new CustomParameterizedException(RegisteredException.ROLE_EXCEPTION.getException(),
					CustomExceptionCode.ROLES_CREATION_ERROR.getErrMsg(), exception,
					CustomExceptionCode.ROLES_CREATION_ERROR.getErrCode());
		} catch (CustomParameterizedException customParameterizedException) {
			logger.error("Error: ---------- Role must be unique .----------");
			throw customParameterizedException;
		} catch (Exception exception) {
			logger.error("Error: ---------- Role not saved .----------");
			logger.error(exception.getMessage());
			throw new CustomParameterizedException(RegisteredException.ROLE_EXCEPTION.getException(),
					CustomExceptionCode.ROLES_CREATION_ERROR.getErrMsg(), exception,
					CustomExceptionCode.ROLES_CREATION_ERROR.getErrCode());
		}

	}

	/**
	 * @author Diksha Gupta
	 * @description To update a role for given data
	 * @param roleModel
	 * @return boolean
	 * @exception CustomParameterizedException
	 */
	@Override
	public RoleMV update(RoleEditVM roleModel, String languageCode) {
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		logger.info("Info: ---------- Role update service. ----------");
		try {
			Role role = roleRepository.findOne(roleModel.getId());
			if (role == null)
				throw new CustomParameterizedException(RegisteredException.ROLE_EXCEPTION.getException(),
						CustomExceptionCode.ROLE_NOT_FOUND_ERROR.getErrMsg(),
						CustomExceptionCode.ROLE_NOT_FOUND_ERROR.getErrCode());
			if (!roleModel.getRoleCode().trim().equalsIgnoreCase(role.getRoleCode().trim())) {
				Role roleNew = roleRepository.findOneByRoleCode(roleModel.getRoleCode());
				if (roleNew != null) {
					throw new CustomParameterizedException(RegisteredException.ROLE_EXCEPTION.getException(),
							CustomExceptionCode.ROLE_CODE_ERROR.getErrMsg(),
							CustomExceptionCode.ROLE_CODE_ERROR.getErrCode());
				}
			}
			RoleHistory roleHistory = new RoleHistory();
			modelMapper.map(role, roleHistory);
			roleHistory.setId(null);
			roleHistory.setRoleId(role.getId());
			if (role.isActiveFlag() != falseValue) {
				if (roleModel.isActiveFlag() == true) {
					role.setActiveFromDate(new Date());
				} else {
					role.setActiveTillDate(new Date());
				}
			}
			Language language = languageRepository.findOneByLanguageCode(languageCode);
			List<RoleData> roleDataList = roleModel.getRoleData();
			List<RoleData> roleDataListNew = new ArrayList<>();
			for (RoleData roleData : roleDataList) {
				RoleData roleDataNew = new RoleData();
				roleDataNew.setLanguageCode(language.getLanguageCode());
				roleDataNew.setLanguageId(language.getId());
				roleDataNew.setRoleDescription(roleData.getRoleDescription());
				roleDataNew.setRoleName(roleData.getRoleName());
				roleDataListNew.add(roleDataNew);
			}
			roleModel.setRoleData(roleDataListNew);
			modelMapper.map(roleModel, role);
			List<GrantedPermissionsList> grantedPermissionsLists = new ArrayList<>();
			for (PermissionList list : roleModel.getPermissionsList()) {
				Permission permission = permissionRepository.findOneByPermissionCode(list.getPermissionCode());
				if (permission != null) {
					GrantedPermissionsList permissionsList = new GrantedPermissionsList();
					permissionsList.setActiveFlag(permission.isActiveFlag());
					permissionsList.setPermissionCode(permission.getPermissionCode());
					permissionsList.setPermissionId(permission.getId());
					List<GrantedPermissionData> dataList = new ArrayList<>();
					for (PermissionData data : permission.getPermissionData()) {
						GrantedPermissionData grantedPermissionData = new GrantedPermissionData();
						grantedPermissionData.setLanguageCode(data.getLanguageCode());
						grantedPermissionData.setLanguageCode(data.getLanguageId());
						grantedPermissionData.setPermissionName(data.getPermissionName());
						dataList.add(grantedPermissionData);
					}
					permissionsList.setPermissionData(dataList);
					permissionsList.setSystemDefined(permission.isSystemDefined());
					grantedPermissionsLists.add(permissionsList);
				}
			}
			role.setPermissionList(grantedPermissionsLists);
			role = roleRepository.save(role);
			auditLogService.auditActionLog("update", "zoyloRole");
			customRepository.save(roleHistory, "zoyloRoleHistory");
			RoleMV roleMV = new RoleMV();
			modelMapper.map(role, roleMV);
			logger.info("Info: ---------- Role updated successfully. ----------");
			return roleMV;
		} catch (MongoException exception) {
			logger.error("Error: ---------- Role not updated for Id: {} ", roleModel.getId());
			logger.error(exception.getMessage());
			throw new CustomParameterizedException(RegisteredException.ROLE_EXCEPTION.getException(),
					CustomExceptionCode.ROLES_UPDATION_ERROR.getErrMsg(), exception,
					CustomExceptionCode.ROLES_UPDATION_ERROR.getErrCode());
		} catch (Exception exception) {
			logger.error("Error: ---------- Role not updated for Id: {} ", roleModel.getId());
			logger.error(exception.getMessage());
			throw new CustomParameterizedException(RegisteredException.ROLE_EXCEPTION.getException(),
					CustomExceptionCode.ROLES_UPDATION_ERROR.getErrMsg(), exception,
					CustomExceptionCode.ROLES_UPDATION_ERROR.getErrCode());
		}
	}

	/**
	 * @author Diksha Gupta
	 * @description To delete a role for given data
	 * @param roleId
	 * @return boolean
	 * @exception CustomParameterizedException
	 */
	@Override
	public Boolean delete(String roleId) {
		logger.info("Info: ---------- Role delete service. ----------");
		try {
			Role role = roleRepository.findOne(roleId);
			if (role == null)
				throw new CustomParameterizedException(RegisteredException.ROLE_EXCEPTION.getException(),
						CustomExceptionCode.ROLE_NOT_FOUND_ERROR.getErrMsg(),
						CustomExceptionCode.ROLE_NOT_FOUND_ERROR.getErrCode());
			RoleHistory roleHistory = new RoleHistory();
			modelMapper.map(role, roleHistory);
			roleHistory.setId(null);
			roleHistory.setRoleId(role.getId());
			role.setActiveFlag(falseValue);
			roleRepository.save(role);
			auditLogService.auditActionLog("inactive", "zoyloRole");
			customRepository.save(roleHistory, "zoyloRoleHistory");
			logger.info("Info: ---------- Role inactive successfully. ----------");
			return Boolean.TRUE;
		} catch (MongoException exception) {
			logger.error("Error: ---------- Role not deleted for Id: {}", roleId);
			logger.error(exception.getMessage());
			throw new CustomParameterizedException(RegisteredException.ROLES_DELETION_ERROR.getException(),
					CustomExceptionCode.ROLES_DELETION_ERROR.getErrMsg(), exception,
					CustomExceptionCode.ROLES_DELETION_ERROR.getErrCode());
		} catch (CustomParameterizedException customParameterizedException) {
			logger.error("Error: ---------- Role must be unique .----------");
			throw customParameterizedException;
		} catch (Exception exception) {
			logger.error("Error: ---------- Role not deleted for Id: {}", roleId);
			logger.error(exception.getMessage());
			throw new CustomParameterizedException(RegisteredException.ROLES_DELETION_ERROR.getException(),
					CustomExceptionCode.ROLES_DELETION_ERROR.getErrMsg(), exception,
					CustomExceptionCode.ROLES_DELETION_ERROR.getErrCode());
		}
	}
	
	/**
	 * @author Ramesh
	 * @description To add a role for given data
	 * @param roleModel
	 * @return RoleMV
	 * @exception CustomParameterizedException
	 */

	@Override
	public RoleMV saveRole(ZoyloRoleVM roleModel, String languageCode) {
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		logger.info("Info: ---------- Role add service. ----------");
		RoleMV roleMV = null;
		try {
			if (roleModel.getRoleCode() != null){
				roleModel.setRoleCode(roleModel.getRoleCode().toUpperCase());
			}
			Role role = roleRepository.findOneByRoleCode(roleModel.getRoleCode());
			if (role != null) {
				throw new CustomParameterizedException(RegisteredException.ROLE_EXCEPTION.getException(),
						CustomExceptionCode.ROLE_CODE_ERROR.getErrMsg(),
						CustomExceptionCode.ROLE_CODE_ERROR.getErrCode());
			}
			role = new Role();
			Language language = languageRepository.findOneByLanguageCode(languageCode);
			modelMapper.map(roleModel, role);
			if (roleModel.isActiveFlag()) {
				role.setActiveFromDate(new Date());
			}
			List<RoleData> roleDataList = roleModel.getRoleData();
			List<RoleData> roleDataListNew = new ArrayList<>();
			for (RoleData roleData : roleDataList) {
				RoleData roleDataNew = new RoleData();
				roleDataNew.setLanguageId(language.getId());
				roleDataNew.setLanguageCode(languageCode);
				roleDataNew.setRoleName(roleData.getRoleName());
				roleDataNew.setRoleDescription(roleData.getRoleDescription());
				roleDataListNew.add(roleDataNew);
			}
			role.setRoleData(roleDataListNew);
			ZoyloAuthCategory zoyloAuthCategory = authCategoryRepository
					.findByAuthCategoryCode(roleModel.getAuthCategoryCode());
			if (zoyloAuthCategory == null) {
				throw new CustomParameterizedException(RegisteredException.ROLE_EXCEPTION.getException(),
						CustomExceptionCode.AUTH_CATEGORY_ERROR.getErrMsg(),
						CustomExceptionCode.AUTH_CATEGORY_ERROR.getErrCode());
			}
			role.setAuthCategoryId(zoyloAuthCategory.getId());
			role.setAuthCategoryCode(zoyloAuthCategory.getAuthCategoryCode());
			role.setAuthCategoryData(zoyloAuthCategory.getAuthCategoryData());
			role = roleRepository.save(role);
			roleMV = new RoleMV();
			modelMapper.map(role, roleMV);
			logger.info("Info: ---------- Role saved successfully. ----------");

		} catch (MongoException exception) {
			logger.error("Error:---------- Role not saved. ----------");
			logger.error(exception.getMessage());
		} catch (Exception exception) {
			logger.error("Error:---------- Role not saved. ----------");
			throw new CustomParameterizedException(RegisteredException.ROLE_EXCEPTION.getException(),
					CustomExceptionCode.ROLES_CREATION_ERROR.getErrMsg(), exception,
					CustomExceptionCode.ROLES_CREATION_ERROR.getErrCode());
		}
		return roleMV;

	}
	/**
	 * @author Ramesh
	 * @description To add a permission for given data
	 * @param roleModel
	 * @return RoleMV
	 * @exception CustomParameterizedException
	 */
	@Override
	public RoleMV assignPermission(ZoyloPermissionVM permissionModel,String languageCode) {
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		logger.info("Info: ---------- Role add service. ----------");
		RoleMV roleMV = null;
        try{
        	Role role = roleRepository.findOneByRoleCode(permissionModel.getRoleCode());
        	Permission permission = permissionRepository.findOneByPermissionCode(permissionModel.getPermissionCode());
        	Language language = languageRepository.findOneByLanguageCode(languageCode);
    	
        	List<GrantedPermissionsList> permissionList = role.getPermissionList();
    		GrantedPermissionsList permissionsListObj = new GrantedPermissionsList();
    		permissionsListObj.setActiveFlag(permission.isActiveFlag());
    		if(permission.isActiveFlag()){
    			permissionsListObj.setActiveFromDate(new Date());
    		}
    		permissionsListObj.setPermissionId(permission.getId());
			permissionsListObj.setPermissionCode(permission.getPermissionCode());
			List<GrantedPermissionData> permissionData = new ArrayList<>();
			GrantedPermissionData permissionDataObj = new GrantedPermissionData();
			
				permissionDataObj.setLanguageId(language.getId());
				permissionDataObj.setLanguageCode(language.getLanguageCode());
				List<PermissionData> permissions = permission.getPermissionData();
				for(PermissionData permissionObj :permissions){
					permissionDataObj.setPermissionName(permissionObj.getPermissionName());
				}
				permissionData.add(permissionDataObj);
			
			permissionsListObj.setPermissionData(permissionData);   	
			// check if permission is already present
        	if(permissionList == null || permissionList.isEmpty()){
        		// create new permission to be added
    			List<GrantedPermissionsList> permissionsListNew = new ArrayList<>();
    			permissionsListNew.add(permissionsListObj);
        		role.setPermissionList(permissionsListNew);
        	}	
			else {
				Optional<GrantedPermissionsList> checkPermissionExist = permissionList.stream().filter(
						permissionObj -> permissionObj.getPermissionCode().equals(permission.getPermissionCode()))
						.findFirst();
				if(checkPermissionExist.isPresent()){
					throw new CustomParameterizedException(RegisteredException.PERMISSION_EXCEPTION.getException(),
							CustomExceptionCode.PERMISSION_CODE_ERROR.getErrMsg(),
							CustomExceptionCode.PERMISSION_CODE_ERROR.getErrCode());
				}
				permissionList.add(permissionsListObj);
				role.setPermissionList(permissionList);
				
			}
        	
    		role = roleRepository.save(role);
    		roleMV = new RoleMV();
    		modelMapper.map(role, roleMV);        
    		logger.info("Info: ---------- Role saved successfully. ----------");
    		
    	}catch (MongoException exception) {
			logger.error("Error:---------- Role not saved. ----------");
			logger.error(exception.getMessage());
		} catch (CustomParameterizedException exception) {
			logger.error("Error:---------- Role must be unique. ----------");
			throw exception;
		} catch (Exception exception) {
			logger.error("Error:---------- Role not saved. ----------");
			throw new CustomParameterizedException(RegisteredException.ROLE_EXCEPTION.getException(),
					CustomExceptionCode.ROLES_CREATION_ERROR.getErrMsg(), exception,
					CustomExceptionCode.ROLES_CREATION_ERROR.getErrCode());
		}
		return roleMV;
	}
	
	/**
	 * @author Balram Sharma
	 * @description To update permission for given role.
	 * @param roleModel
	 * @return RoleMV
	 */
	@Override
	public RoleMV updateRolePermission(ZoyloPermissionVM permissionModel) {
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		RoleMV roleMV = null;
		String roleCodeVM = permissionModel.getRoleCode();
		String permissionCodeVM = permissionModel.getPermissionCode();
		boolean activeFlagVM = permissionModel.isActiveFlag();
		if (roleCodeVM == null) {
			throw new CustomParameterizedException(RegisteredException.ROLE_EXCEPTION.getException(),
					CustomExceptionCode.ROLE_CODE_NOT_FOUND_ERROR.getErrMsg(),
					CustomExceptionCode.ROLE_CODE_NOT_FOUND_ERROR.getErrCode());
		}

		Role roleFromDb = roleRepository.findOneByRoleCode(roleCodeVM);
		if (roleFromDb == null) {
			throw new CustomParameterizedException(RegisteredException.ROLE_EXCEPTION.getException(),
					CustomExceptionCode.ROLE_NOT_FOUND_ERROR.getErrMsg(),
					CustomExceptionCode.ROLE_NOT_FOUND_ERROR.getErrCode());
		}
		List<GrantedPermissionsList> permissionListFromDb = roleFromDb.getPermissionList();
		Optional<GrantedPermissionsList> optionalPermissionObject = permissionListFromDb.stream()
				.filter(grantedpermission -> grantedpermission.getPermissionCode().equals(permissionCodeVM))
				.findFirst();
		if (!optionalPermissionObject.isPresent()) {
			throw new CustomParameterizedException(RegisteredException.ROLE_EXCEPTION.getException(),
					CustomExceptionCode.PERMISSION_NOT_FOUND_ERROR.getErrMsg(),
					CustomExceptionCode.PERMISSION_NOT_FOUND_ERROR.getErrCode());
		}
		
	    GrantedPermissionsList permissionsRef = optionalPermissionObject.get();
	    // If its active and deactivated marked active till as current date
	    if (permissionsRef.isActiveFlag() && !activeFlagVM){
	    	permissionsRef.setActiveTillDate(new Date());
	    } else if (!permissionsRef.isActiveFlag() && activeFlagVM){
		    // If its deactivated and activated marked active from as current date
	    	permissionsRef.setActiveFromDate(new Date());
	    	permissionsRef.setActiveTillDate(null);
	    }
	    // updating activeFlag value by reference object of permission.
	    permissionsRef.setActiveFlag(activeFlagVM);

		try {
			Role roleDbResponse = roleRepository.save(roleFromDb);
			roleMV = new RoleMV();
    		modelMapper.map(roleDbResponse, roleMV);    

		} catch (MongoException exception) {
			logger.error("Error:---------- Role not saved. ----------");
			logger.error(exception.getMessage());
		} catch (Exception exception) {
			logger.error("Error:---------- Role not saved. ----------");
			throw new CustomParameterizedException(RegisteredException.ROLE_EXCEPTION.getException(),
					CustomExceptionCode.ROLES_CREATION_ERROR.getErrMsg(), exception,
					CustomExceptionCode.ROLES_CREATION_ERROR.getErrCode());
		}
		return roleMV;
	}

	/**
	 * @author Balram Sharma
	 * @description search role by role properties.
	 * @param roleCode
	 * @param roleName
	 * @param authCategoryCode
	 * @param authCategoryName
	 * @param activeFlag
	 * @return PageableVM<RoleMV>
	 * @exception CustomParameterizedException
	 */
	@Override
	public PageableVM<RoleMV> searchRoles(Pageable pageable, String roleCode, String roleName, String authCategoryCode,
			String authCategoryName, boolean activeFlag, String languageCode) {
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		Page<Role> rolePages = zoyloRoleCustomRepository.findByRoleProperties(pageable, roleCode, roleName,
				authCategoryCode, authCategoryName, activeFlag, languageCode);
		PageableVM<RoleMV> rolePageableVM = new PageableVM<>();
		if (!rolePages.getContent().isEmpty()) {
			RoleMV[] roleMV = modelMapper.map(rolePages.getContent(), RoleMV[].class);
			rolePageableVM.setContent(Arrays.asList(roleMV));
		} else {
			rolePageableVM.setContent(new ArrayList<>());
		}

		PageInfoVM pageDetails = modelMapper.map(rolePages, PageInfoVM.class);
		rolePageableVM.setPageInfo(pageDetails);
		return rolePageableVM;
	}
	
	/**
	 * @author Ramesh
	 * @description To update a role for given data
	 * @param roleModel
	 * @return boolean
	 * @exception CustomParameterizedException
	 */
	@Override
	public RoleMV updateRole(ZoyloRoleVM roleModel,String languageCode) {
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		logger.info("Info: ---------- Role update service. ----------");
		try {
			Role role = roleRepository.findOneByRoleCode(roleModel.getRoleCode());
			
			if (null == role)
				throw new CustomParameterizedException(RegisteredException.ROLE_EXCEPTION.getException(),
						CustomExceptionCode.ROLE_NOT_FOUND_ERROR.getErrMsg(),
						CustomExceptionCode.ROLE_NOT_FOUND_ERROR.getErrCode());
			
			if (!roleModel.getRoleCode().trim().equalsIgnoreCase(role.getRoleCode().trim())) {
				Role roleNew = roleRepository.findOneByRoleCode(roleModel.getRoleCode());
				if (roleNew != null) {
					throw new CustomParameterizedException(RegisteredException.ROLE_EXCEPTION.getException(),
							CustomExceptionCode.ROLE_CODE_ERROR.getErrMsg(),
							CustomExceptionCode.ROLE_CODE_ERROR.getErrCode());
				}
			}
			RoleHistory roleHistory = new RoleHistory();
			modelMapper.map(role, roleHistory);
			roleHistory.setId(null);
			roleHistory.setRoleId(role.getId());
			
			if (role.isActiveFlag() && !roleModel.isActiveFlag()){
				role.setActiveTillDate(new Date());
			} else if (!role.isActiveFlag() && roleModel.isActiveFlag()){
				role.setActiveFromDate(new Date());				
				role.setActiveTillDate(null);
			}
			
			
			List<RoleData> roleDataListVM = roleModel.getRoleData();
			
			RoleData roleDataVM = roleDataListVM.get(0);
			// below data is given by user 
			String languageCodeVM = roleDataVM.getLanguageCode();
			String roleNameVM = roleDataVM.getRoleName();
			String roleDescriptionVM = roleDataVM.getRoleDescription();
			
			List<RoleData> roleDataListRef = role.getRoleData();
			RoleData roleDataRef = roleDataListRef.stream()
					.filter(roleData -> roleData.getLanguageCode().equals(languageCodeVM)).findFirst().get();
			
			// updating roleName and roleDescription by user given value
			roleDataRef.setRoleName(roleNameVM);
			roleDataRef.setRoleDescription(roleDescriptionVM);
			
		
			ZoyloAuthCategory zoyloAuthCategory = authCategoryRepository
					.findByAuthCategoryCode(role.getAuthCategoryCode());
			
			if (null == zoyloAuthCategory) {
				throw new CustomParameterizedException(RegisteredException.ROLE_EXCEPTION.getException(),
						CustomExceptionCode.AUTH_CATEGORY_ERROR.getErrMsg(),
						CustomExceptionCode.AUTH_CATEGORY_ERROR.getErrCode());
			}
			
			zoyloAuthCategory.setAuthCategoryCode(roleModel.getAuthCategoryCode());
			role.setAuthCategoryId(zoyloAuthCategory.getId());
			role.setAuthCategoryCode(zoyloAuthCategory.getAuthCategoryCode());
			role.setAuthCategoryData(zoyloAuthCategory.getAuthCategoryData());
			role.setActiveFlag(roleModel.isActiveFlag());

			role = roleRepository.save(role);
			auditLogService.auditActionLog("update", "zoyloRole");
			customRepository.save(roleHistory, "zoyloRoleHistory");
			RoleMV roleMV = new RoleMV();
			modelMapper.map(role, roleMV);
			logger.info("Info: ---------- Role updated successfully. ----------");
			return roleMV;
		} catch (MongoException exception) {
			logger.error("Error: ---------- Role not updated for code: {} ", roleModel.getRoleCode());
			logger.error(exception.getMessage());
			throw new CustomParameterizedException(RegisteredException.ROLE_EXCEPTION.getException(),
					CustomExceptionCode.ROLES_UPDATION_ERROR.getErrMsg(), exception,
					CustomExceptionCode.ROLES_UPDATION_ERROR.getErrCode());
		} catch (Exception exception) {
			logger.error("Error: ---------- Role not updated for Code: {} ", roleModel.getRoleCode());
			logger.error(exception.getMessage());
			throw new CustomParameterizedException(RegisteredException.ROLE_EXCEPTION.getException(),
					CustomExceptionCode.ROLES_UPDATION_ERROR.getErrMsg(), exception,
					CustomExceptionCode.ROLES_UPDATION_ERROR.getErrCode());
		}
	}
	/**
	 * @author Balram Sharma
	 * @description To get Role by id
	 * @param roleId
	 * @return RoleMV
	 */
	@Override
	public RoleMV getRoleById(String roleId) {
		Role roleResponseData = roleRepository.findOne(roleId);
		return modelMapper.map(roleResponseData, RoleMV.class);
	}

}
