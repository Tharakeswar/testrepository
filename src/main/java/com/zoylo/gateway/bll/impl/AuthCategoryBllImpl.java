package com.zoylo.gateway.bll.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mongodb.MongoException;
import com.zoylo.gateway.bll.AuthCategoryBll;
import com.zoylo.gateway.domain.AuthCategoryData;
import com.zoylo.gateway.domain.ZoyloAuthCategory;
import com.zoylo.gateway.model.AuthCategoryNameList;
import com.zoylo.gateway.model.PageInfoVM;
import com.zoylo.gateway.model.PageableVM;
import com.zoylo.gateway.model.ZoyloAuthCategoryVM;
import com.zoylo.gateway.mv.ZoyloAuthCategoryMV;
import com.zoylo.gateway.repository.AuthCategoryRepository;
import com.zoylo.gateway.web.rest.errors.CustomExceptionCode;
import com.zoylo.gateway.web.rest.errors.CustomParameterizedException;
import com.zoylo.gateway.web.rest.errors.RegisteredException;

/**
 * 
 * @author Balram Sharma
 * @version 1.0
 * @description Class for Auth Category management
 *
 */

@Service
public class AuthCategoryBllImpl implements AuthCategoryBll {

	@Autowired
	private AuthCategoryRepository authCategoryRepository;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ModelMapper modelMapper;

	/**
	 * @author Balram Sharma
	 * @description To save AuthCategory
	 * @param zoyloAuthCategoryVM
	 * @return ZoyloAuthCategoryMV
	 */
	@Override
	public ZoyloAuthCategoryMV saveAuthCategory(ZoyloAuthCategoryVM zoyloAuthCategoryVM) {

		String authCategoryCodeVM = zoyloAuthCategoryVM.getAuthCategoryCode();
		AuthCategoryData authCategoryDataVM = zoyloAuthCategoryVM.getAuthCategoryData();
		String authCategoryLandingPageUrlVM = zoyloAuthCategoryVM.getAuthCategoryLandingPageUrl();
		boolean activeFlagVM = zoyloAuthCategoryVM.isActiveFlag();

		if (null == authCategoryCodeVM) {
			throw new CustomParameterizedException(RegisteredException.AUTHCATEGORY_EXCEPTION.getException(),
					CustomExceptionCode.AUTHCATEGORY_CODE_NOT_FOUND.getErrMsg(),
					CustomExceptionCode.AUTHCATEGORY_CODE_NOT_FOUND.getErrCode());
		}
		// make the data in upper case
		if (authCategoryCodeVM != null){
			authCategoryCodeVM = authCategoryCodeVM.toUpperCase();
			zoyloAuthCategoryVM.setAuthCategoryCode(authCategoryCodeVM);
		}
		
		ZoyloAuthCategory zoyloAuthCategory = authCategoryRepository.findByAuthCategoryCode(authCategoryCodeVM);
		if (null != zoyloAuthCategory) { //
			throw new CustomParameterizedException(RegisteredException.AUTHCATEGORY_EXCEPTION.getException(),
					CustomExceptionCode.AUTHCATEGORY_CODE_ERROR.getErrMsg(),
					CustomExceptionCode.AUTHCATEGORY_CODE_ERROR.getErrCode());
		}
		if (null == authCategoryDataVM) {
			throw new CustomParameterizedException(RegisteredException.AUTHCATEGORY_EXCEPTION.getException(),
					CustomExceptionCode.AUTH_CATEGORY_ERROR.getErrMsg(),
					CustomExceptionCode.AUTH_CATEGORY_ERROR.getErrCode());
		}
		if (null == authCategoryLandingPageUrlVM) {
			throw new CustomParameterizedException(RegisteredException.AUTHCATEGORY_EXCEPTION.getException(),
					CustomExceptionCode.LANDINGPAGE_URL_NOT_FOUND.getErrMsg(),
					CustomExceptionCode.LANDINGPAGE_URL_NOT_FOUND.getErrCode());
		}

		zoyloAuthCategory = modelMapper.map(zoyloAuthCategoryVM, ZoyloAuthCategory.class);
		List<AuthCategoryData> newAuthCategoryDataList = new ArrayList<AuthCategoryData>();
		newAuthCategoryDataList.add(authCategoryDataVM);
		// setting language data to an object
		zoyloAuthCategory.setAuthCategoryData(newAuthCategoryDataList);
		if (activeFlagVM) {
			zoyloAuthCategory.setActiveFromDate(new Date());
		}

		try {
			ZoyloAuthCategory zoyloAuthCategoryResponseFromDb = authCategoryRepository.save(zoyloAuthCategory);
			return modelMapper.map(zoyloAuthCategoryResponseFromDb, ZoyloAuthCategoryMV.class);

		} catch (Exception exception) {
			logger.error(exception.getMessage());
			throw new CustomParameterizedException(RegisteredException.AUTHCATEGORY_EXCEPTION.getException(),
					CustomExceptionCode.DB_ERROR.getErrMsg(), exception, CustomExceptionCode.DB_ERROR.getErrCode());
		}

	}

	/**
	 * @author Balram Sharma
	 * @description To Update AuthCategory
	 * @param zoyloAuthCategoryVM
	 * @return ZoyloAuthCategoryMV
	 */
	@Override
	public ZoyloAuthCategoryMV updateAuthCategory(ZoyloAuthCategoryVM zoyloAuthCategoryVM) {

		String authCategoryCodeVM = zoyloAuthCategoryVM.getAuthCategoryCode();
		AuthCategoryData authCategoryDataVM = zoyloAuthCategoryVM.getAuthCategoryData();
		String authCategoryLandingPageUrlVM = zoyloAuthCategoryVM.getAuthCategoryLandingPageUrl();
		
		boolean activeFlagVM = zoyloAuthCategoryVM.isActiveFlag();

		ZoyloAuthCategory zoyloAuthCategoryFromDb = authCategoryRepository.findByAuthCategoryCode(authCategoryCodeVM);

		if (null == zoyloAuthCategoryFromDb) {
			throw new CustomParameterizedException(RegisteredException.AUTHCATEGORY_EXCEPTION.getException(),
					CustomExceptionCode.AUTHCATEGORY_CODE_NOT_FOUND.getErrMsg(),
					CustomExceptionCode.AUTHCATEGORY_CODE_NOT_FOUND.getErrCode());
		}

		if (null == authCategoryDataVM) {
			throw new CustomParameterizedException(RegisteredException.AUTHCATEGORY_EXCEPTION.getException(),
					CustomExceptionCode.AUTH_CATEGORY_ERROR.getErrMsg(),
					CustomExceptionCode.AUTH_CATEGORY_ERROR.getErrCode());
		}
		if (null == authCategoryLandingPageUrlVM) {
			throw new CustomParameterizedException(RegisteredException.AUTHCATEGORY_EXCEPTION.getException(),
					CustomExceptionCode.LANDINGPAGE_URL_NOT_FOUND.getErrMsg(),
					CustomExceptionCode.LANDINGPAGE_URL_NOT_FOUND.getErrCode());
		}
		// getting user given data
		String languageCodeVM = authCategoryDataVM.getLanguageCode();
		String authCategoryNameVM = authCategoryDataVM.getAuthCategoryName();
		String authCategoryDescriptionVM = authCategoryDataVM.getAuthCategoryDescription();

		// getting reference of AuthCategoryDataList from
		// zoyloAuthCategoryFromDb;
		List<AuthCategoryData> authCategoryDataListRef = zoyloAuthCategoryFromDb.getAuthCategoryData();
		// getting reference of AuthCategoryData from AuthCategoryDataList;
		AuthCategoryData authCategoryDataRef = authCategoryDataListRef.stream()
				.filter(authCategoryData -> authCategoryData.getLanguageCode().equals(languageCodeVM)).findFirst()
				.get();
		// setting authCategoryNameVM & authCategoryLandingPageUrlVM to an
		// existing zoyloAuthCategory object
		authCategoryDataRef.setAuthCategoryName(authCategoryNameVM);
		authCategoryDataRef.setAuthCategoryDescription(authCategoryDescriptionVM);
		zoyloAuthCategoryFromDb.setAuthCategoryLandingPageUrl(authCategoryLandingPageUrlVM);

		if (zoyloAuthCategoryFromDb.isActiveFlag() != activeFlagVM) {
			if (activeFlagVM) {
				zoyloAuthCategoryFromDb.setActiveFromDate(new Date());
				zoyloAuthCategoryFromDb.setActiveTillDate(null);
			} else {
				zoyloAuthCategoryFromDb.setActiveTillDate(new Date());
			}
		}
		zoyloAuthCategoryFromDb.setActiveFlag(activeFlagVM);
		try {
			ZoyloAuthCategory zoyloAuthCategoryResponseFromDb = authCategoryRepository.save(zoyloAuthCategoryFromDb);
			return modelMapper.map(zoyloAuthCategoryResponseFromDb, ZoyloAuthCategoryMV.class);
		} catch (MongoException exception) {
			logger.error(exception.getMessage());
			throw new CustomParameterizedException(RegisteredException.AUTHCATEGORY_EXCEPTION.getException(),
					CustomExceptionCode.DB_ERROR.getErrMsg(), exception, CustomExceptionCode.DB_ERROR.getErrCode());
		} catch (CustomParameterizedException exception) {
			logger.error(exception.getMessage());
			throw exception;
		} catch (Exception exception) {
			logger.error(exception.getMessage());
			throw new CustomParameterizedException(RegisteredException.AUTHCATEGORY_EXCEPTION.getException(),
					CustomExceptionCode.DB_ERROR.getErrMsg(), exception, CustomExceptionCode.DB_ERROR.getErrCode());
		}

	}

	/**
	 * @author Balram Sharma
	 * @description filtering zoyloAuthCategory by using LanguageCode
	 * @param languageCode
	 * @return List<AuthCategoryNameList>
	 */
	@Override
	public List<AuthCategoryNameList> findAllAuthCode(String languageCode) {
		// getting all data from database
		List<ZoyloAuthCategory> responseFromDb = authCategoryRepository.findByActiveFlagIsTrue();
		List<AuthCategoryNameList> resultList = new ArrayList<>();
		// filtering data by using languageCode and setting to resultList object
		for (ZoyloAuthCategory zoyloAuthCategory : responseFromDb) {
		
			AuthCategoryNameList authCategoryNameList = new AuthCategoryNameList();
			authCategoryNameList.setId(zoyloAuthCategory.getId());
			authCategoryNameList.setAuthCategoryCode(zoyloAuthCategory.getAuthCategoryCode());
			authCategoryNameList.setActiveFlag(zoyloAuthCategory.isActiveFlag());
			authCategoryNameList.setAllowRoleFlag(zoyloAuthCategory.isAllowRoleFlag());
			List<AuthCategoryData> authCategoryDataRef = zoyloAuthCategory.getAuthCategoryData();

			AuthCategoryData filterAuthData = authCategoryDataRef.stream()
					.filter(authCategoryData -> authCategoryData.getLanguageCode().equals(languageCode)).findFirst()
					.get();
			authCategoryNameList.setAuthCategoryName(filterAuthData.getAuthCategoryName());
			resultList.add(authCategoryNameList);
			
		}
		return resultList;
	}

	/**
	 * @author Balram Sharma
	 * @description To getAuthCategory by id
	 * @param id
	 * @return ZoyloAuthCategoryMV
	 */
	@Override
	public ZoyloAuthCategoryMV getAuthCategoryById(String id) {
		ZoyloAuthCategory responseFromDb = authCategoryRepository.findOne(id);
		return modelMapper.map(responseFromDb, ZoyloAuthCategoryMV.class);
	}
	/**
	 * @author Balram Sharma
	 * @description search ZoyloAuthCategory by using authCategoryCode & authCategoryName,
	 *              activeFlag.
	 * @param authCategoryCode
	 * @param authCategoryName
	 * @param activeFlag
	 * @param languageCode
	 * @return PageableVM<ZoyloAuthCategoryMV> 
	 */
	@Override
	public PageableVM<ZoyloAuthCategoryMV> searchAuthCategory(Pageable pageable, String authCategoryCode,
			String authCategoryName, boolean activeFlag, String languageCode) {

		Page<ZoyloAuthCategory> pages = authCategoryRepository.searchAuthCategory(pageable, authCategoryCode,
				authCategoryName, activeFlag, languageCode);

		List<ZoyloAuthCategory> zoyloAuthCategoryListFromDb = pages.getContent();
		ZoyloAuthCategoryMV[] zoyloAuthCategoryMVArray = modelMapper.map(zoyloAuthCategoryListFromDb,
				ZoyloAuthCategoryMV[].class);
		PageableVM<ZoyloAuthCategoryMV> zoyloAuthCategoryPageableMV = new PageableVM<>();
		zoyloAuthCategoryPageableMV.setContent(Arrays.asList(zoyloAuthCategoryMVArray));
		PageInfoVM pageDetails = modelMapper.map(pages, PageInfoVM.class);
		zoyloAuthCategoryPageableMV.setPageInfo(pageDetails);
		return zoyloAuthCategoryPageableMV;
	}
	

}
