package com.zoylo.gateway.bll.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zoylo.gateway.bll.AuditLogService;
import com.zoylo.gateway.domain.AuditLog;
import com.zoylo.gateway.repository.CustomRepositoryImpl;

@Service
public class AuditLogServiceImpl implements AuditLogService{

	@Autowired
	private CustomRepositoryImpl customRepositoryImpl;
	
	/**
	 * @author damini arora
	 * @description To save all the updation record
	 */
	
	@Override
	public boolean auditActionLog(String action, String document) {
		AuditLog auditLog = new AuditLog(document,action);
		customRepositoryImpl.save(auditLog, "auditLog");
		return true;
	}

}
