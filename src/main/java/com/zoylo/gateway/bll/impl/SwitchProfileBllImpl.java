package com.zoylo.gateway.bll.impl;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zoylo.gateway.bll.SwitchProfileBll;
import com.zoylo.gateway.domain.GrantedPermissionsList;
import com.zoylo.gateway.domain.GrantedRolesList;
import com.zoylo.gateway.domain.Role;
import com.zoylo.gateway.domain.ServiceProvidersList;
import com.zoylo.gateway.domain.ZoyloUserAuth;
import com.zoylo.gateway.model.PermissionsListVM;
import com.zoylo.gateway.model.RecipientPermissionListVM;
import com.zoylo.gateway.model.ZoyloAuthVM;
import com.zoylo.gateway.repository.RoleRepository;
import com.zoylo.gateway.repository.ZoyloUserAuthRepository;
import com.zoylo.gateway.security.CustomPermission;
import com.zoylo.gateway.security.CustomUserDetails;
import com.zoylo.gateway.security.jwt.TokenProvider;
import com.zoylo.gateway.web.rest.client.RecipientRestClient;
import com.zoylo.gateway.web.rest.errors.CustomExceptionCode;
import com.zoylo.gateway.web.rest.errors.CustomParameterizedException;
import com.zoylo.gateway.web.rest.errors.RegisteredException;
import com.zoylo.gateway.web.rest.util.LookUp;

/**
 * @version 1.0
 * @author Ankur Goel
 *
 */
@Service
public class SwitchProfileBllImpl implements SwitchProfileBll {

	private final TokenProvider tokenProvider;
	@Autowired
	private RecipientRestClient recipientRestClient;
	@Autowired
	private ZoyloUserAuthRepository zoyloUserAuthRepository;

	@Autowired
	private RoleRepository roleRepository;

	private static final String DATA_CONSTANT = "data";

	public SwitchProfileBllImpl(TokenProvider tokenProvider) {
		this.tokenProvider = tokenProvider;
	}

	/**
	 * Recipient permission
	 * 
	 * @param userId
	 * @return
	 */
	private List<CustomPermission> recipientPermission(String userId) {
		List<CustomPermission> customPermissionList = new ArrayList<CustomPermission>();
		try {
			ResponseEntity<String> userPermission = recipientRestClient.getUserPermission(userId);
			String body = userPermission.getBody();
			JSONObject userJsonObj = new JSONObject(body);
			String userParseData = (String) userJsonObj.getString(DATA_CONSTANT);
			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
			List<RecipientPermissionListVM> recipientPermissions = mapper.readValue(userParseData,
					mapper.getTypeFactory().constructCollectionType(List.class, RecipientPermissionListVM.class));
			for (RecipientPermissionListVM listVM : recipientPermissions) {
				for (PermissionsListVM permissionsListVM : listVM.getPermissionsList()) {
					CustomPermission customPermission = new CustomPermission();
					customPermission.setPermissionCode(permissionsListVM.getPermissionCode());
					customPermission.setPermissionId(permissionsListVM.getPermissionId());
					customPermissionList.add(customPermission);
				}
			}
		} catch (Exception e) {
			throw new CustomParameterizedException(RegisteredException.LOGIN_EXCEPTION.getException(),
					CustomExceptionCode.PERMISSION_NOT_FOUND_ERROR.getErrMsg(),
					CustomExceptionCode.PERMISSION_NOT_FOUND_ERROR.getErrCode());
		}
		return customPermissionList;

	}

	/**
	 * Map permission
	 * 
	 * @param providersList
	 * @return
	 */
	private List<CustomPermission> mapPermission(ServiceProvidersList providersList) {
		List<CustomPermission> customPermissionList = new ArrayList<CustomPermission>();
		List<GrantedRolesList> grantedRolesList = providersList.getGrantedRolesList();
		for (GrantedRolesList rolesList : grantedRolesList) {
			Role role = roleRepository.findOneByRoleCode(rolesList.getRoleCode());
			if (role != null) {
				for (GrantedPermissionsList permission : role.getPermissionList()) {
					CustomPermission customPermission = new CustomPermission();
					customPermission.setPermissionCode(permission.getPermissionCode());
					customPermission.setPermissionId(permission.getPermissionId());
					customPermissionList.add(customPermission);
				}
			}
		}
		return customPermissionList;
	}

	/**
	 * Doctor and diagnostic user permission
	 * 
	 * @param userId
	 * @param serviceProviderId
	 * @return
	 */
	private List<CustomPermission> serviceResourcePermission(String userId, String serviceProviderId,
			String providerType) {
		List<CustomPermission> customPermissionList = new ArrayList<CustomPermission>();
		List<ZoyloAuthVM> list = zoyloUserAuthRepository.findAuthByProviderId(userId, serviceProviderId, providerType);
		if (list != null && !list.isEmpty()) {
			List<CustomPermission> customPermissions = mapPermission(list.get(0).getServiceProvidersList());
			customPermissionList.addAll(customPermissions);
		}
		return customPermissionList;
	}

	/**
	 * Admin permission
	 * 
	 * @param userId
	 * @return
	 */
	private List<CustomPermission> adminPermission(String userId) {
		List<CustomPermission> customPermissionList = new ArrayList<CustomPermission>();
		ZoyloUserAuth zoyloUserAuth = zoyloUserAuthRepository.findByUserId(userId);
		if (zoyloUserAuth != null && zoyloUserAuth.getServiceProvidersList() != null
				&& !zoyloUserAuth.getServiceProvidersList().isEmpty()) {
			ServiceProvidersList providersList = zoyloUserAuth.getServiceProvidersList().get(0);
			List<CustomPermission> customPermissions = mapPermission(providersList);
			customPermissionList.addAll(customPermissions);
		}
		return customPermissionList;
	}

	/**
	 * Switch profile from one auth category to other
	 * 
	 * @param customUserDetails
	 * @param authority
	 * @param serviceProviderId
	 */
	@Override
	public String switchProfile(CustomUserDetails customUserDetails, String authority, String serviceProviderId) {
		try {
			String jwt = "";
			List<CustomPermission> permission = new ArrayList<>();
			if (authority.equalsIgnoreCase(LookUp.RECIPIENT.getLookUpValue())) {
				List<CustomPermission> recipientPermission = recipientPermission(customUserDetails.getId());
				permission.addAll(recipientPermission);
			} else if (authority.equalsIgnoreCase(LookUp.ADMIN.getLookUpValue())) {
				List<CustomPermission> adminPermission = adminPermission(customUserDetails.getId());
				permission.addAll(adminPermission);
			} else if (authority.equalsIgnoreCase(LookUp.DOCTOR.getLookUpValue())) {
				List<CustomPermission> providerPermission = serviceResourcePermission(customUserDetails.getId(),
						serviceProviderId, LookUp.DOCTOR.getLookUpValue());
				permission.addAll(providerPermission);
			} else if (authority.equalsIgnoreCase(LookUp.DIAGONSTIC.getLookUpValue())) {
				List<CustomPermission> providerPermission = serviceResourcePermission(customUserDetails.getId(),
						serviceProviderId, LookUp.DIAGONSTIC.getLookUpValue());
				permission.addAll(providerPermission);
			}

			CustomUserDetails userDetails = new CustomUserDetails(customUserDetails.getId(),
					customUserDetails.getUsername(), customUserDetails.getPassword(), permission,
					customUserDetails.getMobileNumber(), customUserDetails.isZoyloEmployee(),
					customUserDetails.getAuthCategoryList());
			jwt = tokenProvider.createToken(userDetails, authority);
			return jwt;
		} catch (CustomParameterizedException e) {
			throw e;
		} catch (Exception e) {
			throw new CustomParameterizedException(RegisteredException.SWITCH_PROFILE_EXCEPTION.getException(),
					CustomExceptionCode.SWITCH_PROFILE_ERROR.getErrMsg(), e,
					CustomExceptionCode.SWITCH_PROFILE_ERROR.getErrCode());
		}
	}

	/**
	 * To check whether a given user has permission to execute an action within a
	 * given service provider,
	 * 
	 * @param currentUserId
	 * @param serviceProviderId
	 * @param permissionCode
	 * @return boolean
	 */
	@Override
	public boolean isAuthorized(String currentUserId, String serviceProviderId, String permissionCode) {
		List<ZoyloAuthVM> list = zoyloUserAuthRepository.findAuthByProviderIdAndPermissonCode(currentUserId,
				serviceProviderId, permissionCode);
		if (list != null && list.size() > 0) {
			return true;
		}
		return false;
	}

	/**
	 * Get service provider role assign role
	 * 
	 * @param userId
	 * @param serviceProviderId
	 * @param providerType
	 */
	@Override
	public ZoyloAuthVM getServiceProviderUserRole(String userId, String serviceProviderId, String providerTypeCode) {
		try {
			List<ZoyloAuthVM> list = zoyloUserAuthRepository.findAuthByProviderId(userId, serviceProviderId,
					providerTypeCode);
			if (list != null && list.size() > 0) {
				return list.get(0);
			}
		} catch (Exception e) {
			throw new CustomParameterizedException(RegisteredException.USER_AUTH_EXCEPTION.getException(),
					CustomExceptionCode.USER_AUTH_ERROR.getErrMsg(), e,
					CustomExceptionCode.USER_AUTH_ERROR.getErrCode());
		}
		return null;
	}
}
