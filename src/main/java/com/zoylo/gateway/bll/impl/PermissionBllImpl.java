package com.zoylo.gateway.bll.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mongodb.MongoException;
import com.zoylo.gateway.bll.AuditLogService;
import com.zoylo.gateway.bll.PermissionBll;
import com.zoylo.gateway.domain.GrantedPermissionData;
import com.zoylo.gateway.domain.GrantedPermissionsList;
import com.zoylo.gateway.domain.Language;
import com.zoylo.gateway.domain.Permission;
import com.zoylo.gateway.domain.PermissionData;
import com.zoylo.gateway.domain.PermissionHistory;
import com.zoylo.gateway.domain.Role;
import com.zoylo.gateway.domain.ZoyloAuthCategory;
import com.zoylo.gateway.model.PageInfoVM;
import com.zoylo.gateway.model.PageableVM;
import com.zoylo.gateway.model.PermissionEditVM;
import com.zoylo.gateway.model.PermissionVM;
import com.zoylo.gateway.model.PermissionZoyloVM;
import com.zoylo.gateway.mv.PermissionMV;
import com.zoylo.gateway.repository.AuthCategoryRepository;
import com.zoylo.gateway.repository.CustomRepository;
import com.zoylo.gateway.repository.LanguageRepository;
import com.zoylo.gateway.repository.PermissionRepository;
import com.zoylo.gateway.repository.RoleRepository;
import com.zoylo.gateway.web.rest.errors.CustomExceptionCode;
import com.zoylo.gateway.web.rest.errors.CustomParameterizedException;
import com.zoylo.gateway.web.rest.errors.RegisteredException;
import com.zoylo.gateway.web.rest.util.LookUp;

/**
 * 
 * @author Diksha Gupta
 * @version 1.0
 * @description Class for User permission management
 *
 */

@Service
public class PermissionBllImpl implements PermissionBll {

	@Autowired
	private PermissionRepository permissionRepository;

	@Autowired
	private ModelMapper modelMapper;
	@Autowired
	private LanguageRepository languageRepository;
	
	@Autowired
	private CustomRepository customRepository;
	@Autowired
	private AuditLogService auditLogService;
	@Autowired
	private AuthCategoryRepository authCategoryRepository;

	@Autowired
	private RoleRepository roleRepository;
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private boolean falseValue = false;

	/**
	 * @author Diksha Gupta
	 * @description To save permission
	 * @param permissionModel
	 * @return Boolean
	 * @exception CustomParameterizedException
	 */
	@Override
	public PermissionMV savePermission(PermissionVM permissionModel, String languageCode) {
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		Permission permission = permissionRepository.findOneByPermissionCode(permissionModel.getPermissionCode());
		if (permission != null) {
			throw new CustomParameterizedException(RegisteredException.PERMISSION_EXCEPTION.getException(),
					CustomExceptionCode.PERMISSION_CODE_ERROR.getErrMsg(),
					CustomExceptionCode.PERMISSION_CODE_ERROR.getErrCode());
		}
		permission = modelMapper.map(permissionModel, Permission.class);
		if (permission.isActiveFlag() == true) {
			permission.setActiveFromDate(new Date());
		} else {
			permission.setActiveTillDate(new Date());
		}

		try {
			Language language = languageRepository.findOneByLanguageCode(languageCode);
			List<PermissionData> permissionDataList = permissionModel.getPermissiondata();
			List<PermissionData> permissionDataListNew = new ArrayList<>();
			for (PermissionData permissionData : permissionDataList) {
				PermissionData permissionDataNew = new PermissionData();
				permissionDataNew.setLanguageCode(language.getLanguageCode());
				permissionDataNew.setLanguageId(language.getId());
				permissionDataNew.setPermissionDescription(permissionData.getPermissionDescription());
				permissionDataNew.setPermissionName(permissionData.getPermissionName());
				permissionDataListNew.add(permissionDataNew);
			}
			permission.setPermissionData(permissionDataListNew);
			ZoyloAuthCategory zoyloAuthCategory = authCategoryRepository
					.findByAuthCategoryCode(permissionModel.getAuthCategoryCode());
			if (zoyloAuthCategory == null) {
				throw new CustomParameterizedException(RegisteredException.PERMISSION_EXCEPTION.getException(),
						CustomExceptionCode.AUTH_CATEGORY_ERROR.getErrMsg(),
						CustomExceptionCode.AUTH_CATEGORY_ERROR.getErrCode());
			}
			permission.setAuthCategoryCode(zoyloAuthCategory.getAuthCategoryCode());
			permission.setAuthCategoryId(zoyloAuthCategory.getId());
			permission.setAuthCategoryData(zoyloAuthCategory.getAuthCategoryData());
			permission = permissionRepository.save(permission);
			// Save permission in ADMIN ROLE

			String roleCode = "";
			if (permissionModel.getAuthCategoryCode().equalsIgnoreCase(LookUp.DIAGONSTIC.getLookUpValue())) {
				roleCode = LookUp.ROLE_DIAGNOSTIC_CENTER_ADMIN.getLookUpValue();
			} else if (permissionModel.getAuthCategoryCode().equalsIgnoreCase(LookUp.DOCTOR.getLookUpValue())) {
				roleCode = LookUp.ROLE_DOCTOR_CLINIC_ADMIN.getLookUpValue();
			}
			if (!roleCode.isEmpty()) {
				Role role = roleRepository.findOneByRoleCode(roleCode);
				Role adminRole = roleRepository.findOneByRoleCode(LookUp.ROLE_ZOYLO_ADMIN.getLookUpValue());
				if (role != null) {
					mapRole(role, permission);
					roleRepository.save(role);
					mapRole(adminRole, permission);
					roleRepository.save(adminRole);
				} else {
					// TODO exception handling
				}

			}

			PermissionMV permissionMV = new PermissionMV();
			modelMapper.map(permission, permissionMV);
			return permissionMV;
		} catch (MongoException exception) {
			logger.error(exception.getMessage());
			throw new CustomParameterizedException(RegisteredException.PERMISSION_EXCEPTION.getException(),
					CustomExceptionCode.PERMISSIONS_CREATION_ERROR.getErrMsg(), exception,
					CustomExceptionCode.PERMISSIONS_CREATION_ERROR.getErrCode());

		} catch (CustomParameterizedException exception) {
			logger.error(exception.getMessage());
			throw exception;
		} catch (Exception exception) {
			logger.error(exception.getMessage());
			throw new CustomParameterizedException(RegisteredException.PERMISSION_EXCEPTION.getException(),
					CustomExceptionCode.PERMISSIONS_CREATION_ERROR.getErrMsg(), exception,
					CustomExceptionCode.PERMISSIONS_CREATION_ERROR.getErrCode());

		}
	}

	/**
	 * @author Diksha Gupta
	 * @description To get List of all permission
	 * @return List<PermissionDataVM>
	 * @exception CustomParameterizedException
	 */
	@Override
	public List<PermissionMV> getAllPermission() {
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		try {
			List<Permission> listOfPermission = permissionRepository.findAll();
			PermissionMV permissionModelArray[] = modelMapper.map(listOfPermission, PermissionMV[].class);
			List<PermissionMV> permissionMVs = Arrays.asList(permissionModelArray);
			return permissionMVs;
		} catch (MongoException exception) {
			logger.error(exception.getMessage());
			throw new CustomParameterizedException(RegisteredException.PERMISSION_EXCEPTION.getException(),
					CustomExceptionCode.DB_ERROR.getErrMsg(), exception, CustomExceptionCode.DB_ERROR.getErrCode());
		} catch (Exception exception) {
			logger.error(exception.getMessage());
			throw new CustomParameterizedException(RegisteredException.PERMISSION_EXCEPTION.getException(),
					CustomExceptionCode.DB_ERROR.getErrMsg(), exception, CustomExceptionCode.DB_ERROR.getErrCode());
		}
	}

	/**
	 * @author Diksha Gupta
	 * @description To get permission on the basis of permissionCode
	 * @param permissionCode
	 * @return PermissionDataVM
	 * @exception CustomParameterizedException
	 */
	@Override
	public PermissionMV get(String permissionCode) {
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		try {
			Permission permission = permissionRepository.findOne(permissionCode);
			PermissionMV permissionModel = new PermissionMV();
			if (permission != null) {
				permissionModel = modelMapper.map(permission, PermissionMV.class);
			} else {
				permission = permissionRepository.findOneByPermissionCode(permissionCode);
				permissionModel = modelMapper.map(permission, PermissionMV.class);
			}

			return permissionModel;
		} catch (MongoException exception) {
			logger.error(exception.getMessage());
			throw new CustomParameterizedException(RegisteredException.PERMISSION_EXCEPTION.getException(),
					CustomExceptionCode.DB_ERROR.getErrMsg(), exception, CustomExceptionCode.DB_ERROR.getErrCode());
		} catch (Exception exception) {
			logger.error(exception.getMessage());
			throw new CustomParameterizedException(RegisteredException.PERMISSION_EXCEPTION.getException(),
					CustomExceptionCode.DB_ERROR.getErrMsg(), exception, CustomExceptionCode.DB_ERROR.getErrCode());
		}
	}

	/**
	 * @author Diksha Gupta
	 * @description To update permission
	 * @param permissionModel
	 * @return boolean
	 * @exception CustomParameterizedException
	 */
	@Override
	public PermissionMV updatePermission(PermissionEditVM permissionModel, String languageCode) {
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		try {
			Permission permission = permissionRepository.findOne(permissionModel.getId());
			if (permission == null)
				throw new CustomParameterizedException(RegisteredException.PERMISSION_EXCEPTION.getException(),
						CustomExceptionCode.PERMISSION_NOT_FOUND_ERROR.getErrMsg(),
						CustomExceptionCode.PERMISSION_NOT_FOUND_ERROR.getErrCode());
			if (!permissionModel.getPermissionCode().trim().equalsIgnoreCase(permission.getPermissionCode().trim())) {
				Permission permissions = permissionRepository
						.findOneByPermissionCode(permissionModel.getPermissionCode());
				if (permissions != null) {
					throw new CustomParameterizedException(RegisteredException.PERMISSION_EXCEPTION.getException(),
							CustomExceptionCode.PERMISSION_NOT_FOUND_ERROR.getErrMsg(),
							CustomExceptionCode.PERMISSION_NOT_FOUND_ERROR.getErrCode());
				}
			}
			PermissionHistory permisionHistory = new PermissionHistory();
			modelMapper.map(permission, permisionHistory);
			permisionHistory.setId(null);
			permisionHistory.setPermissionId(permission.getId());
			if (permission.isActiveFlag() != permissionModel.isActiveFlag()) {
				if (permissionModel.isActiveFlag() == true) {
					permission.setActiveFromDate(new Date());
				} else {
					permission.setActiveTillDate(new Date());
				}
			}
			Language language = languageRepository.findOneByLanguageCode(languageCode);
			List<PermissionData> permissionDataList = permissionModel.getPermissiondata();
			List<PermissionData> permissionDataListNew = new ArrayList<>();
			for (PermissionData permissionData : permissionDataList) {
				PermissionData permissionDataNew = new PermissionData();
				permissionDataNew.setLanguageCode(language.getLanguageCode());
				permissionDataNew.setLanguageId(language.getId());
				permissionDataNew.setPermissionDescription(permissionData.getPermissionDescription());
				permissionDataNew.setPermissionName(permissionData.getPermissionName());
				permissionDataListNew.add(permissionDataNew);
			}
			permissionModel.setPermissiondata(permissionDataListNew);
			modelMapper.map(permissionModel, permission);
			permission = permissionRepository.save(permission);
			auditLogService.auditActionLog("update", "permission");
			customRepository.save(permisionHistory, "permissionHistory");
			PermissionMV permissionMV = new PermissionMV();
			modelMapper.map(permission, permissionMV);
			return permissionMV;
		} catch (MongoException exception) {
			logger.error(exception.getMessage());
			throw new CustomParameterizedException(RegisteredException.PERMISSION_EXCEPTION.getException(),
					CustomExceptionCode.DB_ERROR.getErrMsg(), exception, CustomExceptionCode.DB_ERROR.getErrCode());
		} catch (CustomParameterizedException exception) {
			logger.error(exception.getMessage());
			throw exception;
		} catch (Exception exception) {
			logger.error(exception.getMessage());
			throw new CustomParameterizedException(RegisteredException.PERMISSION_EXCEPTION.getException(),
					CustomExceptionCode.DB_ERROR.getErrMsg(), exception, CustomExceptionCode.DB_ERROR.getErrCode());
		}
	}

	/**
	 * @author Diksha Gupta
	 * @description To delete permission
	 * @param id
	 * @return boolean
	 * @exception CustomParameterizedException
	 */
	@Override
	public Boolean deletePermission(String id) {
		try {
			Permission permissionData = permissionRepository.findOne(id);
			if (permissionData == null)
				throw new CustomParameterizedException(RegisteredException.PERMISSION_EXCEPTION.getException(),
						CustomExceptionCode.PERMISSION_NOT_FOUND_ERROR.getErrMsg(),
						CustomExceptionCode.PERMISSION_NOT_FOUND_ERROR.getErrCode());
			PermissionHistory permisionHistory = new PermissionHistory();
			modelMapper.map(permissionData, permisionHistory);
			permisionHistory.setId(null);
			permisionHistory.setPermissionId(permissionData.getId());
			permissionData.setActiveFlag(falseValue);
			permissionRepository.save(permissionData);
			auditLogService.auditActionLog("delete", "permission");
			customRepository.save(permisionHistory, "permissionHistory");

			return Boolean.TRUE;
		} catch (MongoException exception) {
			logger.error(exception.getMessage());
			throw new CustomParameterizedException(RegisteredException.PERMISSION_EXCEPTION.getException(),
					CustomExceptionCode.PERMISSION_NOT_FOUND_ERROR.getErrMsg(), exception,
					CustomExceptionCode.PERMISSION_NOT_FOUND_ERROR.getErrCode());
		} catch (CustomParameterizedException exception) {
			logger.error(exception.getMessage());
			throw exception;
		} catch (Exception exception) {
			logger.error(exception.getMessage());
			throw new CustomParameterizedException(RegisteredException.PERMISSION_EXCEPTION.getException(),
					CustomExceptionCode.PERMISSIONS_DELETION_ERROR.getErrMsg(), exception,
					CustomExceptionCode.PERMISSIONS_DELETION_ERROR.getErrCode());
		}
	}

	/**
	 * @author Diskha Gupta
	 * @description To get permission on the basis of module code
	 * @param moduleCode
	 * @return PermissionDataVM
	 * @exception CustomParameterizedException
	 */
	@Override
	public List<PermissionMV> getPermissionByAuthCode(String authCode) {
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		try {
			List<Permission> permission = permissionRepository.findByAuthCategoryCode(authCode);
			if (permission == null)
				return null;
			PermissionMV[] permissionModel = modelMapper.map(permission, PermissionMV[].class);
			return Arrays.asList(permissionModel);
		} catch (Exception exception) {
			logger.error(exception.getMessage());
			throw new CustomParameterizedException(RegisteredException.PERMISSION_EXCEPTION.getException(),
					CustomExceptionCode.DB_ERROR.getErrMsg(), exception, CustomExceptionCode.DB_ERROR.getErrCode());
		}
	}

	private List<GrantedPermissionsList> mapRole(Role role, Permission permission) {
		List<GrantedPermissionsList> permissionsLists = role.getPermissionList();
		GrantedPermissionsList grantedPermissionsList = new GrantedPermissionsList();
		grantedPermissionsList.setActiveFlag(permission.isActiveFlag());
		grantedPermissionsList.setPermissionCode(permission.getPermissionCode());
		grantedPermissionsList.setPermissionId(permission.getId());
		List<GrantedPermissionData> dataList = new ArrayList<>();
		for (PermissionData data : permission.getPermissionData()) {
			GrantedPermissionData grantedPermissionData = new GrantedPermissionData();
			grantedPermissionData.setLanguageCode(data.getLanguageCode());
			grantedPermissionData.setLanguageCode(data.getLanguageId());
			grantedPermissionData.setPermissionName(data.getPermissionName());
			dataList.add(grantedPermissionData);
		}
		grantedPermissionsList.setPermissionData(dataList);
		grantedPermissionsList.setSystemDefined(permission.isSystemDefined());
		permissionsLists.add(grantedPermissionsList);
		return permissionsLists;
	}

	/**
	 * @author Balram Sharma
	 * @description To save Permission
	 * @param PermissionZoyloVM
	 * @return PermissionMV
	 */
	@Override
	public PermissionMV savePermissionData(PermissionZoyloVM permissionZoyloVM) {

		String authCategoryCodeVM = permissionZoyloVM.getAuthCategoryCode().toUpperCase();
		String permissionCodeVM = permissionZoyloVM.getPermissionCode().toUpperCase();
		PermissionData permissionDataVM = permissionZoyloVM.getPermissionData();

		Permission permission = permissionRepository.findOneByPermissionCode(permissionCodeVM);
		if (permission != null) {
			throw new CustomParameterizedException(RegisteredException.PERMISSION_EXCEPTION.getException(),
					CustomExceptionCode.PERMISSION_CODE_ERROR.getErrMsg(),
					CustomExceptionCode.PERMISSION_CODE_ERROR.getErrCode());
		}

		permission = modelMapper.map(permissionZoyloVM, Permission.class);
		List<PermissionData> newPermissionDataList = new ArrayList<PermissionData>();
		newPermissionDataList.add(permissionDataVM);
		permission.setPermissionData(newPermissionDataList);

		if (permission.isActiveFlag()) {
			permission.setActiveFromDate(new Date());
		} else {
			permission.setActiveTillDate(new Date());
		}

		ZoyloAuthCategory zoyloAuthCategory = authCategoryRepository.findByAuthCategoryCode(authCategoryCodeVM);
		if (null == zoyloAuthCategory) {
			throw new CustomParameterizedException(RegisteredException.PERMISSION_EXCEPTION.getException(),
					CustomExceptionCode.AUTH_CATEGORY_ERROR.getErrMsg(),
					CustomExceptionCode.AUTH_CATEGORY_ERROR.getErrCode());
		}
		permission.setAuthCategoryCode(zoyloAuthCategory.getAuthCategoryCode());
		permission.setAuthCategoryId(zoyloAuthCategory.getId());
		permission.setAuthCategoryData(zoyloAuthCategory.getAuthCategoryData());
		try {
			Permission permissionResponseFromDb = permissionRepository.save(permission);
			return modelMapper.map(permissionResponseFromDb, PermissionMV.class);

		} catch (Exception exception) {
			logger.error(exception.getMessage());
			throw new CustomParameterizedException(RegisteredException.PERMISSION_EXCEPTION.getException(),
					CustomExceptionCode.DB_ERROR.getErrMsg(), exception, CustomExceptionCode.DB_ERROR.getErrCode());
		}

	}

	/**
	 * @author Balram Sharma
	 * @description To update existing Permission
	 * @param PermissionZoyloVM
	 * @return PermissionMV
	 */
	@Override
	public PermissionMV updatePermissionData(PermissionZoyloVM permissionZoyloVM) {

		String permissionIdVM = permissionZoyloVM.getId();
		String permissionCodeVM = permissionZoyloVM.getPermissionCode();
		String authCategoryCodeVM = permissionZoyloVM.getAuthCategoryCode();
		boolean activeFlagVM = permissionZoyloVM.isActiveFlag();
		PermissionData permissionDataVM = permissionZoyloVM.getPermissionData();
		String permissionDescriptionVM = permissionDataVM.getPermissionDescription();
		String permissionNameVM = permissionDataVM.getPermissionName();
		String languageCodeVM = permissionDataVM.getLanguageCode();

		Permission permissionFromDb = permissionRepository.findOne(permissionIdVM);
		if (null == permissionFromDb)
			throw new CustomParameterizedException(RegisteredException.PERMISSION_EXCEPTION.getException(),
					CustomExceptionCode.PERMISSION_NOT_FOUND_ERROR.getErrMsg(),
					CustomExceptionCode.PERMISSION_NOT_FOUND_ERROR.getErrCode());
		if (!permissionFromDb.getPermissionCode().trim().equalsIgnoreCase(permissionCodeVM.trim())) {
			throw new CustomParameterizedException(RegisteredException.PERMISSION_EXCEPTION.getException(),
					CustomExceptionCode.PERMISSION_NOT_FOUND_ERROR.getErrMsg(),
					CustomExceptionCode.PERMISSION_NOT_FOUND_ERROR.getErrCode());
		}
		ZoyloAuthCategory zoyloAuthCategoryFromDb = authCategoryRepository.findByAuthCategoryCode(authCategoryCodeVM);
		if (null == zoyloAuthCategoryFromDb) {
			throw new CustomParameterizedException(RegisteredException.PERMISSION_EXCEPTION.getException(),
					CustomExceptionCode.AUTH_CATEGORY_ERROR.getErrMsg(),
					CustomExceptionCode.AUTH_CATEGORY_ERROR.getErrCode());
		}
		// updating permission object with user data.
		permissionFromDb.setAuthCategoryCode(zoyloAuthCategoryFromDb.getAuthCategoryCode());
		permissionFromDb.setAuthCategoryId(zoyloAuthCategoryFromDb.getId());
		permissionFromDb.setAuthCategoryData(zoyloAuthCategoryFromDb.getAuthCategoryData());
		permissionFromDb.setActiveFlag(activeFlagVM);
		List<PermissionData> permissionDataFromDb = permissionFromDb.getPermissionData();
		// filtering list by using languageCode
		PermissionData permissionDataRef = permissionDataFromDb.stream()
				.filter(permissionDataObj -> permissionDataObj.getLanguageCode().equals(languageCodeVM)).findFirst()
				.get();
		permissionDataRef.setPermissionName(permissionNameVM);
		permissionDataRef.setPermissionDescription(permissionDescriptionVM);
		if (permissionFromDb.isActiveFlag() != activeFlagVM) {
			if (activeFlagVM) {
				permissionFromDb.setActiveFromDate(new Date());
			} else {
				permissionFromDb.setActiveTillDate(new Date());
			}
		}

		try {
			Permission permissionResponseFromDb = permissionRepository.save(permissionFromDb);
			return modelMapper.map(permissionResponseFromDb, PermissionMV.class);
		} catch (MongoException exception) {
			logger.error(exception.getMessage());
			throw new CustomParameterizedException(RegisteredException.PERMISSION_EXCEPTION.getException(),
					CustomExceptionCode.DB_ERROR.getErrMsg(), exception, CustomExceptionCode.DB_ERROR.getErrCode());
		} catch (CustomParameterizedException exception) {
			logger.error(exception.getMessage());
			throw exception;
		} catch (Exception exception) {
			logger.error(exception.getMessage());
			throw new CustomParameterizedException(RegisteredException.PERMISSION_EXCEPTION.getException(),
					CustomExceptionCode.DB_ERROR.getErrMsg(), exception, CustomExceptionCode.DB_ERROR.getErrCode());
		}

	}

	/**
	 * @author Balram Sharma
	 * @description search Permission by using permissionCode & permissionName,
	 *              activeFlag, authCategoryCode
	 * @param permissionCode
	 * @param permissionName
	 * @param activeFlag
	 * @param authCategory
	 * @param languageCode
	 * @return PageableVM<PermissionMV>
	 */
	@Override
	public PageableVM<PermissionMV> searchPermissionProperties(Pageable pageable, String permissionCode, String permissionName,
			boolean activeFlag, String authCategoryCode, String languageCode) {

		Page<Permission> pages = permissionRepository.searchPermissionProperties(pageable, permissionCode,
				permissionName, activeFlag, authCategoryCode, languageCode);

		List<Permission> permissionListFromDb = pages.getContent();
		PermissionMV[] zoyloPermissionMVArray = modelMapper.map(permissionListFromDb, PermissionMV[].class);
		PageableVM<PermissionMV> permissionPageableMV = new PageableVM<>();
		permissionPageableMV.setContent(Arrays.asList(zoyloPermissionMVArray));
		PageInfoVM pageDetails = modelMapper.map(pages, PageInfoVM.class);
		permissionPageableMV.setPageInfo(pageDetails);
		return permissionPageableMV;

	}
	/**
	 * @author Balram Sharma
	 * @description To get Permission by id
	 * @param id
	 * @return PermissionMV
	 */
	@Override
	public PermissionMV getPermissionById(String permissionId) {
	
		Permission responseData = permissionRepository.findOne(permissionId);
		return modelMapper.map(responseData, PermissionMV.class);
	}
	
	
	
}
