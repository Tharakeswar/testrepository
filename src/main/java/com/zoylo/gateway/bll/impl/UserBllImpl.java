package com.zoylo.gateway.bll.impl;

import static com.zoylo.gateway.service.util.Constants.OTP_SENT;
import static com.zoylo.gateway.service.util.Constants.REGISTRATION_OTP_TITLE;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.json.JSONObject;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import com.amazonaws.services.simpleemail.model.SendEmailResult;
import com.mongodb.MongoException;
import com.zoylo.core.emailservice.EmailSEService;
import com.zoylo.core.emailservice.EmailTemplate;
import com.zoylo.core.mobilepush.SNSMobilePushService;
import com.zoylo.core.mobilepush.SendPushVM;
import com.zoylo.core.smsservice.SMSService;
import com.zoylo.core.smsservice.SendSmsVM;
import com.zoylo.core.smsservice.SmsServiceResModel;
import com.zoylo.core.utils.StringsUtil;
import com.zoylo.gateway.bll.AuditLogService;
import com.zoylo.gateway.bll.EmailBll;
import com.zoylo.gateway.bll.NotificationBll;
import com.zoylo.gateway.bll.UserBll;
import com.zoylo.gateway.domain.AuthCategoryData;
import com.zoylo.gateway.domain.DefaultAuthCategoryInfo;
import com.zoylo.gateway.domain.EmailInfo;
import com.zoylo.gateway.domain.GrantedRoleData;
import com.zoylo.gateway.domain.GrantedRolesList;
import com.zoylo.gateway.domain.Permission;
import com.zoylo.gateway.domain.PermissionData;
import com.zoylo.gateway.domain.PhoneInfo;
import com.zoylo.gateway.domain.Role;
import com.zoylo.gateway.domain.RoleData;
import com.zoylo.gateway.domain.ServiceProvidersList;
import com.zoylo.gateway.domain.User;
import com.zoylo.gateway.domain.UserProfile;
import com.zoylo.gateway.domain.ZoyloAuthCategory;
import com.zoylo.gateway.domain.ZoyloGrantedPermissionsList;
import com.zoylo.gateway.domain.ZoyloPermissionData;
import com.zoylo.gateway.domain.ZoyloPermissionsList;
import com.zoylo.gateway.domain.ZoyloUserAuth;
import com.zoylo.gateway.domain.history.UserAuthHistory;
import com.zoylo.gateway.domain.history.UserHistory;
import com.zoylo.gateway.model.ChangeMobileEmailVM;
import com.zoylo.gateway.model.Customer;
import com.zoylo.gateway.model.CustomerAttribute;
import com.zoylo.gateway.model.DeliveryTrail;
import com.zoylo.gateway.model.DiagnosticVM;
import com.zoylo.gateway.model.DoctorRoleDataVM;
import com.zoylo.gateway.model.DoctorVM;
import com.zoylo.gateway.model.EcommCustomer;
import com.zoylo.gateway.model.EcommUserDetail;
import com.zoylo.gateway.model.EcommUserRegistrationVM;
import com.zoylo.gateway.model.GhostUserFamilyMemberVM;
import com.zoylo.gateway.model.GhostUserVM;
import com.zoylo.gateway.model.MessageInfo;
import com.zoylo.gateway.model.MobileVerificationVM;
import com.zoylo.gateway.model.OTPDetailVM;
import com.zoylo.gateway.model.OrganizationUserDataVM;
import com.zoylo.gateway.model.PMSUserRoleMV;
import com.zoylo.gateway.model.PMSUserRoleMVList;
import com.zoylo.gateway.model.PageInfoVM;
import com.zoylo.gateway.model.PageableVM;
import com.zoylo.gateway.model.PasswordUpdateVM;
import com.zoylo.gateway.model.PermissionsListVM;
import com.zoylo.gateway.model.RecipientInfo;
import com.zoylo.gateway.model.RecipientPermissionListVM;
import com.zoylo.gateway.model.RecipientVM;
import com.zoylo.gateway.model.RegisterVM;
import com.zoylo.gateway.model.ResendOTPVM;
import com.zoylo.gateway.model.UpdateRegisterVM;
import com.zoylo.gateway.model.UserDataVM;
import com.zoylo.gateway.model.UserDetailVM;
import com.zoylo.gateway.model.UserPMSDataVM;
import com.zoylo.gateway.model.UserProfileDataVM;
import com.zoylo.gateway.model.UserProfileVM;
import com.zoylo.gateway.model.UserResponseVM;
import com.zoylo.gateway.model.UserRoleDataVM;
import com.zoylo.gateway.model.UserRoleVM;
import com.zoylo.gateway.model.UserTypeVM;
import com.zoylo.gateway.model.UserUpdateResponceVM;
import com.zoylo.gateway.model.UserUpdateVM;
import com.zoylo.gateway.model.ZoyloAuthVM;
import com.zoylo.gateway.model.ZoyloLeadGenerationVM;
import com.zoylo.gateway.model.ZoyloNotificationVM;
import com.zoylo.gateway.model.ZoyloUserUpdateVM;
import com.zoylo.gateway.model.ZoyloUserVM;
import com.zoylo.gateway.mv.AuthVM;
import com.zoylo.gateway.mv.ProviderAdminDataMV;
import com.zoylo.gateway.mv.UserAutoSuggestionMV;
import com.zoylo.gateway.mv.UserSuggestionMV;
import com.zoylo.gateway.mv.UserVM;
import com.zoylo.gateway.mv.ZoyloUserAuthVM;
import com.zoylo.gateway.repository.AuthCategoryRepository;
import com.zoylo.gateway.repository.CustomRepository;
import com.zoylo.gateway.repository.CustomUserRepository;
import com.zoylo.gateway.repository.PermissionRepository;
import com.zoylo.gateway.repository.RoleRepository;
import com.zoylo.gateway.repository.UserRepository;
import com.zoylo.gateway.repository.ZoyloUserAuthRepository;
import com.zoylo.gateway.security.jwt.TokenProvider;
import com.zoylo.gateway.service.EmailTokenProvider;
import com.zoylo.gateway.service.util.Constants;
import com.zoylo.gateway.web.rest.client.AdminRestClient;
import com.zoylo.gateway.web.rest.client.RecipientRestClient;
import com.zoylo.gateway.web.rest.errors.CustomExceptionCode;
import com.zoylo.gateway.web.rest.errors.CustomParameterizedException;
import com.zoylo.gateway.web.rest.errors.RegisteredException;
import com.zoylo.gateway.web.rest.errors.RegisteredExceptions;
import com.zoylo.gateway.web.rest.util.LookUp;
import com.zoylo.gateway.web.rest.util.PageableUtil;
import com.zoylo.gateway.web.rest.util.PasswordEncryption;
import com.zoylo.gateway.web.rest.util.ZoyloAdminUserType;
import com.zoylo.gateway.web.rest.util.ZoyloUserType;

import freemarker.template.TemplateException;
import io.github.jhipster.config.JHipsterProperties;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * 
 * @author ankur
 * @version 1.0
 * @description User registration service
 *
 */
@Service
public class UserBllImpl implements UserBll {

	private static final Logger logger = LoggerFactory.getLogger(UserBllImpl.class);

	private SNSMobilePushService snsMobilePushService;
	
	@Autowired
	private TokenProvider tokenProvider;

	@Autowired
	private EmailTokenProvider verifyEmailTokenProvider;
	
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private AdminRestClient adminRestClient;

	@Autowired
	private AuditLogService auditLogService;

	@Autowired
	private CustomRepository customRepository;

	@Autowired
	private RecipientRestClient recipientRestClient;
	
	@Autowired
	private CustomUserRepository customUserRepository;
	
	@Autowired
	private AuthCategoryRepository authCategoryRepository;

	@Autowired
	ZoyloUserAuthRepository userAuthRepository;
	
	@Autowired
	RestTemplate restTemplate;

	@Value("${app.emailTokenExpirationMinutes}")
	private int emailTokenExpirationMinutes;
	
	@Value("${app.otpExpirationMinutes}")
	private int OPTExpirationMinutes;
	
	@Value("${app.mobileNotificationAllowed}")
	private Boolean mobileNotificationAllowed;
	
	@Value("${app.emailNotificationAllowed}")
	private Boolean emailNotificationAllowed;
	
	@Value("${app.validationUrl}")
	private String validationUrl;
	
	@Value("${app.sms.apiurl}")
	private String apiUrl;
	
	@Value("${app.sms.authkey}")
	private String authkey;
	
	@Value("${app.sms.country}")
	private String country;
	
	@Value("${app.sms.sender}")
	private String sender;
	
	@Value("${app.sms.route}")
	private String route;
	
	@Value("${app.ecommerce.userUpdateUrl}")
	private String ecommUserUpdateUrl;
	
	@Value("${app.ecomm.signup.url}")
	private String ecommSignupUrl;
	
	@Value("${app.ecomm.enabled}")
	private Boolean ecommEnabled;
	
	@Value("${app.ecomm.accesstoken}")
	private String accessToken;
	
	@Value("${app.accessKey}")
	private String accesskey;

	@Value("${app.secretKey}")
	private String key;

	@Value("${app.emailFrom}")
	private String emailFrom;

	@Value("${app.registrationPort}")
	private String registrationPort;

	@Value("${app.verifyEmailSubject}")
	private String subject;

	@Value("${app.verifyEmailBody}")
	private String verifyEmailBody;

	@Value("${app.tokenValidDay}")
	private int tokenValidDay;
	
	@Value("${app.guestUserSubject}")
	private String guestUserSubject;
	
	private static final String AUTHORITIES_KEY = "auth";
	private static final String MOBILE_NO = "mobn";
	private static final String USER_NAME = "usr";
	private static final String USER_ID = "uid";
	private static final String STATUS_CODE = "S";
	private static final String MESSAGE_MODE_CODE = "S";
	private static final String EMAIL_MODE_CODE = "E";
	private static final String DEFAULT_AUTH_CATEGORY_CODE="RECIPIENT";
	private static final String DEFAULT_LANGUAGE_CODE="en";	
	private static final String ECOMM_LOGIN_ID="ECOMM_LOGIN_ID";
	private static final String ECOMM_LOGIN_TOKEN="ECOMM_LOGIN_TOKEN";
	private static final String CUSTOMER_DATA="customer_data";
	private static final String TOKEN="token";
	private static final String ID="id";
	private final String MOBILE_NUMBER="mobile_number";	
	private static final boolean FALSEFLAG = false;
	private static final boolean TRUEFLAG = true;
	private static final String AUTHORIZATION = "Authorization";
	private String secretKey;

	@Autowired
	private ZoyloUserAuthRepository zoyloUserAuthRepository;

	@Autowired
	private PermissionRepository permissionRepository;
	
	@Autowired
	private NotificationBll notificationBll;
	
	@Autowired
	private EmailBll emailBll;

	public UserBllImpl(JHipsterProperties jHipsterProperties) {
		this.secretKey = jHipsterProperties.getSecurity().getAuthentication().getJwt().getSecret();
	}

	/**
	 * @param registerVM
	 * @return
	 * @exception CustomParameterizedException
	 *                exception when email and phone number already exists
	 */
	@Override
	public User registerUser(RegisterVM registerVM) {
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		try {
			phoneEmailValidation(registerVM.getEmailAddress(), registerVM.getPhoneNumber());
			User user = new User();
			user.setFirstName(registerVM.getFirstName());
			user.setLastName(registerVM.getLastName());
			user.setPasswordLastSetOn(new Date());
			if (registerVM.getPhoneNumber() != null
					&& !registerVM.getPhoneNumber().startsWith(Constants.INDIA_MOBILE_CODE)) {
				registerVM.setPhoneNumber(Constants.INDIA_MOBILE_CODE + registerVM.getPhoneNumber());
			}
			PhoneInfo info = new PhoneInfo(registerVM.getPhoneNumber());
			user.setPhoneInfo(info);
			user = populateEmailInfoUserForValidation(user, registerVM.getEmailAddress());
			
			user.setDeviceId(registerVM.getDeviceId());
			user.setDeviceType(registerVM.getDeviceType());
			String encryptedPassword = passwordEncoder
					.encode(PasswordEncryption.getEncryptedPassword(registerVM.getEncryptedPassword()));
			user.setEncryptedPassword(encryptedPassword);
			user.setZoyloId(UUID.randomUUID().toString());
			user.setActiveFlag(TRUEFLAG);
			List<ServiceProvidersList> serviceProvidersList = new ArrayList<>();
			if (registerVM.getUserType().equalsIgnoreCase(ZoyloAdminUserType.ADMIN.getUserType())
					|| registerVM.getUserType().equalsIgnoreCase(ZoyloAdminUserType.SUB_ADMIN.getUserType())) {
				user.setActiveFlag(registerVM.getIsActiveFlag());
				user.setIsZoyloEmployee(registerVM.getIsZoyloEmployee());
				if (registerVM.getAdminDetail() == null) {
					throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
							CustomExceptionCode.ADMIN_ROLE_REQUIRED.getErrMsg(),
							CustomExceptionCode.ADMIN_ROLE_REQUIRED.getErrCode());
				}

				ServiceProvidersList serviceProvider = setRole(registerVM.getAdminDetail().getRoleCode(), null);
				serviceProvidersList.add(serviceProvider);
			}

			user.setLoginEnabled(TRUEFLAG);
			int sixDigitOtp = new Random().nextInt(999999);	//NOSONAR
			if (sixDigitOtp <= 99999) {
				sixDigitOtp = 100000 + sixDigitOtp;
			}
			user.getPhoneInfo().setOtp(sixDigitOtp);
			user = userRepository.save(user);
			logger.info("--- token validity days : {} ", tokenValidDay);
			EmailSEService emailSEService = new EmailSEService();
			EmailTemplate emailTemplate = new EmailTemplate();
			emailTemplate.setAccessKey(accesskey);
			emailTemplate.setSecretKey(key);
			emailTemplate.setEmailFrom(emailFrom);
			emailTemplate.setSendTo(user.getEmailInfo().getEmailAddress());
			emailTemplate.setSubject("Zoylo.com | Welcome to zoylo");
			/*
			 * Calendar cal = Calendar.getInstance(); cal.setTime(new Date());
			 * cal.add(Calendar.DATE, tokenValidDay); long now =
			 * (cal.getTimeInMillis()); Date validity = new Date(now); String
			 * token =
			 * Jwts.builder().setSubject(user.getEmailInfo().getEmailAddress())
			 * .claim(AUTHORITIES_KEY, "" + "," + "").claim(MOBILE_NO,
			 * user.getPhoneInfo().getPhoneNumber()) .claim(USER_ID,
			 * user.getId()).claim(USER_NAME,
			 * user.getPhoneInfo().getPhoneNumber())
			 * .signWith(SignatureAlgorithm.HS512,
			 * secretKey).setExpiration(validity).compact();
			 */
			
			String verificationLink = getVerificationLink(user.getEmailInfo().getVerificationToken(), user.getEmailInfo().getEmailAddress());
			emailTemplate.setBody(emailBll.verificationLinkTemplate(user.getFirstName(), verificationLink));
			
			// Sends OTP to registered Mobile
			SMSService smsService = new SMSService();
			SendSmsVM sendSmsVM = new SendSmsVM();
			sendSmsVM.setMessage("Use " + sixDigitOtp
					+ " as One Time Password (OTP) to activate your Zoylo account. Do not share this OTP with anyone for security reasons. For more details, call us at 1800 3000 5454.");
			sendSmsVM.setNumber(user.getPhoneInfo().getPhoneNumber()); // to
																		// do
			sendSmsVM.setSecretKey(key);
			sendSmsVM.setAccesskey(accesskey);
			sendSmsVM.setApiUrl(apiUrl);
			sendSmsVM.setCountry(country);
			sendSmsVM.setAuthkey(authkey);
			sendSmsVM.setRoute(route);
			sendSmsVM.setSender(sender);
			// save email info
			String messageId = "";
			if(emailNotificationAllowed) {
				SendEmailResult sendEmailResult = emailSEService.sendEmail(emailTemplate);
				messageId = sendEmailResult.getMessageId();
			}
			if ((registerVM.getUserType().equalsIgnoreCase(ZoyloAdminUserType.ADMIN.getUserType())
					|| registerVM.getUserType().equalsIgnoreCase(ZoyloAdminUserType.SUB_ADMIN.getUserType()))

					|| ((registerVM.getUserType().equalsIgnoreCase(ZoyloUserType.DOCTOR.getUserType()))
							&& (registerVM.getEncryptedPassword() == null
									|| registerVM.getEncryptedPassword().isEmpty()))) {
			} else {
				/*try {
					ZoyloNotificationVM zoyloNotificationVM = new ZoyloNotificationVM();
					RecipientInfo recipientInfo = new RecipientInfo();
					MessageInfo messageInfo = new MessageInfo();

					recipientInfo.setRecipientFirstName(user.getFirstName());
					recipientInfo.setRecipientLastName(user.getLastName());
					recipientInfo.setRecipientZoyloId(user.getZoyloId());
					recipientInfo.setRecipientId(user.getId());
					recipientInfo.setRecipientEmail(emailTemplate.getSendTo());

					messageInfo.setMessageText(emailTemplate.getBody());

					List<DeliveryTrail> deliveryTrail = new ArrayList<>();
					DeliveryTrail trail = new DeliveryTrail();
					trail.setDeliveryAttemptedAt(new Date());
					trail.setDeliveryStatusCode(STATUS_CODE);
					deliveryTrail.add(trail);
					zoyloNotificationVM.setNotificationMessageId(messageId);
					zoyloNotificationVM.setNotificationModeCode(EMAIL_MODE_CODE);
					zoyloNotificationVM.setNotificationStatusCode(STATUS_CODE);
					zoyloNotificationVM.setRecipientInfo(recipientInfo);
					zoyloNotificationVM.setRetryCounter(1);
					zoyloNotificationVM.setMessageInfo(messageInfo);
					zoyloNotificationVM.setDeliveryTrail(deliveryTrail);

					adminRestClient.save(zoyloNotificationVM);
				} catch (Exception exception) {
					logger.info("unable to save notification email");
				}*/
				notificationBll.saveNotificationAfterEmail(user, emailTemplate, messageId);


				// SMS data save

				String smsMessageId = "";
				if(mobileNotificationAllowed){
					SmsServiceResModel smsServiceResModel = smsService.sendSMSMessage(sendSmsVM);
					smsMessageId = smsServiceResModel.getRequestId();
				}
				/*try {
					ZoyloNotificationVM zoyloNotification = new ZoyloNotificationVM();
					RecipientInfo recipientPhoneInfo = new RecipientInfo();
					MessageInfo messagePhoneInfo = new MessageInfo();
					recipientPhoneInfo.setRecipientFirstName(user.getFirstName());
					recipientPhoneInfo.setRecipientLastName(user.getLastName());
					recipientPhoneInfo.setRecipientZoyloId(user.getZoyloId());
					recipientPhoneInfo.setRecipientId(user.getId());
					recipientPhoneInfo.setRecipientPhone(sendSmsVM.getNumber());

					messagePhoneInfo.setMessageText(sendSmsVM.getMessage());
					List<DeliveryTrail> deliveryTrails = new ArrayList<>();
					DeliveryTrail trails = new DeliveryTrail();
					trails.setDeliveryAttemptedAt(new Date());
					trails.setDeliveryStatusCode(STATUS_CODE);
					deliveryTrails.add(trails);

					zoyloNotification.setDeliveryTrail(deliveryTrails);
					zoyloNotification.setNotificationMessageId(emailMessageId);
					zoyloNotification.setNotificationModeCode(MESSAGE_MODE_CODE);
					zoyloNotification.setNotificationStatusCode(STATUS_CODE);
					zoyloNotification.setRecipientInfo(recipientPhoneInfo);
					zoyloNotification.setRetryCounter(1);
					zoyloNotification.setMessageInfo(messagePhoneInfo);

					adminRestClient.save(zoyloNotification);
				} catch (Exception exception) {
					logger.info("unable to save notification sms");
				}*/
				notificationBll.saveNotificationAfterSMS(user, sendSmsVM, smsMessageId);

				logger.info(OTP_SENT);
			}
			if (user.getIsZoyloEmployee() != null && user.getIsZoyloEmployee()) {
				ZoyloUserAuth zoyloUserAuth = new ZoyloUserAuth();
				zoyloUserAuth.setEmailId(user.getEmailInfo().getEmailAddress());
				zoyloUserAuth.setPhoneNumber(user.getPhoneInfo().getPhoneNumber());
				zoyloUserAuth.setZoyloId(user.getZoyloId());
				zoyloUserAuth.setUserId(user.getId());
				zoyloUserAuth.setServiceProvidersList(serviceProvidersList);
				zoyloUserAuthRepository.save(zoyloUserAuth);
			}
			if (ecommEnabled && null != registerVM.getUserType()
					&& !registerVM.getUserType().equalsIgnoreCase(Constants.DOCTOR)) {
				Map<String, String> ecommerceSignupResMap = ecommSignup(registerVM);
				if (ecommerceSignupResMap != null && ecommerceSignupResMap.size() > 0) {
					user.setEcommId(ecommerceSignupResMap.get(ECOMM_LOGIN_ID));
					user.setEcommToken(ecommerceSignupResMap.get(ECOMM_LOGIN_TOKEN));
				}

			}
			return user;
		} catch (CustomParameterizedException exception) {
			throw exception;
		} catch (MongoException exception) {
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.DB_ERROR.getErrMsg(), exception, CustomExceptionCode.DB_ERROR.getErrCode());

		} catch (Exception exception) {
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.REGISTERATION_FAIL.getErrMsg(), exception,
					CustomExceptionCode.REGISTERATION_FAIL.getErrCode());
		}
	}
	
	@Override
	public User registerUserIfNotPresentForCampaign(RegisterVM registerVM){
		
		String phoneNumber = registerVM.getPhoneNumber();
		
		String number = Constants.INDIA_MOBILE_CODE + phoneNumber;
		User user = userRepository.findOneByPhoneInfoPhoneNumber(phoneNumber, number);
		if (user != null) {

			PhoneInfo phoneInfoRef = user.getPhoneInfo();

			if (phoneInfoRef.isVerifiedFlag()) {
				return user;
			}else{
				PhoneInfo phoneInfo = generateOTP(phoneInfoRef, phoneInfoRef.getPhoneNumber());
				sendGeneratedOTP(phoneInfo.getOtp(),phoneInfo.getPhoneNumber(),user);
				user.setPhoneInfo(phoneInfo);
				user = userRepository.save(user);
				return user;
			}

		}

		return registerUserForCampaign(registerVM);
	}
	
	private int getRandomOTP(){
		int sixDigitOtp = new Random().nextInt(999999);	//NOSONAR
		if (sixDigitOtp <= 99999) {
			sixDigitOtp = 100000 + sixDigitOtp;
		}
		return sixDigitOtp;
	}
	
	private PhoneInfo generateOTP(PhoneInfo phoneInfo, String phoneNumber){
		if(phoneInfo == null) {
		 phoneInfo = new PhoneInfo();
		}
		int otp = getRandomOTP();
		phoneInfo.setOtp(otp);
        phoneInfo.setOtpSentOn(new Date());
        phoneInfo.setPhoneNumber(phoneNumber);
        phoneInfo.setVerifiedFlag(false);
        phoneInfo.setVerifiedOn(null);
        return phoneInfo;
	}
	
	private void sendGeneratedOTP(int otp, String phoneNumber, User user){
		SMSService smsService = new SMSService();
		SendSmsVM sendSmsVM = new SendSmsVM();
		sendSmsVM.setMessage("Use " + otp
				+ " as One Time Password (OTP) to confirm the phone number. Do not share this OTP with anyone for security reasons. "
				+ "For more details, call us at 1800 3000 5454.");
		sendSmsVM.setNumber(phoneNumber); // to
																	// do
		sendSmsVM.setSecretKey(key);
		sendSmsVM.setAccesskey(accesskey);
		sendSmsVM.setApiUrl(apiUrl);
		sendSmsVM.setCountry(country);
		sendSmsVM.setAuthkey(authkey);
		sendSmsVM.setRoute(route);
		sendSmsVM.setSender(sender);
		String smsMessageId = "";
		
		if(mobileNotificationAllowed){
			SmsServiceResModel smsServiceResModel = smsService.sendSMSMessage(sendSmsVM);
			smsMessageId = smsServiceResModel.getRequestId();
		}
		captureNotificationWithoutUser(sendSmsVM, smsMessageId, user);
	}
	
	public void captureNotificationWithoutUser(SendSmsVM sendSmsVM, String messageId, User user) {
		notificationBll.saveNotificationAfterSMS(user, sendSmsVM, messageId);
	}

	
	
	/**
	 * @param registerVM
	 * @description This method is used only for registering user for Campaign
	 * @return User
	 *  
	 */
	@Override
	public User registerUserForCampaign(RegisterVM registerVM) {
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		try {
			User user = new User();
			user.setFirstName(registerVM.getFirstName());
			user.setLastName(registerVM.getLastName());
			user.setPasswordLastSetOn(new Date());
			if (registerVM.getPhoneNumber() != null
					&& !registerVM.getPhoneNumber().startsWith(Constants.INDIA_MOBILE_CODE)) {
				registerVM.setPhoneNumber(Constants.INDIA_MOBILE_CODE + registerVM.getPhoneNumber());
			}
			PhoneInfo info = new PhoneInfo(registerVM.getPhoneNumber());
			user.setPhoneInfo(info);
			user = populateEmailInfoUserForValidation(user, registerVM.getEmailAddress());
			
			user.setDeviceId(registerVM.getDeviceId());
			user.setDeviceType(registerVM.getDeviceType());
			String encryptedPassword = passwordEncoder
					.encode(PasswordEncryption.getEncryptedPassword(registerVM.getEncryptedPassword()));
			user.setEncryptedPassword(encryptedPassword);
			user.setZoyloId(UUID.randomUUID().toString());
			user.setActiveFlag(TRUEFLAG);
			List<ServiceProvidersList> serviceProvidersList = new ArrayList<>();
			if (registerVM.getUserType().equalsIgnoreCase(ZoyloAdminUserType.ADMIN.getUserType())
					|| registerVM.getUserType().equalsIgnoreCase(ZoyloAdminUserType.SUB_ADMIN.getUserType())) {
				user.setActiveFlag(registerVM.getIsActiveFlag());
				user.setIsZoyloEmployee(registerVM.getIsZoyloEmployee());
				if (registerVM.getAdminDetail() == null) {
					throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
							CustomExceptionCode.ADMIN_ROLE_REQUIRED.getErrMsg(),
							CustomExceptionCode.ADMIN_ROLE_REQUIRED.getErrCode());
				}

				ServiceProvidersList serviceProvider = setRole(registerVM.getAdminDetail().getRoleCode(), null);
				serviceProvidersList.add(serviceProvider);
			}

			user.setLoginEnabled(TRUEFLAG);
			info = generateOTP(info, info.getPhoneNumber());
			int sixDigitOtp = info.getOtp();
			
			user = userRepository.save(user);
            			logger.info("--- token validity days : {} ", tokenValidDay);
			EmailSEService emailSEService = new EmailSEService();
			EmailTemplate emailTemplate = new EmailTemplate();
			emailTemplate.setAccessKey(accesskey);
			emailTemplate.setSecretKey(key);
			emailTemplate.setEmailFrom(emailFrom);
			emailTemplate.setSendTo(user.getEmailInfo().getEmailAddress());
			emailTemplate.setSubject("Zoylo.com | Welcome to zoylo");
			String verificationLink = getVerificationLink(user.getEmailInfo().getVerificationToken(), user.getEmailInfo().getEmailAddress());
			emailTemplate.setBody(emailBll.verificationLinkTemplate(user.getFirstName(), verificationLink));
			// Sends OTP to registered Mobile
			SMSService smsService = new SMSService();
			SendSmsVM sendSmsVM = new SendSmsVM();
			sendSmsVM.setMessage("Use " + sixDigitOtp
					+ " as One Time Password (OTP) to activate your Zoylo account. Do not share this OTP with anyone for security reasons. For more details, call us at 1800 3000 5454.");
			sendSmsVM.setNumber(user.getPhoneInfo().getPhoneNumber()); // to
																		// do
			sendSmsVM.setSecretKey(key);
			sendSmsVM.setAccesskey(accesskey);
			sendSmsVM.setApiUrl(apiUrl);
			sendSmsVM.setCountry(country);
			sendSmsVM.setAuthkey(authkey);
			sendSmsVM.setRoute(route);
			sendSmsVM.setSender(sender);
			// save email info
			String messageId = "";
			if(emailNotificationAllowed) {
				SendEmailResult sendEmailResult = emailSEService.sendEmail(emailTemplate);
				messageId = sendEmailResult.getMessageId();
			}
			
			notificationBll.saveNotificationAfterEmail(user, emailTemplate, messageId);
			// SMS data save
			String smsMessageId = "";
			if (mobileNotificationAllowed) {
				SmsServiceResModel smsServiceResModel = smsService.sendSMSMessage(sendSmsVM);
				smsMessageId = smsServiceResModel.getRequestId();
			}
			sendGeneratedOTP(sixDigitOtp, user.getPhoneInfo().getPhoneNumber(), user);

			logger.info(OTP_SENT);
			return user;
		} catch (CustomParameterizedException exception) {
			throw exception;
		} catch (MongoException exception) {
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.DB_ERROR.getErrMsg(), exception, CustomExceptionCode.DB_ERROR.getErrCode());

		} catch (Exception exception) {
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.REGISTERATION_FAIL.getErrMsg(), exception,
					CustomExceptionCode.REGISTERATION_FAIL.getErrCode());
		}
	}

	
	/**
	 * returns the verification link with token
	 * @param verToken
	 * @param emailAddress
	 * @return
	 */
	private String getVerificationLink(String  verToken, String emailAddress){
		
		return validationUrl+ "?verToken=" + verToken
                +"&&emailId=" + emailAddress;
	}
	
	private User populateEmailInfoUserForValidation(User user, String emailAddress){
		EmailInfo emailInfo = new EmailInfo();
		emailInfo.setEmailAddress(emailAddress);
		emailInfo.setVerificationToken(verifyEmailTokenProvider.generateEmailVerificationToken(emailAddress));
		emailInfo.setTokenSentAt(new Date());
		emailInfo.setTokenExpiresAt(new Date(emailInfo.getTokenSentAt().getTime() + emailTokenExpirationMinutes * 60000));
		user.setEmailInfo(emailInfo);
		return user;
	}

	/***
	 * 
	 * @description Get Recipient User type
	 * @return user list
	 */
	@Override
	public List<UserTypeVM> getRecipentUserType() {
		try {
			List<UserTypeVM> userTypeVMs = new ArrayList<>();
			ZoyloUserType[] zoyloUserTypes = ZoyloUserType.class.getEnumConstants();
			for (int i = 0; i < zoyloUserTypes.length; i++) {
				UserTypeVM userTypeVM = new UserTypeVM();
				userTypeVM.setUserType(zoyloUserTypes[i].getUserType());
				userTypeVM.setUserCode(zoyloUserTypes[i].getUserCode());
				userTypeVM.setName(zoyloUserTypes[i].getName());
				userTypeVMs.add(userTypeVM);
			}
			return userTypeVMs;
		} catch (Exception exception) {
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.USER_TYPE_EXCEPTION.getErrMsg(), exception,
					CustomExceptionCode.USER_TYPE_EXCEPTION.getErrCode());
		}
	}

	/***
	 * 
	 * @description Get Admin User type
	 * @return user list
	 */
	@Override
	public List<UserTypeVM> getAdminUserType() {
		try {
			List<UserTypeVM> userTypeVMs = new ArrayList<>();
			ZoyloAdminUserType[] zoyloUserTypes = ZoyloAdminUserType.class.getEnumConstants();
			for (int i = 0; i < zoyloUserTypes.length; i++) {
				UserTypeVM userTypeVM = new UserTypeVM();
				userTypeVM.setUserType(zoyloUserTypes[i].getUserType());
				userTypeVM.setUserCode(zoyloUserTypes[i].getUserCode());
				userTypeVM.setName(zoyloUserTypes[i].getName());
				userTypeVMs.add(userTypeVM);
			}
			return userTypeVMs;
		} catch (Exception exception) {
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.USER_TYPE_EXCEPTION.getErrMsg(), exception,
					CustomExceptionCode.USER_TYPE_EXCEPTION.getErrCode());
		}

	}

	/**
	 * @description Delete User
	 * @return
	 */
	@Override
	public void deleteUser(String userId) {
		try {
			User user = userRepository.findOneById(userId);

			if (user == null) {
				throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
						CustomExceptionCode.USER_NOT_FOUND.getErrMsg(),
						CustomExceptionCode.USER_NOT_FOUND.getErrCode());
			}
			// to maintain history
			UserHistory history = new UserHistory();
			modelMapper.map(user, history);
			history.setId(null);
			history.setUserId(user.getId());

			user.setDeleteFlag(TRUEFLAG);
			user.setDeletedOn(new Date());
			user.setActiveFlag(FALSEFLAG);
			userRepository.save(user);
			// to save history
			auditLogService.auditActionLog(Constants.DELETE, Constants.ZOYLO_USER);
			customRepository.save(history, Constants.ZOYLO_USER_HISTORY);
		} catch (CustomParameterizedException e) {
			throw e;

		} catch (MongoException exception) {
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.DB_ERROR.getErrMsg(), exception, CustomExceptionCode.DB_ERROR.getErrCode());
		} catch (Exception exception) {
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.DB_ERROR.getErrMsg(), exception, CustomExceptionCode.DB_ERROR.getErrCode());
		}
	}

	/**
	 * @description Update admin User
	 * @return
	 */
	@Override
	public User updateUser(UpdateRegisterVM registerModel) {
		try {
			String mobile = registerModel.getPhoneNumber();
			if (registerModel.getPhoneNumber() != null
					&& !registerModel.getPhoneNumber().startsWith(Constants.INDIA_MOBILE_CODE)) {
				registerModel.setPhoneNumber(Constants.INDIA_MOBILE_CODE + registerModel.getPhoneNumber());
			}
			User user = userRepository.findOneById(registerModel.getId());
			if (user == null) {
				throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
						CustomExceptionCode.USER_NOT_FOUND.getErrMsg(),
						CustomExceptionCode.USER_NOT_FOUND.getErrCode());
			}
			// to maintain history
			UserHistory history = new UserHistory();
			modelMapper.map(user, history);
			history.setId(null);
			history.setUserId(user.getId());
			// update email address
			if (registerModel.getEmailAddress() != null
					&& !user.getEmailInfo().getEmailAddress().equalsIgnoreCase(registerModel.getEmailAddress())) {
				User userEmailData = userRepository.findByEmailAddressAndDeleteFlag(registerModel.getEmailAddress(),
						false);
				if (userEmailData != null) {
					throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
							CustomExceptionCode.EMAIL_EXISTS.getErrMsg(),
							CustomExceptionCode.EMAIL_EXISTS.getErrCode());
				}

				EmailInfo emailInfo = new EmailInfo(registerModel.getEmailAddress());
				user.setEmailInfo(emailInfo);
			}
			// update phone number
			if (registerModel.getPhoneNumber() != null
					&& (!user.getPhoneInfo().getPhoneNumber().equalsIgnoreCase(registerModel.getPhoneNumber())
							&& !user.getPhoneInfo().getPhoneNumber().equalsIgnoreCase(mobile))) {
				User userMobileData = userRepository.findByPhoneNumberAndDeleteFlag(registerModel.getPhoneNumber(),
						false);

				if (userMobileData != null) {
					throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
							CustomExceptionCode.MOBILE_NUMBER_EXISTS.getErrMsg(),
							CustomExceptionCode.MOBILE_NUMBER_EXISTS.getErrCode());
				}
				PhoneInfo phoneInfo = new PhoneInfo(registerModel.getPhoneNumber());
				user.setPhoneInfo(phoneInfo);

			}
			// Update role and permission
			List<ServiceProvidersList> serviceProvidersList = new ArrayList<>();
			if (registerModel.getAdminDetail() != null) {
				List<String> roleCodes = registerModel.getAdminDetail().getRoleCode();
				ServiceProvidersList serviceProvider = setRole(roleCodes, null);
				serviceProvidersList.add(serviceProvider);
			}
			if (registerModel.getFirstName() != null && !registerModel.getFirstName().isEmpty())
				user.setFirstName(registerModel.getFirstName());
			if (registerModel.getLastName() != null && !registerModel.getLastName().isEmpty())
				user.setLastName(registerModel.getLastName());
			if (registerModel.getDeviceId() != null && registerModel.getDeviceId().isEmpty())
				user.setDeviceId(registerModel.getDeviceId());
			if (registerModel.getDeviceType() != null && !registerModel.getDeviceType().isEmpty())
				user.setDeviceType(registerModel.getDeviceType());
			if (registerModel.getEncryptedPassword() != null && !registerModel.getEncryptedPassword().isEmpty()) {
				String encryptedPassword = passwordEncoder
						.encode(PasswordEncryption.getEncryptedPassword(registerModel.getEncryptedPassword()));
				user.setEncryptedPassword(encryptedPassword);
			}
			if (registerModel.getIsZoyloEmployee() != null)
				user.setIsZoyloEmployee(registerModel.getIsZoyloEmployee());
			user.setActiveFlag(registerModel.getIsActiveFlag());
			User users = userRepository.save(user);

			if (users.getIsZoyloEmployee()) {
				ZoyloUserAuth zoyloUserAuth = zoyloUserAuthRepository.findByUserId(user.getId());
				if (zoyloUserAuth == null) {
					zoyloUserAuth = new ZoyloUserAuth();
					zoyloUserAuth.setEmailId(users.getEmailInfo().getEmailAddress());
					zoyloUserAuth.setPhoneNumber(users.getPhoneInfo().getPhoneNumber());
					zoyloUserAuth.setZoyloId(users.getZoyloId());
					zoyloUserAuth.setUserId(users.getId());
				}
				zoyloUserAuth.setServiceProvidersList(serviceProvidersList);
				zoyloUserAuthRepository.save(zoyloUserAuth);
			}
			// to save history
			auditLogService.auditActionLog(Constants.UPDATE, Constants.ZOYLO_USER);
			customRepository.save(history, Constants.ZOYLO_USER_HISTORY);
			return users;

		} catch (CustomParameterizedException e) {
			throw e;
		} catch (MongoException exception) {
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.DB_ERROR.getErrMsg(), exception, CustomExceptionCode.DB_ERROR.getErrCode());
		} catch (Exception exception) {
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.DB_ERROR.getErrMsg(), exception, CustomExceptionCode.DB_ERROR.getErrCode());
		}

	}

	/**
	 * @description use for mobile verification
	 * @param mobileVerificationVM
	 *            {MobileVerificationVM}
	 * @return
	 */
	@Override
	public void mobileVerification(MobileVerificationVM mobileVerificationVM) {
		try {
			User user = userRepository.findOneById(mobileVerificationVM.getId());
			if (user == null) {
				throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
						CustomExceptionCode.USER_NOT_FOUND.getErrMsg(),
						CustomExceptionCode.USER_NOT_FOUND.getErrCode());
			}
			// to maintain history
			UserHistory history = new UserHistory();
			modelMapper.map(user, history);
			history.setId(null);
			history.setUserId(user.getId());

			if (String.valueOf(mobileVerificationVM.getOtp()).length() != 6) {
				throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
						CustomExceptionCode.OTP_LENGTH.getErrMsg(), CustomExceptionCode.OTP_LENGTH.getErrCode());
			}

			PhoneInfo phoneInfo = user.getPhoneInfo();
			if (phoneInfo.getOtp() != mobileVerificationVM.getOtp()) {
				throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
						CustomExceptionCode.OTP_NOT_MATCH.getErrMsg(), CustomExceptionCode.OTP_NOT_MATCH.getErrCode());
			}

			if (((new Date().getTime() / 60000)
					- (phoneInfo.getOtpSentOn().getTime() / 60000)) > OPTExpirationMinutes) {
				throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
						CustomExceptionCode.OTP_EXPIRED.getErrMsg(), CustomExceptionCode.OTP_EXPIRED.getErrCode());
			}
			// to save history
			auditLogService.auditActionLog(Constants.MOBILE_VERIFICATION, Constants.ZOYLO_USER);
			customRepository.save(history, Constants.ZOYLO_USER_HISTORY);
			phoneInfo.setVerifiedFlag(TRUEFLAG);
			phoneInfo.setVerifiedOn(new Date());
			user.setPhoneInfo(phoneInfo);
			userRepository.save(user);

			UserProfileVM userProfileVM = new UserProfileVM();
			userProfileVM.setFirstName(user.getFirstName());
			userProfileVM.setLastName(user.getLastName());
			userProfileVM.setZoyloId(user.getZoyloId());
			userProfileVM.setUserId(user.getId());
			/*set gender for doctor*/
			ResponseEntity<String> resourceData=adminRestClient.getGenderByUserId(mobileVerificationVM.getId());
			String body = resourceData.getBody();
			JSONObject userJsonObj = new JSONObject(body);
			String gender = (String) userJsonObj.getString(Constants.DATA);
			if(gender!=null) {
				userProfileVM.setGender(gender);
			}

			List<Permission> permissions = permissionRepository
					.findByAuthCategoryCode(LookUp.RECIPIENT.getLookUpValue());
			RecipientPermissionListVM recipientpermissionListVM = new RecipientPermissionListVM();
			recipientpermissionListVM.setRecipientZoyloId(user.getZoyloId());
			recipientpermissionListVM.setRecipientId(user.getId());

			List<PermissionsListVM> permissionsListVMs = new ArrayList<>();
			for (Permission permission : permissions) {
				PermissionsListVM permissionsListVM = new PermissionsListVM();
				permissionsListVM.setPermissionCode(permission.getPermissionCode());
				permissionsListVM.setPermissionId(permission.getId());
				List<com.zoylo.gateway.model.PermissionData> permissionDatas = new ArrayList<>();
				if (permission.getPermissionData() != null) {
					for (PermissionData permissionData : permission.getPermissionData()) {
						com.zoylo.gateway.model.PermissionData recPermissionData = new com.zoylo.gateway.model.PermissionData();
						recPermissionData.setLanguageCode(permissionData.getLanguageCode());
						recPermissionData.setLanguageId(permissionData.getLanguageId());
						recPermissionData.setPermissionName(permissionData.getPermissionName());
						permissionDatas.add(recPermissionData);
					}
				}
				permissionsListVM.setPermissionData(permissionDatas);
				permissionsListVMs.add(permissionsListVM);
			}
			recipientpermissionListVM.setPermissionsList(permissionsListVMs);
			userProfileVM.setGrantedPermissionsList(recipientpermissionListVM);
			try {
				recipientRestClient.setUserProfile(userProfileVM);
			} catch (Exception exception) {

				throw new CustomParameterizedException(RegisteredExceptions.REGISTRATION_EXCEPTION.getException(),
						CustomExceptionCode.FAIL_TO_CREATE_USER_PROFILE.getErrMsg(), exception,
						CustomExceptionCode.FAIL_TO_CREATE_USER_PROFILE.getErrCode());
			}
			// send mobile and password to controller

		} catch (CustomParameterizedException e) {
			throw e;
		} catch (AuthenticationException exception) {
			throw new CustomParameterizedException(RegisteredException.LOGIN_EXCEPTION.getException(),
					CustomExceptionCode.LOGIN_FAIL.getErrMsg(), exception, CustomExceptionCode.LOGIN_FAIL.getErrCode());
		} catch (Exception exception) {
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.DB_ERROR.getErrMsg(), exception, CustomExceptionCode.DB_ERROR.getErrCode());
		}
	}

	/**
	 * @author Mehraj Malik
	 */
	@Override
	public String resendOTP(ResendOTPVM otpvm) {
		logger.info("--- In OTP SEND method");
		if (otpvm.getMobileNo() != null && !otpvm.getMobileNo().startsWith(Constants.INDIA_MOBILE_CODE)) {
			otpvm.setMobileNo(Constants.INDIA_MOBILE_CODE + otpvm.getMobileNo());
		}
		Optional<User> userOptional = userRepository.findByPhoneNumber(otpvm.getMobileNo());

		if (!userOptional.isPresent()) {
			logger.info("--- Could not found user ByPhoneNumber {}  to send OTP  ",otpvm.getMobileNo());
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.USER_NOT_FOUND.getErrCode());
		} else {
			logger.info("--- User  found ByPhoneNumber {}  to sending OTP  ",otpvm.getMobileNo());
			User user = userOptional.get();
			// to maintain history
			UserHistory history = new UserHistory();
			modelMapper.map(user, history);
			history.setId(null);
			history.setUserId(user.getId());

			Integer sixDigitOtp = new Random().nextInt(999999);	//NOSONAR
			if (sixDigitOtp <= 99999) {
				sixDigitOtp = 100000 + sixDigitOtp;
			}
			// to save history
			auditLogService.auditActionLog(Constants.RESEND_OTP, Constants.ZOYLO_USER);
			customRepository.save(history, Constants.ZOYLO_USER_HISTORY);
			logger.info("--- OTP History saved");
			PhoneInfo phoneInfo = user.getPhoneInfo();
			phoneInfo.setOtp(sixDigitOtp);
			phoneInfo.setOtpSentOn(new Date());
			if(phoneInfo.getResendOtpCount()==null) {
				phoneInfo.setResendOtpCount(Constants.INITIAL_RESEND_OTP_COUNT);
			}else {
				phoneInfo.setResendOtpCount(phoneInfo.getResendOtpCount()+Constants.INITIAL_RESEND_OTP_COUNT);				
			}
			user.setPhoneInfo(phoneInfo);

			// Sends OTP to registered Mobile
			SMSService smsService = new SMSService();
			SendSmsVM sendSmsVM = new SendSmsVM();
			sendSmsVM.setMessage(REGISTRATION_OTP_TITLE + sixDigitOtp);
			sendSmsVM.setNumber(user.getPhoneInfo().getPhoneNumber()); // to
																		// do
			sendSmsVM.setSecretKey(key);
			sendSmsVM.setAccesskey(accesskey);
			sendSmsVM.setApiUrl(apiUrl);
			sendSmsVM.setCountry(country);
			sendSmsVM.setAuthkey(authkey);
			sendSmsVM.setRoute(route);
			sendSmsVM.setSender(sender);
			// smsService.sendSMSMessage(sendSmsVM);
			// SMS data save
			String messageId = "";
			if(mobileNotificationAllowed) {
				SmsServiceResModel smsServiceResModel = smsService.sendSMSMessage(sendSmsVM);
				messageId = smsServiceResModel.getRequestId();
			}
			logger.info("--- OTP sent..saving in DB");
			userRepository.save(user);
			logger.info("--- OTP saved in User Collection");
			/*try {
				logger.info("---In Notification block");
				ZoyloNotificationVM zoyloNotification = new ZoyloNotificationVM();
				RecipientInfo recipientPhoneInfo = new RecipientInfo();
				MessageInfo messagePhoneInfo = new MessageInfo();
				recipientPhoneInfo.setRecipientFirstName(user.getFirstName());
				recipientPhoneInfo.setRecipientLastName(user.getLastName());
				recipientPhoneInfo.setRecipientZoyloId(user.getZoyloId());
				recipientPhoneInfo.setRecipientId(user.getId());
				recipientPhoneInfo.setRecipientPhone(sendSmsVM.getNumber());

				messagePhoneInfo.setMessageText(sendSmsVM.getMessage());
				List<DeliveryTrail> deliveryTrails = new ArrayList<>();
				DeliveryTrail trails = new DeliveryTrail();
				trails.setDeliveryAttemptedAt(new Date());
				trails.setDeliveryStatusCode(STATUS_CODE);
				deliveryTrails.add(trails);

				zoyloNotification.setDeliveryTrail(deliveryTrails);
				zoyloNotification.setNotificationMessageId(messageId);
				zoyloNotification.setNotificationModeCode(MESSAGE_MODE_CODE);
				zoyloNotification.setNotificationStatusCode(STATUS_CODE);
				zoyloNotification.setRecipientInfo(recipientPhoneInfo);
				zoyloNotification.setRetryCounter(1);
				zoyloNotification.setMessageInfo(messagePhoneInfo);
				logger.info("--- in Notification Before AdminRestClint save()");
				adminRestClient.save(zoyloNotification);
				logger.info("--- in Notification After AdminRestClint save()");
			} catch (Exception exception) {
				logger.info("--- in Notification After AdminRestClint excpetion occured : {}",exception);
				throw new CustomParameterizedException(RegisteredException.DATA_SAVE_EXCEPTION.getException(),
						CustomExceptionCode.DATA_NOT_SAVE.getErrMsg(), exception,
						CustomExceptionCode.DATA_NOT_SAVE.getErrCode());
			}*/
			notificationBll.saveNotificationAfterSMS(user, sendSmsVM, messageId);


			logger.info(OTP_SENT);
			return sixDigitOtp.toString();
		}
	}

	/***
	 * Get user page wise
	 * 
	 * @return page wise user details
	 */
	@Override
	public PageableVM<UserDetailVM> getAllByPage(Pageable pageable) {
		try {
			modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
			Page<User> userList = userRepository.findByDeleteFlagAndIsZoyloEmployee(pageable, Boolean.FALSE,
					Boolean.TRUE);
			PageableVM<UserDetailVM> pageableVM = new PageableVM<>();
			if (userList.getContent() != null && !userList.getContent().isEmpty()) {
				UserDetailVM[] userDetailArray = modelMapper.map(userList.getContent(), UserDetailVM[].class);
				List<UserDetailVM> userDetailVMs = Arrays.asList(userDetailArray);
				List<UserDetailVM> userDetailVMsNew = new ArrayList<>();
				for (UserDetailVM userDetail : userDetailVMs) {
					UserDetailVM userDetailVM = userDetail;
					ZoyloUserAuth zoyloUserAuth = zoyloUserAuthRepository.findByUserId(userDetail.getId());
					if (zoyloUserAuth != null) {
						List<ServiceProvidersList> serviceProvidersList = zoyloUserAuth.getServiceProvidersList();
						List<UserRoleVM> userRoleVMs = new ArrayList<>();
						for (ServiceProvidersList serviceProviders : serviceProvidersList) {
							if (serviceProviders.getProviderTypeCode() == null) {
								List<GrantedRolesList> grantedRolesList = serviceProviders.getGrantedRolesList();
								for (GrantedRolesList grantedRoles : grantedRolesList) {
									UserRoleVM userRoleVM = new UserRoleVM();
									userRoleVM.setRoleCode(grantedRoles.getRoleCode());
									userRoleVM.setRoleId(grantedRoles.getRoleId());
									userRoleVMs.add(userRoleVM);
								}
							} else {
								List<GrantedRolesList> grantedRolesList = serviceProviders.getGrantedRolesList();
								for (GrantedRolesList grantedRoles : grantedRolesList) {
									UserRoleVM userRoleVM = new UserRoleVM();
									userRoleVM.setRoleCode(grantedRoles.getRoleCode());
									userRoleVM.setRoleId(grantedRoles.getRoleId());
									userRoleVM.setProviderId(serviceProviders.getProviderId());
									userRoleVM.setProviderName(serviceProviders.getProviderName());
									userRoleVMs.add(userRoleVM);
								}
							}
						}
						userDetailVM.setUserRoleVM(userRoleVMs);
					}
					userDetailVMsNew.add(userDetailVM);
				}
				pageableVM.setContent(userDetailVMsNew);
			} else {
				pageableVM.setContent(new ArrayList<>());
			}
			PageInfoVM infoVM = modelMapper.map(userList, PageInfoVM.class);
			pageableVM.setPageInfo(infoVM);
			return pageableVM;
		} catch (Exception exception) {
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.DB_ERROR.getErrMsg(), exception, CustomExceptionCode.DB_ERROR.getErrCode());
		}
	}

	/**
	 * Set Mobile number and Email id for social media registration
	 * 
	 * @param changeMobileEmailVM
	 *            {@code ChangeMobileEmailVM}}
	 * @return user
	 */
	@Override
	public User setMobileAndEmailId(ChangeMobileEmailVM changeMobileEmailVM) {
		try {
			if (changeMobileEmailVM.getPhoneNumber() != null
					&& !changeMobileEmailVM.getPhoneNumber().startsWith(Constants.INDIA_MOBILE_CODE)) {
				changeMobileEmailVM.setPhoneNumber(Constants.INDIA_MOBILE_CODE + changeMobileEmailVM.getPhoneNumber());
			}
			User user = userRepository.findOneById(changeMobileEmailVM.getId());
			if (user == null) {
				throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
						CustomExceptionCode.USER_NOT_FOUND.getErrMsg(),
						CustomExceptionCode.USER_NOT_FOUND.getErrCode());
			}
			// update email address
			User userEmailData = userRepository.findByEmailAddressAndDeleteFlag(changeMobileEmailVM.getEmailAddress(),
					false);
			if (userEmailData != null) {
				throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
						CustomExceptionCode.EMAIL_EXISTS.getErrMsg(), CustomExceptionCode.EMAIL_EXISTS.getErrCode());
			}

			EmailInfo emailInfo = new EmailInfo(changeMobileEmailVM.getEmailAddress());
			user.setEmailInfo(emailInfo);
			// update phone number
			User userMobileData = userRepository.findByPhoneNumberAndDeleteFlag(changeMobileEmailVM.getPhoneNumber(),
					false);
			if (userMobileData != null) {
				throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
						CustomExceptionCode.MOBILE_NUMBER_EXISTS.getErrMsg(),
						CustomExceptionCode.MOBILE_NUMBER_EXISTS.getErrCode());
			}

			PhoneInfo phoneInfo = new PhoneInfo(changeMobileEmailVM.getPhoneNumber());
			user.setPhoneInfo(phoneInfo);
			userRepository.save(user);
			return user;
		} catch (CustomParameterizedException e) {
			throw e;
		} catch (MongoException exception) {
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.DB_ERROR.getErrMsg(), exception, CustomExceptionCode.DB_ERROR.getErrCode());
		} catch (Exception exception) {
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.REGISTERATION_FAIL.getErrMsg(), exception,
					CustomExceptionCode.REGISTERATION_FAIL.getErrCode());
		}
	}

	/***
	 * Doctor lead generation
	 * 
	 * @param doctorVM
	 *            {@code DoctorVM}
	 * @exception CustomParameterizedException
	 */
	@Override
	public void saveDoctorDetail(DoctorVM doctorVM) {
		try {
			User user = null;
			if (doctorVM != null) {
				user = userRepository.findOneById(doctorVM.getId());
			} else {
				throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
						CustomExceptionCode.USER_NOT_FOUND.getErrMsg(),
						CustomExceptionCode.USER_NOT_FOUND.getErrCode());
			}

			if (user == null) {
				throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
						CustomExceptionCode.USER_NOT_FOUND.getErrMsg(),
						CustomExceptionCode.USER_NOT_FOUND.getErrCode());
			}
			ZoyloLeadGenerationVM leadGenerationVM = new ZoyloLeadGenerationVM();
			leadGenerationVM = modelMapper.map(doctorVM, ZoyloLeadGenerationVM.class);
			leadGenerationVM.setFirstName(user.getFirstName());
			leadGenerationVM.setLastName(user.getLastName());
			leadGenerationVM.setMobileNumber(user.getPhoneInfo().getPhoneNumber());
			leadGenerationVM.setEmailId(user.getEmailInfo().getEmailAddress());
			try {
				adminRestClient.saveLead(leadGenerationVM);
			} catch (Exception exception) {

				throw new CustomParameterizedException(RegisteredExceptions.REGISTRATION_EXCEPTION.getException(),
						CustomExceptionCode.FAIL_TO_SAVE_DOCTOR.getErrMsg(), exception,
						CustomExceptionCode.FAIL_TO_SAVE_DOCTOR.getErrCode());

			}
		} catch (CustomParameterizedException e) {
			throw e;
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	/***
	 * Diagnostic lead generation
	 * 
	 * @param diagonsticVM
	 *            {@code DiagonsticVM}
	 * @exception CustomParameterizedException
	 */
	@Override
	public void saveDiagnosticDetail(DiagnosticVM diagonsticVM) {
		try {
			User user = userRepository.findOneById(diagonsticVM.getId());
			if (user == null) {
				throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
						CustomExceptionCode.USER_NOT_FOUND.getErrMsg(),
						CustomExceptionCode.USER_NOT_FOUND.getErrCode());
			}
			ZoyloLeadGenerationVM leadGenerationVM = new ZoyloLeadGenerationVM();
			leadGenerationVM = modelMapper.map(diagonsticVM, ZoyloLeadGenerationVM.class);
			leadGenerationVM.setFirstName(user.getFirstName());
			leadGenerationVM.setLastName(user.getLastName());
			leadGenerationVM.setMobileNumber(user.getPhoneInfo().getPhoneNumber());
			leadGenerationVM.setEmailId(user.getEmailInfo().getEmailAddress());
			try {
				adminRestClient.saveLead(leadGenerationVM);
			} catch (CustomParameterizedException e) {
				throw e;
			}
		} catch (CustomParameterizedException e) {
			throw e;
		} catch (Exception exception) {
			throw new CustomParameterizedException(RegisteredExceptions.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.FAIL_TO_SAVE_DIAGONSTIC.getErrMsg(), exception,
					CustomExceptionCode.FAIL_TO_SAVE_DIAGONSTIC.getErrCode());
		}

	}

	/**
	 * Active the user's registered email
	 * 
	 * @author Mehraj Malik
	 * @param token
	 *            token sent to user's registered email
	 * @throws EMAIL_ALREADY_VERIFIED
	 *             if email is already verified
	 * @throws EMAIL_NOT_EXISTS
	 *             if no user found by the email Id
	 */
	@Override
	public boolean validateToken(String token) {
		logger.info("--- Veryfing user token for email verification : {}", token);
		if (StringUtils.hasText(token) && this.tokenProvider.validateToken(token)) {
			logger.info("--- Token is valid");
			Authentication authentication = tokenProvider.getAuthentication(token);
			String email = authentication.getName();
			logger.info("Email to be verified : {}", email);
			Optional<User> userByEmail = userRepository.findByEmailAddress(email);
			if (userByEmail.isPresent()) {
				logger.info("User found by email : {}", email);
				User user = userByEmail.get();
				if (user.getEmailInfo().isVerifiedFlag() == true) {
					logger.info("Email is already verified");
					throw new CustomParameterizedException(
							RegisteredException.EMAIL_VERIFICATION_EXCEPTION.getException(),
							CustomExceptionCode.EMAIL_ALREADY_VERIFIED.getErrMsg(),
							CustomExceptionCode.EMAIL_ALREADY_VERIFIED.getErrCode());
				} else {
					userByEmail.get().getEmailInfo().setVerifiedFlag(true);
					userByEmail.get().getEmailInfo().setVerifiedOn(new Date());
					logger.info("Email verified succesfully");
					userRepository.save(user);
					return true;
				}
			} else {
				logger.info("User does not exist with id : {} ", email);
				throw new CustomParameterizedException(RegisteredException.EMAIL_VERIFICATION_EXCEPTION.getException(),
						CustomExceptionCode.EMAIL_NOT_EXISTS.getErrMsg(),
						CustomExceptionCode.EMAIL_NOT_EXISTS.getErrCode());
			}
		} else {
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.TOKEN_EXPIRE.getErrMsg(), CustomExceptionCode.TOKEN_EXPIRE.getErrCode());
		}
	}

	/**
	 * @author Diksha Gupta
	 * @description to add ghost user while booking appointment to doctor
	 * @param ghostUserVM
	 * @return ghostUserVM
	 * @exception CustomParameterizedException
	 */
	@Override
	public GhostUserVM addGhostUser(GhostUserVM ghostUserVM) {
		logger.info("---In service method for {}",ghostUserVM.getFirstName()+"_"+ghostUserVM.getLastName());
		try {

			if (ghostUserVM.getEmailId() != null && !ghostUserVM.getEmailId().isEmpty()) {
				
				User userEmailData = userRepository.findByEmailAddressAndDeleteFlag(ghostUserVM.getEmailId(), false);
				if (userEmailData != null) {
					logger.info("--- Found by email ID : ");
					throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
							CustomExceptionCode.EMAIL_EXISTS.getErrMsg(),
							CustomExceptionCode.EMAIL_EXISTS.getErrCode());
				}
			}
			if (ghostUserVM.getMobileNumber() != null && !ghostUserVM.getMobileNumber().isEmpty()) {
				User userMobile = userRepository.findByPhoneNumberAndDeleteFlag(ghostUserVM.getMobileNumber(), false);
				if (!ghostUserVM.getMobileNumber().startsWith(Constants.INDIA_MOBILE_CODE)) {
					ghostUserVM.setMobileNumber(Constants.INDIA_MOBILE_CODE + ghostUserVM.getMobileNumber());
				}

				if (userMobile != null) {
					logger.info("--- Found by phoneNumber : ");
					throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
							CustomExceptionCode.MOBILE_NUMBER_EXISTS.getErrMsg(),
							CustomExceptionCode.MOBILE_NUMBER_EXISTS.getErrCode());
				}
			}
			logger.info("--- Creating Ghost User for : {}",ghostUserVM.getFirstName()+"_"+ghostUserVM.getLastName());
			User user = new User();
			user.setFirstName(ghostUserVM.getFirstName());
			user.setLastName(ghostUserVM.getLastName());
			EmailInfo emailInfo = new EmailInfo(ghostUserVM.getEmailId());
			user.setEmailInfo(emailInfo);
			PhoneInfo phoneInfo = new PhoneInfo(ghostUserVM.getMobileNumber());
			user.setPhoneInfo(phoneInfo);
			user.setZoyloId(UUID.randomUUID().toString());
			user.setActiveFlag(true);
			user.setLoginEnabled(true);
			user = userRepository.save(user);
			logger.info("---  Ghost User created for : {} with ID {}",ghostUserVM.getFirstName()+"_"+ghostUserVM.getLastName(),user.getId());
			if (ghostUserVM.getMobileNumber() != null && !ghostUserVM.getMobileNumber().isEmpty()) {
				ResendOTPVM resendOTPVM = new ResendOTPVM();
				logger.info("--- Before OTP SEND");
				resendOTPVM.setMobileNo(ghostUserVM.getMobileNumber());
				logger.info("--- Before OTP SEND method");
				resendOTP(resendOTPVM);
				logger.info("--- After OTP SEND method");
			}
			ghostUserVM.setUserId(user.getId());
			ghostUserVM.setZoyloId(user.getZoyloId());
			return ghostUserVM;
		} catch (CustomParameterizedException customParameterizedException) {
			throw customParameterizedException;
		} catch (Exception exception) {
			logger.info("--- Exception in UserBllImpl#addGhostUser : {} ",exception);
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.REGISTERATION_FAIL.getErrMsg(), exception,
					CustomExceptionCode.REGISTERATION_FAIL.getErrCode());
		}
	}

	/**
	 * @author Diksha gupta
	 * @description get user description
	 */
	@Override
	public UserProfileDataVM getUser(String id) {
		try {
			UserProfileDataVM userProfileDataVM = new UserProfileDataVM();
			User user = userRepository.findOne(id);
			if (user != null) {
				if (user.getEmailInfo() != null) {
					userProfileDataVM.setEmailAddress(user.getEmailInfo().getEmailAddress());
					userProfileDataVM.setEmailVerified(user.getEmailInfo().isVerifiedFlag());
				}
				if (user.getPhoneInfo() != null) {
					userProfileDataVM.setPhoneNumber(user.getPhoneInfo().getPhoneNumber());
				}
				userProfileDataVM.setFirstName(user.getFirstName());
				userProfileDataVM.setMiddleName(user.getMiddleName());
				userProfileDataVM.setLastName(user.getLastName());
				userProfileDataVM.setUserId(user.getId());
				userProfileDataVM.setZoyloId(user.getZoyloId());
			}
			return userProfileDataVM;
		} catch (CustomParameterizedException customParameterizedException) {
			throw customParameterizedException;
		} catch (Exception exception) {
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.USER_NOT_FOUND.getErrMsg(), exception,
					CustomExceptionCode.USER_NOT_FOUND.getErrCode());
		}
	}

	/**
	 * @author Diksha gupta
	 * @description get user description
	 */
	@Override
	public UserProfileDataVM getUserData(String email, String mobile) {
		try {
			if (mobile != null && !mobile.startsWith(Constants.INDIA_MOBILE_CODE)) {
				mobile = Constants.INDIA_MOBILE_CODE + mobile;
			}
			UserProfileDataVM userProfileDataVM = new UserProfileDataVM();
			User user = null;
			if (email != null && !email.isEmpty()) {
				user = userRepository.findByEmailAddressAndDeleteFlag(email, false);
			}
			if (user != null) {
				if (user.getEmailInfo() != null) {
					userProfileDataVM.setEmailAddress(user.getEmailInfo().getEmailAddress());
				}
				if (user.getPhoneInfo() != null) {
					userProfileDataVM.setPhoneNumber(user.getPhoneInfo().getPhoneNumber());
					userProfileDataVM.setMobileVerified(user.getPhoneInfo().isVerifiedFlag());
				}
				userProfileDataVM.setFirstName(user.getFirstName());
				userProfileDataVM.setLastName(user.getLastName());
				userProfileDataVM.setUserId(user.getId());
				userProfileDataVM.setZoyloId(user.getZoyloId());
			} else {
				if (mobile != null && !mobile.isEmpty()) {
					user = userRepository.findByPhoneNumberAndDeleteFlag(mobile, false);
				}
				if (user != null) {

					if (user.getEmailInfo() != null) {
						userProfileDataVM.setEmailAddress(user.getEmailInfo().getEmailAddress());
					}
					if (user.getPhoneInfo() != null) {
						userProfileDataVM.setPhoneNumber(user.getPhoneInfo().getPhoneNumber());
						userProfileDataVM.setMobileVerified(user.getPhoneInfo().isVerifiedFlag());
					}
					userProfileDataVM.setFirstName(user.getFirstName());
					userProfileDataVM.setLastName(user.getLastName());
					userProfileDataVM.setUserId(user.getId());
					userProfileDataVM.setZoyloId(user.getZoyloId());

				} else {
					return null;
				}
			}
			return userProfileDataVM;
		} catch (CustomParameterizedException customParameterizedException) {
			throw customParameterizedException;
		} catch (Exception exception) {
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.USER_NOT_FOUND.getErrMsg(), exception,
					CustomExceptionCode.USER_NOT_FOUND.getErrCode());
		}
	}

	/**
	 * @author piyush.singh
	 * @description send mail
	 * @return void
	 * @throws IOException 
	 * @throws TemplateException 
	 */

	public void sendGhostUserMail(User user) throws TemplateException, IOException {
		String emailId = user.getEmailInfo().getEmailAddress();
		String authrties = "" + "," + "";
		String mobileNo = user.getPhoneInfo().getPhoneNumber();
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.DATE, tokenValidDay);
		long now = (cal.getTimeInMillis());
		Date validity = new Date(now);
		EmailSEService emailSEService = new EmailSEService();
		EmailTemplate emailTemplate = new EmailTemplate();
		emailTemplate.setAccessKey(accesskey);
		emailTemplate.setSecretKey(key);
		emailTemplate.setEmailFrom(emailFrom);
		emailTemplate.setSendTo(emailId);
		emailTemplate.setSubject(guestUserSubject);
		String token = Jwts.builder().setSubject(emailId).claim(AUTHORITIES_KEY, authrties).claim(MOBILE_NO, mobileNo)
				.claim(USER_ID, emailId).claim(USER_NAME, emailId).signWith(SignatureAlgorithm.HS512, secretKey)
				.setExpiration(validity).compact();
				/*String body=verifyEmailBody + ": \n\t " + registrationPort + "/api/forgetpassword/activate?token=" + token;
				emailTemplate.setBody(emailCommonTemplate(body));*/
		//done		
		/*String emailBody="<h1 style=\"font-size:1.1em;margin-top:0px;\">Dear "+user.getFirstName()+", </h1>" +
				"\n "+verifyEmailBody+ ": \n\t " + registrationPort + "/api/forgetpassword/activate?token=" + token;*/
				emailTemplate.setBody(emailBll.ghostEmailVerification(user.getFirstName()));
		String messageId = "";
		if(emailNotificationAllowed) {
			SendEmailResult sendEmailResult = emailSEService.sendEmail(emailTemplate);
			messageId = sendEmailResult.getMessageId();
		}
		/*try {
			ZoyloNotificationVM zoyloNotificationVM = new ZoyloNotificationVM();
			RecipientInfo recipientInfo = new RecipientInfo();
			MessageInfo messageInfo = new MessageInfo();

			recipientInfo.setRecipientFirstName(user.getFirstName());
			recipientInfo.setRecipientLastName(user.getLastName());
			recipientInfo.setRecipientZoyloId(user.getZoyloId());
			recipientInfo.setRecipientId(user.getId());
			recipientInfo.setRecipientEmail(emailTemplate.getSendTo());

			messageInfo.setMessageText(emailTemplate.getBody());

			List<DeliveryTrail> deliveryTrail = new ArrayList<>();
			DeliveryTrail trail = new DeliveryTrail();
			trail.setDeliveryAttemptedAt(new Date());
			trail.setDeliveryStatusCode(STATUS_CODE);
			deliveryTrail.add(trail);

			zoyloNotificationVM.setNotificationMessageId(sendEmailResult.getMessageId());
			zoyloNotificationVM.setNotificationModeCode(EMAIL_MODE_CODE);
			zoyloNotificationVM.setNotificationStatusCode(STATUS_CODE);
			zoyloNotificationVM.setRecipientInfo(recipientInfo);
			zoyloNotificationVM.setRetryCounter(1);
			zoyloNotificationVM.setMessageInfo(messageInfo);
			zoyloNotificationVM.setDeliveryTrail(deliveryTrail);

			adminRestClient.save(zoyloNotificationVM);
		} catch (Exception exception) {
			logger.info("unable to save notification email");
		}*/
		notificationBll.saveNotificationAfterEmail(user, emailTemplate, messageId);
	}

	/***
	 * fatch user data by mobile
	 * 
	 * 
	 */
	@Override
	public UserResponseVM getUserByMobile(RegisterVM registerModel) {

		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		try {
			UserResponseVM userResponseVM = null;
			User userEmailData = null;
			if (registerModel.getEmailAddress() != null && !registerModel.getEmailAddress().equals("")) {
				userEmailData = userRepository.findByEmailAddressAndDeleteFlag(registerModel.getEmailAddress(), false);
				if (userEmailData != null) {
					userResponseVM = modelMapper.map(userEmailData, UserResponseVM.class);
					PhoneInfo phoneInfo = userResponseVM.getPhoneInfo();
					if (phoneInfo != null) {
						phoneInfo.setPhoneNumber(phoneInfo.getPhoneNumber());
						userResponseVM.setPhoneInfo(phoneInfo);
					}
					if(userResponseVM.getEmailInfo()!= null){
						userResponseVM.setEmailVerified(userResponseVM.getEmailInfo().isVerifiedFlag());
					}
					userResponseVM.setRegisterType("emailId");
					String userType = getUserType(userEmailData.getId(), userEmailData.getIsZoyloEmployee());
					userResponseVM.setUserType(userType);
					userResponseVM.setIsUserExist(true);
					return userResponseVM;
				}
			}

			if (registerModel.getPhoneNumber() != null && !registerModel.getPhoneNumber().equals("")) {
				User userMobile = userRepository.findByPhoneNumberAndDeleteFlag(registerModel.getPhoneNumber(), false);

				if (userMobile != null) {
					userResponseVM = modelMapper.map(userMobile, UserResponseVM.class);
					PhoneInfo phoneInfo = userResponseVM.getPhoneInfo();
					if (phoneInfo != null) {
						phoneInfo.setPhoneNumber(phoneInfo.getPhoneNumber());
						userResponseVM.setPhoneInfo(phoneInfo);
					}
					if(userResponseVM.getEmailInfo()!= null){
						userResponseVM.setEmailVerified(userResponseVM.getEmailInfo().isVerifiedFlag());
					}
					userResponseVM.setRegisterType("mobile");
					String userType = getUserType(userMobile.getId(), userMobile.getIsZoyloEmployee());
					userResponseVM.setUserType(userType);
					userResponseVM.setIsUserExist(true);
					return userResponseVM;
				} else {
					return userResponseVM;
				}
			}
			return userResponseVM;
		} catch (CustomParameterizedException exception) {
			throw exception;
		} catch (MongoException exception) {
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.DB_ERROR.getErrMsg(), exception, CustomExceptionCode.DB_ERROR.getErrCode());
		} catch (Exception exception) {
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.REGISTERATION_FAIL.getErrMsg(), exception,
					CustomExceptionCode.REGISTERATION_FAIL.getErrCode());
		}

	}

	/**
	 * @author Diksha Gupt
	 * @description to add ghost user while booking appointment to doctor
	 * @param ghostUserVM
	 * @return ghostUserVM
	 * @exception CustomParameterizedException
	 */
	@Override
	public UserPMSDataVM savePMSUser(UserDataVM userDataVM) {
		try {
			if (userDataVM.getPhoneNumber() != null) {
				if (!userDataVM.getPhoneNumber().startsWith(Constants.INDIA_MOBILE_CODE)) {
					userDataVM.setPhoneNumber(Constants.INDIA_MOBILE_CODE + userDataVM.getPhoneNumber());
				}
			}
			User user = new User();
			boolean isUpdate = false;

			if (userDataVM.getUserId() != null && !userDataVM.getUserId().isEmpty()) {
				isUpdate = true;
				user = userRepository.findOneById(userDataVM.getUserId());
			}

			UserPMSDataVM userPMSDataVM = new UserPMSDataVM();
			if (isUpdate) {
				if (!user.getEmailInfo().getEmailAddress().equalsIgnoreCase(userDataVM.getEmailAddress())) {
					User userEmailData = userRepository.findByEmailAddressAndDeleteFlag(userDataVM.getEmailAddress(),
							false);
					if (userEmailData != null) {
						userPMSDataVM.setExistsEmail(true);
						userPMSDataVM.setUserId(userEmailData.getId());
						userPMSDataVM.setZoyloId(userEmailData.getZoyloId());
						return userPMSDataVM;
					}
				}
			} else {
				User userEmailData = userRepository.findByEmailAddressAndDeleteFlag(userDataVM.getEmailAddress(),
						false);
				if (userEmailData != null) {
					userPMSDataVM.setExistsEmail(true);
					userPMSDataVM.setUserId(userEmailData.getId());
					userPMSDataVM.setZoyloId(userEmailData.getZoyloId());
					return userPMSDataVM;
				}
				User userMobileData = userRepository.findByPhoneNumberAndDeleteFlag(userDataVM.getPhoneNumber(), false);

				if (userMobileData != null) {
					userPMSDataVM.setExistsMobile(true);
					userPMSDataVM.setUserId(userMobileData.getId());
					userPMSDataVM.setZoyloId(userMobileData.getZoyloId());
					return userPMSDataVM;
				}
			}
			if (userDataVM.getFirstName() != null && !userDataVM.getFirstName().isEmpty()) {
				user.setFirstName(userDataVM.getFirstName());
			}
			if (userDataVM.getLastName() != null && !userDataVM.getLastName().isEmpty()) {
				user.setLastName(userDataVM.getLastName());
			}
			if (userDataVM.getEmailAddress() != null && !userDataVM.getEmailAddress().isEmpty()) {
				EmailInfo emailInfo = new EmailInfo(userDataVM.getEmailAddress());
				user.setEmailInfo(emailInfo);
			}
			if (!isUpdate) {
				PhoneInfo phoneInfo = new PhoneInfo(userDataVM.getPhoneNumber());
				user.setPhoneInfo(phoneInfo);
				user.setZoyloId(UUID.randomUUID().toString());
			}
			if (userDataVM.getPassword() != null && !userDataVM.getPassword().isEmpty()) {
				String encryptedPassword = passwordEncoder
						.encode(PasswordEncryption.getEncryptedPassword(userDataVM.getPassword()));
				user.setEncryptedPassword(encryptedPassword);
			}
			List<String> roleCodes = new ArrayList<>();
			List<ServiceProvidersList> serviceProvidersList = new ArrayList<>();
			roleCodes.add(userDataVM.getRole());
			ServiceProvidersList serviceProvider = setRole(roleCodes, userDataVM);
			serviceProvidersList.add(serviceProvider);
			user.setActiveFlag(TRUEFLAG);
			user.setLoginEnabled(TRUEFLAG);
			user = userRepository.save(user);
			ZoyloUserAuth zoyloUserAuth = zoyloUserAuthRepository.findByUserId(user.getId());
			if (zoyloUserAuth == null) {
				zoyloUserAuth = new ZoyloUserAuth();
				zoyloUserAuth.setEmailId(user.getEmailInfo().getEmailAddress());
				zoyloUserAuth.setPhoneNumber(user.getPhoneInfo().getPhoneNumber());
				zoyloUserAuth.setZoyloId(user.getZoyloId());
				zoyloUserAuth.setUserId(user.getId());
			}
			zoyloUserAuth.setServiceProvidersList(serviceProvidersList);
			zoyloUserAuthRepository.save(zoyloUserAuth);
			/*
			 * if (!isUpdate) { ResendOTPVM resendOTPVM = new ResendOTPVM();
			 * resendOTPVM.setMobileNo(userDataVM.getPhoneNumber());
			 * resendOTP(resendOTPVM); }
			 */
			userPMSDataVM.setFirstName(user.getFirstName());
			userPMSDataVM.setLastName(user.getLastName());
			userPMSDataVM.setEmailAddress(user.getEmailInfo().getEmailAddress());
			userPMSDataVM.setPhoneNumber(user.getPhoneInfo().getPhoneNumber());
			userPMSDataVM.setUserId(user.getId());
			userPMSDataVM.setZoyloId(user.getZoyloId());
			userPMSDataVM.setExistsEmail(false);
			userPMSDataVM.setExistsMobile(false);

			return userPMSDataVM;

		} catch (CustomParameterizedException customParameterizedException) {
			throw customParameterizedException;
		} catch (Exception exception) {
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.REGISTERATION_FAIL.getErrMsg(), exception,
					CustomExceptionCode.REGISTERATION_FAIL.getErrCode());
		}
	}

	/**
	 * Method to add a Ghost user
	 * 
	 * @author Iti Gupta
	 * @param ghostUserFamilyMemberVM
	 * @return GhostUserFamilyMemberVM
	 */
	@Override
	public GhostUserFamilyMemberVM addGhostUserFamilyMember(GhostUserFamilyMemberVM ghostUserFamilyMemberVM) {
		try {
			User user = new User();
			user.setFirstName(ghostUserFamilyMemberVM.getFirstName());
			user.setLastName(ghostUserFamilyMemberVM.getLastName());
			user.setZoyloId(UUID.randomUUID().toString());
			user = userRepository.save(user);
			ghostUserFamilyMemberVM.setUserId(user.getId());
			ghostUserFamilyMemberVM.setZoyloId(user.getZoyloId());
			return ghostUserFamilyMemberVM;
		} catch (CustomParameterizedException customParameterizedException) {
			throw customParameterizedException;
		} catch (Exception exception) {
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.REGISTERATION_FAIL.getErrMsg(), exception,
					CustomExceptionCode.REGISTERATION_FAIL.getErrCode());
		}
	}

	@Override
	public UserProfileDataVM getWalkinUserData(String search) {
		try {
			User user = null;
			UserProfileDataVM userProfileDataVM = new UserProfileDataVM();
			if (search.contains("@")) {
				user = userRepository.findByEmailAddressAndDeleteFlag(search, false);
			} else {

				user = userRepository.findByPhoneNumberAndDeleteFlag(search, false);

			}
			if (user != null) {
				if (user.getEmailInfo() != null) {
					userProfileDataVM.setEmailAddress(user.getEmailInfo().getEmailAddress());
				}
				if (user.getPhoneInfo() != null) {
					userProfileDataVM.setPhoneNumber(user.getPhoneInfo().getPhoneNumber());
					userProfileDataVM.setMobileVerified(user.getPhoneInfo().isVerifiedFlag());
				}
				userProfileDataVM.setFirstName(user.getFirstName());
				userProfileDataVM.setLastName(user.getLastName());
				userProfileDataVM.setUserId(user.getId());
				userProfileDataVM.setZoyloId(user.getZoyloId());
			} else {
				return null;
			}
			return userProfileDataVM;
		} catch (CustomParameterizedException customParameterizedException) {
			throw customParameterizedException;
		} catch (Exception exception) {
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.USER_NOT_FOUND.getErrMsg(), exception,
					CustomExceptionCode.USER_NOT_FOUND.getErrCode());
		}
	}

	/**
	 * Provides a collection of User by deviceType
	 * 
	 * @author Mehraj Malik
	 * 
	 * @param deviceType
	 *            Device type by the Users to be found
	 * 
	 */
	@Override
	public PageableVM<User> getByDeviceType(String deviceType, Pageable pageable) {
		try {
			Page<User> usersPage = userRepository.findByDeviceTypeIgnoreCase(deviceType, pageable);
			PageableVM<User> pageableVM = new PageableVM<>();
			if (!usersPage.getContent().isEmpty()) {
				List<User> users = usersPage.getContent();
				pageableVM.setContent(Arrays.asList(modelMapper.map(users, User[].class)));
			} else {
				pageableVM.setContent(Collections.emptyList());
			}
			pageableVM.setPageInfo(modelMapper.map(usersPage, PageInfoVM.class));
			return pageableVM;
		} catch (Exception exception) {
			throw new CustomParameterizedException(RegisteredException.USER_FETCH_EXCEPTION.getException(),
					CustomExceptionCode.USER_NOT_FOUND.getErrMsg(), exception,
					CustomExceptionCode.USER_NOT_FOUND.getErrCode());
		}
	}

	/**
	 * @author Devendra.Kumar
	 * @description To search user by first name or last name
	 * @param pageable
	 * @param name
	 * @return PageableVM<UserDetailVM>
	 */
	@Override
	public PageableVM<UserDetailVM> getAllByName(Pageable pageable, String name) {
		try {
			Page<User> userList = userRepository.findByDeleteFlagAndFirstNameIgnoreCaseAndIsZoyloEmployee(pageable,
					FALSEFLAG, name.trim(), Boolean.TRUE);
			if (userList.getContent().isEmpty()) {
				userList = userRepository.findByDeleteFlagAndLastNameIgnoreCaseAndIsZoyloEmployee(pageable, FALSEFLAG,
						name.trim(), Boolean.TRUE);
			}

			PageableVM<UserDetailVM> pageableVM = new PageableVM<>();

			if (userList.getContent() != null && !userList.getContent().isEmpty()) {
				UserDetailVM[] userDetailArray = modelMapper.map(userList.getContent(), UserDetailVM[].class);
				List<UserDetailVM> userDetailVMs = Arrays.asList(userDetailArray);
				List<UserDetailVM> userDetailVMsNew = new ArrayList<>();
				for (UserDetailVM userDetail : userDetailVMs) {
					UserDetailVM userDetailVM = userDetail;
					ZoyloUserAuth zoyloUserAuth = zoyloUserAuthRepository.findByUserId(userDetail.getId());
					if (zoyloUserAuth != null) {
						List<ServiceProvidersList> serviceProvidersList = zoyloUserAuth.getServiceProvidersList();
						List<UserRoleVM> userRoleVMs = new ArrayList<>();
						for (ServiceProvidersList serviceProviders : serviceProvidersList) {
							if (serviceProviders.getProviderTypeId() == null) {
								List<GrantedRolesList> grantedRolesList = serviceProviders.getGrantedRolesList();
								for (GrantedRolesList grantedRoles : grantedRolesList) {
									UserRoleVM userRoleVM = new UserRoleVM();
									userRoleVM.setRoleCode(grantedRoles.getRoleCode());
									userRoleVM.setRoleId(grantedRoles.getRoleId());
									userRoleVMs.add(userRoleVM);
								}
							}
						}
						userDetailVM.setUserRoleVM(userRoleVMs);
					}
					userDetailVMsNew.add(userDetailVM);
				}
				pageableVM.setContent(userDetailVMsNew);
			} else {
				pageableVM.setContent(new ArrayList<>());
			}

			// if (!userList.getContent().isEmpty()) {
			// UserDetailVM[] userDetailArray =
			// modelMapper.map(userList.getContent(), UserDetailVM[].class);
			// pageableVM.setContent(Arrays.asList(userDetailArray));
			// } else {
			// pageableVM.setContent(new ArrayList<>());
			// }
			PageInfoVM infoVM = modelMapper.map(userList, PageInfoVM.class);
			pageableVM.setPageInfo(infoVM);
			return pageableVM;
		} catch (Exception exception) {
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.DB_ERROR.getErrMsg(), exception, CustomExceptionCode.DB_ERROR.getErrCode());
		}
	}

	@Override
	public void sendNotifiation(SendPushVM sendPushVM) {
		snsMobilePushService.sendPushNotification(sendPushVM, sendPushVM.getPlateformName());

	}

	private GrantedRolesList updateAutority(Role role) {
		GrantedRolesList grantedRole = new GrantedRolesList();
		grantedRole.setActiveFlag(role.isActiveFlag());
		grantedRole.setActiveFromDate(role.getActiveFromDate());
		grantedRole.setActiveTillDate(role.getActiveTillDate());
		grantedRole.setGrantedPermissionsList(role.getPermissionList());
		grantedRole.setRoleCode(role.getRoleCode());
		List<GrantedRoleData> grantedRoleDatas = new ArrayList<>();
		for (RoleData roleData : role.getRoleData()) {
			GrantedRoleData grantedRoleData = new GrantedRoleData();
			grantedRoleData.setLanguageCode(roleData.getLanguageCode());
			grantedRoleData.setLanguageId(roleData.getLanguageId());
			grantedRoleData.setRoleName(roleData.getRoleName());
			grantedRoleDatas.add(grantedRoleData);
		}
		grantedRole.setRoleData(grantedRoleDatas);
		grantedRole.setRoleId(role.getId());
		grantedRole.setSystemDefined(role.isSystemDefined());
		return grantedRole;
	}

	/**
	 * Save Admin role
	 * 
	 * @param roleCodes
	 * @return
	 */
	@Override
	public ServiceProvidersList setRole(List<String> roleCodes, UserDataVM userDataVM) {
		List<GrantedRolesList> grantedRolesLists = new ArrayList<>();
		for (String roleCode : roleCodes) {
			Role role = roleRepository.findOneByRoleCode(roleCode);
			if (role == null) {
				throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
						CustomExceptionCode.ROLE_NOT_FOUND_ERROR.getErrMsg(),
						CustomExceptionCode.ROLE_NOT_FOUND_ERROR.getErrCode());
			}

			GrantedRolesList grantedRole = updateAutority(role);
			grantedRolesLists.add(grantedRole);
		}
		ServiceProvidersList serviceProvider = new ServiceProvidersList();
		if (userDataVM != null) {
			serviceProvider.setProviderCode(userDataVM.getProviderCode());
			serviceProvider.setProviderId(userDataVM.getProviderId());
			serviceProvider.setProviderName(userDataVM.getProviderName());
			serviceProvider.setProviderSearchName(userDataVM.getProviderSearchName());
			serviceProvider.setProviderTypeCode(userDataVM.getProviderTypeCode());
			serviceProvider.setProviderTypeData(userDataVM.getProviderTypeData());
			serviceProvider.setProviderTypeId(userDataVM.getProviderTypeId());
		}
		serviceProvider.setGrantedRolesList(grantedRolesLists);
		return serviceProvider;
	}

	@Override
	public void setDoctorRole(DoctorRoleDataVM userDataVM) {
		List<GrantedRolesList> grantedRolesLists = new ArrayList<>();
		Role role = roleRepository.findOneByRoleCode(userDataVM.getRole());
		if (role == null) {
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.ROLE_NOT_FOUND_ERROR.getErrMsg(),
					CustomExceptionCode.ROLE_NOT_FOUND_ERROR.getErrCode());
		}

		GrantedRolesList grantedRole = updateAutority(role);
		grantedRolesLists.add(grantedRole);
		ServiceProvidersList serviceProvider = new ServiceProvidersList();
		List<ServiceProvidersList> serviceProvidersList = new ArrayList<>();
		User user = userRepository.findOneById(userDataVM.getUserId());
		ZoyloUserAuth zoyloUserAuth = zoyloUserAuthRepository.findByUserId(user.getId());
		for (int clinicCount = 0; clinicCount < userDataVM.getProviderIdList().size(); clinicCount++) {
			serviceProvider.setProviderCode(userDataVM.getProviderCode());
			serviceProvider.setProviderId(userDataVM.getProviderIdList().get(clinicCount));
			serviceProvider.setProviderName(userDataVM.getProviderNameList().get(clinicCount));
			serviceProvider.setProviderSearchName(userDataVM.getProviderSearchName());
			serviceProvider.setProviderTypeCode(userDataVM.getProviderTypeCode());
			serviceProvider.setProviderTypeData(userDataVM.getProviderTypeData());
			serviceProvider.setProviderTypeId(userDataVM.getProviderTypeId());
			serviceProvider.setGrantedRolesList(grantedRolesLists);
			if (zoyloUserAuth == null) {
				zoyloUserAuth = new ZoyloUserAuth();
				zoyloUserAuth.setEmailId(user.getEmailInfo().getEmailAddress());
				zoyloUserAuth.setPhoneNumber(user.getPhoneInfo().getPhoneNumber());
				zoyloUserAuth.setZoyloId(user.getZoyloId());
				zoyloUserAuth.setUserId(user.getId());
			} else {
				serviceProvidersList = zoyloUserAuth.getServiceProvidersList();
			}
			serviceProvidersList.add(serviceProvider);
			zoyloUserAuth.setServiceProvidersList(serviceProvidersList);
			zoyloUserAuthRepository.save(zoyloUserAuth);
		}

	}

	/**
	 * Email Id and phone number validation
	 * 
	 * @param emailId
	 * @param phoneNumber
	 */
	private void phoneEmailValidation(String emailId, String phoneNumber) {

		User userData = userRepository.findByEmailInfoEmailAddressAndPhoneInfoPhoneNumber(emailId, phoneNumber);

		if (userData != null) {
			User userMobileData = userRepository.findByPhoneNumberAndVerifiedFlagAndDeleteFlag(phoneNumber, true,
					false);

			if (userMobileData != null) {
				throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
						CustomExceptionCode.MOBILE_NUMBER_EXISTS.getErrMsg(),
						CustomExceptionCode.MOBILE_NUMBER_EXISTS.getErrCode());

			} else {
				throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
						CustomExceptionCode.MOBILE_NUMBER_ALREADY_EXIST_NOT_VERIFIED.getErrMsg(),
						CustomExceptionCode.MOBILE_NUMBER_ALREADY_EXIST_NOT_VERIFIED.getErrCode());
			}

		} else {
			User userMobile = userRepository.findByPhoneNumberAndDeleteFlag(phoneNumber, false);
			if (userMobile != null) {
				User userMobileData = userRepository.findByPhoneNumberAndVerifiedFlagAndDeleteFlag(phoneNumber, true,
						false);

				if (userMobileData != null) {
					throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
							CustomExceptionCode.MOBILE_NUMBER_EXISTS.getErrMsg(),
							CustomExceptionCode.MOBILE_NUMBER_EXISTS.getErrCode());

				} else {
					User userEmailData = userRepository.findByEmailAddressAndDeleteFlag(emailId, false);
					if (userEmailData != null) {
						throw new CustomParameterizedException(
								RegisteredException.REGISTRATION_EXCEPTION.getException(),
								CustomExceptionCode.EMAIL_EXISTS.getErrMsg(),
								CustomExceptionCode.EMAIL_EXISTS.getErrCode());
					} else {
						EmailInfo emailInfo = new EmailInfo();
						emailInfo.setEmailAddress(emailId);

						userMobile.setEmailInfo(emailInfo);
						userRepository.save(userMobile);

						throw new CustomParameterizedException(
								RegisteredException.REGISTRATION_EXCEPTION.getException(),
								CustomExceptionCode.MOBILE_NUMBER_ALREADY_EXIST_NOT_VERIFIED.getErrMsg(),
								CustomExceptionCode.MOBILE_NUMBER_ALREADY_EXIST_NOT_VERIFIED.getErrCode());
					}

				}
			} else {
				User userEmailData = userRepository.findByEmailAddressAndDeleteFlag(emailId, false);
				if (userEmailData != null) {
					throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
							CustomExceptionCode.EMAIL_EXISTS.getErrMsg(),
							CustomExceptionCode.EMAIL_EXISTS.getErrCode());
				}
			}

		}

	}

	@Override
	public UserUpdateResponceVM updateUserEmailAndMobileIfNotExist(RegisterVM registerVM) {
		try {
			String phone = registerVM.getPhoneNumber();
			if (registerVM.getPhoneNumber() != null
					&& !registerVM.getPhoneNumber().startsWith(Constants.INDIA_MOBILE_CODE)) {
				phone = registerVM.getPhoneNumber();
				registerVM.setPhoneNumber(Constants.INDIA_MOBILE_CODE + registerVM.getPhoneNumber());
			}
			UserUpdateResponceVM updateResponceVM = new UserUpdateResponceVM();
			updateResponceVM.setEmailExits(false);
			updateResponceVM.setMobileExits(false);
			updateResponceVM.setUpdatedSuccessfully(false);

			User user = userRepository.findOneById(registerVM.getUserId());
			if (user == null) {
				throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
						CustomExceptionCode.USER_NOT_FOUND.getErrMsg(),
						CustomExceptionCode.USER_NOT_FOUND.getErrCode());
			}
			// to maintain history
			UserHistory history = new UserHistory();
			modelMapper.map(user, history);
			history.setId(null);
			history.setUserId(user.getId());
			// update email address
			if (registerVM.getEmailAddress() != null
					&& !user.getEmailInfo().getEmailAddress().equalsIgnoreCase(registerVM.getEmailAddress())) {
				User userEmailData = userRepository.findByEmailAddressAndDeleteFlag(registerVM.getEmailAddress(),
						false);
				if (userEmailData != null) {
					updateResponceVM.setEmailExits(true);
					return updateResponceVM;
				}
				EmailInfo emailInfo = new EmailInfo(registerVM.getEmailAddress(), false, new Date());
				user.setEmailInfo(emailInfo);
			}
			// update phone number
			if (registerVM.getPhoneNumber() != null
					&& (!user.getPhoneInfo().getPhoneNumber().equalsIgnoreCase(registerVM.getPhoneNumber())
							|| !user.getPhoneInfo().getPhoneNumber().equalsIgnoreCase(phone))) {

				User userMobileData = userRepository.findByPhoneNumberAndDeleteFlag(registerVM.getPhoneNumber(), false);

				if (userMobileData != null) {
					updateResponceVM.setMobileExits(true);
					return updateResponceVM;
				}
				PhoneInfo phoneInfo = new PhoneInfo(registerVM.getPhoneNumber(), false, new Date());
				user.setPhoneInfo(phoneInfo);
			}
			User updatedUser = userRepository.save(user);
			// to save history
			auditLogService.auditActionLog(Constants.UPDATE, Constants.ZOYLO_USER);
			customRepository.save(history, Constants.ZOYLO_USER_HISTORY);
			updateResponceVM.setUserId(updatedUser.getId());
			updateResponceVM.setUpdatedSuccessfully(true);
			return updateResponceVM;
		} catch (CustomParameterizedException e) {
			throw e;
		} catch (MongoException exception) {
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.DB_ERROR.getErrMsg(), exception, CustomExceptionCode.DB_ERROR.getErrCode());
		} catch (Exception exception) {
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.DB_ERROR.getErrMsg(), exception, CustomExceptionCode.DB_ERROR.getErrCode());
		}
	}

	@Override
	public UserUpdateResponceVM updateUserEmailAndMobile(RegisterVM registerVM) {
		try {
			String phone = registerVM.getPhoneNumber();
			if (registerVM.getPhoneNumber() != null
					&& !registerVM.getPhoneNumber().startsWith(Constants.INDIA_MOBILE_CODE)) {
				phone = registerVM.getPhoneNumber();
				registerVM.setPhoneNumber(Constants.INDIA_MOBILE_CODE + registerVM.getPhoneNumber());
			}
			UserUpdateResponceVM updateResponceVM = new UserUpdateResponceVM();
			updateResponceVM.setEmailExits(false);
			updateResponceVM.setMobileExits(false);
			updateResponceVM.setUpdatedSuccessfully(false);

			User user = userRepository.findOneById(registerVM.getUserId());
			if (user == null) {
				throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
						CustomExceptionCode.USER_NOT_FOUND.getErrMsg(),
						CustomExceptionCode.USER_NOT_FOUND.getErrCode());
			}
			// to maintain history
			UserHistory history = new UserHistory();
			modelMapper.map(user, history);
			history.setId(null);
			history.setUserId(user.getId());
			// update email address
			if (registerVM.getEmailAddress() != null
					&& !user.getEmailInfo().getEmailAddress().equalsIgnoreCase(registerVM.getEmailAddress())) {
				User userEmailData = userRepository.findByEmailAddressAndDeleteFlag(registerVM.getEmailAddress(),
						false);
				if (userEmailData != null) {
					updateResponceVM.setEmailExits(true);
					return updateResponceVM;
				}
				EmailInfo emailInfo = new EmailInfo(registerVM.getEmailAddress(), false, new Date());
				user.setEmailInfo(emailInfo);
			}
			// update phone number
			if (registerVM.getPhoneNumber() != null
					&& (!user.getPhoneInfo().getPhoneNumber().equalsIgnoreCase(registerVM.getPhoneNumber())
							&& !user.getPhoneInfo().getPhoneNumber().equalsIgnoreCase(phone))) {
				User userMobileData = userRepository.findByPhoneNumberAndDeleteFlag(registerVM.getPhoneNumber(), false);

				if (userMobileData != null) {
					updateResponceVM.setMobileExits(true);
					return updateResponceVM;
				}
				PhoneInfo phoneInfo = new PhoneInfo(registerVM.getPhoneNumber(), false, new Date());
				user.setPhoneInfo(phoneInfo);
			}
			if(!StringUtils.isEmpty(registerVM.getFirstName())){
				user.setFirstName(registerVM.getFirstName());
			}
			if(registerVM.getMiddleName()!=null){
				user.setMiddleName(registerVM.getMiddleName());
			}
			if(registerVM.getLastName()!=null){
				user.setLastName(registerVM.getLastName());
			}			
			User updatedUser = userRepository.save(user);
			if(!StringUtils.isEmpty(registerVM.getFirstName()) || registerVM.getMiddleName()!=null || registerVM.getLastName()!=null){
				ZoyloUserUpdateVM zoyloUserUpdateVM=new ZoyloUserUpdateVM();
				zoyloUserUpdateVM.setId(registerVM.getUserId());
				if(!StringUtils.isEmpty(registerVM.getFirstName())) {
					zoyloUserUpdateVM.setFirstName(registerVM.getFirstName());					
				}
				if(registerVM.getMiddleName()!=null) {
					zoyloUserUpdateVM.setMiddleName(registerVM.getMiddleName());					
				}
				if(registerVM.getLastName()!=null) {
					zoyloUserUpdateVM.setLastName(registerVM.getLastName());					
				}
				recipientRestClient.updateUserProfile(zoyloUserUpdateVM);
			}
			// to save history
			auditLogService.auditActionLog(Constants.UPDATE, Constants.ZOYLO_USER);
			customRepository.save(history, Constants.ZOYLO_USER_HISTORY);
			updateResponceVM.setUserId(updatedUser.getId());
			updateResponceVM.setUpdatedSuccessfully(true);
			return updateResponceVM;
		} catch (CustomParameterizedException e) {
			throw e;
		} catch (MongoException exception) {
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.DB_ERROR.getErrMsg(), exception, CustomExceptionCode.DB_ERROR.getErrCode());
		} catch (Exception exception) {
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.DB_ERROR.getErrMsg(), exception, CustomExceptionCode.DB_ERROR.getErrCode());
		}
	}

	/**
	 * @author Diksha Gupta
	 * @description To update password for pms user by userid
	 * @param passwordUpdateVM
	 * @return
	 */
	@Override
	public void updatePassword(PasswordUpdateVM passwordUpdateVM) {
		try {
			User user = userRepository.findOneById(passwordUpdateVM.getUserId());
			if (user == null) {
				throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
						CustomExceptionCode.USER_NOT_FOUND.getErrMsg(),
						CustomExceptionCode.USER_NOT_FOUND.getErrCode());
			}
			String encryptedPassword = passwordEncoder
					.encode(PasswordEncryption.getEncryptedPassword(passwordUpdateVM.getPassword()));
			user.setEncryptedPassword(encryptedPassword);
			userRepository.save(user);
		} catch (Exception exception) {
			logger.error(exception.getMessage());
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.USER_NOT_FOUND.getErrMsg(), CustomExceptionCode.USER_NOT_FOUND.getErrCode());
		}
	}

	/**
	 * @author Diksha Gupta
	 * @description To get pms user role by userId and providerId
	 * @param PMSUserRoleMVList
	 * @return PMSUserRoleMVList
	 */
	@Override
	public PMSUserRoleMVList getPMSUserRole(PMSUserRoleMVList pMSUserRoleMVList) {
		PMSUserRoleMVList pMSUserRoleMVLists = new PMSUserRoleMVList();
		List<PMSUserRoleMV> pMSUserRoleMVs = new ArrayList<>();
		for (PMSUserRoleMV pMSUserRole : pMSUserRoleMVList.getpMSUserRoleMVs()) {
			List<ZoyloAuthVM> zoyloAuthVMs = zoyloUserAuthRepository.findAuthByProviderId(pMSUserRole.getUserId(),
					pMSUserRole.getProviderId());
			if (zoyloAuthVMs != null && zoyloAuthVMs.size() > 0) {
				ZoyloAuthVM zoyloAuthVM = zoyloAuthVMs.get(0);
				PMSUserRoleMV pMSUserRoleMV = new PMSUserRoleMV();
				pMSUserRoleMV.setUserId(pMSUserRole.getUserId());
				pMSUserRoleMV.setProviderId(zoyloAuthVM.getServiceProvidersList().getProviderId());
				pMSUserRoleMV.setRoleId(zoyloAuthVM.getServiceProvidersList().getGrantedRolesList().get(0).getRoleId());
				pMSUserRoleMV
						.setRoleCode(zoyloAuthVM.getServiceProvidersList().getGrantedRolesList().get(0).getRoleCode());
				pMSUserRoleMV.setGrantedRoleData(
						zoyloAuthVM.getServiceProvidersList().getGrantedRolesList().get(0).getRoleData());
				pMSUserRoleMVs.add(pMSUserRoleMV);
			}
		}
		pMSUserRoleMVLists.setpMSUserRoleMVs(pMSUserRoleMVs);
		return pMSUserRoleMVLists;
	}

	/**
	 * Get Service Provider Admin user by role code and provider id
	 * 
	 * @param serviceProviderId
	 * @param providerTypeCode
	 * @param roleCode
	 */
	@Override
	public ProviderAdminDataMV getProviderUserDetail(String serviceProviderId, String providerTypeCode,
			String roleCode) {
		try {
			List<ZoyloUserAuthVM> list = zoyloUserAuthRepository.findAuthByProviderIdAndRoleCode(serviceProviderId,
					providerTypeCode, roleCode);
			if (list == null || list.isEmpty()) {
				throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
						CustomExceptionCode.USER_NOT_FOUND.getErrMsg(),
						CustomExceptionCode.USER_NOT_FOUND.getErrCode());
			}
			ProviderAdminDataMV providerAdminDataMV = new ProviderAdminDataMV();
			String userId = list.get(0).getUserId();
			User user = userRepository.findOne(userId);
			providerAdminDataMV.setFirstName(user.getFirstName());
			providerAdminDataMV.setLastName(user.getLastName());
			providerAdminDataMV.setMiddleName(user.getMiddleName());
			if (user.getEmailInfo() != null) {
				providerAdminDataMV.setEmailAddress(user.getEmailInfo().getEmailAddress());
			}
			if (user.getPhoneInfo() != null) {
				providerAdminDataMV.setPhoneNumber(user.getPhoneInfo().getPhoneNumber());
			}
			providerAdminDataMV.setUserId(userId);
			providerAdminDataMV.setZoyloId(user.getZoyloId());
			return providerAdminDataMV;
		} catch (CustomParameterizedException e) {
			throw e;
		} catch (Exception e) {
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.USER_NOT_FOUND.getErrMsg(), CustomExceptionCode.USER_NOT_FOUND.getErrCode());
		}

	}

	/**
	 * update zoylo user email,phone number and userType.
	 * 
	 * @author prabhat.singh
	 * @param registerVM
	 * 
	 */
	@Override
	public UserUpdateResponceVM updateUserEmailAndMobileAndUserType(RegisterVM registerVM) {
		try {
			String phone = registerVM.getPhoneNumber();
			if (registerVM.getPhoneNumber() != null
					&& !registerVM.getPhoneNumber().startsWith(Constants.INDIA_MOBILE_CODE)) {
				phone = registerVM.getPhoneNumber();
				registerVM.setPhoneNumber(Constants.INDIA_MOBILE_CODE + registerVM.getPhoneNumber());
			}
			UserUpdateResponceVM updateResponceVM = new UserUpdateResponceVM();
			updateResponceVM.setUpdatedSuccessfully(false);
			User user = userRepository.findOneById(registerVM.getUserId());
			if (user == null) {
				throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
						CustomExceptionCode.USER_NOT_FOUND.getErrMsg(),
						CustomExceptionCode.USER_NOT_FOUND.getErrCode());
			}
			// to maintain history
			UserHistory history = new UserHistory();
			modelMapper.map(user, history);
			history.setId(null);
			history.setUserId(user.getId());
			// update email address
			if (user.getEmailInfo() != null) {
				if (user.getEmailInfo().getEmailAddress() != null) {
					if (registerVM.getEmailAddress() != null
							&& !user.getEmailInfo().getEmailAddress().equalsIgnoreCase(registerVM.getEmailAddress())) {
						EmailInfo emailInfo = new EmailInfo(registerVM.getEmailAddress(), false, new Date());
						user.setEmailInfo(emailInfo);
					}
				} else {
					EmailInfo emailInfo = new EmailInfo(registerVM.getEmailAddress(), false, new Date());
					user.setEmailInfo(emailInfo);
				}
			} else {
				if (registerVM.getEmailAddress() != null) {
					EmailInfo emailInfo = new EmailInfo(registerVM.getEmailAddress(), false, new Date());
					user.setEmailInfo(emailInfo);
				}
			}
			// update phone number
			if (registerVM.getPhoneNumber() != null
					&& (!user.getPhoneInfo().getPhoneNumber().equalsIgnoreCase(registerVM.getPhoneNumber())
							|| !user.getPhoneInfo().getPhoneNumber().equalsIgnoreCase(phone))) {
				PhoneInfo phoneInfo = new PhoneInfo(registerVM.getPhoneNumber(), false, new Date());
				user.setPhoneInfo(phoneInfo);
			}
			// user.setUserType(registerVM.getUserType());
			User updatedUser = userRepository.save(user);
			// to save history
			auditLogService.auditActionLog(Constants.UPDATE, Constants.ZOYLO_USER);
			customRepository.save(history, Constants.ZOYLO_USER_HISTORY);
			updateResponceVM.setUserId(updatedUser.getId());
			updateResponceVM.setUpdatedSuccessfully(true);
			return updateResponceVM;
		} catch (CustomParameterizedException e) {
			throw e;
		} catch (MongoException exception) {
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.DB_ERROR.getErrMsg(), exception, CustomExceptionCode.DB_ERROR.getErrCode());
		} catch (Exception exception) {
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.DB_ERROR.getErrMsg(), exception, CustomExceptionCode.DB_ERROR.getErrCode());
		}
	}

	private String getUserType(String userId, Boolean isZoyloEmployee) {
		String userType = ZoyloUserType.RECIPIENT.getUserType();
		if (isZoyloEmployee != null && isZoyloEmployee) {
			return ZoyloAdminUserType.ADMIN.getUserType();
		}
		ZoyloUserAuth zoyloUserAuth = zoyloUserAuthRepository.findByUserId(userId);
		if (zoyloUserAuth != null) {
			List<ServiceProvidersList> providersList = zoyloUserAuth.getServiceProvidersList();
			if (providersList != null && !providersList.isEmpty()) {
				if (providersList.get(0).getProviderTypeCode().equalsIgnoreCase(LookUp.DIAGONSTIC.getLookUpValue())) {
					return ZoyloUserType.DIAGONSTIC.getUserType();
				} else {
					return ZoyloUserType.DOCTOR.getUserType();

				}
			}
		}
		return userType;
	}

	/**
	 * Get Service Provider Admin user by role code and provider id
	 * 
	 * @author Diksha gupta
	 * @param serviceProviderId
	 * @param providerTypeCode
	 * @param roleCode
	 */
	@Override
	public PMSUserRoleMVList getProviderUserDetail(String serviceProviderId, String roleCode) {
		try {
			PMSUserRoleMVList pMSUserRoleMVLists = new PMSUserRoleMVList();
			List<PMSUserRoleMV> pMSUserRoleMVs = new ArrayList<>();
			List<ZoyloUserAuthVM> zoyloAuthVMs = zoyloUserAuthRepository
					.findAuthByProviderIdAndRoleCode(serviceProviderId, roleCode);
			if (zoyloAuthVMs != null && zoyloAuthVMs.size() > 0) {

				for (ZoyloUserAuthVM zoyloAuthVM : zoyloAuthVMs) {
					PMSUserRoleMV pMSUserRoleMV = new PMSUserRoleMV();
					pMSUserRoleMV.setUserId(zoyloAuthVM.getUserId());
					pMSUserRoleMV.setProviderId(zoyloAuthVM.getServiceProvidersList().getProviderId());
					pMSUserRoleMV
							.setRoleId(zoyloAuthVM.getServiceProvidersList().getGrantedRolesList().get(0).getRoleId());
					pMSUserRoleMV.setRoleCode(
							zoyloAuthVM.getServiceProvidersList().getGrantedRolesList().get(0).getRoleCode());
					pMSUserRoleMV.setGrantedRoleData(
							zoyloAuthVM.getServiceProvidersList().getGrantedRolesList().get(0).getRoleData());
					pMSUserRoleMVs.add(pMSUserRoleMV);
				}
			}

			pMSUserRoleMVLists.setpMSUserRoleMVs(pMSUserRoleMVs);
			return pMSUserRoleMVLists;
		} catch (CustomParameterizedException e) {
			throw e;
		} catch (Exception e) {
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.USER_NOT_FOUND.getErrMsg(), CustomExceptionCode.USER_NOT_FOUND.getErrCode());
		}

	}

	/*
	 * private String getPhoneNumber(String phone) { if (phone != null &&
	 * !phone.isEmpty()) { if (!phone.startsWith(Constants.INDIA_MOBILE_CODE)) {
	 * return phone; } else { phone = phone.substring(3, phone.length()); } }
	 * return phone; }
	 */

	private void sendEmail(User user) throws TemplateException, IOException {
		if (user.getEmailInfo() != null && user.getEmailInfo().getEmailAddress() != null
				&& !user.getEmailInfo().getEmailAddress().isEmpty()) {
			EmailSEService emailSEService = new EmailSEService();
			EmailTemplate emailTemplate = new EmailTemplate();
			emailTemplate.setAccessKey(accesskey);
			emailTemplate.setSecretKey(key);
			emailTemplate.setEmailFrom(emailFrom);
			emailTemplate.setSendTo(user.getEmailInfo().getEmailAddress());
			emailTemplate.setSubject("Zoylo.com | Welcome to zoylo");
            //dome
			emailTemplate.setBody(emailBll.accountActive(user.getFirstName()));
			String messageId = "";
			if(emailNotificationAllowed) {
				SendEmailResult sendEmailResult = emailSEService.sendEmail(emailTemplate);
				messageId = sendEmailResult.getMessageId();
			}
			/*try {
				ZoyloNotificationVM zoyloNotificationVM = new ZoyloNotificationVM();
				RecipientInfo recipientInfo = new RecipientInfo();
				MessageInfo messageInfo = new MessageInfo();

				recipientInfo.setRecipientFirstName(user.getFirstName());
				recipientInfo.setRecipientLastName(user.getLastName());
				recipientInfo.setRecipientZoyloId(user.getZoyloId());
				recipientInfo.setRecipientId(user.getId());
				recipientInfo.setRecipientEmail(emailTemplate.getSendTo());

				messageInfo.setMessageText(emailTemplate.getBody());

				List<DeliveryTrail> deliveryTrail = new ArrayList<>();
				DeliveryTrail trail = new DeliveryTrail();
				trail.setDeliveryAttemptedAt(new Date());
				trail.setDeliveryStatusCode(STATUS_CODE);
				deliveryTrail.add(trail);

				zoyloNotificationVM.setNotificationMessageId(messageId);
				zoyloNotificationVM.setNotificationModeCode(EMAIL_MODE_CODE);
				zoyloNotificationVM.setNotificationStatusCode(STATUS_CODE);
				zoyloNotificationVM.setRecipientInfo(recipientInfo);
				zoyloNotificationVM.setRetryCounter(1);
				zoyloNotificationVM.setMessageInfo(messageInfo);
				zoyloNotificationVM.setDeliveryTrail(deliveryTrail);

				adminRestClient.save(zoyloNotificationVM);
			} catch (Exception exception) {
				logger.info("unable to save notification email");
			}*/
			notificationBll.saveNotificationAfterEmail(user, emailTemplate, messageId);
		}
	}

	/***
	 * Method to update User
	 * 
	 * @author Iti Gupta
	 */
	@Override
	public User updateUser(UserUpdateVM userUpdateVM) {
		try {
			logger.info("Enter in updateUser(UserUpdateVM userUpdateVM) and id:  " + userUpdateVM.getUserId());

			User user = userRepository.findOneById(userUpdateVM.getUserId());
			if (user != null) {
				// to maintain history
				UserHistory history = new UserHistory();
				modelMapper.map(user, history);
				history.setId(null);
				history.setUserId(user.getId());
				user.setFirstName(userUpdateVM.getFirstName());
				user.setMiddleName(userUpdateVM.getMiddleName());
				user.setLastName(userUpdateVM.getLastName());

				userRepository.save(user);
				// to save history
				auditLogService.auditActionLog(Constants.UPDATE, Constants.ZOYLO_USER);
				customRepository.save(history, Constants.ZOYLO_USER_HISTORY);

				logger.info("Exit from updateUser(UserUpdateVM userUpdateVM)");

				return user;
			} else {
				throw new CustomParameterizedException(RegisteredException.USER_PROFILE_EXCEPTION.getException(),
						CustomExceptionCode.USER_NOT_FOUND.getErrMsg(),
						CustomExceptionCode.USER_NOT_FOUND.getErrCode());
			}
		} catch (Exception exception) {
			logger.error("Exception occor while saving user profile detail in db{} ", exception.getMessage());
			throw new CustomParameterizedException(RegisteredException.USER_PROFILE_EXCEPTION.getException(),
					CustomExceptionCode.USER_NOT_FOUND.getErrCode(), exception,
					CustomExceptionCode.USER_NOT_FOUND.getErrMsg());
		}

	}

	/**
	 * @author damini.arora
	 * @description to get all Users
	 * @return PageableVM<UserVM>
	 */
/*
	@Override
	public PageableVM<UserVM> getAll(Pageable pageable) {
		try {
			Page<User> listOfUsers = userRepository.findByDeleteFlag(FALSEFLAG, pageable);
			PageableVM<UserVM> pageableVM = new PageableVM<>();
			if (!listOfUsers.getContent().isEmpty()) {
				UserVM[] userMV = modelMapper.map(listOfUsers.getContent(), UserVM[].class);
				pageableVM.setContent(Arrays.asList(userMV));
			} else {
				pageableVM.setContent(new ArrayList<>());
			}
			PageInfoVM infoVM = modelMapper.map(listOfUsers, PageInfoVM.class);
			pageableVM.setPageInfo(infoVM);
			return pageableVM;
		} catch (Exception exception) {
			logger.error(CustomExceptionCode.USER_NOT_FOUND.getErrMsg());
			throw new CustomParameterizedException(RegisteredException.USER_PROFILE_EXCEPTION.getException(),
					CustomExceptionCode.USER_NOT_FOUND.getErrMsg(), exception,
					CustomExceptionCode.USER_NOT_FOUND.getErrCode());
		}
	}*/
	

	/**
	 * @author arvind.rawat
	 * @description to get all Users
	 * @return PageableVM<UserVM>
	 */
	@Override
	public PageableVM<UserVM> getAll(Pageable pageable, String serachField) {
		try {
			try {
				Map<Long, List<User>> map =  customUserRepository.getAllAdminUsers(pageable, serachField);
				List<User> listOfUsers = new ArrayList<>();
				Long total = 0l;
				for (Map.Entry<Long, List<User>> entry : map.entrySet()) {
					listOfUsers = entry.getValue();
					total = entry.getKey();
				}
				PageableVM<UserVM> pageableVM = new PageableVM<>();
				if (!listOfUsers.isEmpty()) {
					UserVM[] userMV = modelMapper.map(listOfUsers, UserVM[].class);
					List<UserVM> users = new ArrayList<>();
					for (UserVM user : userMV) {
						ZoyloUserAuth zoyloUserAuth = zoyloUserAuthRepository.findByUserId(user.getId());
						if (zoyloUserAuth != null) {
							List<ServiceProvidersList> serviceProvidersList = zoyloUserAuth.getServiceProvidersList();
							List<UserRoleVM> userRoleVMs = new ArrayList<>();
							for (ServiceProvidersList serviceProviders : serviceProvidersList) {
								List<GrantedRolesList> grantedRolesList = serviceProviders.getGrantedRolesList();
								for (GrantedRolesList grantedRoles : grantedRolesList) {
									UserRoleVM userRoleVM = new UserRoleVM();
									userRoleVM.setRoleCode(grantedRoles.getRoleCode());
									userRoleVM.setRoleId(grantedRoles.getRoleId());
									userRoleVM.setProviderId(serviceProviders.getProviderId());
									userRoleVM.setProviderName(serviceProviders.getProviderName());
									userRoleVM.setProviderType(serviceProviders.getProviderTypeCode());
									userRoleVMs.add(userRoleVM);
								}
							}
							user.setUserRoleVM(userRoleVMs);
							users.add(user);
						} else {
							users.add(user);
						}
					}
					pageableVM.setContent(users);
				} else {
					pageableVM.setContent(new ArrayList<>());
				}
				PageInfoVM infoVM = PageableUtil.getPageMetaInfo(pageable, listOfUsers, total);
				pageableVM.setPageInfo(infoVM);
				return pageableVM;
			} catch (Exception exception) {
				logger.error(CustomExceptionCode.USER_NOT_FOUND.getErrMsg());
				throw new CustomParameterizedException(RegisteredException.USER_PROFILE_EXCEPTION.getException(),
						CustomExceptionCode.USER_NOT_FOUND.getErrMsg(), exception,
						CustomExceptionCode.USER_NOT_FOUND.getErrCode());
			}
		} catch (Exception exception) {
			logger.error("Exception occor while fetching all adminuser detail.");
			throw new CustomParameterizedException(RegisteredException.USER_PROFILE_EXCEPTION.getException(),
					CustomExceptionCode.ADMIN_USER_FETCH_EXCEPTION.getErrMsg(),
					CustomExceptionCode.ADMIN_USER_FETCH_EXCEPTION.getErrCode());
		}
	}

	/**
	 * @author damini.arora
	 * @description to update User Profile
	 * @param userVM
	 * @return UserVM
	 */
	@Override
	public UserVM update(UserVM userVM) {
		try {
			boolean emailVerifiedFlag = false;
			boolean phoneVerifiedFlag = false;
			modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
			User user = userRepository.findOne(userVM.getId());
			if (user == null) {
				logger.error(CustomExceptionCode.USER_UPDATE_ERROR.getErrMsg());
				throw new CustomParameterizedException(RegisteredException.USER_PROFILE_EXCEPTION.getException(),
						CustomExceptionCode.USER_UPDATE_ERROR.getErrMsg(),
						CustomExceptionCode.USER_UPDATE_ERROR.getErrCode());
			}
			if (!userVM.getEmailInfo().getEmailAddress().trim()
					.equalsIgnoreCase(user.getEmailInfo().getEmailAddress().trim())) {
				User userData = userRepository
						.findByEmailInfoEmailAddressAndDeleteFlag(userVM.getEmailInfo().getEmailAddress(), FALSEFLAG);
				if (userData != null) {
					logger.error(CustomExceptionCode.USER_EMAIL_ALREADY_EXIST.getErrMsg());
					throw new CustomParameterizedException(RegisteredException.USER_PROFILE_EXCEPTION.getException(),
							CustomExceptionCode.USER_EMAIL_ALREADY_EXIST.getErrMsg(),
							CustomExceptionCode.USER_EMAIL_ALREADY_EXIST.getErrCode());
				} else {
					emailVerifiedFlag = true;
				}
			}
			if (!userVM.getPhoneInfo().getPhoneNumber().trim()
					.equalsIgnoreCase(user.getPhoneInfo().getPhoneNumber().trim())) {
				User userPhoneNumber = userRepository
						.findByPhoneInfoPhoneNumberAndDeleteFlag(userVM.getPhoneInfo().getPhoneNumber(), FALSEFLAG);
				if (userPhoneNumber != null) {
					logger.error(CustomExceptionCode.USER_PHONE_NUMBER_ALREADY_EXIST.getErrMsg());
					throw new CustomParameterizedException(RegisteredException.USER_PROFILE_EXCEPTION.getException(),
							CustomExceptionCode.USER_PHONE_NUMBER_ALREADY_EXIST.getErrMsg(),
							CustomExceptionCode.USER_PHONE_NUMBER_ALREADY_EXIST.getErrCode());
				} else {
					phoneVerifiedFlag = true;
				}
			}
			// to maintain history
			UserHistory history = new UserHistory();
			modelMapper.map(user, history);
			history.setId(null);
			history.setUserId(user.getId());
			if (userVM.isActiveFlag() != user.isActiveFlag()) {
				if (userVM.isActiveFlag()) {
					user.setActiveFromDate(new Date());
				} else {
					user.setActiveTillDate(new Date());
				}
			}
			user.setId(userVM.getId());
			user.setFirstName(userVM.getFirstName());
			user.setLastName(userVM.getLastName());
			user.setMiddleName(userVM.getMiddleName());
			user.setActiveFlag(userVM.isActiveFlag());
			user.setAccountLocked(userVM.isAccountLocked());
			user.setLoginEnabled(userVM.isLoginEnabled());
			user.setIsZoyloEmployee(userVM.getIsZoyloEmployee());
			user.getEmailInfo().setEmailAddress(userVM.getEmailInfo().getEmailAddress());
			user.getPhoneInfo().setPhoneNumber(userVM.getPhoneInfo().getPhoneNumber());
			if (phoneVerifiedFlag) {
				user.getPhoneInfo().setVerifiedFlag(FALSEFLAG);
				user.getPhoneInfo().setVerifiedOn(new Date());
			}
			if (emailVerifiedFlag) {
				user.getEmailInfo().setVerifiedFlag(FALSEFLAG);
				user.getEmailInfo().setVerifiedOn(new Date());
			}
			if (Optional.ofNullable(user).isPresent()) {

				User userUpdatedData = userRepository.save(user);
				// to save history
				auditLogService.auditActionLog("update", "zoyloUser");
				customRepository.save(history, "zoyloUserHistory");
				return modelMapper.map(userUpdatedData, UserVM.class);
			} else {
				logger.error(CustomExceptionCode.USER_UPDATE_ERROR.getErrMsg());
				throw new CustomParameterizedException(RegisteredException.USER_PROFILE_EXCEPTION.getException(),
						CustomExceptionCode.USER_UPDATE_ERROR.getErrMsg(),
						CustomExceptionCode.USER_UPDATE_ERROR.getErrCode());
			}
		} catch (CustomParameterizedException exception) {
			logger.error(CustomExceptionCode.USER_UPDATE_ERROR.getErrMsg());
			throw exception;
		} catch (Exception exception) {
			logger.error(CustomExceptionCode.USER_UPDATE_ERROR.getErrMsg());
			throw new CustomParameterizedException(RegisteredException.USER_PROFILE_EXCEPTION.getException(),
					CustomExceptionCode.USER_UPDATE_ERROR.getErrMsg(), exception,
					CustomExceptionCode.USER_UPDATE_ERROR.getErrCode());

		}
	}
	
	/**
	 * @author Naga Raju
	 * @description get user data to add under an organization
	 */
	@Override
	public OrganizationUserDataVM fetchOrganizationUserData(String id) {
		try {
			OrganizationUserDataVM orgUserDataVM = new OrganizationUserDataVM();
			User user = userRepository.findOne(id);
			if (user != null) {
				if (user.getEmailInfo() != null) {
					orgUserDataVM.setEmailAddress(user.getEmailInfo().getEmailAddress());
				}
				if (user.getPhoneInfo() != null) {
					orgUserDataVM.setPhoneNumber(user.getPhoneInfo().getPhoneNumber());
				}
				orgUserDataVM.setFirstName(user.getFirstName());
				orgUserDataVM.setLastName(user.getLastName());
				orgUserDataVM.setMiddleName(user.getMiddleName());
				orgUserDataVM.setActiveFlag(user.isActiveFlag());
				orgUserDataVM.setActiveFromDate(user.getActiveFromDate());
				orgUserDataVM.setActiveTillDate(user.getActiveTillDate());
				orgUserDataVM.setUserId(user.getId());
				orgUserDataVM.setZoyloId(user.getZoyloId());
			}
			return orgUserDataVM;
		} catch (CustomParameterizedException customParameterizedException) {
			throw customParameterizedException;
		} catch (Exception exception) {
			logger.error(CustomExceptionCode.USER_DATA_FETCH_EXCEPTION.getErrMsg());
			throw new CustomParameterizedException(RegisteredException.USER_FETCH_EXCEPTION.getException(),
					CustomExceptionCode.USER_DATA_FETCH_EXCEPTION.getErrMsg(), exception,
					CustomExceptionCode.USER_DATA_FETCH_EXCEPTION.getErrCode());
		}
	}

	/**
	 * @author damini.arora
	 * @description to get all users by email, phone number
	 * @param value
	 * @return DoctorMV
	 */
	@Override
	public UserSuggestionMV autoSuggestion(String value) {
		try {
		if (value.contains("+91")) {
			value = value.replace("+91", "");
		}
		Set<String> result = new HashSet<>();
		//For UserName	
		List<User> userName=userRepository.findByUserName(value);
		
		if(!userName.isEmpty()&&userName != null)
		{
			for(User user:userName)
			{
				result.add(user.getUserName());
			}
		}
		//For Email
		List<User> email=userRepository.findByEmail(value);
		
		if(!email.isEmpty() && email!=null)
		{
			for(User user:email)
			{
				result.add(user.getEmailInfo().getEmailAddress());
			}
		}
		//For PhoneNumber
		
		List<User> phone=userRepository.findByPhone(value);
		
		if(!phone.isEmpty() && phone!=null)
		{
			for(User user:phone)
			{
				result.add(user.getPhoneInfo().getPhoneNumber());
			}
		}
		//For Name
        List<User> name=userRepository.findByName(value);
		
		if(!name.isEmpty() && name!=null)
		{
			for(User user:name)
			{
				if(user.getFirstName()!=null)
				result.add(user.getFirstName());
				
				if(user.getMiddleName()!=null)
				result.add(user.getMiddleName());
				
				if(user.getLastName()!=null)
				result.add(user.getLastName());
			}
		}
		
		UserSuggestionMV userResult= new UserSuggestionMV();
		userResult.setResult(result);
		return userResult;
		} catch (Exception exception) {
			logger.error(CustomExceptionCode.USER_NOT_FOUND.getErrMsg());
			throw new CustomParameterizedException(RegisteredException.USER_FETCH_EXCEPTION.getException(),
					CustomExceptionCode.USER_NOT_FOUND.getErrMsg(), exception,
					CustomExceptionCode.USER_NOT_FOUND.getErrCode());
		}
	}

	/**
	 * @author nagaraju
	 * @description Create User
	 * @param zoyloUserVM, input data to create user
	 * @return UserVM
	 */
	@Override
	public UserVM createUser(ZoyloUserVM zoyloUserVM) {
		try {
			if (zoyloUserVM.getEmailId() != null && !zoyloUserVM.getEmailId().isEmpty()) {
				User userEmailData = userRepository.findByEmailAddressAndDeleteFlag(zoyloUserVM.getEmailId(), false);
				if (userEmailData != null) {
					throw new CustomParameterizedException(RegisteredException.USER_CREATION_EXCEPTION.getException(),
							CustomExceptionCode.EMAIL_EXISTS.getErrMsg(),
							CustomExceptionCode.EMAIL_EXISTS.getErrCode());
				}
			}
			if (zoyloUserVM.getMobileNumber() != null && !zoyloUserVM.getMobileNumber().isEmpty()) {
				User userMobile = userRepository.findByPhoneNumberAndDeleteFlag(zoyloUserVM.getMobileNumber(), false);
				if (!zoyloUserVM.getMobileNumber().startsWith(Constants.INDIA_MOBILE_CODE)) {
					zoyloUserVM.setMobileNumber(Constants.INDIA_MOBILE_CODE + zoyloUserVM.getMobileNumber());
				}
				if (userMobile != null) {
					throw new CustomParameterizedException(RegisteredException.USER_CREATION_EXCEPTION.getException(),
							CustomExceptionCode.MOBILE_NUMBER_EXISTS.getErrMsg(),
							CustomExceptionCode.MOBILE_NUMBER_EXISTS.getErrCode());
				}
			}
			if(zoyloUserVM.getFirstName()== null || zoyloUserVM.getFirstName().length()==0){
				throw new CustomParameterizedException(RegisteredException.USER_CREATION_EXCEPTION.getException(),
						CustomExceptionCode.USER_FIRST_NAME_MANDATORY.getErrMsg(),
						CustomExceptionCode.USER_FIRST_NAME_MANDATORY.getErrCode());
			}
			if(zoyloUserVM.getLastName()== null || zoyloUserVM.getLastName().length()==0){
				throw new CustomParameterizedException(RegisteredException.USER_CREATION_EXCEPTION.getException(),
						CustomExceptionCode.USER_LAST_NAME_MANDATORY.getErrMsg(),
						CustomExceptionCode.USER_LAST_NAME_MANDATORY.getErrCode());
			}
			User user = new User();
			user.setFirstName(zoyloUserVM.getFirstName());
			user.setLastName(zoyloUserVM.getLastName());
			user.setMiddleName(zoyloUserVM.getMiddleName());
			user.setUserName(zoyloUserVM.getUserName());
			String encryptedPassword = passwordEncoder
					.encode(PasswordEncryption.getEncryptedPassword(zoyloUserVM.getEncryptedPassword()));
			user.setEncryptedPassword(encryptedPassword);
			EmailInfo emailInfo = new EmailInfo(zoyloUserVM.getEmailId());
			user.setEmailInfo(emailInfo);
			PhoneInfo phoneInfo = new PhoneInfo(zoyloUserVM.getMobileNumber());
			user.setPhoneInfo(phoneInfo);
			user.setZoyloId(UUID.randomUUID().toString());
			user.setActiveFlag(zoyloUserVM.isActiveFlag());
			if(zoyloUserVM.isActiveFlag()){
				user.setActiveFromDate(new Date());
			}else{
				user.setActiveTillDate(new Date());
			}
			user.setLoginEnabled(zoyloUserVM.isLoginEnabled());
			user.setAccountLocked(zoyloUserVM.isAccountLocked());
			user.setIsZoyloEmployee(zoyloUserVM.getIsZoyloEmployee());
			user = userRepository.save(user);

			// populate user profile
			UserProfile userprof = new UserProfile();
			userprof.setUserId(user.getId());
			userprof.setFirstName(user.getFirstName());
			userprof.setMiddleName(user.getMiddleName());
			userprof.setLastName(user.getLastName());
			userprof.setZoyloId(user.getZoyloId());

			DefaultAuthCategoryInfo defaultAuthCategoryInfo = new DefaultAuthCategoryInfo();
			ZoyloAuthCategory zoyloAuthCategory = authCategoryRepository.findByAuthCategoryCode(DEFAULT_AUTH_CATEGORY_CODE);
			defaultAuthCategoryInfo.setAuthCategoryCode(zoyloAuthCategory.getAuthCategoryCode());
			defaultAuthCategoryInfo.setAuthCategoryId(zoyloAuthCategory.getId());
			defaultAuthCategoryInfo.setAuthCategoryLandingPageUrl(zoyloAuthCategory.getAuthCategoryLandingPageUrl());

			List<AuthCategoryData> authCategoryDataList = zoyloAuthCategory.getAuthCategoryData();
			AuthCategoryData authCat = new AuthCategoryData();

			for(AuthCategoryData authCategoryDataObj : authCategoryDataList){
				if(authCategoryDataObj.getLanguageCode().equalsIgnoreCase(DEFAULT_LANGUAGE_CODE)){
					authCat.setLanguageCode(authCategoryDataObj.getLanguageCode());
					authCat.setLanguageId(authCategoryDataObj.getLanguageId());
					authCat.setAuthCategoryName(authCategoryDataObj.getAuthCategoryName());
					break;
				}
			}
			List<AuthCategoryData> authCategoryDataListNew = new ArrayList<AuthCategoryData>();
			authCategoryDataListNew.add(authCat);
			defaultAuthCategoryInfo.setAuthCategoryData(authCategoryDataListNew);

			userprof.setDefaultAuthCategoryInfo(defaultAuthCategoryInfo);

			List<Permission> permissionsListFromDb = permissionRepository
					.findByAuthCategoryCode(LookUp.RECIPIENT.getLookUpValue());

			List<ZoyloGrantedPermissionsList> grantedPermissionsList = new ArrayList<ZoyloGrantedPermissionsList>();
			ZoyloGrantedPermissionsList grantedPermission = new ZoyloGrantedPermissionsList();

			grantedPermission.setRecipientId(user.getId());
			grantedPermission.setRecipientZoyloId(user.getZoyloId());

			List<ZoyloPermissionsList> permissionsList = new ArrayList<ZoyloPermissionsList>();
			for(Permission permission: permissionsListFromDb ){
				ZoyloPermissionsList zoyloPermissionsList = new ZoyloPermissionsList();
				zoyloPermissionsList.setPermissionId(permission.getId());
				zoyloPermissionsList.setPermissionCode(permission.getPermissionCode());

				List<PermissionData> permissionDataList = permission.getPermissionData();
				List<ZoyloPermissionData>  newPermissionDataList = new ArrayList<ZoyloPermissionData>();

				for(PermissionData permissionData: permissionDataList){
					ZoyloPermissionData permissionDataObj = new ZoyloPermissionData();
					permissionDataObj.setLanguageId(permissionData.getLanguageId());
					permissionDataObj.setLanguageCode(permissionData.getLanguageCode());
					permissionDataObj.setPermissionName(permissionData.getPermissionName());
					newPermissionDataList.add(permissionDataObj);
				}
				zoyloPermissionsList.setPermissionData(newPermissionDataList);
				permissionsList.add(zoyloPermissionsList);

				grantedPermission.setPermissionsList(permissionsList);
				grantedPermission.setActiveFlag(permission.isActiveFlag());
				grantedPermission.setActiveFromDate(permission.getActiveFromDate());
				grantedPermission.setActiveTillDate(permission.getActiveTillDate());
			}
			grantedPermissionsList.add(grantedPermission);
			userprof.setGrantedPermissionsList(grantedPermissionsList);
			try {
				recipientRestClient.createUserProfile(userprof);
			} catch (Exception exception) {
				throw new CustomParameterizedException(RegisteredExceptions.REGISTRATION_EXCEPTION.getException(),
						CustomExceptionCode.FAIL_TO_CREATE_USER_PROFILE.getErrMsg(), exception,
						CustomExceptionCode.FAIL_TO_CREATE_USER_PROFILE.getErrCode());
			}
			return modelMapper.map(user, UserVM.class);
		} catch (CustomParameterizedException customParameterizedException) {
			throw customParameterizedException;
		} catch (Exception exception) {
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.REGISTERATION_FAIL.getErrMsg(), exception,
					CustomExceptionCode.REGISTERATION_FAIL.getErrCode());
		}
	}

	/**
	 * @author nagaraju
	 * @description update User
	 * @param zoyloUserVM, input data to update user
	 * @return UserVM
	 */
	@Override
	public UserVM updateUser(ZoyloUserVM zoyloUserVM) {
		try {
			modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
			User user = userRepository.findOne(zoyloUserVM.getId());
			if (user == null) {
				logger.error(CustomExceptionCode.USER_UPDATE_ERROR.getErrMsg());
				throw new CustomParameterizedException(RegisteredException.USER_PROFILE_EXCEPTION.getException(),
						CustomExceptionCode.USER_UPDATE_ERROR.getErrMsg(),
						CustomExceptionCode.USER_UPDATE_ERROR.getErrCode());
			}
			if (!zoyloUserVM.getEmailId().trim()
					.equalsIgnoreCase(user.getEmailInfo().getEmailAddress().trim())) {
				User userData = userRepository
						.findByEmailInfoEmailAddressAndDeleteFlag(zoyloUserVM.getEmailId(), FALSEFLAG);
				if (userData != null) {
					logger.error(CustomExceptionCode.USER_EMAIL_ALREADY_EXIST.getErrMsg());
					throw new CustomParameterizedException(RegisteredException.USER_PROFILE_EXCEPTION.getException(),
							CustomExceptionCode.USER_EMAIL_ALREADY_EXIST.getErrMsg(),
							CustomExceptionCode.USER_EMAIL_ALREADY_EXIST.getErrCode());
				}
			}
			if (!zoyloUserVM.getMobileNumber().trim()
					.equalsIgnoreCase(user.getPhoneInfo().getPhoneNumber().trim())) {
				User userPhoneNumber = userRepository
						.findByPhoneInfoPhoneNumberAndDeleteFlag(zoyloUserVM.getMobileNumber(), FALSEFLAG);
				if (userPhoneNumber != null) {
					logger.error(CustomExceptionCode.USER_PHONE_NUMBER_ALREADY_EXIST.getErrMsg());
					throw new CustomParameterizedException(RegisteredException.USER_PROFILE_EXCEPTION.getException(),
							CustomExceptionCode.USER_PHONE_NUMBER_ALREADY_EXIST.getErrMsg(),
							CustomExceptionCode.USER_PHONE_NUMBER_ALREADY_EXIST.getErrCode());
				}
			}
			// to maintain history
			UserHistory history = new UserHistory();
			modelMapper.map(user, history);
			history.setId(null);
			history.setUserId(user.getId());
			if (zoyloUserVM.isActiveFlag() != user.isActiveFlag()) {
				if (zoyloUserVM.isActiveFlag()) {
					user.setActiveFromDate(new Date());
				} else {
					user.setActiveTillDate(new Date());
				}
			}
			user.setId(zoyloUserVM.getId());
			user.setFirstName(zoyloUserVM.getFirstName());
			user.setLastName(zoyloUserVM.getLastName());
			user.setMiddleName(zoyloUserVM.getMiddleName());
			user.setActiveFlag(zoyloUserVM.isActiveFlag());
			user.setAccountLocked(zoyloUserVM.isAccountLocked());
			user.setLoginEnabled(zoyloUserVM.isLoginEnabled());
			user.setIsZoyloEmployee(zoyloUserVM.getIsZoyloEmployee());
			user.getEmailInfo().setEmailAddress(zoyloUserVM.getEmailId());
			user.getPhoneInfo().setPhoneNumber(zoyloUserVM.getMobileNumber());

			if (zoyloUserVM.getEncryptedPassword() != null) {
				String encryptedPassword = passwordEncoder
						.encode(PasswordEncryption.getEncryptedPassword(zoyloUserVM.getEncryptedPassword()));
				user.setEncryptedPassword(encryptedPassword);
			}

			ZoyloUserUpdateVM userupdatevm = new ZoyloUserUpdateVM();
			userupdatevm.setId(user.getId());
			userupdatevm.setFirstName(user.getFirstName());
			userupdatevm.setLastName(user.getLastName());
			userupdatevm.setMiddleName(user.getMiddleName());
			userupdatevm.setZoyloId(user.getZoyloId());

			if (Optional.ofNullable(user).isPresent()) {
				User userUpdatedData = userRepository.save(user);
				// to save history
				auditLogService.auditActionLog("update", "zoyloUser");
				customRepository.save(history, "zoyloUserHistory");
				try {
					recipientRestClient.updateUserProfile(userupdatevm);
				} catch (Exception exception) {
					throw new CustomParameterizedException(RegisteredExceptions.USER_PROFILE_EXCEPTION.getException(),
							CustomExceptionCode.FAIL_TO_UPDATE_USER_PROFILE.getErrMsg(), exception,
							CustomExceptionCode.FAIL_TO_UPDATE_USER_PROFILE.getErrCode());
				}
				return modelMapper.map(userUpdatedData, UserVM.class);
			} else {
				logger.error(CustomExceptionCode.USER_UPDATE_ERROR.getErrMsg());
				throw new CustomParameterizedException(RegisteredException.USER_PROFILE_EXCEPTION.getException(),
						CustomExceptionCode.USER_UPDATE_ERROR.getErrMsg(),
						CustomExceptionCode.USER_UPDATE_ERROR.getErrCode());
			}
		} catch (CustomParameterizedException exception) {
			logger.error(CustomExceptionCode.USER_UPDATE_ERROR.getErrMsg());
			throw exception;
		} catch (Exception exception) {
			logger.error(CustomExceptionCode.USER_UPDATE_ERROR.getErrMsg());
			throw new CustomParameterizedException(RegisteredException.USER_PROFILE_EXCEPTION.getException(),
					CustomExceptionCode.USER_UPDATE_ERROR.getErrMsg(), exception,
					CustomExceptionCode.USER_UPDATE_ERROR.getErrCode());
		}
	}

	/**
	 * @author nagaraju
	 * @description search users
	 * @param activeFlag, status of user
	 * @param isZoyloEmployee, flag to denote if the user being searched is a zoylo employee
	 * @param pageable
	 * @return PageableVM<ZoyloUserVM>
	 */
	@Override
	public PageableVM<ZoyloUserVM> search(Pageable pageable, boolean activeFlag, boolean isZoyloEmployee,
			String firstName, String lastName, String emailId, String mobileNumber) {
		try {
			modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
			List<User> usersList = new ArrayList<User>();
			
			Page<User> users = customUserRepository.searchUser(pageable, activeFlag, isZoyloEmployee, firstName, lastName, emailId, mobileNumber);
//			Page<User> users = userRepository.findByActiveFlagAndIsZoyloEmployee(pageable, activeFlag, isZoyloEmployee);
			if (users != null) {
				usersList = users.getContent();
			}
			List<ZoyloUserVM> newUsersList = new ArrayList<ZoyloUserVM>();
			for (User user : usersList) {
				ZoyloUserVM mappedObj = modelMapper.map(user, ZoyloUserVM.class);
				mappedObj.setMobileNumber(user.getPhoneInfo() != null ? user.getPhoneInfo().getPhoneNumber(): null);
				mappedObj.setEmailId(user.getEmailInfo() != null ? user.getEmailInfo().getEmailAddress(): null);
				newUsersList.add(mappedObj);
			}
			PageableVM<ZoyloUserVM> pageableVM = new PageableVM<>();
			if (!newUsersList.isEmpty()) {
				pageableVM.setContent(newUsersList);
			} else {
				pageableVM.setContent(new ArrayList<>());
			}
			PageInfoVM infoVM = modelMapper.map(users, PageInfoVM.class);
			pageableVM.setPageInfo(infoVM);
			return pageableVM;
		} catch (Exception exception) {
			logger.error(CustomExceptionCode.USER_SEARCH_FAILED.getErrMsg());
			throw exception;
		}
	}
	
	/**
	 * Fetch user details by {@link User#getId()}
	 * @param id should match a id in {@link User} collection
	 * @throws CustomParameterizedException
	 */
	public ZoyloUserVM fetchUserRoleDetail (String id) {
		// fetch user details with id;
	  modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
	  User user = userRepository.findOneById(id);
	  if (user == null) {
		logger.error(CustomExceptionCode.USER_UPDATE_ERROR.getErrMsg());
		throw new CustomParameterizedException(RegisteredException.USER_PROFILE_EXCEPTION.getException(),
				CustomExceptionCode.USER_UPDATE_ERROR.getErrMsg(),
				CustomExceptionCode.USER_UPDATE_ERROR.getErrCode());
	  }
	  ZoyloUserAuth userAuth = userAuthRepository.findByUserId(id);
		
	  ZoyloUserVM zoyloUser = modelMapper.map(user, ZoyloUserVM.class);
	  zoyloUser.setMobileNumber(user.getPhoneInfo() != null ? user.getPhoneInfo().getPhoneNumber(): null);
	  zoyloUser.setEmailId(user.getEmailInfo() != null ? user.getEmailInfo().getEmailAddress(): null);
	  if (userAuth != null){
	    zoyloUser.setUserRole(convertZoyloUserAuthToUserRoleDataVM(userAuth));
	  }  
	  return zoyloUser;
	}
	
	public List<User> fetchUserList (Boolean activeFlag, Boolean zoyloEmployee){
		  List<User> userList = customUserRepository.fetchUserList(zoyloEmployee, activeFlag);

		return userList;
	}
	
	/**
	 * Flatten zoyloUserAuth to Flat list of List<UserRoleDataVM>
	 * @param userAuth
	 * @return
	 */
	
	private List<UserRoleDataVM> convertZoyloUserAuthToUserRoleDataVM (ZoyloUserAuth userAuth) {
		
		List<ServiceProvidersList> providerList = userAuth.getServiceProvidersList();
		if (providerList == null || providerList.size()<=0){
			return new ArrayList<UserRoleDataVM>();
		}
		final List<GrantedRolesList> grantedRolesList = providerList.stream()
                .map(ServiceProvidersList::getGrantedRolesList)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
		List <UserRoleDataVM> userRoleDataVM = grantedRolesList.stream().map(userRole).collect(Collectors.toList());
		return userRoleDataVM;
	}
	
	/**
	 * Mapping GrantedRolesList to UserRoleDataVM
	 * Caution : Use this mapper only for Admin Roles
	 */
	
	Function <GrantedRolesList , UserRoleDataVM> userRole = new Function <GrantedRolesList , UserRoleDataVM>(){

		@Override
		public UserRoleDataVM apply(GrantedRolesList grantedRolesList) {
			UserRoleDataVM userRole = new UserRoleDataVM();
			Optional<GrantedRoleData> roleData = grantedRolesList.getRoleData().stream().filter(lroleData -> lroleData.getLanguageCode().equals(DEFAULT_LANGUAGE_CODE)).findFirst();
			userRole.setRoleCode(grantedRolesList.getRoleCode());
			if (roleData.isPresent()) {
			  userRole.setRoleName(roleData.get().getRoleName());
			}
			userRole.setActiveFlag(grantedRolesList.isActiveFlag());
			return userRole;
		}
		
	};

	/**
	 * @author Devendra.Kumar
	 */
	@Override
	public List<UserAutoSuggestionMV> getIdAndName(String value) {
		try {
			// For UserName
			List<User> users = userRepository.findByUserNameStartsWithIgnoreCaseAndDeleteFlag(value.trim(),
					Boolean.FALSE);

			List<UserAutoSuggestionMV> autosuggestionMVs = new ArrayList<>();
			for (User user : users) {
				UserAutoSuggestionMV autosuggestionMV = new UserAutoSuggestionMV();
				autosuggestionMV.setId(user.getId());
				autosuggestionMV.setFullName(user.getUserName());
				autosuggestionMV.setFirstName(user.getFirstName());
				autosuggestionMV.setLastName(user.getLastName());
				autosuggestionMV.setMiddleName(user.getMiddleName());
				autosuggestionMV.setZoyloId(user.getZoyloId());
				autosuggestionMV.setPhoneNumber(user.getPhoneInfo().getPhoneNumber());
				autosuggestionMV.setEmail(user.getEmailInfo().getEmailAddress());
				autosuggestionMVs.add(autosuggestionMV);
			}

			users = userRepository.findByPhoneInfoPhoneNumberStartsWithAndDeleteFlag(value.trim(), Boolean.FALSE);
			for (User user : users) {
				UserAutoSuggestionMV autosuggestionMV = new UserAutoSuggestionMV();
				autosuggestionMV.setId(user.getId());
				autosuggestionMV.setFullName(user.getUserName());
				autosuggestionMV.setFirstName(user.getFirstName());
				autosuggestionMV.setLastName(user.getLastName());
				autosuggestionMV.setMiddleName(user.getMiddleName());
				autosuggestionMV.setZoyloId(user.getZoyloId());
				autosuggestionMV.setPhoneNumber(user.getPhoneInfo().getPhoneNumber());
				autosuggestionMV.setEmail(user.getEmailInfo().getEmailAddress());
				autosuggestionMVs.add(autosuggestionMV);
			}

			return autosuggestionMVs;
		} catch (Exception exception) {
			logger.error(CustomExceptionCode.USER_NOT_FOUND.getErrMsg());
			throw new CustomParameterizedException(RegisteredException.USER_FETCH_EXCEPTION.getException(),
					CustomExceptionCode.USER_NOT_FOUND.getErrMsg(), exception,
					CustomExceptionCode.USER_NOT_FOUND.getErrCode());
		}
	}

	/**
	 * @author damini.arora
	 * @description to get User by phone number
	 * @param phoneNumber
	 * @return User
	 */
	@Override
	public User getUserByPhoneNumber(String phoneNumber) {
		try {
			User user = new User();
			String number = Constants.INDIA_MOBILE_CODE + phoneNumber;
			user = userRepository.findOneByPhoneInfoPhoneNumber(phoneNumber, number);
			if (user != null) {
				return user;
			} else {
				return new User();
			}
		} catch (Exception exception) {
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.USER_NOT_FOUND.getErrMsg(), exception,
					CustomExceptionCode.USER_NOT_FOUND.getErrCode());
		}
	}
	
	/**
	 * @author damini.arora
	 * @description to get User by email id
	 * @param emailId
	 * @return User
	 */
	@Override
	public User getUserByEmail(String email) {
		try {
			User user = userRepository.findOneByEmailInfoEmailAddress(email);
			if (user != null) {
				return user;
			} else {
				return null;
			}

		} catch (Exception exception) {
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.USER_NOT_FOUND.getErrMsg(), exception,
					CustomExceptionCode.USER_NOT_FOUND.getErrCode());
		}
	}
	
	
	/* (non-Javadoc)
	 * @see com.zoylo.gateway.bll.UserBll#verifyEmail(java.lang.String, java.lang.String)
	 */
	@Override
	public Boolean verifyEmailToken(String verifyToken, String emailAddress) {

		Optional<User> userOp = userRepository.findByEmailAddress(emailAddress);
		if (userOp.isPresent()) {
			logger.info("User found by email : {}", emailAddress);

			User user = userOp.get();
			EmailInfo emailInfo = user.getEmailInfo();
			Date currDate = new Date();
			
			if (emailInfo == null){
				logger.info("Email Info does not exist with email id : {} ", emailAddress);
				throw new CustomParameterizedException(RegisteredException.EMAIL_VERIFICATION_EXCEPTION.getException(),
						CustomExceptionCode.EMAIL_NOT_EXISTS.getErrMsg(),
						CustomExceptionCode.EMAIL_NOT_EXISTS.getErrCode());
			}

			if (emailInfo.isVerifiedFlag()) {
				logger.error("Email is already verified" + emailAddress);
				throw new CustomParameterizedException(RegisteredException.EMAIL_VERIFICATION_EXCEPTION.getException(),
						CustomExceptionCode.EMAIL_ALREADY_VERIFIED.getErrMsg(),
						CustomExceptionCode.EMAIL_ALREADY_VERIFIED.getErrCode());
			}

			if (currDate.after(emailInfo.getTokenExpiresAt())) {
				logger.error("Email token has expired" + emailAddress);
				throw new CustomParameterizedException(RegisteredException.EMAIL_VERIFICATION_EXCEPTION.getException(),
						CustomExceptionCode.EMAIL_TOKEN_EXPIRED.getErrMsg(),
						CustomExceptionCode.EMAIL_TOKEN_EXPIRED.getErrCode());
			}

			if (verifyToken.equals(emailInfo.getVerificationToken())
					&& currDate.before(emailInfo.getTokenExpiresAt())) {
				
				emailInfo.setVerificationToken(null);
				emailInfo.setTokenExpiresAt(null);
				emailInfo.setTokenSentAt(null);
				emailInfo.setVerifiedFlag(true);
				emailInfo.setVerifiedOn(new Date());
				user.setEmailInfo(emailInfo);
				userRepository.save(user);
				
				return true;
				
			} else {
				return false;
			}
		} else {
 			logger.info("User does not exist with id : {} ", emailAddress);
			throw new CustomParameterizedException(RegisteredException.EMAIL_VERIFICATION_EXCEPTION.getException(),
					CustomExceptionCode.EMAIL_NOT_EXISTS.getErrMsg(),
					CustomExceptionCode.EMAIL_NOT_EXISTS.getErrCode());
		}
	}
	/**
	 * @throws IOException 
	 * @throws TemplateException 
	 * 
	 */
	@Override
	public void generateEmailToken(String emailAddress) {

		Optional<User> userOp = userRepository.findByEmailAddress(emailAddress);
		if (userOp.isPresent()) {
			logger.info("User found by email : {}", emailAddress);

			User user = userOp.get();
			EmailInfo emailInfo = user.getEmailInfo();
			
			if (emailInfo == null){
				logger.info("Email Info does not exist with email id : {} ", emailAddress);
				throw new CustomParameterizedException(RegisteredException.EMAIL_VERIFICATION_EXCEPTION.getException(),
						CustomExceptionCode.EMAIL_NOT_EXISTS.getErrMsg(),
						CustomExceptionCode.EMAIL_NOT_EXISTS.getErrCode());
			}
	
			if (emailInfo.isVerifiedFlag()) {
				logger.error("Email is already verified" + emailAddress);
				throw new CustomParameterizedException(RegisteredException.EMAIL_VERIFICATION_EXCEPTION.getException(),
						CustomExceptionCode.EMAIL_ALREADY_VERIFIED.getErrMsg(),
						CustomExceptionCode.EMAIL_ALREADY_VERIFIED.getErrCode());
			}
			
			user= populateEmailInfoUserForValidation(user, emailAddress);
			EmailSEService emailSEService = new EmailSEService();
			EmailTemplate emailTemplate = new EmailTemplate();
			emailTemplate.setAccessKey(accesskey);
			emailTemplate.setSecretKey(key);
			emailTemplate.setEmailFrom(emailFrom);
			emailTemplate.setSendTo(user.getEmailInfo().getEmailAddress());
			emailTemplate.setSubject("Zoylo.com | Email Verification");
			String verificationLink = getVerificationLink(user.getEmailInfo().getVerificationToken(), user.getEmailInfo().getEmailAddress());
			try {
				emailTemplate.setBody(emailBll.verificationLinkTemplate(user.getFirstName(), verificationLink));
			} catch (Exception e) {
				logger.error("EmailTemplate exception:" + e);
				throw new CustomParameterizedException(RegisteredException.EMAIL_TEMPLATE_EXCEPTION.getException(),
						CustomExceptionCode.EXCEPTION_OCCOR_IN_EMAIL_TEMPLTE.getErrMsg(),
						CustomExceptionCode.EXCEPTION_OCCOR_IN_EMAIL_TEMPLTE.getErrCode());
			}
			String messageId = null;
			if(emailNotificationAllowed) {
			  SendEmailResult sendEmailResult = emailSEService.sendEmail(emailTemplate);
			  messageId = sendEmailResult.getMessageId();
			}
			
			populateZoyloNotification(emailTemplate, user, messageId);
			userRepository.save(user);

		} else {
			logger.info("User does not exist with id : {} ", emailAddress);
			throw new CustomParameterizedException(RegisteredException.EMAIL_VERIFICATION_EXCEPTION.getException(),
					CustomExceptionCode.EMAIL_NOT_EXISTS.getErrMsg(),
					CustomExceptionCode.EMAIL_NOT_EXISTS.getErrCode());
		}
	}	
	public String verificationEmailBody(String name, String link){
		String body="<h1 style=\"font-size:1.1em;margin-top:0px;\">Dear "+name+",</h1>" + 
		"\n Welcome to Zoylo - A revolutionary map based online healthcare platform in India.\n"
		+ "Experience better healthcare services at your fingertips using your Zoylo account.\n<br/>"
		+ "\n\t <br/>Please use the below link"  + "to validate your account.\n<br/>"		
		+ link;
		return body;
	}
	
	/**
	 * 
	 * @param emailTemplate - {@link EmailTemplate} defined to hold email information
	 * @param user - User information from {@link ZoyloUser}
	 * @param messageId - Represent the message from AWS
	 */
	private void populateZoyloNotification(EmailTemplate emailTemplate, User user, String messageId){
		try {
			ZoyloNotificationVM zoyloNotificationVM = new ZoyloNotificationVM();
			RecipientInfo recipientInfo = new RecipientInfo();
			MessageInfo messageInfo = new MessageInfo();

			recipientInfo.setRecipientFirstName(user.getFirstName());
			recipientInfo.setRecipientLastName(user.getLastName());
			recipientInfo.setRecipientZoyloId(user.getZoyloId());
			recipientInfo.setRecipientId(user.getId());
			recipientInfo.setRecipientEmail(emailTemplate.getSendTo());

			messageInfo.setMessageText(emailTemplate.getBody());

			List<DeliveryTrail> deliveryTrail = new ArrayList<>();
			DeliveryTrail trail = new DeliveryTrail();
			trail.setDeliveryAttemptedAt(new Date());
			trail.setDeliveryStatusCode(STATUS_CODE);
			deliveryTrail.add(trail);
			zoyloNotificationVM.setNotificationMessageId(messageId);
			zoyloNotificationVM.setNotificationModeCode(EMAIL_MODE_CODE);
			zoyloNotificationVM.setNotificationStatusCode(STATUS_CODE);
			zoyloNotificationVM.setRecipientInfo(recipientInfo);
			zoyloNotificationVM.setRetryCounter(1);
			zoyloNotificationVM.setMessageInfo(messageInfo);
			zoyloNotificationVM.setDeliveryTrail(deliveryTrail);

			adminRestClient.save(zoyloNotificationVM);
			logger.info("saved email notification");
		} catch (Exception exception) {
			logger.info("unable to save notification email");
		}
	}

	@Override
	public PMSUserRoleMV getPracticeLocationRole(String serviceProviderId,String userId) {
		
		try {
			PMSUserRoleMV pMSUserRoleMV =new PMSUserRoleMV();
			List<ZoyloAuthVM> zoyloAuthVMs = zoyloUserAuthRepository
					.findAuthByProviderId(userId, serviceProviderId);
			if (zoyloAuthVMs != null && zoyloAuthVMs.size() > 0) {
					pMSUserRoleMV.setUserId(userId);
					pMSUserRoleMV.setProviderId(serviceProviderId);
					pMSUserRoleMV
							.setRoleId(zoyloAuthVMs.get(0).getServiceProvidersList().getGrantedRolesList().get(0).getRoleId());
					pMSUserRoleMV.setRoleCode(
							zoyloAuthVMs.get(0).getServiceProvidersList().getGrantedRolesList().get(0).getRoleCode());
					pMSUserRoleMV.setGrantedRoleData(
							zoyloAuthVMs.get(0).getServiceProvidersList().getGrantedRolesList().get(0).getRoleData());
			}
			
		return pMSUserRoleMV;
		} catch (CustomParameterizedException e) {
			throw e;
		} catch (Exception e) {
			throw new CustomParameterizedException(RegisteredException.USER_AUTH_EXCEPTION.getException(),
					CustomExceptionCode.USER_NOT_FOUND.getErrMsg(), CustomExceptionCode.USER_NOT_FOUND.getErrCode());
		}
		
	}
	
	
	
	/**
	 * @author Devendra.Kumar
	 * @description To get Role
	 * @param serviceProviderId
	 * @param userId
	 */
	@Override
	public List<AuthVM> getUserRoleByProviderId(String serviceProviderId, List<String> userIds) {
		try {
			List<AuthVM> zoyloAuthVMList = new ArrayList<>();
			for (String userId : userIds) {
				List<ZoyloAuthVM> zoyloAuthVMs = zoyloUserAuthRepository.findAuthByProviderId(userId,
						serviceProviderId);
				if (zoyloAuthVMs != null && zoyloAuthVMs.size() > 0) {
					AuthVM authVM = new AuthVM();
					ZoyloAuthVM zoyloAuthVM = zoyloAuthVMs.get(0);
					authVM.setServiceProvidersList(zoyloAuthVM.getServiceProvidersList());
					authVM.setUserId(userId);
					zoyloAuthVMList.add(authVM);
				}
			}
			return zoyloAuthVMList;
		} catch (Exception exception) {
			logger.info("unable to get User Roles : "+exception);
		}
		return null;
	}
	
	/**
	 * @author Devendra.Kumar
	 * @description To update User role
	 */
	public ZoyloUserAuth updateRoles(String userId, String providerId, String roleCode) {
		try {
			ZoyloUserAuth userAuth = zoyloUserAuthRepository.findByUserId(userId);
			ZoyloUserAuth savedUserAuth = null;
			if (userAuth != null) {
				
				//maintain UserAuth History 
				UserAuthHistory history = new UserAuthHistory();
				modelMapper.map(userAuth, history);
				history.setId(null);
				history.setDocumentId(userAuth.getId());
				
				for (ServiceProvidersList serviceProvidersList : userAuth.getServiceProvidersList()) {
					if (serviceProvidersList.getProviderId().equals(providerId)) {
						List<GrantedRolesList> grantedRolesLists = serviceProvidersList.getGrantedRolesList();
						List<GrantedRolesList> newGrantedRolesLists = new ArrayList<>();
						if (grantedRolesLists != null && grantedRolesLists.size() > 0) {
							GrantedRolesList grantedRolesList = grantedRolesLists.get(0);
							if (!grantedRolesList.getRoleCode().equalsIgnoreCase(roleCode)) {
								Role role = roleRepository.findOneByRoleCode(roleCode);
								if (role != null) {
									grantedRolesList.setRoleId(role.getId());
									grantedRolesList.setRoleCode(role.getRoleCode());
									GrantedRoleData grantedRoleData[] = modelMapper.map(grantedRolesList.getRoleData(),
											GrantedRoleData[].class);
									grantedRolesList.setRoleData(Arrays.asList(grantedRoleData));
									role.getRoleData();
									grantedRolesList.setActiveFromDate(role.getActiveFromDate());
									grantedRolesList.setActiveTillDate(role.getActiveTillDate());
									grantedRolesList.setActiveFlag(role.isActiveFlag());
									grantedRolesList.setGrantedPermissionsList(role.getPermissionList());
									grantedRolesList.setSystemDefined(role.isSystemDefined());
									newGrantedRolesLists.add(grantedRolesList);
									savedUserAuth = zoyloUserAuthRepository.save(userAuth);
									
									auditLogService.auditActionLog("update", "zoyloUserAuth");
									customRepository.save(history, "zoyloUserAuthHistory");									
									break;
								}
							} else {
								// exception
								logger.info("User have already this role : "+roleCode);
							}
							// newGrantedRolesLists.add(grantedRolesList);
						}
						// serviceProvidersList.setGrantedRolesList(newGrantedRolesLists);
					}
				}
				return savedUserAuth;
			}

		} catch (Exception exception) {
			logger.error("unable to update User Roles : " + exception);
			throw new CustomParameterizedException(RegisteredException.USER_AUTH_EXCEPTION.getException(),
					CustomExceptionCode.USER_ROLE_UPDATE_EXCEPTION.getErrMsg(),
					CustomExceptionCode.USER_ROLE_UPDATE_EXCEPTION.getErrCode());
		}
		return null;
	}

	/**
	 * @author Devendra.Kumar
	 * @description Get Map<userPhone,userId> on the basis of given phoneNumbers
	 * @param phoneNumbers
	 * @return Map<String, String>
	 * @exception CustomParameterizedException
	 */
	@Override
	public Map<String, String> getUserIdByPhoneNumber(List<String> phoneNumbers) {
		try{
			List<User> users = userRepository.findByPhoneInfoPhoneNumberIn(phoneNumbers);
			Map<String, String> userIdPhoneMap = new HashMap<>(users.size());
			if (!users.isEmpty()) {
				users.stream().forEach(user -> {
					String phoneNumber = user.getPhoneInfo().getPhoneNumber();
					if(phoneNumber.contains("+91")){
						phoneNumber = phoneNumber.replace("+91", "");
					}
					userIdPhoneMap.put(phoneNumber, user.getId());
				});
			}
			return userIdPhoneMap;
		}catch(Exception exception){
			logger.error("Exception occured in gateway while getting userIds on the basis of user PhoneNumbers : {}", exception);
			throw new CustomParameterizedException(RegisteredException.USER_FETCH_EXCEPTION.getException(),
					CustomExceptionCode.USER_IDS_FETECH_EXCEPTION.getErrMsg(),
					CustomExceptionCode.USER_IDS_FETECH_EXCEPTION.getErrCode());
		}
	}
	
	/**
	 * @author arvind.rawat
	 * @description To create email template
	 */
	@Override
	public String emailCommonTemplate(String body) {
		StringBuffer email = new StringBuffer();
		email.append("<!doctype html>\n" + 
				"<html>\n" + 
				"<head>\n" + 
				"<meta charset=\"utf-8\">\n" + 
				"<title>Zoylo</title>\n" + 
				"<style>\n" + 
				"\n" + 
				"@media only screen and (max-width: 700px)  {\n" + 
				"          .device-width {width:600px!important; padding:0;\n" + 
				"	}  \n" + 
				"      }\n" + 
				"	  @media only screen and (max-width: 670px)  {\n" + 
				"          .device-width {width:560px!important; padding:0;\n" + 
				"		  }  \n" + 
				"		  \n" + 
				"      }\n" + 
				"	   @media only screen and (max-width: 630px)  {\n" + 
				"          .device-width {width:530px!important; padding:0;\n" + 
				"		   } \n" + 
				"      }\n" + 
				"      \n" + 
				"  @media only screen and (max-width: 599px) {\n" + 
				"          .device-width {width:420px!important; padding:0;\n" + 
				"		  font-size:12px;\n" + 
				"		  padding-left:0px !important;\n" + 
				"		  padding-right:0px !important;\n" + 
				"		 }\n" + 
				"	\n" + 
				"   }	   \n" + 
				"	   @media only screen and (max-width: 439px) {\n" + 
				"	 \n" + 
				"          .device-width {width:360px!important; padding:0;\n" + 
				"		  font-size:11px;\n" + 
				"		   padding-left:0px !important;\n" + 
				"		  padding-right:0px !important;\n" + 
				"		  }\n" + 
				"      }\n" + 
				"	  \n" + 
				"	   @media only screen and (max-width: 379px) {\n" + 
				"          .device-width {width:300px!important; padding:0;\n" + 
				"		  font-size:11px;\n" + 
				"		   padding-left:0px !important;\n" + 
				"		  padding-right:0px !important;\n" + 
				"		  }\n" + 
				"      }\n" + 
				"	  @media only screen and (max-width: 330px) {\n" + 
				"          .device-width {width:290px!important; padding:0;\n" + 
				"		  font-size:11px;\n" + 
				"		   padding-left:0px !important;\n" + 
				"		  padding-right:0px !important;\n" + 
				"		  }\n" + 
				"      }\n" + 
				"	  .image-responsive img{\n" + 
				"	width: 100%;\n" + 
				"    height: auto;\n" + 
				"	max-width:100%;\n" + 
				"	  } \n" + 
				"	  a{\n" + 
				"		  color:#000;\n" + 
				"	  }\n" + 
				"	 \n" + 
				"</style>\n" + 
				"\n" + 
				"\n" + 
				"</head>\n" + 
				"\n" + 
				"<body  width=\"620\">\n" + 
				"<table   class=\"device-width\"  width=\"615\" border=\"0\" cellpadding=\\\"0\\\" cellspacing=\"0\" style=\"font-family: Verdana, Geneva, sans-serif; font-size:12px;\">\n" + 
				"  <tbody>\n" + 
				"  \n" + 
				"    <tr>\n" + 
				"      <td  align=\"right\" class=\"device-width\" >\n" + 
				"   \n" + 
				"      <a href=\"https://www.zoylo.com/\"><img class=\"image-responsive\"  src=\"https://s3.ap-south-1.amazonaws.com/kellton-images/mailTemplate/logo.png\" alt=\"\"/></a>\n" + 
				"      \n" + 
				"        <p style=\"margin-top:5px;border-top:20px solid #8e5ba6;margin-bottom:0px; border-radius:25px 25px 0 0;\"></p>\n" + 
				"                 \n" + 
				"      </td>\n" + 
				"    </tr> \n" + 
				"    <tr border=\"\" style=\"border:1px solid #000;\">\n" + 
				"    <td><table class=\"device-width para-align\" width=\"615\" border=\"0\" cellpadding=\"5px\" style=\"font-family: Verdana, Geneva, sans-serif;margin-top:-17px; padding-left:20px; padding-right:20px; border:1px solid #ddd; border-top:0px; border-bottom-left-radius:25px;border-bottom-right-radius:25px;\">\n" + 
				"      <tbody>\n" + 
				"         <tr>\n" + 
				"           <td style=\"text-align:justify;padding-bottom:0px\">\n" + 
							body+
				"    \n" + 
				"    <p>"+
				"    </p>\n" + 
				"    \n" + 
				"    <p><a href=\"https://www.zoylo.com/\">CLICK HERE</a> to login, view and update your profile.<br>\n" + 
				"In case of any queries or assistance, please call us on our Toll Free Helpline 1800 3000 5454 or send an mail to <a href=\"mailto:contactus@zoylo.com\">contactus@zoylo.com</a>\n" + 
				"</p>\n" + 
				"    \n" + 
				" \n" + 
				"  <p style=\"margin-bottom:-10px;\">Thank you for choosing Zoylo as your health partner. </p>\n" + 
				"  \n" + 
				"           </td>\n" + 
				"         </tr>       \n" + 
				"        <tr>\n" + 
				"       \n" + 
				"        <td style=\"padding-top:0\">\n" + 
				"          <table cellpadding=\"0\" cellspacing=\"0\" class=\"device-width\" width=\"615\" border=\"0\" style=\"font-family: Verdana, Geneva, sans-serif;margin-top:-10px;\">" + 
				"            <tbody>" + 
				"              <tr>" + 
				"                <td width=\"447\"><p style=\"margin-bottom:-7px;\">Keep up the good health!</p>\n" + 
				"               <p> Team Zoylo  </p>               \n" + 
				"                \n" + 
				"                </td>\n" + 
				"              <td width=\"143\" style=\"padding:5px; padding-bottom:0px;float:right; margin-top:0px\">\n" + 
				"          <span style=\"padding-right:3px;\"><a href=\"https://www.facebook.com/zoylodigihealth/?ref=hl\" target=\"_blank\"><img src=\"https://s3.ap-south-1.amazonaws.com/kellton-images/mailTemplate/fb.png\" /></a></span>\n" + 
				"          <span style=\"padding-right:3px;\"><a href=\"https://plus.google.com/114900063212671186185\" target=\"_blank\"><img  src=\"https://s3.ap-south-1.amazonaws.com/kellton-images/mailTemplate/gplus.png\" /></a></span>\n" + 
				"          <span style=\"padding-right:3px;\"><a href=\"https://twitter.com/ZoyloDigiHealth\" target=\"_blank\"><img  src=\"https://s3.ap-south-1.amazonaws.com/kellton-images/mailTemplate/twiter.png\" /></a></span>\n" + 
				"          <span style=\"padding-right:3px;\"><a href=\"http://zoylodigihealth.blogspot.in/\" target=\"_blank\"><img  src=\"https://s3.ap-south-1.amazonaws.com/kellton-images/mailTemplate/blog.png\" /></a></span>\n" + 
				"          <span><a href=\"https://www.pinterest.com/zoylodigihealth/\" target=\"_blank\"><img src=\"https://s3.ap-south-1.amazonaws.com/kellton-images/mailTemplate/pintrest.png\" /></a></span>\n" + 
				"          <p style=\"color:#8e5ba6;text-align:center;margin-top:5px;\"><b><a href=\"http://www.zoylo.com/\" target=\"_blank\" style=\"text-decoration:none; color:#8e5ba6;\">www.zoylo.com</a></b></p>\n" + 
				"          \n" + 
				"          </td>\n" + 
				"              </tr>\n" + 
				"              <tr>\n" + 
				"    <td>\n" + 
				"    <p style=\"margin-top:-10px;margin-bottom:0px;\">Contact us: <span style=\"padding-right:10px; padding-left:10px;\"><img src=\"https://s3.ap-south-1.amazonaws.com/kellton-images/mailTemplate/phn.png\" alt=\"\" width=\"15\"/> 1800 3000 5454</span>\n" + 
				"     <span style=\"padding-left:10px;\"><img src=\"https://s3.ap-south-1.amazonaws.com/kellton-images/mailTemplate/mail.png\" alt=\"\" width=\"17\"/></span>&nbsp; <span><a href=\"mailto:contactus@zoylo.com\" style=\"color:#000;\">contactus@zoylo.com</a> </span>\n" + 
				"    </p>\n" + 
				"    </td>\n" + 
				"    \n" + 
				"    </tr>  \n" + 
				"            </tbody>\n" + 
				"          </table>  \n" + 
				"          </td>      \n" + 
				"        </tr>\n" + 
				"              \n" + 
				"      </tbody>\n" + 
				"    </table>\n" + 
				"     </td>\n" + 
				"    \n" + 
				"    </tr> \n" + 
				"    \n" + 
				"    <tr style=\"font-size:7px; text-align:justify; background-color:#fff; color:#000; padding:20px;\">\n" + 
				"     <td>\n" + 
				"     <p><b>Disclaimer:</b> <br>\n" + 
				"    This e-mail and any files transmitted with it are for the sole use of the intended recipient(s) and may contain confidential and privileged information. If you are not the intended recipient, please contact the sender by reply e-mail and destroy all copies and the original message. Any unauthorized review, use, disclosure, dissemination, forwarding, printing or copying of this email or any action taken in reliance on this e-mail is strictly prohibited and may be unlawful. The recipient acknowledges that Zoylo Digihealth Private Limited or its subsidiaries and associated companies are unable to exercise control or ensure or guarantee the integrity of/over the contents of the information contained in e-mail transmissions and further acknowledges that any views expressed in this message are those of the individual sender and no binding nature of the message shall be implied or assumed unless the sender does so expressly with due authority of Zoylo Digihealth Private Limited. Before opening any attachments please check them for viruses and defects. \n" + 
				"</p>\n" + 
				"</td>\n" + 
				"</tr>\n" + 
				"    \n" + 
				"  </tbody>\n" + 
				"</table>\n" + 
				"</body>\n" + 
				"</html>\n" + 
				"");
		return email.toString();	
	}

	/**
	 * @author Balram Sharma
	 * @description This api is used to populate all user details to ecommerce.
	 */
	@Override
	public String populateUserToEcomm(Date startDate, Date endDate) {

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		List<User> userList = userRepository.findByCreatedDateBetween(startDate, endDate);
		userList.parallelStream().forEach(user -> {
			EcommUserDetail ecommUserDetail = new EcommUserDetail();
			Customer customer = new Customer();
			/*
			 * Setting all user information
			 */
			if(user.getPhoneInfo() != null) {
			String phoneNumber = user.getPhoneInfo().getPhoneNumber();
			if(phoneNumber.startsWith("+91")){
				phoneNumber = phoneNumber.substring(3, phoneNumber.length());
			}
			customer.setCustomer_number(phoneNumber);
			}
			customer.setFirstname(user.getFirstName());
			customer.setLastname(user.getLastName());
			customer.setEmail(user.getEmailInfo() != null ? user.getEmailInfo().getEmailAddress() : "");
			ecommUserDetail.setCustomer(customer);
			ecommUserDetail.setPassword(user.getEncryptedPassword());
			HttpEntity<EcommUserDetail> httpEntity = new HttpEntity<EcommUserDetail>(ecommUserDetail, headers);

			try {
				restTemplate.exchange(ecommUserUpdateUrl, HttpMethod.POST, httpEntity, String.class);
			} catch (HttpStatusCodeException exception) {
				logger.error(exception.getMessage());
			}
		});

		return "Completed";

	}

	@Override
	public User updateUserForCampaign(OTPDetailVM otpDetail) {
		// TODO Auto-generated method stub
		int otpNumber = otpDetail.getOtpNumber();
		String phoneNumber = otpDetail.getPhoneNumber();
		String number = Constants.INDIA_MOBILE_CODE + phoneNumber;
		User user = userRepository.findOneByPhoneInfoPhoneNumber(phoneNumber, number);		
		PhoneInfo phoneInfoRef = user.getPhoneInfo();
		
		if(otpNumber == phoneInfoRef.getOtp()){
			
			phoneInfoRef.setVerifiedFlag(true);
			phoneInfoRef.setVerifiedOn(new Date());
			user.setPhoneInfo(phoneInfoRef);
		    return userRepository.save(user);
		}
		
		return user;
	}

	/**
	 * @author arvind.rawat
	 * @description to check user email is verified or not
	 */
	@Override
	public Boolean checkEmailVerified(String userId) {
		try {
			Optional<User> user = userRepository.findById(userId);
			if (user.isPresent()) {
				if (user.get().getEmailInfo().isVerifiedFlag() == true) {
					return true;

				} else {
					return false;
				}
			} else {
				throw new CustomParameterizedException(RegisteredException.USER_FETCH_EXCEPTION.getException(),
						CustomExceptionCode.USER_NOT_FOUND.getErrMsg(),
						CustomExceptionCode.USER_NOT_FOUND.getErrCode());
			}

		} catch (CustomParameterizedException customException) {
			throw customException;
		} catch (Exception exception) {
			throw new CustomParameterizedException(RegisteredException.USER_FETCH_EXCEPTION.getException(),
					CustomExceptionCode.CHECK_EMAIL_VERIFIED_EXCEPTION.getErrMsg(), exception,
					CustomExceptionCode.CHECK_EMAIL_VERIFIED_EXCEPTION.getErrCode());
		}
	}
	
	/***
	 * @author arvind.rawat
	 * @description this method is used for ecommerce signup
	 * @param recipientVm
	 * @return Map<String, String>
	 */	
	@Override
	public Map<String, String> ecommSignup(RecipientVM recipientVm) {
		logger.info("++++++ Entery in ecommSignup ++++++");
		logger.debug("Parameters from front end : {} ", recipientVm);
		try {
			Map<String, String> ecommSignupResponseData = new HashMap<>();
			EcommUserRegistrationVM ecommUserRegistrationVM = new EcommUserRegistrationVM();
			EcommCustomer ecommCustomer = new EcommCustomer();

			ecommCustomer.setEmail(recipientVm.getEmailAddress());
			ecommCustomer.setFirstname(recipientVm.getFirstName());
			ecommCustomer.setLastname(recipientVm.getLastName());

			List<CustomerAttribute> customAttributes = new ArrayList<>();
			CustomerAttribute customerAttribute = new CustomerAttribute();
			customerAttribute.setValue(recipientVm.getPhoneNumber());
			customerAttribute.setAttribute_code(MOBILE_NUMBER);
			customAttributes.add(customerAttribute);
			ecommCustomer.setCustom_attributes(customAttributes);
			ecommUserRegistrationVM.setCustomer(ecommCustomer);
			ecommUserRegistrationVM.setPassword(recipientVm.getEncryptedPassword());

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);		
			logger.debug("Ecomm access token is : {} ",accessToken);
			String authorizationValue="Bearer "+accessToken;
			headers.set(AUTHORIZATION, authorizationValue);
			logger.debug("Headers for ecomm signup : {} ",headers);
			logger.debug("request body for ecomm signup is : {} ", ecommUserRegistrationVM);
			logger.debug("URL for ecomm signup  is : {} ", ecommSignupUrl);
			HttpEntity<EcommUserRegistrationVM> httpEntity = new HttpEntity<EcommUserRegistrationVM>(
					ecommUserRegistrationVM, headers);
			ResponseEntity<String> responseEntity = restTemplate.exchange(ecommSignupUrl, HttpMethod.POST, httpEntity,
					String.class);
			logger.debug("Response from ecommerce signup api is  : " + responseEntity);
			String body = responseEntity.getBody();
			JSONObject jObject = new JSONObject(body);
			String apiStatus = jObject.getString(Constants.SUCCESS);
			String token = jObject.getString(TOKEN);
			if (!apiStatus.equalsIgnoreCase(Constants.TRUE) && StringsUtil.isNullOrEmpty(token)) {
				logger.error("ecommerce signup api status : {} ", apiStatus);
				throw new CustomParameterizedException(RegisteredException.ECOMM_LOGIN_EXCEPTION.getException(),
						CustomExceptionCode.ECOMM_SIGNUP_STATUS_EXCEPTION.getErrMsg(),
						CustomExceptionCode.ECOMM_SIGNUP_STATUS_EXCEPTION.getErrCode());
			}
			JSONObject jsonObject = jObject.getJSONObject(CUSTOMER_DATA);
			String loginId = (String) jsonObject.getString(ID);
			ecommSignupResponseData.put(ECOMM_LOGIN_ID, loginId);
			ecommSignupResponseData.put(ECOMM_LOGIN_TOKEN, token);
			logger.debug("ecommerce signup response is : {} ", ecommSignupResponseData);
			logger.info("------ Exit from ecommSignup ------");
			return ecommSignupResponseData;
		} catch (CustomParameterizedException customException) {
			throw customException;
		} catch (HttpStatusCodeException exception) {
			logger.error("Exception while accessing ecommerce signup api :  {} ", exception.getMessage());
			throw new CustomParameterizedException(RegisteredException.ECOMM_SIGNUP_EXCEPTION.getException(),
					CustomExceptionCode.ECOMM_SIGNUP_API_ACCESS_EXCEPTION.getErrMsg(), exception.getMessage(),
					CustomExceptionCode.ECOMM_SIGNUP_API_ACCESS_EXCEPTION.getErrCode());
		} catch (Exception exception) {
			logger.error("Exception while ecommerce signup :  {} ", exception.getMessage());
			throw new CustomParameterizedException(RegisteredException.ECOMM_SIGNUP_EXCEPTION.getException(),
					CustomExceptionCode.ECOMM_SIGNUP_EXCEPTION.getErrMsg(), exception.getMessage(),
					CustomExceptionCode.ECOMM_SIGNUP_EXCEPTION.getErrCode());
		}
	}
	
	/**
	 * @author damini.arora
	 * @description to add ghost user while booking appointment to doctor
	 * @param ghostUserVM
	 * @return ghostUserVM
	 * @exception CustomParameterizedException
	 */
	@Override
	public GhostUserVM addGhostUserInDiagnostics(GhostUserVM ghostUserVM) {
		logger.info("---In service method for {}",ghostUserVM.getFirstName()+"_"+ghostUserVM.getLastName());
		try {
			if (ghostUserVM.getMobileNumber() != null && !ghostUserVM.getMobileNumber().isEmpty()) {
				User userMobile = userRepository.findByPhoneNumberAndDeleteFlag(ghostUserVM.getMobileNumber(), false);
				if (!ghostUserVM.getMobileNumber().startsWith(Constants.INDIA_MOBILE_CODE)) {
					ghostUserVM.setMobileNumber(Constants.INDIA_MOBILE_CODE + ghostUserVM.getMobileNumber());
				}

				if (userMobile != null) {
					logger.info("--- Found by phoneNumber : ");
					throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
							CustomExceptionCode.MOBILE_NUMBER_EXISTS.getErrMsg(),
							CustomExceptionCode.MOBILE_NUMBER_EXISTS.getErrCode());
				}
			}
			logger.info("--- Creating Ghost User for : {}",ghostUserVM.getFirstName()+"_"+ghostUserVM.getLastName());
			User user = new User();
			user.setFirstName(ghostUserVM.getFirstName());
			user.setLastName(ghostUserVM.getLastName());
			EmailInfo emailInfo = new EmailInfo(ghostUserVM.getEmailId());
			user.setEmailInfo(emailInfo);
			PhoneInfo phoneInfo = new PhoneInfo(ghostUserVM.getMobileNumber());
			user.setPhoneInfo(phoneInfo);
			user.setZoyloId(UUID.randomUUID().toString());
			user.setActiveFlag(true);
			user.setLoginEnabled(true);
			user = userRepository.save(user);
			logger.info("---  Ghost User created for : {} with ID {}",ghostUserVM.getFirstName()+"_"+ghostUserVM.getLastName(),user.getId());
			if (ghostUserVM.getMobileNumber() != null && !ghostUserVM.getMobileNumber().isEmpty()) {
				ResendOTPVM resendOTPVM = new ResendOTPVM();
				logger.info("--- Before OTP SEND");
				resendOTPVM.setMobileNo(ghostUserVM.getMobileNumber());
				logger.info("--- Before OTP SEND method");
				resendOTP(resendOTPVM);
				logger.info("--- After OTP SEND method");
			}
			ghostUserVM.setUserId(user.getId());
			ghostUserVM.setZoyloId(user.getZoyloId());
			return ghostUserVM;
		} catch (CustomParameterizedException customParameterizedException) {
			throw customParameterizedException;
		} catch (Exception exception) {
			logger.error("--- Exception in UserBllImpl#addGhostUser : {} ",exception);
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.REGISTERATION_FAIL.getErrMsg(), exception,
					CustomExceptionCode.REGISTERATION_FAIL.getErrCode());
		}
	}

}