package com.zoylo.gateway.bll.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.amazonaws.services.simpleemail.model.SendEmailResult;
import com.zoylo.core.emailservice.EmailSEService;
import com.zoylo.core.emailservice.EmailTemplate;
import com.zoylo.core.smsservice.SMSService;
import com.zoylo.core.smsservice.SendSmsVM;
import com.zoylo.core.smsservice.SmsServiceResModel;
import com.zoylo.gateway.bll.EmailBll;
import com.zoylo.gateway.bll.ForgotPasswordBll;
import com.zoylo.gateway.bll.NotificationBll;
import com.zoylo.gateway.bll.UserBll;
import com.zoylo.gateway.domain.Permission;
import com.zoylo.gateway.domain.PermissionData;
import com.zoylo.gateway.domain.PhoneInfo;
import com.zoylo.gateway.domain.User;
import com.zoylo.gateway.model.ForgotPasswordOtpVM;
import com.zoylo.gateway.model.ForgotPasswordVM;
import com.zoylo.gateway.model.GhostResetPasswordVM;
import com.zoylo.gateway.model.MobileVerificationVM;
import com.zoylo.gateway.model.PermissionsListVM;
import com.zoylo.gateway.model.RecipientPermissionListVM;
import com.zoylo.gateway.model.UserProfileVM;
import com.zoylo.gateway.repository.PermissionRepository;
import com.zoylo.gateway.repository.UserRepository;
import com.zoylo.gateway.security.CustomUserDetails;
import com.zoylo.gateway.security.jwt.TokenProvider;
import com.zoylo.gateway.service.util.Constants;
import com.zoylo.gateway.web.rest.client.AdminRestClient;
import com.zoylo.gateway.web.rest.client.RecipientRestClient;
import com.zoylo.gateway.web.rest.errors.CustomExceptionCode;
import com.zoylo.gateway.web.rest.errors.CustomParameterizedException;
import com.zoylo.gateway.web.rest.errors.RegisteredException;
import com.zoylo.gateway.web.rest.errors.RegisteredExceptions;
import com.zoylo.gateway.web.rest.util.LookUp;
import com.zoylo.gateway.web.rest.util.PasswordEncryption;

import freemarker.template.TemplateException;
import io.github.jhipster.config.JHipsterProperties;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * 
 * @author piyush.singh
 * @version 1.0
 *
 */
@Service
public class ForgotPasswordBllImpl implements ForgotPasswordBll {
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	private static final String AUTHORITIES_KEY = "auth";
	private static final String MOBILE_NO = "mobn";
	private static final String USER_NAME = "usr";
	private static final String USER_ID = "uid";
	private String secretKey;
	private String responseStatus = "status";
	private String success = "SUCCESS";
	private static final int TIMEOUT = 60000;
	private static final String STATUS_CODE = "S";
	private static final String MESSAGE_MODE_CODE = "S";
	private static final String EMAIL_MODE_CODE = "E";

	@Autowired
	private TokenProvider tokenProvider;

	@Value("${app.accessKey}")
	private String accesskey;

	@Value("${app.secretKey}")
	private String key;

	@Value("${app.emailFrom}")
	private String emailFrom;

	@Value("${app.port}")
	private String port;

	@Value("${app.subject}")
	private String subject;

	@Value("${app.body}")
	private String body;

	@Value("${app.tokenValidDay}")
	private int tokenValidDay;

	@Autowired
	private PasswordEncoder passwordEncoder;
	private Boolean FALSEFLAG = Boolean.FALSE;

	@Value("${app.otpExpirationMinutes}")
	private int otpExpirationMinutes;

	@Value("${app.change-password-body}")
	private String changePasswordBody;

	@Value("${app.home-page}")
	private String homePageUrl;
	
	@Value("${app.mobileNotificationAllowed}")
	private Boolean mobileNotificationAllowed;
	
	@Value("${app.emailNotificationAllowed}")
	private Boolean emailNotificationAllowed;
	
	@Value("${app.sms.apiurl}")
	private String apiUrl;
	
	@Value("${app.sms.authkey}")
	private String authkey;
	
	@Value("${app.sms.country}")
	private String country;
	
	@Value("${app.sms.sender}")
	private String sender;
	
	@Value("${app.sms.route}")
	private String route;
	
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RecipientRestClient recipientRestClient;

	@Autowired
	private AdminRestClient adminRestClient;

	@Autowired
	private PermissionRepository permissionRepository;
	
	@Autowired
	private NotificationBll notificationBll;
	@Autowired
	private UserBll userBll;
	@Autowired
	private EmailBll emailBll;
	

	public ForgotPasswordBllImpl(JHipsterProperties jHipsterProperties) {
		this.secretKey = jHipsterProperties.getSecurity().getAuthentication().getJwt().getSecret();
	}

	/**
	 * @author piyush.singh
	 * @description forgot password send mail
	 * @return void
	 * @throws IOException 
	 * @throws TemplateException 
	 */
	@Override
	@Transactional
	public void sendForgotPasswordMail(String emailId) {
		Optional<User> userEmailData = userRepository.findByEmailAddressAndVerifiedFlag(emailId, FALSEFLAG, FALSEFLAG);
		if (!(userEmailData.isPresent())) {// check if email id is not register
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.EMAIL_NOT_EXISTS.getErrMsg(),
					CustomExceptionCode.EMAIL_NOT_EXISTS.getErrCode());
		}

		String authrties = "" + "," + "";
		String mobileNo = userEmailData.get().getPhoneInfo().getPhoneNumber();
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.DATE, tokenValidDay);
		long now = (cal.getTimeInMillis());
		Date validity = new Date(now);
		EmailSEService emailSEService = new EmailSEService();
		EmailTemplate emailTemplate = new EmailTemplate();
		emailTemplate.setAccessKey(accesskey);
		emailTemplate.setSecretKey(key);
		emailTemplate.setEmailFrom(emailFrom);
		emailTemplate.setSendTo(emailId);
		emailTemplate.setSubject(subject);

		String token = Jwts.builder().setSubject(emailId).claim(AUTHORITIES_KEY, authrties).claim(MOBILE_NO, mobileNo)
				.claim(USER_ID, emailId).claim(USER_NAME, emailId).signWith(SignatureAlgorithm.HS512, secretKey)
				.setExpiration(validity).compact();
		//done
		/*String emailBodybody="<h1 style=\"font-size:1.1em;margin-top:0px;\">Dear "+userEmailData.get().getFirstName()+", </h1>" +
				"\n "+body+"\n"+
				"\n\t " + port + "?token=" + token;*/
		//String emailBodybody=body + ": \n\t " + port + "?token=" + token;
		try {
			emailTemplate.setBody(emailBll.resetPassword(userEmailData.get().getFirstName(), token));
		} catch (Exception e) {
			log.error("EmailTemplate exception:" + e);
			throw new CustomParameterizedException(RegisteredException.EMAIL_TEMPLATE_EXCEPTION.getException(),
					CustomExceptionCode.EXCEPTION_OCCOR_IN_EMAIL_TEMPLTE.getErrMsg(),
					CustomExceptionCode.EXCEPTION_OCCOR_IN_EMAIL_TEMPLTE.getErrCode());
		}
		//emailTemplate.setBody(body + ": \n\t " + port + "?token=" + token);

		String messageId = "";
		if (emailNotificationAllowed && userEmailData.get().getEmailInfo().isVerifiedFlag()) {
			SendEmailResult sendEmailResult = emailSEService.sendEmail(emailTemplate);
			messageId = sendEmailResult.getMessageId();
		}
		/*try {
			ZoyloNotificationVM zoyloNotificationVM = new ZoyloNotificationVM();
			RecipientInfo recipientInfo = new RecipientInfo();
			MessageInfo messageInfo = new MessageInfo();

			recipientInfo.setRecipientFirstName(userEmailData.get().getFirstName());
			recipientInfo.setRecipientLastName(userEmailData.get().getLastName());
			recipientInfo.setRecipientZoyloId(userEmailData.get().getZoyloId());
			recipientInfo.setRecipientId(userEmailData.get().getId());
			recipientInfo.setRecipientEmail(emailTemplate.getSendTo());

			messageInfo.setMessageText(emailTemplate.getBody());

			List<DeliveryTrail> deliveryTrail = new ArrayList<>();
			DeliveryTrail trail = new DeliveryTrail();
			trail.setDeliveryAttemptedAt(new Date());
			trail.setDeliveryStatusCode(STATUS_CODE);
			deliveryTrail.add(trail);

			zoyloNotificationVM.setNotificationMessageId(sendEmailResult.getMessageId());
			zoyloNotificationVM.setNotificationModeCode(EMAIL_MODE_CODE);
			zoyloNotificationVM.setNotificationStatusCode(STATUS_CODE);
			zoyloNotificationVM.setRecipientInfo(recipientInfo);
			zoyloNotificationVM.setRetryCounter(1);
			zoyloNotificationVM.setMessageInfo(messageInfo);
			zoyloNotificationVM.setDeliveryTrail(deliveryTrail);

			adminRestClient.save(zoyloNotificationVM);
		} catch (Exception exception) {
			log.info("unable to save notification email");
		}*/
		notificationBll.saveNotificationAfterEmail(userEmailData.get(), emailTemplate, messageId);
	}

	/**
	 * @author piyush.singh
	 * @description validation of forgot password link
	 * @return map
	 */
	@Override
	public Map<String, String> validateToken(String authToken) {
		Map<String, String> userDataAfterVerification = new HashMap<String, String>();
		if (StringUtils.hasText(authToken) && this.tokenProvider.validateToken(authToken)) {
			Authentication authentication = this.tokenProvider.getAuthentication(authToken);
			SecurityContextHolder.getContext().setAuthentication(authentication);
			CustomUserDetails customUserDetails = (CustomUserDetails) authentication.getPrincipal();
			userDataAfterVerification.put("id", customUserDetails.getId());

			userDataAfterVerification.put("token", authToken);
		} else {
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.TOKEN_EXPIRE.getErrMsg(), CustomExceptionCode.TOKEN_EXPIRE.getErrCode());
		}
		return userDataAfterVerification;
	}

	/**
	 * @author piyush.singh
	 * @description forgot password : save new password
	 * @return map
	 */

	@Transactional
	@Override
	public Map<String, String> setNewPassword(ForgotPasswordVM forgotPasswordVM) {
		String newPassword = PasswordEncryption.getEncryptedPassword(forgotPasswordVM.getNewPassword());
		String newPasswordCheck = PasswordEncryption.getEncryptedPassword(forgotPasswordVM.getNewPasswordCheck());
		forgotPasswordVM.setNewPassword(newPassword);
		forgotPasswordVM.setNewPasswordCheck(newPasswordCheck);
		if (forgotPasswordVM.getNewPassword().trim().equals(forgotPasswordVM.getNewPasswordCheck().trim())) {
			validateToken(forgotPasswordVM.getToken());
			Optional<User> userEmailData = userRepository
					.findByEmailAddressAndVerifiedFlag(forgotPasswordVM.getEmailId(), FALSEFLAG, FALSEFLAG);
			if (!(userEmailData.isPresent())) {
				throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
						CustomExceptionCode.EMAIL_NOT_EXISTS.getErrMsg(),
						CustomExceptionCode.EMAIL_NOT_EXISTS.getErrCode());
			}
			User user = userRepository.findOneById(userEmailData.get().getId());
			if (user == null) {
				throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
						CustomExceptionCode.EMAIL_NOT_EXISTS.getErrMsg(),
						CustomExceptionCode.EMAIL_NOT_EXISTS.getErrCode());
			}
			String encryptedPassword = passwordEncoder.encode(forgotPasswordVM.getNewPassword());
			user.setEncryptedPassword(encryptedPassword);
			user.setPasswordLastSetOn(new Date());
			userRepository.save(user);
			Map<String, String> saveStatusPassword = new HashMap<String, String>();
			saveStatusPassword.put(responseStatus, success);
			return saveStatusPassword;
		} else {
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.PASSWORD_WRONG.getErrMsg(), CustomExceptionCode.PASSWORD_WRONG.getErrCode());
		}
	}

	/**
	 * @author piyush.singh
	 * @description forgot password : send otp 6 digit
	 * @return map
	 * @throws IOException 
	 * @throws TemplateException 
	 */
	@Override
	public String sendForgotPasswordOtp(String mobileNumber){
		if (mobileNumber != null && !mobileNumber.startsWith(Constants.INDIA_MOBILE_CODE)) {
			mobileNumber = Constants.INDIA_MOBILE_CODE + mobileNumber;
		}
		User userPhoneNumber = userRepository.findByMobileNumber(mobileNumber);

		if (userPhoneNumber == null) {
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.MOBILE_NUMBER_NOT_EXISTS.getErrMsg(),
					CustomExceptionCode.MOBILE_NUMBER_NOT_EXISTS.getErrCode());

		}
		Random generateSixDigit = new Random();	//NOSONAR
		int sixDigitOtp = generateSixDigit.nextInt(999999);
		if (sixDigitOtp <= 99999) {
			sixDigitOtp = 100000 + sixDigitOtp;
		}
		String userId = userPhoneNumber.getId();
		User user = userRepository.findOneById(userId);
		PhoneInfo phoneInfo = user.getPhoneInfo();
		phoneInfo.setOtp(sixDigitOtp);
		phoneInfo.setOtpSentOn(new Date());		
		if(phoneInfo.getResendOtpCount()==null) {
			phoneInfo.setResendOtpCount(Constants.DEFAULT_RESEND_OTP_COUNT);
		}else {
			phoneInfo.setResendOtpCount(phoneInfo.getResendOtpCount()+Constants.INITIAL_RESEND_OTP_COUNT);				
		}
		user.setPhoneInfo(phoneInfo);
		userRepository.save(user);
		String messageId = null; 
		SMSService smsService = new SMSService();
		SendSmsVM sendSmsVM = new SendSmsVM();
		sendSmsVM.setMessage("Use " + sixDigitOtp
				+ " as One Time Password (OTP) to reset your Zoylo account password. Do not share this OTP with anyone for security reasons. For more details, call us at 1800 3000 5454.");
		sendSmsVM.setNumber(mobileNumber); // to do
		sendSmsVM.setSecretKey(key);
		sendSmsVM.setAccesskey(accesskey);
		sendSmsVM.setApiUrl(apiUrl);
		sendSmsVM.setCountry(country);
		sendSmsVM.setAuthkey(authkey);
		sendSmsVM.setRoute(route);
		sendSmsVM.setSender(sender);
		// smsService.sendSMSMessage(sendSmsVM);
		// SMS data save
		String smsMessageId = "";
		if (mobileNotificationAllowed){
			SmsServiceResModel smsServiceResModel = smsService.sendSMSMessage(sendSmsVM);
			smsMessageId = smsServiceResModel.getRequestId();
		}
		/*try {
			ZoyloNotificationVM zoyloNotification = new ZoyloNotificationVM();
			RecipientInfo recipientPhoneInfo = new RecipientInfo();
			MessageInfo messagePhoneInfo = new MessageInfo();
			recipientPhoneInfo.setRecipientFirstName(user.getFirstName());
			recipientPhoneInfo.setRecipientLastName(user.getLastName());
			recipientPhoneInfo.setRecipientZoyloId(user.getZoyloId());
			recipientPhoneInfo.setRecipientId(user.getId());
			recipientPhoneInfo.setRecipientPhone(sendSmsVM.getNumber());

			messagePhoneInfo.setMessageText(sendSmsVM.getMessage());
			List<DeliveryTrail> deliveryTrails = new ArrayList<>();
			DeliveryTrail trails = new DeliveryTrail();
			trails.setDeliveryAttemptedAt(new Date());
			trails.setDeliveryStatusCode(STATUS_CODE);
			deliveryTrails.add(trails);

			zoyloNotification.setDeliveryTrail(deliveryTrails);
			zoyloNotification.setNotificationMessageId(messageId);
			zoyloNotification.setNotificationModeCode(MESSAGE_MODE_CODE);
			zoyloNotification.setNotificationStatusCode(STATUS_CODE);
			zoyloNotification.setRecipientInfo(recipientPhoneInfo);
			zoyloNotification.setRetryCounter(1);
			zoyloNotification.setMessageInfo(messagePhoneInfo);

			adminRestClient.save(zoyloNotification);
		} catch (Exception exception) {
			log.info("unable to save notifications ");
		}*/
		notificationBll.saveNotificationAfterSMS(user, sendSmsVM, smsMessageId);

		if (user.getEmailInfo() != null && user.getEmailInfo().getEmailAddress() != null && user.getEmailInfo().isVerifiedFlag()) {
			EmailSEService emailSEService = new EmailSEService();
			EmailTemplate emailTemplate = new EmailTemplate();
			emailTemplate.setAccessKey(accesskey);
			emailTemplate.setSecretKey(key);
			emailTemplate.setEmailFrom(emailFrom);
			emailTemplate.setSendTo(user.getEmailInfo().getEmailAddress());
			emailTemplate.setSubject("Zoylo.com | One Time Password (OTP) to reset your Zoylo account password.");
			//String body = "Dear " + user.getFirstName() + ",\n <br/>" + "Use " + sixDigitOtp
			//done1
			/*String body="<h1 style=\"font-size:1.1em;margin-top:0px;\">Dear "+user.getFirstName()+", </h1>" + 
					"Use " + sixDigitOtp
					+ " as One Time Password (OTP) to reset your Zoylo account password.\n"
					+ "Do not share this OTP with anyone for security reasons.";*/
			try {
				emailTemplate.setBody(emailBll.generateOTP(user.getFirstName(), sixDigitOtp));
			} catch (Exception e) {
				log.error("EmailTemplate exception:" + e);
				throw new CustomParameterizedException(RegisteredException.EMAIL_TEMPLATE_EXCEPTION.getException(),
						CustomExceptionCode.EXCEPTION_OCCOR_IN_EMAIL_TEMPLTE.getErrMsg(),
						CustomExceptionCode.EXCEPTION_OCCOR_IN_EMAIL_TEMPLTE.getErrCode());
			}	
			String emailMessageId = "";
			if(emailNotificationAllowed) {
				SendEmailResult sendEmailResult = emailSEService.sendEmail(emailTemplate);
				emailMessageId = sendEmailResult.getMessageId();
			}

			/*try {
				ZoyloNotificationVM zoyloNotificationVM = new ZoyloNotificationVM();
				RecipientInfo recipientInfo = new RecipientInfo();
				MessageInfo messageInfo = new MessageInfo();

				recipientInfo.setRecipientFirstName(user.getFirstName());
				recipientInfo.setRecipientLastName(user.getLastName());
				recipientInfo.setRecipientZoyloId(user.getZoyloId());
				recipientInfo.setRecipientId(user.getId());
				recipientInfo.setRecipientEmail(emailTemplate.getSendTo());

				messageInfo.setMessageText(emailTemplate.getBody());

				List<DeliveryTrail> deliveryTrail = new ArrayList<>();
				DeliveryTrail trail = new DeliveryTrail();
				trail.setDeliveryAttemptedAt(new Date());
				trail.setDeliveryStatusCode(STATUS_CODE);
				deliveryTrail.add(trail);

				zoyloNotificationVM.setNotificationMessageId(emailMessageId);
				zoyloNotificationVM.setNotificationModeCode(EMAIL_MODE_CODE);
				zoyloNotificationVM.setNotificationStatusCode(STATUS_CODE);
				zoyloNotificationVM.setRecipientInfo(recipientInfo);
				zoyloNotificationVM.setRetryCounter(1);
				zoyloNotificationVM.setMessageInfo(messageInfo);
				zoyloNotificationVM.setDeliveryTrail(deliveryTrail);

				adminRestClient.save(zoyloNotificationVM);
			} catch (Exception exception) {

			}*/
			notificationBll.saveNotificationAfterEmail(user, emailTemplate, emailMessageId);
		}

		return userId;
	}

	/**
	 * @author piyush.singh
	 * @description forgot password : verify otp
	 * @return map
	 */

	@Override
	public Map<String, String> verifyOtp(MobileVerificationVM mobileVerificationVM) {
		try {
		User user = userRepository.findOneById(mobileVerificationVM.getId());
		if (user == null) {
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.USER_NOT_FOUND.getErrMsg(), CustomExceptionCode.USER_NOT_FOUND.getErrCode());
		}
		if (String.valueOf(mobileVerificationVM.getOtp()).length() != 6) {
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.OTP_LENGTH.getErrMsg(), CustomExceptionCode.OTP_LENGTH.getErrCode());
		}

		PhoneInfo phoneInfo = user.getPhoneInfo();
		if (phoneInfo.getOtp() != mobileVerificationVM.getOtp()) {
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.OTP_NOT_MATCH.getErrMsg(), CustomExceptionCode.OTP_NOT_MATCH.getErrCode());
		}
		if (((new Date().getTime() / TIMEOUT)
				- (phoneInfo.getOtpSentOn().getTime() / TIMEOUT)) > otpExpirationMinutes) {
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.OTP_EXPIRED.getErrMsg(), CustomExceptionCode.OTP_EXPIRED.getErrCode());
		}
		phoneInfo.setVerifiedFlag(true);
		phoneInfo.setOtpSentOn(new Date());
		phoneInfo.setResendOtpCount(Constants.DEFAULT_RESEND_OTP_COUNT);
		user.setPhoneInfo(phoneInfo);
		user = userRepository.save(user);
		UserProfileVM userProfileVM = new UserProfileVM();
		userProfileVM.setFirstName(user.getFirstName());
		userProfileVM.setLastName(user.getLastName());
		userProfileVM.setZoyloId(user.getZoyloId());
		userProfileVM.setUserId(user.getId());
		/*set gender for doctor*/
		ResponseEntity<String> resourceData=adminRestClient.getGenderByUserId(mobileVerificationVM.getId());
		String body = resourceData.getBody();
		JSONObject userJsonObj = new JSONObject(body);
		String gender = (String) userJsonObj.getString(Constants.DATA);
		if(gender!=null) {
			userProfileVM.setGender(gender);
		}
		
		List<Permission> permissions = permissionRepository.findByAuthCategoryCode(LookUp.RECIPIENT.getLookUpValue());
		RecipientPermissionListVM recipientpermissionListVM = new RecipientPermissionListVM();
		recipientpermissionListVM.setRecipientZoyloId(user.getZoyloId());
		recipientpermissionListVM.setRecipientId(user.getId());

		List<PermissionsListVM> permissionsListVMs = new ArrayList<>();
		for (Permission permission : permissions) {
			PermissionsListVM permissionsListVM = new PermissionsListVM();
			permissionsListVM.setPermissionCode(permission.getPermissionCode());
			permissionsListVM.setPermissionId(permission.getId());
			List<com.zoylo.gateway.model.PermissionData> permissionDatas = new ArrayList<>();
			if (permission.getPermissionData() != null) {
				for (PermissionData permissionData : permission.getPermissionData()) {
					com.zoylo.gateway.model.PermissionData recPermissionData = new com.zoylo.gateway.model.PermissionData();
					recPermissionData.setLanguageCode(permissionData.getLanguageCode());
					recPermissionData.setLanguageId(permissionData.getLanguageId());
					recPermissionData.setPermissionName(permissionData.getPermissionName());
					permissionDatas.add(recPermissionData);
				}
			}
			permissionsListVM.setPermissionData(permissionDatas);
			permissionsListVMs.add(permissionsListVM);
		}
		recipientpermissionListVM.setPermissionsList(permissionsListVMs);
		userProfileVM.setGrantedPermissionsList(recipientpermissionListVM);
		try {
			recipientRestClient.setUserProfile(userProfileVM);
		} catch (Exception exception) {
			throw new CustomParameterizedException(RegisteredExceptions.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.FAIL_TO_CREATE_USER_PROFILE.getErrMsg(), exception,
					CustomExceptionCode.FAIL_TO_CREATE_USER_PROFILE.getErrCode());

		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.DATE, 1);
		long now = (cal.getTimeInMillis());
		Date validity = new Date(now);
		String authrties = "" + "," + "";
		Map<String, String> otpStatus = new HashMap<String, String>();

		String token = Jwts.builder().setSubject(user.getId()).claim(AUTHORITIES_KEY, authrties)
				.claim(MOBILE_NO, phoneInfo.getPhoneNumber()).claim(USER_ID, user.getId())
				.claim(USER_NAME, user.getEmailInfo().getEmailAddress()).signWith(SignatureAlgorithm.HS512, secretKey)
				.setExpiration(validity).compact();
		otpStatus.put("token", token);
		otpStatus.put("otp", Integer.toString(mobileVerificationVM.getOtp()));
		otpStatus.put("id", mobileVerificationVM.getId());

		return otpStatus;
		}catch(Exception exception) {
			throw new CustomParameterizedException(RegisteredExceptions.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.EXCEPTION_OCCOR_WHILE_VERIFYING_OTP.getErrMsg(), exception,
					CustomExceptionCode.EXCEPTION_OCCOR_WHILE_VERIFYING_OTP.getErrCode());

	}
	}

	/**
	 * @author piyush.singh
	 * @description forgot password : set new password
	 * @return map
	 * @throws IOException 
	 * @throws TemplateException 
	 */
	@Transactional
	@Override
	public Map<String, String> setNewPassworUsingOtp(ForgotPasswordOtpVM forgotPasswordOtpVM) {
		String newPassword = PasswordEncryption.getEncryptedPassword(forgotPasswordOtpVM.getNewPassword());
		String newPasswordCheck = PasswordEncryption.getEncryptedPassword(forgotPasswordOtpVM.getNewPasswordCheck());
		forgotPasswordOtpVM.setNewPassword(newPassword);
		forgotPasswordOtpVM.setNewPasswordCheck(newPasswordCheck);
		if (forgotPasswordOtpVM.getNewPassword().trim().equals(forgotPasswordOtpVM.getNewPasswordCheck().trim())) {
			User user = userRepository.findOneById(forgotPasswordOtpVM.getUserId());
			if (user == null) {
				throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
						CustomExceptionCode.USER_NOT_FOUND.getErrMsg(),
						CustomExceptionCode.USER_NOT_FOUND.getErrCode());
			}
			MobileVerificationVM mobileVerificationVM = new MobileVerificationVM();
			mobileVerificationVM.setId(forgotPasswordOtpVM.getUserId());
			mobileVerificationVM.setOtp(forgotPasswordOtpVM.getOtp());
			PhoneInfo phoneInfo = user.getPhoneInfo();
			if (phoneInfo.getOtp() != mobileVerificationVM.getOtp()) {
				throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
						CustomExceptionCode.OTP_NOT_MATCH.getErrMsg(), CustomExceptionCode.OTP_NOT_MATCH.getErrCode());
			}
			if (((new Date().getTime() / 60000)
					- (phoneInfo.getOtpSentOn().getTime() / 60000)) > otpExpirationMinutes) {
				throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
						CustomExceptionCode.OTP_EXPIRED.getErrMsg(), CustomExceptionCode.OTP_EXPIRED.getErrCode());
			}
			phoneInfo.setVerifiedFlag(true);
			phoneInfo.setOtpSentOn(new Date());
			user.setPhoneInfo(phoneInfo);
			String encryptedPassword = passwordEncoder.encode(forgotPasswordOtpVM.getNewPassword());
			user.setEncryptedPassword(encryptedPassword);
			user.setPasswordLastSetOn(new Date());
			userRepository.save(user);

			String mailId = user.getEmailInfo().getEmailAddress();
			if (mailId != null && user.getEmailInfo().isVerifiedFlag()) {
				EmailSEService emailSEService = new EmailSEService();
				EmailTemplate emailTemplate = new EmailTemplate();
				emailTemplate.setAccessKey(accesskey);
				emailTemplate.setSecretKey(key);
				emailTemplate.setEmailFrom(emailFrom);
				emailTemplate.setSendTo(mailId);
				emailTemplate.setSubject(" Zoylo.com | Your account password is successfully changed.");
				//String body = "Dear " + user.getFirstName() + ",\n<br/>"
				//done
			/*	String body="<h1 style=\"font-size:1.1em;margin-top:0px;\">Dear "+user.getFirstName()+", </h1>\n" + 
						"The password for your Zoylo account has been changed successfull. \n" + homePageUrl+"<br/>"
						+"\n to access your account, view and manage your patient appointments and transaction history online.";*/
						try {
							emailTemplate.setBody(emailBll.passwordChangedSuccessfully(user.getFirstName()));
						} catch (Exception e) {
							log.error("EmailTemplate exception:" + e);
							throw new CustomParameterizedException(RegisteredException.EMAIL_TEMPLATE_EXCEPTION.getException(),
									CustomExceptionCode.EXCEPTION_OCCOR_IN_EMAIL_TEMPLTE.getErrMsg(),
									CustomExceptionCode.EXCEPTION_OCCOR_IN_EMAIL_TEMPLTE.getErrCode());
						}
				String messageId = "";
				if(emailNotificationAllowed) {
					SendEmailResult sendEmailResult = emailSEService.sendEmail(emailTemplate);
					messageId = sendEmailResult.getMessageId();
				}
				/*try {
					ZoyloNotificationVM zoyloNotificationVM = new ZoyloNotificationVM();
					RecipientInfo recipientInfo = new RecipientInfo();
					MessageInfo messageInfo = new MessageInfo();

					recipientInfo.setRecipientFirstName(user.getFirstName());
					recipientInfo.setRecipientLastName(user.getLastName());
					recipientInfo.setRecipientZoyloId(user.getZoyloId());
					recipientInfo.setRecipientId(user.getId());
					recipientInfo.setRecipientEmail(emailTemplate.getSendTo());

					messageInfo.setMessageText(emailTemplate.getBody());

					List<DeliveryTrail> deliveryTrail = new ArrayList<>();
					DeliveryTrail trail = new DeliveryTrail();
					trail.setDeliveryAttemptedAt(new Date());
					trail.setDeliveryStatusCode(STATUS_CODE);
					deliveryTrail.add(trail);

					zoyloNotificationVM.setNotificationMessageId(sendEmailResult.getMessageId());
					zoyloNotificationVM.setNotificationModeCode(EMAIL_MODE_CODE);
					zoyloNotificationVM.setNotificationStatusCode(STATUS_CODE);
					zoyloNotificationVM.setRecipientInfo(recipientInfo);
					zoyloNotificationVM.setRetryCounter(1);
					zoyloNotificationVM.setMessageInfo(messageInfo);
					zoyloNotificationVM.setDeliveryTrail(deliveryTrail);

					adminRestClient.save(zoyloNotificationVM);
				} catch (Exception exception) {
					log.info("unable to save notification email");
				}*/
				notificationBll.saveNotificationAfterEmail(user, emailTemplate, messageId);
			}

			Map<String, String> saveStatusPassword = new HashMap<String, String>();
			saveStatusPassword.put(responseStatus, success);
			return saveStatusPassword;
		} else {
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.PASSWORD_WRONG.getErrMsg(), CustomExceptionCode.PASSWORD_WRONG.getErrCode());
		}
	}

	/**
	 * @author piyush.singh
	 * @description ghost password set
	 * @return map
	 * @throws IOException 
	 * @throws TemplateException 
	 */
	@Override
	public Map<String, String> setGhostNewPassword(GhostResetPasswordVM ghostResetPasswordVM) {

		log.info("validating token");
		Map<String, String> userDataAfterVerification = validateToken(ghostResetPasswordVM.getToken());
		User userData = null;
		if (ghostResetPasswordVM.getNewPassword() != null) {
			Optional<User> user = userRepository.findById(userDataAfterVerification.get("id"));

			if (!user.isPresent()) {
				throw new CustomParameterizedException(RegisteredException.LOGIN_EXCEPTION.getException(),
						CustomExceptionCode.MOBILE_NUMBER_NOT_EXISTS.getErrMsg(),
						CustomExceptionCode.MOBILE_NUMBER_NOT_EXISTS.getErrCode());
			}
			userData = user.get();
			if (!userData.getPhoneInfo().isVerifiedFlag()) {
				throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
						CustomExceptionCode.MOBILE_NOT_VERIFIED.getErrMsg(),
						CustomExceptionCode.MOBILE_NOT_VERIFIED.getErrCode());
			} else if (userData.getPhoneInfo().getOtp() == 0) {
				throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
						CustomExceptionCode.OTP_NOT_FOUND.getErrMsg(), CustomExceptionCode.OTP_NOT_FOUND.getErrCode());
			}

			String encryptedPassword = passwordEncoder
					.encode(PasswordEncryption.getEncryptedPassword(ghostResetPasswordVM.getNewPassword()));

			userData.setEncryptedPassword(encryptedPassword);
			userData.setPasswordLastSetOn(new Date());
			PhoneInfo phoneInfo = userData.getPhoneInfo();
			phoneInfo.setOtp(0);
			userData = userRepository.save(userData);

			if (userData.getEmailInfo() != null && userData.getEmailInfo().getEmailAddress() != null && userData.getEmailInfo().isVerifiedFlag()) {
				String mailId = userData.getEmailInfo().getEmailAddress();
				if (mailId != null) {
					EmailSEService emailSEService = new EmailSEService();
					EmailTemplate emailTemplate = new EmailTemplate();
					emailTemplate.setAccessKey(accesskey);
					emailTemplate.setSecretKey(key);
					emailTemplate.setEmailFrom(emailFrom);
					emailTemplate.setSendTo(mailId);
					emailTemplate.setSubject(" Zoylo.com | Your account password is successfully changed.");

					//String body = "Dear " + userData.getFirstName() + ",\n <br/>"
					//done
					/*String body="<h1 style=\"font-size:1.1em;margin-top:0px;\">Dear "+userData.getFirstName()+", </h1>" + 
							"The password for your Zoylo account has been changed successfull. \n" + homePageUrl+"<br/>"
							+ "\n to access your account, view and manage your patient appointments and transaction history online.";*/
							
							

					//emailTemplate.setBody(body);
					try {
						emailTemplate.setBody(emailBll.passwordChangedSuccessfully(userData.getFirstName()));
					} catch (Exception e) {
						log.error("EmailTemplate exception:" + e);
						throw new CustomParameterizedException(RegisteredException.EMAIL_TEMPLATE_EXCEPTION.getException(),
								CustomExceptionCode.EXCEPTION_OCCOR_IN_EMAIL_TEMPLTE.getErrMsg(),
								CustomExceptionCode.EXCEPTION_OCCOR_IN_EMAIL_TEMPLTE.getErrCode());
					}
					String messageId = "";
					if(emailNotificationAllowed) {
						SendEmailResult sendEmailResult = emailSEService.sendEmail(emailTemplate);
						messageId = sendEmailResult.getMessageId();
					}
					/*try {
						ZoyloNotificationVM zoyloNotificationVM = new ZoyloNotificationVM();
						RecipientInfo recipientInfo = new RecipientInfo();
						MessageInfo messageInfo = new MessageInfo();

						recipientInfo.setRecipientFirstName(userData.getFirstName());
						recipientInfo.setRecipientLastName(userData.getLastName());
						recipientInfo.setRecipientZoyloId(userData.getZoyloId());
						recipientInfo.setRecipientEmail(emailTemplate.getSendTo());

						messageInfo.setMessageText(emailTemplate.getBody());

						List<DeliveryTrail> deliveryTrail = new ArrayList<>();
						DeliveryTrail trail = new DeliveryTrail();
						trail.setDeliveryAttemptedAt(new Date());
						trail.setDeliveryStatusCode(STATUS_CODE);
						deliveryTrail.add(trail);

						zoyloNotificationVM.setNotificationMessageId(sendEmailResult.getMessageId());
						zoyloNotificationVM.setNotificationModeCode(EMAIL_MODE_CODE);
						zoyloNotificationVM.setNotificationStatusCode(STATUS_CODE);
						zoyloNotificationVM.setRecipientInfo(recipientInfo);
						zoyloNotificationVM.setRetryCounter(1);
						zoyloNotificationVM.setMessageInfo(messageInfo);
						zoyloNotificationVM.setDeliveryTrail(deliveryTrail);

						adminRestClient.save(zoyloNotificationVM);
					} catch (Exception exception) {
						log.info("unable to save notification email");
					}*/
					notificationBll.saveNotificationAfterEmail(userData, emailTemplate, messageId);
				}
			}

		} else {
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.PASSWORD_EMPTY.getErrMsg(), CustomExceptionCode.PASSWORD_EMPTY.getErrCode());
		}
		Map<String, String> saveStatusPassword = new HashMap<String, String>();
		saveStatusPassword.put(responseStatus, success);
		saveStatusPassword.put("firstName", userData.getFirstName());
		saveStatusPassword.put("lastName", userData.getLastName());
		if (userData.getPhoneInfo() != null) {
			saveStatusPassword.put("phoneNumber", userData.getPhoneInfo().getPhoneNumber());
		}
		if (userData.getEmailInfo() != null) {
			saveStatusPassword.put("email", userData.getEmailInfo().getEmailAddress());
		}
		return saveStatusPassword;
	}

}
