package com.zoylo.gateway.bll.impl;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zoylo.core.emailservice.EmailTemplate;
import com.zoylo.core.smsservice.SendSmsVM;
import com.zoylo.gateway.bll.NotificationBll;
import com.zoylo.gateway.domain.User;
import com.zoylo.gateway.model.DeliveryTrail;
import com.zoylo.gateway.model.MessageInfo;
import com.zoylo.gateway.model.RecipientInfo;
import com.zoylo.gateway.model.ZoyloNotificationVM;
import com.zoylo.gateway.web.rest.client.AdminRestClient;

/**
 * 
 * @author Devendra.Kumar
 * @version 1.0
 *
 */
@Service
public class NotificationBllImpl implements NotificationBll {

	private static final String STATUS_CODE = "S";
	private static final String MESSAGE_MODE_CODE = "S";
	private static final String EMAIL_MODE_CODE = "E";

	private static final Logger logger = LoggerFactory.getLogger(NotificationBllImpl.class);

	@Autowired
	private AdminRestClient adminRestClient;

	/**
	 * @author damini.arora
	 * @description To save ZoyloNotification after sent email.
	 * @param user
	 * @param emailTemplate
	 * @param sendEmailResult
	 */
	@Override
	public void saveNotificationAfterEmail(User user, EmailTemplate emailTemplate,
			String messageId) {
		try {
			ZoyloNotificationVM zoyloNotificationVM = new ZoyloNotificationVM();
			RecipientInfo recipientInfo = new RecipientInfo();
			MessageInfo messageInfo = new MessageInfo();

			recipientInfo.setRecipientFirstName(user.getFirstName());
			recipientInfo.setRecipientLastName(user.getLastName());
			recipientInfo.setRecipientZoyloId(user.getZoyloId());
			recipientInfo.setRecipientId(user.getId());
			recipientInfo.setRecipientEmail(emailTemplate.getSendTo());

			messageInfo.setMessageText(emailTemplate.getBody());

			List<DeliveryTrail> deliveryTrail = new ArrayList<>();
			DeliveryTrail trail = new DeliveryTrail();
			trail.setDeliveryAttemptedAt(new Date());
			trail.setDeliveryStatusCode(STATUS_CODE);
			deliveryTrail.add(trail);

			zoyloNotificationVM.setNotificationMessageId(messageId);
			zoyloNotificationVM.setNotificationModeCode(EMAIL_MODE_CODE);
			zoyloNotificationVM.setNotificationStatusCode(STATUS_CODE);
			zoyloNotificationVM.setRecipientInfo(recipientInfo);
			zoyloNotificationVM.setRetryCounter(1);
			zoyloNotificationVM.setMessageInfo(messageInfo);
			zoyloNotificationVM.setDeliveryTrail(deliveryTrail);

			adminRestClient.save(zoyloNotificationVM);
		} catch (Exception exception) {

		}
	}

	/**
	 * @author damini.arora
	 * @description To save ZoyloNotification after sent SMS.
	 * @param user
	 * @param emailTemplate
	 * @param sendEmailResult
	 */
	@Override
	public void saveNotificationAfterSMS(User user, SendSmsVM sendSmsVM, String messageId) {
		try {
			ZoyloNotificationVM zoyloNotification = new ZoyloNotificationVM();
			RecipientInfo recipientPhoneInfo = new RecipientInfo();
			MessageInfo messagePhoneInfo = new MessageInfo();
			recipientPhoneInfo.setRecipientFirstName(user.getFirstName());
			recipientPhoneInfo.setRecipientLastName(user.getLastName());
			recipientPhoneInfo.setRecipientZoyloId(user.getZoyloId());
			recipientPhoneInfo.setRecipientId(user.getId());
			recipientPhoneInfo.setRecipientPhone(sendSmsVM.getNumber());

			messagePhoneInfo.setMessageText(sendSmsVM.getMessage());
			List<DeliveryTrail> deliveryTrails = new ArrayList<>();
			DeliveryTrail trails = new DeliveryTrail();
			trails.setDeliveryAttemptedAt(new Date());
			trails.setDeliveryStatusCode(STATUS_CODE);
			deliveryTrails.add(trails);

			zoyloNotification.setDeliveryTrail(deliveryTrails);
			zoyloNotification.setNotificationMessageId(messageId);
			zoyloNotification.setNotificationModeCode(MESSAGE_MODE_CODE);
			zoyloNotification.setNotificationStatusCode(STATUS_CODE);
			zoyloNotification.setRecipientInfo(recipientPhoneInfo);
			zoyloNotification.setRetryCounter(1);
			zoyloNotification.setMessageInfo(messagePhoneInfo);

			adminRestClient.save(zoyloNotification);
		} catch (Exception exception) {
			logger.info("unable to save notification sms");
		}
	}

}
