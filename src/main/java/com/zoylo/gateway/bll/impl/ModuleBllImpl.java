package com.zoylo.gateway.bll.impl;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mongodb.MongoException;
import com.zoylo.gateway.bll.AuditLogService;
import com.zoylo.gateway.bll.ModuleBll;
import com.zoylo.gateway.domain.Module;
import com.zoylo.gateway.domain.ModuleHistory;
import com.zoylo.gateway.model.ModuleDataVM;
import com.zoylo.gateway.model.ModuleEditVM;
import com.zoylo.gateway.model.ModuleVM;
import com.zoylo.gateway.mv.ModuleMV;
import com.zoylo.gateway.repository.CustomRepository;
import com.zoylo.gateway.repository.ModuleRepository;
import com.zoylo.gateway.web.rest.errors.CustomExceptionCode;
import com.zoylo.gateway.web.rest.errors.CustomParameterizedException;
import com.zoylo.gateway.web.rest.errors.RegisteredException;

/**
 * @author damini.arora
 * @version 1.0
 *
 */
@Service
public class ModuleBllImpl implements ModuleBll {

	@Autowired
	ModuleRepository moduleRepository;

	@Autowired
	private ModelMapper modelMapper;
	@Autowired
	private CustomRepository customRepository;
	@Autowired
	private AuditLogService auditLogService;
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private static final boolean FALSEFLAG = false;
	private static final boolean TRUEFLAG = true;

	/**
	 * @author damini.arora
	 * @description To save Module
	 * @param moduleModel
	 * @return ModuleMV
	 * @exception CustomParameterizedException
	 */
	@Override
	public ModuleMV saveModule(ModuleVM moduleModel, String languageCode) {
		Module module = modelMapper.map(moduleModel, Module.class);
		try {
			Module moduleInfo = moduleRepository.findByModuleCode(moduleModel.getModuleCode(), FALSEFLAG);
			if (moduleInfo == null) {
				if (module.isActiveFlag()) {
					module.setActiveFromDate(new Date());
				} else {
					module.setActiveTillDate(new Date());
				}

			
				Module moduleData = moduleRepository.save(module);
				return modelMapper.map(moduleData, ModuleMV.class);

			} else {
				throw new CustomParameterizedException(RegisteredException.MODULE_EXCEPTION.getException(),
						CustomExceptionCode.MODULE_SAVE_ERROR.getErrCode());
			}
		} catch (CustomParameterizedException exception) {
			logger.error(CustomExceptionCode.MODULE_SAVE_ERROR.getErrMsg());
			throw exception;
		} catch (Exception exception) {
			logger.error(CustomExceptionCode.MODULE_SAVE_ERROR.getErrMsg());
			throw new CustomParameterizedException(RegisteredException.MODULE_EXCEPTION.getException(),
					CustomExceptionCode.MODULE_CODE_ALREADY_EXIST.getErrMsg(),exception,
					CustomExceptionCode.MODULE_CODE_ALREADY_EXIST.getErrCode());

		}
	}

	/**
	 * @author damini.arora
	 * @description To get List of all modules
	 * @return List Of Module Data VM
	 * @exception CustomParameterizedException
	 */
	@Override
	public List<ModuleDataVM> getAllModule() {
		try {
			List<Module> listOfModule = moduleRepository.findByDeleteFlag(FALSEFLAG);
			List<ModuleDataVM> moduleModel = Arrays.asList(modelMapper.map(listOfModule, ModuleDataVM[].class));
			return moduleModel;
		} catch (Exception exception) {
			logger.error(CustomExceptionCode.NO_MODULE_FOUND.getErrMsg());
			throw new CustomParameterizedException(RegisteredException.MODULE_EXCEPTION.getException(),
					CustomExceptionCode.NO_MODULE_FOUND.getErrMsg(), exception,
					CustomExceptionCode.NO_MODULE_FOUND.getErrCode());

		}
	}

	/**
	 * @author damini.arora
	 * @description To get module on the basis of moduleCode
	 * @param moduleCode
	 * @return ModuleDataVM
	 * @exception CustomParameterizedException
	 */
	@Override
	public ModuleDataVM getByModuleCode(String moduleCode) {
		try {
			Module module = moduleRepository.findByModuleCode(moduleCode, FALSEFLAG);
			if (!module.isDeleteFlag()) {
				return modelMapper.map(module, ModuleDataVM.class);
			} else {
				throw new CustomParameterizedException(RegisteredException.MODULE_EXCEPTION.getException(),
						CustomExceptionCode.MODULE_NOT_EXIST.getErrCode());
			}
		} catch (CustomParameterizedException exception) {
			logger.error(CustomExceptionCode.MODULE_NOT_EXIST.getErrMsg());
			throw exception;
		} catch (Exception exception) {
			logger.error(CustomExceptionCode.MODULE_NOT_EXIST.getErrMsg());
			throw new CustomParameterizedException(RegisteredException.MODULE_EXCEPTION.getException(),
					CustomExceptionCode.MODULE_NOT_EXIST.getErrMsg(),exception,
					CustomExceptionCode.MODULE_NOT_EXIST.getErrCode());

		}
	}

	/**
	 * @author damini.arora
	 * @description To update module
	 * @return ModuleMV
	 * @exception CustomParameterizedException
	 */
	@Override
	public ModuleMV updateModule(ModuleEditVM moduleModel) {

		try {
			Module module = moduleRepository.findOne(moduleModel.getId());
			if (module == null) {
				throw new CustomParameterizedException(RegisteredException.MODULE_EXCEPTION.getException(),
						CustomExceptionCode.MODULE_NOT_EXIST.getErrCode());
			}
			if (!moduleModel.getModuleCode().trim().equalsIgnoreCase(module.getModuleCode().trim())) {
				Module moduleData = moduleRepository.findByModuleCode(moduleModel.getModuleCode(), FALSEFLAG);
				if (moduleData != null) {
					throw new CustomParameterizedException(RegisteredException.MODULE_EXCEPTION.getException(),
							CustomExceptionCode.MODULE_UPDATE_ERROR.getErrCode());
				}
			}
			ModuleHistory moduleHistory = new ModuleHistory();
			modelMapper.map(module, moduleHistory);
			moduleHistory.setId(null);
			moduleHistory.setModuleId(module.getId());
			if (moduleModel.isActiveFlag() != module.isActiveFlag()) {
				if (moduleModel.isActiveFlag()) {
					module.setActiveFromDate(new Date());
				} else {
					module.setActiveTillDate(new Date());
				}
			}
			modelMapper.map(moduleModel, module);
			if (Optional.ofNullable(module).isPresent()) {
				Module moduleData = moduleRepository.save(module);
				auditLogService.auditActionLog("update", "module");
				customRepository.save(moduleHistory, "moduleHistory");
				return modelMapper.map(moduleData, ModuleMV.class);
			} else {
				throw new CustomParameterizedException(RegisteredException.MODULE_EXCEPTION.getException(),
						CustomExceptionCode.MODULE_UPDATE_ERROR.getErrCode());
			}
		} catch (CustomParameterizedException exception) {
			logger.error(CustomExceptionCode.MODULE_UPDATE_ERROR.getErrMsg());
			throw exception;
		} catch (MongoException exception) {
			logger.error(CustomExceptionCode.MODULE_UPDATE_ERROR.getErrMsg());
			throw new CustomParameterizedException(RegisteredException.MODULE_EXCEPTION.getException(),
					CustomExceptionCode.MODULE_UPDATE_ERROR.getErrMsg(),exception,
					CustomExceptionCode.MODULE_UPDATE_ERROR.getErrCode());
		}
	}

	/**
	 * @author damini arora
	 * @description To delete module
	 * @param id
	 * @return Boolean
	 * @exception CustomParameterizedException
	 */
	@Override
	public Boolean deleteById(String id) {
		try {
			Module moduleData = moduleRepository.findOne(id);
			if (Optional.ofNullable(moduleData).isPresent() && !moduleData.isDeleteFlag()) {
				ModuleHistory moduleHistory = new ModuleHistory();
				modelMapper.map(moduleData, moduleHistory);
				moduleHistory.setId(null);
				moduleHistory.setModuleId(moduleData.getId());
				moduleData.setDeleteFlag(TRUEFLAG);
				moduleData.setDeleteOn(new Date());
				moduleRepository.save(moduleData);
				auditLogService.auditActionLog("delete", "module");
				customRepository.save(moduleHistory, "moduleHistory");
				return Boolean.TRUE;
			} else
				throw new CustomParameterizedException(RegisteredException.MODULE_EXCEPTION.getException(),
						CustomExceptionCode.MODULE_NOT_DELETE.getErrCode());

		} catch (CustomParameterizedException exception) {
			logger.error(exception.getMessage());
			throw exception;
		} catch (Exception exception) {
			logger.error(CustomExceptionCode.MODULE_NOT_DELETE.getErrMsg());
			throw new CustomParameterizedException(RegisteredException.MODULE_EXCEPTION.getException(),
					CustomExceptionCode.MODULE_NOT_DELETE.getErrMsg(),exception,
					CustomExceptionCode.MODULE_NOT_DELETE.getErrCode());

		}

	}

}
