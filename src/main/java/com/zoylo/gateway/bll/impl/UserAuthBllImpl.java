package com.zoylo.gateway.bll.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.zoylo.gateway.bll.UserAuthBll;
import com.zoylo.gateway.domain.GrantedRoleData;
import com.zoylo.gateway.domain.GrantedRolesList;
import com.zoylo.gateway.domain.Role;
import com.zoylo.gateway.domain.RoleData;
import com.zoylo.gateway.domain.ServiceProvidersList;
import com.zoylo.gateway.domain.ZoyloUserAuth;
import com.zoylo.gateway.model.UserRoleDataVM;
import com.zoylo.gateway.repository.PermissionRepository;
import com.zoylo.gateway.repository.RoleRepository;
import com.zoylo.gateway.repository.ZoyloUserAuthRepository;
import com.zoylo.gateway.web.rest.errors.CustomExceptionCode;
import com.zoylo.gateway.web.rest.errors.CustomParameterizedException;
import com.zoylo.gateway.web.rest.errors.RegisteredException;

/**
 * 
 * @author Naga Raju
 * @version 1.0
 * @description User auth service
 *
 */
@Service
public class UserAuthBllImpl implements UserAuthBll {

	private static final Logger logger = LoggerFactory.getLogger(UserAuthBllImpl.class);

	@Autowired
	PermissionRepository permissionRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	ZoyloUserAuthRepository userAuthRepository;

	/**
	 * @author nagaraju
	 * @description add role to a User
	 * @param userRoleDataVM, input data to assign role
	 * @return ZoyloUserAuth
	 */
	@Override
	public ZoyloUserAuth addRole(UserRoleDataVM userRoleDataVM) {
		try {
			ZoyloUserAuth userAuth = new ZoyloUserAuth();
			List<ServiceProvidersList> serviceProvidersList;
			List<GrantedRolesList> grantedRolesList;

			if (userRoleDataVM.getId() == null || userRoleDataVM.getId().length() == 0) {
				throw new CustomParameterizedException(RegisteredException.USER_ID_MANDATORY.getException(),
						CustomExceptionCode.USER_ID_MANDATORY.getErrMsg(),
						CustomExceptionCode.USER_ID_MANDATORY.getErrCode());
			}

			ZoyloUserAuth userAuthFromDb = userAuthRepository.findByUserId(userRoleDataVM.getId());

			if (userAuthFromDb == null) {

				if (userRoleDataVM.getZoyloId() == null || userRoleDataVM.getZoyloId().length() == 0) {
					throw new CustomParameterizedException(RegisteredException.ZOYLO_ID_MANDATORY.getException(),
							CustomExceptionCode.ZOYLO_ID_MANDATORY.getErrMsg(),
							CustomExceptionCode.ZOYLO_ID_MANDATORY.getErrCode());
				}

				if (userRoleDataVM.getEmailId() == null || userRoleDataVM.getEmailId().length() == 0) {
					throw new CustomParameterizedException(RegisteredException.EMAIL_ID_MANDATORY.getException(),
							CustomExceptionCode.EMAIL_ID_MANDATORY.getErrMsg(),
							CustomExceptionCode.EMAIL_ID_MANDATORY.getErrCode());
				}

				if (userRoleDataVM.getPhoneNumber() == null || userRoleDataVM.getPhoneNumber().length() == 0) {
					throw new CustomParameterizedException(RegisteredException.PHONE_NUMBER_MANDATORY.getException(),
							CustomExceptionCode.PHONE_NUMBER_MANDATORY.getErrMsg(),
							CustomExceptionCode.PHONE_NUMBER_MANDATORY.getErrCode());
				}

				if (userRoleDataVM.getRoleCode() == null || userRoleDataVM.getRoleCode().length() == 0) {
					throw new CustomParameterizedException(RegisteredException.ROLE_CODE_MANDATORY.getException(),
							CustomExceptionCode.ROLE_CODE_MANDATORY.getErrMsg(),
							CustomExceptionCode.ROLE_CODE_MANDATORY.getErrCode());
				}
				userAuth.setUserId(userRoleDataVM.getId());
				userAuth.setZoyloId(userRoleDataVM.getZoyloId());
				userAuth.setEmailId(userRoleDataVM.getEmailId());
				userAuth.setPhoneNumber(userRoleDataVM.getPhoneNumber());
				serviceProvidersList = new ArrayList<ServiceProvidersList>();
				ServiceProvidersList serviceProvidersListObj = new ServiceProvidersList();
				serviceProvidersList.add(serviceProvidersListObj);
				grantedRolesList = new ArrayList<GrantedRolesList>();
			} else {
				serviceProvidersList = userAuthFromDb.getServiceProvidersList();
				grantedRolesList = serviceProvidersList.get(0).getGrantedRolesList();
				userAuth = userAuthFromDb;
			}
			
			Optional<GrantedRolesList> gRoles = grantedRolesList.stream().filter(gRole -> gRole.getRoleCode().equals(userRoleDataVM.getRoleCode())).findFirst();
			
			// throw error if role-code already exists
			if (gRoles.isPresent()){
				throw new CustomParameterizedException(RegisteredException.ROLE_CODE_MAPPED_ALREADY.getException(),
						CustomExceptionCode.ROLE_CODE_MAPPED_ALREADY.getErrMsg(),
						CustomExceptionCode.ROLE_CODE_MAPPED_ALREADY.getErrCode());
			}

			GrantedRolesList grantedRolesListObj = new GrantedRolesList();

			Role role = roleRepository.findOneByRoleCode(userRoleDataVM.getRoleCode());
			grantedRolesListObj.setRoleId(role.getId());
			grantedRolesListObj.setRoleCode(role.getRoleCode());
			grantedRolesListObj.setSystemDefined(role.isSystemDefined());
			grantedRolesListObj.setActiveFlag(role.isActiveFlag());
			grantedRolesListObj.setActiveFromDate(role.getActiveFromDate());
			grantedRolesListObj.setActiveTillDate(role.getActiveTillDate());
			grantedRolesListObj.setGrantedPermissionsList(role.getPermissionList());

			List<GrantedRoleData> roledataList = new ArrayList<GrantedRoleData>();

			List<RoleData> roledataListFromRole = role.getRoleData();

			for (RoleData roleDataObj : roledataListFromRole) {
				GrantedRoleData grantedRoleDataObj = new GrantedRoleData();
				grantedRoleDataObj.setLanguageCode(roleDataObj.getLanguageCode());
				grantedRoleDataObj.setLanguageId(roleDataObj.getLanguageId());
				grantedRoleDataObj.setRoleName(roleDataObj.getRoleName());
				roledataList.add(grantedRoleDataObj);
			}

			grantedRolesListObj.setRoleData(roledataList);
			grantedRolesList.add(grantedRolesListObj);
			serviceProvidersList.get(0).setGrantedRolesList(grantedRolesList);

			userAuth.setServiceProvidersList(serviceProvidersList);

			ZoyloUserAuth savedUserAuth = userAuthRepository.save(userAuth);
			return savedUserAuth;

		} catch (CustomParameterizedException customParameterizedException) {
			throw customParameterizedException;
		} catch (Exception exception) {
			throw new CustomParameterizedException(RegisteredException.USER_ROLE_ASSIGN_EXCEPTION.getException(),
					CustomExceptionCode.FAIL_TO_ASSIGN_ROLE.getErrMsg(), exception,
					CustomExceptionCode.FAIL_TO_ASSIGN_ROLE.getErrCode());
		}
	}

	/**
	 * @author nagaraju
	 * @description update role to a User
	 * @param userRoleDataVM, input data to assign role
	 * @return ZoyloUserAuth
	 */
	@Override
	public ZoyloUserAuth updateRole(UserRoleDataVM userRoleDataVM) {
		try {
			ZoyloUserAuth userAuthFromDB = userAuthRepository.findByUserId(userRoleDataVM.getId());
			List<ServiceProvidersList> serviceProvidersListFromDB = userAuthFromDB.getServiceProvidersList();
			for (ServiceProvidersList serviceProvidersList : serviceProvidersListFromDB) {
				List<GrantedRolesList> grantedRolesListFromDB = serviceProvidersList.getGrantedRolesList();
				GrantedRolesList grantedRolesList = grantedRolesListFromDB.stream()
						.filter(role -> role.getRoleCode().equals(userRoleDataVM.getRoleCode())).findFirst().get();
				if (grantedRolesList != null) {
					grantedRolesList.setActiveFlag(userRoleDataVM.isActiveFlag());
				}
			}
			ZoyloUserAuth updatedUserAuth = userAuthRepository.save(userAuthFromDB);
			return updatedUserAuth;
		} catch (CustomParameterizedException customParameterizedException) {
			throw customParameterizedException;
		} catch (Exception exception) {
			throw new CustomParameterizedException(RegisteredException.USER_ROLE_ASSIGN_EXCEPTION.getException(),
					CustomExceptionCode.FAIL_TO_ASSIGN_ROLE.getErrMsg(), exception,
					CustomExceptionCode.FAIL_TO_ASSIGN_ROLE.getErrCode());
		}
	}

}
