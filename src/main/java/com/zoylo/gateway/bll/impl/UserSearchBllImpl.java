package com.zoylo.gateway.bll.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.json.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zoylo.gateway.bll.UserBll;
import com.zoylo.gateway.bll.UserSearchBll;
import com.zoylo.gateway.domain.LookupMV;
import com.zoylo.gateway.domain.LookupValueData;
import com.zoylo.gateway.domain.LookupValueMV;
import com.zoylo.gateway.domain.ProviderTypeData;
import com.zoylo.gateway.domain.SearchCategoryData;
import com.zoylo.gateway.domain.SearchKeyData;
import com.zoylo.gateway.domain.SearchOperatorData;
import com.zoylo.gateway.domain.SearchParamater;
import com.zoylo.gateway.domain.UserSearch;
import com.zoylo.gateway.model.UserProfileDataVM;
import com.zoylo.gateway.model.UserSearchData;
import com.zoylo.gateway.mv.UserSearchMV;
import com.zoylo.gateway.repository.UserSearchRepository;
import com.zoylo.gateway.security.CustomUserDetails;
import com.zoylo.gateway.web.rest.client.AdminRestClient;
import com.zoylo.gateway.web.rest.errors.CustomExceptionCode;
import com.zoylo.gateway.web.rest.errors.CustomParameterizedException;
import com.zoylo.gateway.web.rest.errors.ErrorConstants;
import com.zoylo.gateway.web.rest.errors.RegisteredException;
import com.zoylo.gateway.web.rest.util.DiagnosticPopularSearch;
import com.zoylo.gateway.web.rest.util.DoctorPopularSearch;

/**
 * 
 * @author Devendra.Kumar
 * @version 1.0
 *
 */
@Service
public class UserSearchBllImpl implements UserSearchBll {

	@Autowired
	private UserSearchRepository repository;

	@Autowired
	private ModelMapper modelMapper;
	
	@Autowired
	private AdminRestClient adminRestClient;
	
	@Autowired
	private UserBll userBll;

	/**
	 * @author Devendra.Kumar
	 * @description To save User Search in data base.
	 * @param userSearch
	 * @return UserSearchMV
	 * @exception CustomParameterizedException
	 */
	@Override
	public UserSearchMV saveDiagnosticUserSearchData(CustomUserDetails customUserDetails,UserSearchData userSearchData) {
		try {
			/*UserSearch userSearch = getUserSearch(customUserDetails.getId(),userSearchData,ErrorConstants.DIAGNOSTIC_CENTER);
			UserSearch savedUserSearch = repository.save(userSearch);
			return modelMapper.map(savedUserSearch, UserSearchMV.class);*/
			return null;
		} catch (CustomParameterizedException exception) {
			throw exception;
		} catch (Exception exception) {
			throw new CustomParameterizedException(RegisteredException.USER_SEARCH_EXCEPTION.getException(),
					CustomExceptionCode.FAIL_TO_SAVE_DIAGNOSTIC_USER_SEARCH_DATA.getErrMsg(), exception,
					CustomExceptionCode.FAIL_TO_SAVE_DIAGNOSTIC_USER_SEARCH_DATA.getErrCode());
		}
	}
	
	private UserSearch getUserSearch(String userId,UserSearchData userSearchData,String providerType){
		try{
			UserSearch userSearch = new UserSearch();
			String languageId = "";
			String languageCode = "";
			SearchCategoryData searchCategoryData = new SearchCategoryData();
			LookupMV lookupMV = getLookupByLookupCode(ErrorConstants.SERACH_CATEGORY_TYPE);
			if (lookupMV != null) {
				List<LookupValueMV> valueDatas = lookupMV.getLookupValueList();
				for (LookupValueMV lookupValueMV : valueDatas) {
					if (lookupValueMV.getLookupValueCode().equals(ErrorConstants.USER_SEARCH)) {
						userSearch.setSearchCategoryId(lookupValueMV.getLookupValueId());
						userSearch.setSearchCategoryCode(lookupValueMV.getLookupValueCode());
						List<LookupValueData> data = lookupValueMV.getLookupValueData();
						languageId = data.get(0).getLanguageId();
						languageCode = data.get(0).getLanguageCode();
						searchCategoryData.setLanguageId(data.get(0).getLanguageId());
						searchCategoryData.setLanguageCode(data.get(0).getLanguageCode());
						searchCategoryData.setSearchCategoryName(data.get(0).getValueDisplayName());
					}
				}
			}
			userSearch.setSearchCategoryData(Arrays.asList(searchCategoryData));
			UserProfileDataVM userData = userBll.getUser(userSearchData.getUserId());
			if (userData != null) {
				userSearch.setUserId(userId);
				userSearch.setUserZoyloId(userData.getZoyloId());
				userSearch.setUserMobileNumber(userData.getPhoneNumber());
			}
			userSearch.setSearchedOn(new Date());
			userSearch.setLocationGpsLatitude(userSearchData.getLatitude() + "");
			userSearch.setLocationGpsLongitude(userSearchData.getLongitude() + "");

			lookupMV = getLookupByLookupCode(ErrorConstants.PROVIDER_TYPE);
			ProviderTypeData providerTypeData = new ProviderTypeData();
			if (lookupMV != null) {
				List<LookupValueMV> valueDatas = lookupMV.getLookupValueList();
				for (LookupValueMV lookupValueMV : valueDatas) {
					if (lookupValueMV.getLookupValueCode().equals(providerType)) {
						userSearch.setProviderTypeId(lookupValueMV.getLookupValueId());
						userSearch.setProviderTypeCode(lookupValueMV.getLookupValueCode());
						List<LookupValueData> data = lookupValueMV.getLookupValueData();
						providerTypeData.setLanguageId(data.get(0).getLanguageId());
						providerTypeData.setLanguageCode(data.get(0).getLanguageCode());
						providerTypeData.setProviderTypeName(data.get(0).getValueDisplayName());
					}
					
				}
			}
			userSearch.setProviderTypeData(Arrays.asList(providerTypeData));

			SearchParamater searchParamater = new SearchParamater();
			searchParamater.setSearchParameterId(UUID.randomUUID().toString());
			searchParamater.setSearchKeyCode(userSearchData.getSearchParameter().toUpperCase().replaceAll(" ", "_"));
			searchParamater.setSearchKeyId(UUID.randomUUID().toString());
			SearchKeyData searchKeyData = new SearchKeyData();
			searchKeyData.setLanguageCode(languageId);
			searchKeyData.setLanguageId(languageCode);
			searchKeyData.setSearchKeyName(userSearchData.getSearchParameter());
			searchParamater.setSearchKeyData(Arrays.asList(searchKeyData));

			lookupMV = getLookupByLookupCode(ErrorConstants.SEARCH_OPERATOR);
			SearchOperatorData searchOperatorData = new SearchOperatorData();
			if (lookupMV != null) {
				List<LookupValueMV> valueDatas = lookupMV.getLookupValueList();
				for (LookupValueMV lookupValueMV : valueDatas) {
					if (lookupValueMV.getLookupValueCode().equals(ErrorConstants.LIKE)) {
						searchParamater.setSearchOperatorId(lookupValueMV.getLookupValueId());
						searchParamater.setSearchOperatorCode(lookupValueMV.getLookupValueCode());
						List<LookupValueData> data = lookupValueMV.getLookupValueData();
						searchOperatorData.setLanguageId(data.get(0).getLanguageId());
						searchOperatorData.setLanguageCode(data.get(0).getLanguageCode());
						searchOperatorData.setSearchOperatorName(data.get(0).getValueDisplayName());
					}
				}
			}
			searchParamater.setSearchOperatorData(Arrays.asList(searchOperatorData));
			searchParamater.setSearchStartValue("");
			searchParamater.setSearchEndValue("");
			userSearch.setSearchParamaterList(Arrays.asList(searchParamater));
			userSearch.setActiveFlag(true);
			return userSearch;
		}catch (Exception exception) {
			throw new CustomParameterizedException(RegisteredException.USER_SEARCH_EXCEPTION.getException(),
					CustomExceptionCode.FAIL_TO_SAVE_DIAGNOSTIC_USER_SEARCH_DATA.getErrMsg(), exception,
					CustomExceptionCode.FAIL_TO_SAVE_DIAGNOSTIC_USER_SEARCH_DATA.getErrCode());
		}
	}
	/**
	 * @author Devendra.Kumar
	 * @description To get Lookup data by lookup code.
	 * @param lookupCode
	 * @return LookupMV
	 */
	private LookupMV getLookupByLookupCode(String lookupCode) {
		try {
			ResponseEntity<String> lookup = adminRestClient.getLookupByParam(lookupCode);
			String body = lookup.getBody();
			JSONObject userJsonObj = new JSONObject(body);
			String userParseData = userJsonObj.getString("data");
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
			mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
			List<LookupMV> lookupMV = mapper.readValue(userParseData,
					mapper.getTypeFactory().constructCollectionType(List.class, LookupMV.class));
			if (!lookupMV.isEmpty()) {
				return lookupMV.get(0);
			} else {
				return null;
			}
		} catch (Exception exception) {
			throw new CustomParameterizedException(RegisteredException.USER_SEARCH_EXCEPTION.getException(),
					CustomExceptionCode.LOOKUP_ERROR.getErrMsg(), exception,
					CustomExceptionCode.LOOKUP_ERROR.getErrCode());
		}
	}

	/**
	 * @author Devendra.Kumar
	 * @description To get top 5 User search data
	 * @param userId
	 * @return List<UserSearchMV>
	 * @exception CustomParameterizedException
	 */
	@Override
	public List<Map<String,String>> getTopFive(String userId, String providerType) {
		try {
			List<UserSearch> userSearchs = repository.findTopFiveByUserId(userId, providerType);
			Set<String> result = new LinkedHashSet<>();
			boolean resultSize = false;
			for (UserSearch userSearch : userSearchs) {
				for (SearchParamater searchParamater : userSearch.getSearchParamaterList()) {
					if (!searchParamater.getSearchKeyData().isEmpty()
							&& !searchParamater.getSearchKeyData().get(0).getSearchKeyName().trim().equals("")) {
						result.add(searchParamater.getSearchKeyData().get(0).getSearchKeyName());
					}
					if (result.size() == 5) {
						resultSize = true;
						break;
					}
				}
				if (resultSize) {
					break;
				}
			}
			
			List<Map<String,String>> results = new ArrayList<>();
			for(String searchKey : result){
				Map<String,String> resultMap = new HashMap<>();
				resultMap.put("searchKeyData", searchKey);
				results.add(resultMap);
			}
			return results;
		} catch (Exception exception) {
			throw new CustomParameterizedException(RegisteredException.USER_SEARCH_EXCEPTION.getException(),
					CustomExceptionCode.USERSEARCH_GET_TOP5_ERROR.getErrMsg(), exception,
					CustomExceptionCode.USERSEARCH_GET_TOP5_ERROR.getErrCode());
		}
	}

	/**
	 * @author Devendra.Kumar
	 */
	@Override
	public List<String> getPopularSearch(String providerType) {
		List<String> searchCode = new ArrayList<>();
		if (providerType.equalsIgnoreCase(ErrorConstants.DOCTOR_CLINIC)) {
			for (DoctorPopularSearch search : DoctorPopularSearch.values()) {
				searchCode.add(search.getSearch());
			}
		} else if (providerType.equalsIgnoreCase(ErrorConstants.DIAGNOSTIC_CENTER)) {
			for (DiagnosticPopularSearch search : DiagnosticPopularSearch.values()) {
				searchCode.add(search.getSearch());
			}
		}
		return searchCode;
	}

	/**
	 * @author Devendra.Kumar
	 * @description To save doctor UserSearch data in database.
	 * @param userSearchData
	 * @return UserSearchMV
	 */
	@Override
	public UserSearchMV saveDoctorUserSearchData(CustomUserDetails customUserDetails,UserSearchData userSearchData) {
		try {
			/*UserSearch userSearch = getUserSearch(customUserDetails.getId(),userSearchData,ErrorConstants.DOCTOR_CLINIC);
			UserSearch savedUserSearch = repository.save(userSearch);
			return modelMapper.map(savedUserSearch, UserSearchMV.class);*/
			return null;
		} catch (CustomParameterizedException exception) {
			throw exception;
		} catch (Exception exception) {
			throw new CustomParameterizedException(RegisteredException.USER_SEARCH_EXCEPTION.getException(),
					CustomExceptionCode.FAIL_TO_SAVE_DOCTOR_USER_SEARCH_DATA.getErrMsg(), exception,
					CustomExceptionCode.FAIL_TO_SAVE_DOCTOR_USER_SEARCH_DATA.getErrCode());
		}
	}
}
