package com.zoylo.gateway.bll.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zoylo.gateway.bll.UserPermissionBll;
import com.zoylo.gateway.domain.UserPermission;
import com.zoylo.gateway.repository.UserPermissionRepository;


@Service
public class UserPermissionBllImpl implements UserPermissionBll {

	@Autowired
	private UserPermissionRepository userPermissionRepository;

	/**
	 * @author diksha gupta
	 * @description To get all roles and permissions By userId.
	 * @param
	 */
	@Override
	public List<UserPermission> findPermissionListByUserId(String userId) {
		return userPermissionRepository.findByUserId(userId);
	}

	

}
