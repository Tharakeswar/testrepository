package com.zoylo.gateway.bll.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twilio.Twilio;
import com.twilio.jwt.accesstoken.AccessToken;
import com.twilio.jwt.accesstoken.ChatGrant;
import com.zoylo.gateway.bll.AuditLogService;
import com.zoylo.gateway.bll.MobileTokenBll;
import com.zoylo.gateway.domain.LookupMV;
import com.zoylo.gateway.domain.LookupValueData;
import com.zoylo.gateway.domain.LookupValueMV;
import com.zoylo.gateway.domain.Permission;
import com.zoylo.gateway.domain.PermissionData;
import com.zoylo.gateway.domain.PhoneInfo;
import com.zoylo.gateway.domain.TokenData;
import com.zoylo.gateway.domain.User;
import com.zoylo.gateway.domain.ZoyloUserToken;
import com.zoylo.gateway.domain.history.UserHistory;
import com.zoylo.gateway.model.MobileVerificationVM;
import com.zoylo.gateway.model.PermissionsListVM;
import com.zoylo.gateway.model.RecipientPermissionListVM;
import com.zoylo.gateway.model.UserProfileVM;
import com.zoylo.gateway.mv.TokenVM;
import com.zoylo.gateway.repository.CustomRepository;
import com.zoylo.gateway.repository.PermissionRepository;
import com.zoylo.gateway.repository.UserRepository;
import com.zoylo.gateway.repository.ZoyloUserTokenRepository;
import com.zoylo.gateway.security.CustomPermission;
import com.zoylo.gateway.security.CustomUserDetails;
import com.zoylo.gateway.security.jwt.TokenProvider;
import com.zoylo.gateway.service.util.Constants;
import com.zoylo.gateway.web.rest.client.AdminRestClient;
import com.zoylo.gateway.web.rest.client.RecipientRestClient;
import com.zoylo.gateway.web.rest.errors.CustomExceptionCode;
import com.zoylo.gateway.web.rest.errors.CustomParameterizedException;
import com.zoylo.gateway.web.rest.errors.RegisteredException;
import com.zoylo.gateway.web.rest.errors.RegisteredExceptions;
import com.zoylo.gateway.web.rest.util.LookUp;

/**
 * @author Mehraj Malik
 * @version 1.0
 *
 */
@Service
public class MobileTokenBllImpl implements MobileTokenBll {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private RecipientRestClient recipientRestClient;

	@Autowired
	private PermissionRepository permissionRepository;

	@Autowired
	private AuditLogService auditLogService;

	@Autowired
	private CustomRepository customRepository;

	@Autowired
	private TokenProvider tokenProvider;
	
	@Autowired
	private AdminRestClient adminRestClient;

	@Value("${app.otpExpirationMinutes}")
	private int oPTExpirationMinutes;
	
	

	
	
	@Autowired
	private ZoyloUserTokenRepository zoyloUserTokenRepository;

	@Override
	public Map<String, String> getMobileToken(MobileVerificationVM mobileVerificationVM) {
		try {
			User user = userRepository.findOneById(mobileVerificationVM.getId());
			if (user == null) {
				throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
						CustomExceptionCode.USER_NOT_FOUND.getErrMsg(),
						CustomExceptionCode.USER_NOT_FOUND.getErrCode());
			}
			Map<String, String> map=new HashMap<>();
			// to maintain history
			UserHistory history = new UserHistory();
			modelMapper.map(user, history);
			history.setId(null);
			history.setUserId(user.getId());

			if (String.valueOf(mobileVerificationVM.getOtp()).length() != 6) {
				throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
						CustomExceptionCode.OTP_LENGTH.getErrMsg(), CustomExceptionCode.OTP_LENGTH.getErrCode());
			}

			PhoneInfo phoneInfo = user.getPhoneInfo();
			if (phoneInfo.getOtp() != mobileVerificationVM.getOtp()) {
				throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
						CustomExceptionCode.OTP_NOT_MATCH.getErrMsg(), CustomExceptionCode.OTP_NOT_MATCH.getErrCode());
			}

			/*if (((new Date().getTime() / 60000)
					- (phoneInfo.getOtpSentOn().getTime() / 60000)) > oPTExpirationMinutes) {
				throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
						CustomExceptionCode.OTP_EXPIRED.getErrMsg(), CustomExceptionCode.OTP_EXPIRED.getErrCode());
			}*/
			// to save history
			auditLogService.auditActionLog(Constants.MOBILE_VERIFICATION, Constants.ZOYLO_USER);
			customRepository.save(history, Constants.ZOYLO_USER_HISTORY);
			phoneInfo.setVerifiedFlag(true);
			phoneInfo.setVerifiedOn(new Date());
			phoneInfo.setResendOtpCount(Constants.DEFAULT_RESEND_OTP_COUNT);
			user.setPhoneInfo(phoneInfo);
			User savedUser = userRepository.save(user);

			UserProfileVM userProfileVM = new UserProfileVM();
			userProfileVM.setFirstName(user.getFirstName());
			userProfileVM.setLastName(user.getLastName());
			userProfileVM.setZoyloId(user.getZoyloId());
			userProfileVM.setUserId(user.getId());
			/*set gender for doctor*/
			ResponseEntity<String> resourceData=adminRestClient.getGenderByUserId(mobileVerificationVM.getId());
			String body = resourceData.getBody();
			JSONObject userJsonObj = new JSONObject(body);
			String gender = (String) userJsonObj.getString(Constants.DATA);
			if(gender!=null) {
				userProfileVM.setGender(gender);
			}

			List<Permission> permissions = permissionRepository
					.findByAuthCategoryCode(LookUp.RECIPIENT.getLookUpValue());
			RecipientPermissionListVM recipientpermissionListVM = new RecipientPermissionListVM();
			recipientpermissionListVM.setRecipientZoyloId(user.getZoyloId());
			recipientpermissionListVM.setRecipientId(user.getId());

			List<PermissionsListVM> permissionsListVMs = new ArrayList<>();
			for (Permission permission : permissions) {
				PermissionsListVM permissionsListVM = new PermissionsListVM();
				permissionsListVM.setPermissionCode(permission.getPermissionCode());
				permissionsListVM.setPermissionId(permission.getId());
				List<com.zoylo.gateway.model.PermissionData> permissionDatas = new ArrayList<>();
				if (permission.getPermissionData() != null) {
					for (PermissionData permissionData : permission.getPermissionData()) {
						com.zoylo.gateway.model.PermissionData recPermissionData = new com.zoylo.gateway.model.PermissionData();
						recPermissionData.setLanguageCode(permissionData.getLanguageCode());
						recPermissionData.setLanguageId(permissionData.getLanguageId());
						recPermissionData.setPermissionName(permissionData.getPermissionName());
						permissionDatas.add(recPermissionData);
					}
				}
				permissionsListVM.setPermissionData(permissionDatas);
				permissionsListVMs.add(permissionsListVM);
			}
			recipientpermissionListVM.setPermissionsList(permissionsListVMs);
			userProfileVM.setGrantedPermissionsList(recipientpermissionListVM);
			try {
				recipientRestClient.setUserProfile(userProfileVM);
			
			} catch (Exception exception) {

				throw new CustomParameterizedException(RegisteredExceptions.REGISTRATION_EXCEPTION.getException(),
						CustomExceptionCode.FAIL_TO_CREATE_USER_PROFILE.getErrMsg(), exception,
						CustomExceptionCode.FAIL_TO_CREATE_USER_PROFILE.getErrCode());
			}
			// send mobile token
			
			map.put("token", getMobileToken(savedUser));
			return map;
		} catch (CustomParameterizedException e) {
			throw e;
		} catch (AuthenticationException exception) {
			throw new CustomParameterizedException(RegisteredException.LOGIN_EXCEPTION.getException(),
					CustomExceptionCode.LOGIN_FAIL.getErrMsg(), exception, CustomExceptionCode.LOGIN_FAIL.getErrCode());
		} catch (Exception exception) {
			throw new CustomParameterizedException(RegisteredException.REGISTRATION_EXCEPTION.getException(),
					CustomExceptionCode.DB_ERROR.getErrMsg(), exception, CustomExceptionCode.DB_ERROR.getErrCode());
		}
	}

	/**
	 * Provides a JWT Token
	 * 
	 * @author Mehraj Malik
	 */
	private String getMobileToken(User savedUser) {
		Set<String> authCategoryList = new HashSet<>();
		authCategoryList.add(LookUp.RECIPIENT.getLookUpValue());
		String id = savedUser.getId();
		String email = savedUser.getEmailInfo().getEmailAddress();
		CustomUserDetails userDetails = new CustomUserDetails(id, email, savedUser.getEncryptedPassword(),
				recipientPermission(id), savedUser.getPhoneInfo().getPhoneNumber(), false, authCategoryList);
		return tokenProvider.createToken(userDetails, LookUp.RECIPIENT.getLookUpValue());
	}

	/**
	 * 
	 * @author Mehraj Malik
	 */
	private List<CustomPermission> recipientPermission(String userId) {
		List<CustomPermission> customPermissionList = new ArrayList<>();
		try {
			ResponseEntity<String> userPermission = recipientRestClient.getUserPermission(userId);
			String body = userPermission.getBody();
			JSONObject userJsonObj = new JSONObject(body);
			String userParseData = userJsonObj.getString("data");
			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
			List<RecipientPermissionListVM> recipientPermissions = mapper.readValue(userParseData,
					mapper.getTypeFactory().constructCollectionType(List.class, RecipientPermissionListVM.class));
			for (RecipientPermissionListVM listVM : recipientPermissions) {
				for (PermissionsListVM permissionsListVM : listVM.getPermissionsList()) {
					CustomPermission customPermission = new CustomPermission();
					customPermission.setPermissionCode(permissionsListVM.getPermissionCode());
					customPermission.setPermissionId(permissionsListVM.getPermissionId());
					customPermissionList.add(customPermission);
				}
			}
		} catch (Exception e) {
			throw new CustomParameterizedException(RegisteredException.LOGIN_EXCEPTION.getException(),
					CustomExceptionCode.PERMISSION_NOT_FOUND_ERROR.getErrMsg(),
					CustomExceptionCode.PERMISSION_NOT_FOUND_ERROR.getErrCode());
		}
		return customPermissionList;

	}

}
