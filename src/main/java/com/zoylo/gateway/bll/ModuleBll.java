package com.zoylo.gateway.bll;

import java.util.List;

import com.zoylo.gateway.model.ModuleDataVM;
import com.zoylo.gateway.model.ModuleEditVM;
import com.zoylo.gateway.model.ModuleVM;
import com.zoylo.gateway.mv.ModuleMV;

/**
 * @author damini.arora
 * @version 1.0
 *
 */
public interface ModuleBll {

	public ModuleMV saveModule(ModuleVM moduleModel, String languageCode);

	public List<ModuleDataVM> getAllModule();

	public ModuleDataVM getByModuleCode(String getByModuleCode);

	public ModuleMV updateModule(ModuleEditVM moduleModel);

	public Boolean deleteById(String id);

}
