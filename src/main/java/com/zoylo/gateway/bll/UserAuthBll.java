package com.zoylo.gateway.bll;

import com.zoylo.gateway.domain.ZoyloUserAuth;
import com.zoylo.gateway.model.UserRoleDataVM;

/**
 * @author nagaraju
 * Interface for user auth.
 */
public interface UserAuthBll {
	public ZoyloUserAuth addRole(UserRoleDataVM userRoleDataVM);
	public ZoyloUserAuth updateRole(UserRoleDataVM userRoleDataVM);
}
