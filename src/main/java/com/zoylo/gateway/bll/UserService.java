package com.zoylo.gateway.bll;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.zoylo.gateway.config.Constants;
import com.zoylo.gateway.domain.Authority;
import com.zoylo.gateway.domain.User;
import com.zoylo.gateway.repository.AuthorityRepository;
import com.zoylo.gateway.repository.UserRepository;
import com.zoylo.gateway.security.AuthoritiesConstants;
import com.zoylo.gateway.security.SecurityUtils;
import com.zoylo.gateway.service.dto.UserDTO;
import com.zoylo.gateway.service.util.RandomUtil;

/**
 * Service class for managing users.
 */
@Service
public class UserService {

	private final Logger log = LoggerFactory.getLogger(UserService.class);

	private final UserRepository userRepository;

	private final PasswordEncoder passwordEncoder;

	private final AuthorityRepository authorityRepository;

	public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder,
			AuthorityRepository authorityRepository) {
		this.userRepository = userRepository;
		this.passwordEncoder = passwordEncoder;
		this.authorityRepository = authorityRepository;
	}

	public Optional<User> activateRegistration(String key) {
		log.debug("Activating user for activation key {}", key);
		
		return Optional.empty();
	}

	public Optional<User> completePasswordReset(
			String newPassword/* , String key */) {
		log.debug("Reset user password for reset key {}",newPassword);

		
		return Optional.empty();
	}

	public Optional<User> requestPasswordReset(String mail) {
		return userRepository.findByEmailAddress(mail).filter(User::isActiveFlag).map(user -> {
			
			userRepository.save(user);
			return user;
		});
	}

	public User createUser(String login, String password, String firstName, String lastName, String email,
			String imageUrl, String langKey) {

		User newUser = new User();
		Authority authority = authorityRepository.findOne(AuthoritiesConstants.USER);
		Set<Authority> authorities = new HashSet<>();
		String encryptedPassword = passwordEncoder.encode(password);
		//newUser.setUserName(login);
		newUser.setEncryptedPassword(encryptedPassword);
		newUser.setFirstName(firstName);
		newUser.setLastName(lastName);
		newUser.getEmailInfo().setEmailAddress(email);
		newUser.setActiveFlag(false);
		authorities.add(authority);
		newUser.setAuthorities(authorities);
		userRepository.save(newUser);
		log.debug("Created Information for User: {} {} {}", newUser,imageUrl,langKey);
		return newUser;
	}

	public User createUser(UserDTO userDTO) {
		User user = new User();
		//user.setUserName(userDTO.getLogin());
		user.getEmailInfo().setEmailAddress(userDTO.getEmail());
		
		if (userDTO.getAuthorities() != null) {
			Set<Authority> authorities = new HashSet<>();
			userDTO.getAuthorities().forEach(authority -> authorities.add(authorityRepository.findOne(authority)));
			user.setAuthorities(authorities);
		}
		String encryptedPassword = passwordEncoder.encode(RandomUtil.generatePassword());
		user.setEncryptedPassword(encryptedPassword);
		
		user.setActiveFlag(true);
		userRepository.save(user);
		log.debug("Created Information for User: {}", user);
		return user;
	}

	/**
	 * Update basic information (first name, last name, email, language) for the
	 * current user.
	 *
	 * @param firstName
	 *            first name of user
	 * @param lastName
	 *            last name of user
	 * @param email
	 *            email id of user
	 * @param langKey
	 *            language key
	 * @param imageUrl
	 *            image URL of user
	 */
	public void updateUser(String firstName, String lastName, String email, String langKey, String imageUrl) {
		userRepository.findOneByUserName(SecurityUtils.getCurrentUserLogin()).ifPresent(user -> {
			 user.setFirstName(firstName);
			 user.setLastName(lastName);
			user.getEmailInfo().setEmailAddress(email);
			userRepository.save(user);
			log.debug("Changed Information for User: {} {} {}", user,langKey,imageUrl);
		});
	}

	/**
	 * Update all information for a specific user, and return the modified user.
	 *
	 * @param userDTO
	 *            user to update
	 * @return updated user
	 */
	public Optional<UserDTO> updateUser(UserDTO userDTO) {
		return Optional.of(userRepository.findOne(userDTO.getId())).map(user -> {
			//user.setUserName(userDTO.getLogin());
			
			user.getEmailInfo().setEmailAddress(userDTO.getEmail());
			user.setActiveFlag(userDTO.isActivated());
			Set<Authority> managedAuthorities = user.getAuthorities();
			managedAuthorities.clear();
			userDTO.getAuthorities().stream().map(authorityRepository::findOne).forEach(managedAuthorities::add);
			userRepository.save(user);
			log.debug("Changed Information for User: {}", user);
			return user;
		}).map(UserDTO::new);
	}


	public void changePassword(String password) {
		userRepository.findOneByUserName(SecurityUtils.getCurrentUserLogin()).ifPresent(user -> {
			String encryptedPassword = passwordEncoder.encode(password);
			user.setEncryptedPassword(encryptedPassword);
			userRepository.save(user);
			log.debug("Changed password for User: {}", user);
		});
	}

	public Page<UserDTO> getAllManagedUsers(Pageable pageable) {
		return userRepository.findAllByUserNameNot(pageable, Constants.ANONYMOUS_USER).map(UserDTO::new);
	}

	public Optional<User> getUserWithAuthoritiesByLogin(String login) {
		return userRepository.findOneByUserName(login);
	}

	public User getUserWithAuthorities(String id) {
		return userRepository.findOne(id);
	}

	public User getUserWithAuthorities() {
		return userRepository.findOneByUserName(SecurityUtils.getCurrentUserLogin()).orElse(null);
	}

	/**
	 * Not activated users should be automatically deleted after 3 days.
	 * <p>
	 * This is scheduled to get fired everyday, at 01:00 (am).
	 */
	//@Scheduled(cron = "0 0 1 * * ?")
	public void removeNotActivatedUsers() {
		/*List<User> users = userRepository
				.findAllByActiveFlagIsFalseAndCreatedDateBefore(Instant.now().minus(3, ChronoUnit.DAYS));
		for (User user : users) {
			log.debug("Deleting not activated user {}", user.getPhoneInfo().getPhoneNumber());
			userRepository.delete(user);
		}*/
	}

	/**

	 * @return a list of all the authorities
	 */
	public List<String> getAuthorities() {
		return authorityRepository.findAll().stream().map(Authority::getName).collect(Collectors.toList());
	}
}

