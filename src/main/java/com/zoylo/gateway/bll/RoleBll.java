package com.zoylo.gateway.bll;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.zoylo.gateway.model.PageableVM;
import com.zoylo.gateway.model.RoleEditVM;
import com.zoylo.gateway.model.RoleVM;
import com.zoylo.gateway.model.ZoyloPermissionVM;
import com.zoylo.gateway.model.ZoyloRoleVM;
import com.zoylo.gateway.mv.RoleMV;

/**
 * 
 * @author Diksha Gupta
 * @version 1.0
 * @description User role management
 *
 */
public interface RoleBll {
	public List<RoleMV> getall();

	public List<RoleMV> getRole(String roleCode, String authCategoryCode);

	public RoleMV update(RoleEditVM roleEditModel, String languageCode);

	public Boolean delete(String roleId);

	public RoleMV save(RoleVM roleModel, String languageCode);
	
    public RoleMV saveRole(ZoyloRoleVM roleModel,String languageCode);
	
	public RoleMV assignPermission(ZoyloPermissionVM permissionVM,String languageCode);
	
	public PageableVM<RoleMV> searchRoles(Pageable pageable, String roleCode, String roleName, String authCategoryCode,
			String authCategoryName, boolean activeFlag, String languageCode);
 
	public RoleMV updateRole(ZoyloRoleVM roleModel,String languageCode);

	/**
	 * @author Balram Sharma
	 * @description To update permission for given role.
	 * @param roleModel
	 * @return RoleMV
	 */
	public RoleMV updateRolePermission(ZoyloPermissionVM permissionModel);
	/**
	 * @author Balram Sharma
	 * @description To get Role by id
	 * @param roleId
	 * @return RoleMV
	 */
	public RoleMV getRoleById(String roleId);

}
