package com.zoylo.gateway.bll;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestBody;

import com.zoylo.core.mobilepush.SendPushVM;
import com.zoylo.gateway.domain.PhoneInfo;
import com.zoylo.gateway.domain.ServiceProvidersList;
import com.zoylo.gateway.domain.User;
import com.zoylo.gateway.domain.ZoyloUserAuth;
import com.zoylo.gateway.model.ChangeMobileEmailVM;
import com.zoylo.gateway.model.DiagnosticVM;
import com.zoylo.gateway.model.DoctorRoleDataVM;
import com.zoylo.gateway.model.DoctorVM;
import com.zoylo.gateway.model.GhostUserFamilyMemberVM;
import com.zoylo.gateway.model.GhostUserVM;
import com.zoylo.gateway.model.MobileVerificationVM;
import com.zoylo.gateway.model.OTPDetailVM;
import com.zoylo.gateway.model.OrganizationUserDataVM;
import com.zoylo.gateway.model.PMSUserRoleMV;
import com.zoylo.gateway.model.PMSUserRoleMVList;
import com.zoylo.gateway.model.PageableVM;
import com.zoylo.gateway.model.PasswordUpdateVM;
import com.zoylo.gateway.model.RecipientVM;
import com.zoylo.gateway.model.RegisterVM;
import com.zoylo.gateway.model.ResendOTPVM;
import com.zoylo.gateway.model.SocialMediaLoginVM;
import com.zoylo.gateway.model.SocialMediaVM;
import com.zoylo.gateway.model.UpdateRegisterVM;
import com.zoylo.gateway.model.UserDataVM;
import com.zoylo.gateway.model.UserDetailVM;
import com.zoylo.gateway.model.UserPMSDataVM;
import com.zoylo.gateway.model.UserProfileDataVM;
import com.zoylo.gateway.model.UserResponseVM;
import com.zoylo.gateway.model.UserTypeVM;
import com.zoylo.gateway.model.UserUpdateResponceVM;
import com.zoylo.gateway.model.UserUpdateVM;
import com.zoylo.gateway.model.ZoyloAuthVM;
import com.zoylo.gateway.model.ZoyloUserVM;
import com.zoylo.gateway.mv.UserAutoSuggestionMV;
import com.zoylo.gateway.mv.AuthVM;
import com.zoylo.gateway.mv.ProviderAdminDataMV;
import com.zoylo.gateway.mv.UserSuggestionMV;
import com.zoylo.gateway.mv.UserVM;

import freemarker.template.TemplateException;

/**
 * 
 * @author Ankur
 * @version 1.0
 * @description User Registration service
 *
 */
public interface UserBll {
	public User registerUser(RegisterVM registerModel);

	public List<UserTypeVM> getRecipentUserType();

	public List<UserTypeVM> getAdminUserType();

	public void deleteUser(String userId);

	public User updateUser(UpdateRegisterVM registerModel);

	public void mobileVerification(MobileVerificationVM mobileVerificationVM);

	public PageableVM<UserDetailVM> getAllByPage(Pageable pageable);

	public User setMobileAndEmailId(ChangeMobileEmailVM changeMobileEmailVM);

	public void saveDoctorDetail(DoctorVM doctorVM);

	public void saveDiagnosticDetail(DiagnosticVM diagonsticVM);

	public boolean validateToken(String token);

	public GhostUserVM addGhostUser(GhostUserVM ghostUserVM);

	public GhostUserFamilyMemberVM addGhostUserFamilyMember(GhostUserFamilyMemberVM ghostUserFamilyMemberVM);

	public UserProfileDataVM getUser(String id);

	public UserProfileDataVM getUserData(String email, String mobile);

	public String resendOTP(ResendOTPVM otpvm);

	public UserResponseVM getUserByMobile(RegisterVM registerModel);

	public UserPMSDataVM savePMSUser(UserDataVM userDataVM);

	public UserProfileDataVM getWalkinUserData(String search);

	public PageableVM<User> getByDeviceType(String deviceType, Pageable pageable);

	public PageableVM<UserDetailVM> getAllByName(Pageable pageable, String firstName);

	public void sendNotifiation(SendPushVM sendPushVM);

	public UserUpdateResponceVM updateUserEmailAndMobile(RegisterVM registerVM);

	public ServiceProvidersList setRole(List<String> roleCodes, UserDataVM userDataVM);

	void setDoctorRole(DoctorRoleDataVM userDataVM);

	public UserUpdateResponceVM updateUserEmailAndMobileIfNotExist(RegisterVM registerVM);
	
	public UserUpdateResponceVM updateUserEmailAndMobileAndUserType(RegisterVM registerVM);
	
	public void updatePassword(PasswordUpdateVM passwordUpdateVM);

	public PMSUserRoleMVList getPMSUserRole(PMSUserRoleMVList pMSUserRoleMVList);

	public ProviderAdminDataMV getProviderUserDetail(String serviceProviderId, String providerTypeCode,
			String roleCode);

	public PMSUserRoleMVList getProviderUserDetail(String serviceProviderId, String roleCode);
	
	public User updateUser(UserUpdateVM userUpdateVM);

	public PageableVM<UserVM> getAll(Pageable pageable,String serachField);

	public UserVM update(UserVM userVM);
	
	public OrganizationUserDataVM fetchOrganizationUserData(String id);

	public UserSuggestionMV autoSuggestion(String value);

	public UserVM createUser(ZoyloUserVM zoyloUserVM);

	public UserVM updateUser(ZoyloUserVM zoyloUserVM);

	public PageableVM<ZoyloUserVM> search(Pageable pageable, boolean activeFlag, boolean isZoyloEmployee,
			String firstName, String lastName, String emailId, String mobileNumber);

	public List<UserAutoSuggestionMV> getIdAndName(String value);
	
	public ZoyloUserVM fetchUserRoleDetail (String id);

	public User getUserByPhoneNumber(String phoneNumber);

	public User getUserByEmail(String email);

	public List<User> fetchUserList (Boolean activeFlag, Boolean zoyloEmployee);
	
	/**
	 * @author RakeshH
	 * Returns <code>true</code> if verifyToken is valid
	 * @param verifyToken
	 * @param emailAddress
	 * @return
	 */
    public Boolean verifyEmailToken(String verifyToken , String emailAddress);
    
	/**
	 * @author RakeshH
	 * Generates token for the given <code>emailAddress</code> if present
	 * @param verifyToken
	 * @param emailAddress
	 * @return
	 * @throws IOException 
	 * @throws TemplateException 
	 */
    public void generateEmailToken(String emailAddress);

	public PMSUserRoleMV getPracticeLocationRole(String serviceproviderid,String userId);
	public List<AuthVM> getUserRoleByProviderId(String serviceProviderId, List<String> userId);
	public ZoyloUserAuth updateRoles(String userId, String providerId, String roleCode);

	public Map<String,String> getUserIdByPhoneNumber(List<String> phoneNumbers);

	public String emailCommonTemplate(String body);

	public User registerUserForCampaign(RegisterVM registerVM);

	public String populateUserToEcomm(Date startDate, Date endDate);
	
	public User registerUserIfNotPresentForCampaign(RegisterVM registerVM);

	public User updateUserForCampaign(OTPDetailVM otpDetail);

	public Boolean checkEmailVerified(String id);
	public Map<String, String> ecommSignup(@RequestBody RecipientVM recipientVm);
	
	public GhostUserVM addGhostUserInDiagnostics(GhostUserVM ghostUserVM);

    
}
