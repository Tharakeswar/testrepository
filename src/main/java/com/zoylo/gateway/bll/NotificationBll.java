package com.zoylo.gateway.bll;

import com.zoylo.core.emailservice.EmailTemplate;
import com.zoylo.core.smsservice.SendSmsVM;
import com.zoylo.gateway.domain.User;

public interface NotificationBll {
	public void saveNotificationAfterEmail(User user, EmailTemplate emailTemplate, String messageId);

	public void saveNotificationAfterSMS(User user, SendSmsVM sendSmsVM, String messageId);
}
