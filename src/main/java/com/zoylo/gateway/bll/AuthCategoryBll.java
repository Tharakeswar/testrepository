package com.zoylo.gateway.bll;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.zoylo.gateway.model.AuthCategoryNameList;
import com.zoylo.gateway.model.PageableVM;
import com.zoylo.gateway.model.ZoyloAuthCategoryVM;
import com.zoylo.gateway.mv.ZoyloAuthCategoryMV;
/**
 * 
 * @author Balram Sharma
 * @version 1.0
 * @description That is used to declare business method of AuthCategory
 *
 */
public interface AuthCategoryBll {
	/**
	 * @author Balram Sharma
	 * @description To save AuthCategory
	 * @param zoyloAuthCategoryVM
	 * @return ZoyloAuthCategoryMV
	 */
	ZoyloAuthCategoryMV saveAuthCategory(ZoyloAuthCategoryVM zoyloAuthCategoryVM);

	/**
	 * @author Balram Sharma
	 * @description To Update AuthCategory
	 * @param zoyloAuthCategoryVM
	 * @return ZoyloAuthCategoryMV
	 */
	ZoyloAuthCategoryMV updateAuthCategory(ZoyloAuthCategoryVM zoyloAuthCategoryVM);

	/**
	 * @author Balram Sharma
	 * @description filtering zoyloAuthCategory by using LanguageCode
	 * @param languageCode
	 * @return List<AuthCategoryNameList>
	 */
	List<AuthCategoryNameList> findAllAuthCode(String languageCode);
	/**
	 * @author Balram Sharma
	 * @description To getAuthCategory by id
	 * @param id
	 * @return ZoyloAuthCategoryMV
	 */
	ZoyloAuthCategoryMV getAuthCategoryById(String id);
	/**
	 * @author Balram Sharma
	 * @description search ZoyloAuthCategory by using authCategoryCode & authCategoryName,
	 *              activeFlag.
	 * @param authCategoryCode
	 * @param authCategoryName
	 * @param activeFlag
	 * @param languageCode
	 * @return PageableVM<ZoyloAuthCategoryMV> 
	 */
	PageableVM<ZoyloAuthCategoryMV> searchAuthCategory(Pageable pageable, String authCategoryCode,
			String authCategoryName, boolean activeFlag, String languageCode);

}
