package com.zoylo.gateway.bll;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.zoylo.gateway.model.PageableVM;
import com.zoylo.gateway.model.PermissionEditVM;
import com.zoylo.gateway.model.PermissionVM;
import com.zoylo.gateway.model.PermissionZoyloVM;
import com.zoylo.gateway.mv.PermissionMV;

/**
 * 
 * @author Diksha Gupta
 * @version 1.0
 * @description User permission management
 *
 */
public interface PermissionBll {
	public PermissionMV savePermission(PermissionVM permissionModel, String languageCode);

	public List<PermissionMV> getAllPermission();

	public PermissionMV get(String permissionCode);

	public PermissionMV updatePermission(PermissionEditVM permissionModel, String languageCode);

	public Boolean deletePermission(String id);

	public List<PermissionMV> getPermissionByAuthCode(String authCode);
	/**
	 * @author Balram Sharma
	 * @description To save Permission
	 * @param PermissionMV
	 * @return PermissionMV
	 */
	public PermissionMV savePermissionData(PermissionZoyloVM permissionZoyloVM);
	/**
	 * @author Balram Sharma
	 * @description To update existing Permission
	 * @param PermissionZoyloVM
	 * @return PermissionMV
	 */
	public PermissionMV updatePermissionData(PermissionZoyloVM permissionZoyloVM);
	/**
	 * @author Balram Sharma
	 * @description search Permission by using permissionCode & permissionName, activeFlag, authCategoryCode
	 * @param permissionCode
	 * @param permissionName
	 * @param activeFlag
	 * @param authCategory
	 * @param languageCode
	 * @return PageableVM<PermissionMV>
	 */
	public PageableVM<PermissionMV> searchPermissionProperties(Pageable pageable,
			String permissionCode, String permissionName, boolean activeFlag, String authCategory, String languageCode);
	/**
	 * @author Balram Sharma
	 * @description To get Permission by id
	 * @param id
	 * @return PermissionMV
	 */
	public PermissionMV getPermissionById(String permissionId);

}
