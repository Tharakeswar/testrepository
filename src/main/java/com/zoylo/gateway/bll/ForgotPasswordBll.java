package com.zoylo.gateway.bll;

import java.util.Map;

import com.zoylo.gateway.model.ForgotPasswordOtpVM;
import com.zoylo.gateway.model.ForgotPasswordVM;
import com.zoylo.gateway.model.GhostResetPasswordVM;
import com.zoylo.gateway.model.MobileVerificationVM;

public interface ForgotPasswordBll {

	public void sendForgotPasswordMail(String emnailId);

	public String sendForgotPasswordOtp(String mobileNumebr);

	public Map<String, String> verifyOtp(MobileVerificationVM mobileVerificationVM);

	public Map<String, String> setNewPassworUsingOtp(ForgotPasswordOtpVM forgotPasswordOtpVM);

	public Map<String, String> validateToken(String authToken);

	public Map<String, String> setNewPassword(ForgotPasswordVM forgotPasswordVM);

	public Map<String, String> setGhostNewPassword(GhostResetPasswordVM ghostResetPasswordVM);

}
