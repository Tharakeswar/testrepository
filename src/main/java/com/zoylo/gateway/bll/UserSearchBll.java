package com.zoylo.gateway.bll;

import java.util.List;
import java.util.Map;

import com.zoylo.gateway.model.UserSearchData;
import com.zoylo.gateway.mv.UserSearchMV;
import com.zoylo.gateway.security.CustomUserDetails;

/**
 * 		
 * @author Devendra.Kumar
 * @version 1.0
 *
 */
public interface UserSearchBll {

	public UserSearchMV saveDiagnosticUserSearchData(CustomUserDetails customUserDetails,UserSearchData userSearch);
	
	public List<Map<String,String>> getTopFive(String userId,String providerType);

	public List<String> getPopularSearch(String providerType);

	public UserSearchMV saveDoctorUserSearchData(CustomUserDetails customUserDetails,UserSearchData userSearch);
}
