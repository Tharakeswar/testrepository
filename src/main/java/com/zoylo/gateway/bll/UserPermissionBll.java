package com.zoylo.gateway.bll;

import java.util.List;

import com.zoylo.gateway.domain.UserPermission;



public interface UserPermissionBll {

	public List<UserPermission> findPermissionListByUserId(String userId);

}
