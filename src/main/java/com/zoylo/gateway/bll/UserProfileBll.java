package com.zoylo.gateway.bll;

import com.zoylo.gateway.model.ChangePasswordVM;
import com.zoylo.gateway.model.PasswordErrorVM;

/***
 * @author arvind.rawat
 * @Version 1.0
 *
 */
public interface UserProfileBll {

public PasswordErrorVM changePassword(String userId, ChangePasswordVM changePasswordVM);

}
