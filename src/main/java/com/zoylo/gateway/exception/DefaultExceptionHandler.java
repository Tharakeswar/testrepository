package com.zoylo.gateway.exception;
import java.util.Collection;
import java.util.Map;

import javax.validation.ConstraintViolationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.zoylo.gateway.service.util.FieldError;
import com.zoylo.gateway.service.util.LemonUtil;



/**
 * Exception handlers
 * 
 * 
 */
@RestControllerAdvice
@RequestMapping(produces = "application/json")
public class DefaultExceptionHandler {
	
    private final Log log = LogFactory.getLog(getClass());

    
	/**
	 * Handles constraint violation exceptions
	 * 
	 * @param ex the exception
	 * @return the error response
	 */
    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(value = HttpStatus.OK)
    public Map<String, Object> handleConstraintViolationException(ConstraintViolationException ex) {
    	
		Collection<FieldError> errors = FieldError.getErrors(ex.getConstraintViolations());
		
		return LemonUtil.mapOf("exception", "ConstraintViolationException", "errors", errors);
    }
	
    /**
	 * Handles user data exceptions
	 * 
	 * @param ex the exception
	 * @return the error response
	 */
    @ExceptionHandler(UserDataException.class)
    @ResponseStatus(value = HttpStatus.OK)
    public Map<String, Object> handleUserDataException(UserDataException exception) {
        log.warn("UserDataException:", exception);        
		return LemonUtil.mapOf("exception", "UserDataException", "code", exception.getUserDataCode().getErrCode());
    }
    
}
