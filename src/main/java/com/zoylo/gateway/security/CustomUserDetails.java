package com.zoylo.gateway.security;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.StringUtils;

public class CustomUserDetails implements UserDetails {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String id;
	private final String username;
	private final String password;
	private final String mobileNumber;
	// private final List<CustomRole> role;
	private final List<CustomPermission> permission;
	private final Boolean isZoyloEmployee;
	private final Set<String> authCategoryList;

	public CustomUserDetails(String id, String lowercaseLogin, String password, List<CustomPermission> permission,
			String mobileNumber, Boolean isZoyloEmployee, Set<String> authCategoryList) {
		this.id = id;
		this.username = lowercaseLogin;
		this.password = password;
		this.permission = permission;
		this.mobileNumber = mobileNumber;
		this.isZoyloEmployee = isZoyloEmployee;
		this.authCategoryList = authCategoryList;
	}

	public String getId() {
		return id;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		List<String> permissionList = permission.stream().map(CustomPermission::getPermissionCode)
				.collect(Collectors.toList());
		String permissions = StringUtils.collectionToCommaDelimitedString(permissionList);
		return AuthorityUtils.commaSeparatedStringToAuthorityList(permissions);
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	public Boolean isZoyloEmployee() {
		return isZoyloEmployee;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public List<CustomPermission> getPermission() {
		return permission;
	}

	public Set<String> getAuthCategoryList() {
		return authCategoryList;
	}

}
