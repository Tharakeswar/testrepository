package com.zoylo.gateway.security;

import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Utility class for Spring Security.
 */
public final class SecurityUtils {

    private SecurityUtils() {
    }

    /**
     * Get the login of the current user.
     *
     * @return the login of the current user
     */
    public static String getCurrentUserLogin() {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication authentication = securityContext.getAuthentication();
        String userName = null;
        if (authentication != null) {
            if (authentication.getPrincipal() instanceof UserDetails) {
                UserDetails springSecurityUser = (UserDetails) authentication.getPrincipal();
                userName = springSecurityUser.getUsername();
            } else if (authentication.getPrincipal() instanceof String) {
                userName = (String) authentication.getPrincipal();
            }
        }
        return userName;
    }
    
    public static String getCurrentUserId() {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication authentication = securityContext.getAuthentication();
        String userName = null;
        if (authentication != null) {
            if (authentication.getPrincipal() instanceof UserDetails) {
                CustomUserDetails springSecurityUser = (CustomUserDetails) authentication.getPrincipal();
                userName = springSecurityUser.getId();
            } else if (authentication.getPrincipal() instanceof String) {
                userName = (String) authentication.getPrincipal();
            }
        }
        return userName;
    }
    
        

    /**
     * Get the JWT of the current user.
     *
     * @return the JWT of the current user
     */
    public static String getCurrentUserJWT() {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication authentication = securityContext.getAuthentication();
        if (authentication != null && authentication.getCredentials() instanceof String) {
            return (String) authentication.getCredentials();
        }
        return null;
    }

    /**
     * Check if a user is authenticated.
     *
     * @return true if the user is authenticated, false otherwise
     */
    public static boolean isAuthenticated() {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication authentication = securityContext.getAuthentication();
        if (authentication != null) {
            return authentication.getAuthorities().stream()
                .noneMatch(grantedAuthority -> grantedAuthority.getAuthority().equals(AuthoritiesConstants.ANONYMOUS));
        }
        return false;
    }

    /**
     * If the current user has a specific authority (security role).
     * <p>
     * The name of this method comes from the isUserInRole() method in the Servlet API
     *
     * @param authority the authority to check
     * @return true if the current user has the authority, false otherwise
     */
    public static boolean isCurrentUserInRole(String authority) {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication authentication = securityContext.getAuthentication();
        if (authentication != null) {
            return authentication.getAuthorities().stream()
                .anyMatch(grantedAuthority -> grantedAuthority.getAuthority().equals(authority));
        }
        return false;
    }
    
    public static boolean validatedUserApiUrl(Map<String, String> urlMap, String userApiUrl, String requestedType) {
    	// TODO : special case for payment since they do not start with api
		if((userApiUrl.contains("paytm/payment") || userApiUrl.contains("payment")) && "POST".equals(requestedType) ) {
			return true;
		}
		int userApiUrlIndex = userApiUrl.indexOf("api/");

		if (userApiUrlIndex == -1) {
			return false;
		}
		String userApiFilterString = null;
		if (userApiUrl.contains("?")) {
			userApiFilterString = userApiUrl.substring(userApiUrlIndex, userApiUrl.indexOf("?"));
		} else {
			userApiFilterString = userApiUrl.substring(userApiUrlIndex);
		}
		String[] userApiUrlElement = userApiFilterString.split("/");
		Map<String, String> filteredMap = urlMap.entrySet().stream()
				.filter(x -> x.getKey().startsWith(userApiUrlElement[0] + "/" + userApiUrlElement[1])
						&& x.getKey().split("/").length == userApiUrlElement.length)
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
		if (filteredMap == null || filteredMap.isEmpty()) {
			return false;
		} else {
			return validatedUserApiUrlKey(filteredMap, userApiUrlElement, requestedType);
		}
	}
	private static boolean validatedUserApiUrlKey(Map<String, String> filteredMap, String[] userApiUrlElement,
			String requestedType) {
		for (Entry<String, String> lMap : filteredMap.entrySet()) {
			if (!validatedApiKey(lMap, userApiUrlElement, requestedType)) {
				continue;
			} else {
				return true;
			}
		}
		return false;
	}
	private static boolean validatedApiKey(Entry<String, String> lMap, String[] userApiUrlElement,
			String requestedType) {
		Boolean returnFlag = true;
		String urlFormate = lMap.getKey();
		int urlFormateIndex = urlFormate.indexOf("api/");
		String urlFormateChunk = urlFormate.substring(urlFormateIndex);
		String[] urlFormateArr = urlFormateChunk.split("/");
		for (int i = 0; i < urlFormateArr.length; i++) {
			if (!urlFormateArr[i].equals(userApiUrlElement[i])
					&& !(urlFormateArr[i].startsWith("{") && urlFormateArr[i].endsWith("}"))) {
				returnFlag = false;
			}
		}
		if (!lMap.getValue().contains(requestedType)) {
			returnFlag = false;
		}
		return returnFlag;
	}
}
