package com.zoylo.gateway.security;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zoylo.gateway.domain.GrantedPermissionsList;
import com.zoylo.gateway.domain.GrantedRolesList;
import com.zoylo.gateway.domain.Role;
import com.zoylo.gateway.domain.ServiceProvidersList;
import com.zoylo.gateway.domain.User;
import com.zoylo.gateway.domain.ZoyloUserAuth;
import com.zoylo.gateway.model.PermissionsListVM;
import com.zoylo.gateway.model.RecipientPermissionListVM;
import com.zoylo.gateway.repository.RoleRepository;
import com.zoylo.gateway.repository.UserRepository;
import com.zoylo.gateway.repository.ZoyloUserAuthRepository;
import com.zoylo.gateway.service.util.Constants;
import com.zoylo.gateway.web.rest.client.RecipientRestClient;
import com.zoylo.gateway.web.rest.errors.CustomExceptionCode;
import com.zoylo.gateway.web.rest.errors.CustomParameterizedException;
import com.zoylo.gateway.web.rest.errors.RegisteredException;
import com.zoylo.gateway.web.rest.util.LookUp;

/**
 * Authenticate a user from the database.
 */
@Component("userDetailsService")
public class DomainUserDetailsService implements UserDetailsService {

	private final Logger log = LoggerFactory.getLogger(DomainUserDetailsService.class);
	private final UserRepository userRepository;
	@Autowired
	private ZoyloUserAuthRepository authRepository;

	@Autowired
	private RecipientRestClient recipientRestClient;

	@Autowired
	private RoleRepository roleRepository;

	private static final String DATA_CONSTANT = "data";

	private Set<String> authCategoryList = new HashSet<>();

	public DomainUserDetailsService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public CustomUserDetails loadUserByUsername(final String login) {
		log.debug("Authenticating {}", login);
		String lowercaseLogin = login.toLowerCase(Locale.ENGLISH);
		Optional<User> user = null;
		if (login.contains("@")) {
			user = userRepository.findByEmailAddress(lowercaseLogin);
			if (!user.isPresent()) {
				throw new CustomParameterizedException(RegisteredException.LOGIN_EXCEPTION.getException(),
						CustomExceptionCode.EMAIL_NOT_EXISTS.getErrMsg(),
						CustomExceptionCode.EMAIL_NOT_EXISTS.getErrCode());
			}
		} else if (login.startsWith(Constants.INDIA_MOBILE_CODE)) {
			user = userRepository.findByPhoneNumber(lowercaseLogin);
			if (!user.isPresent()) {
				String phone = login.substring(3, login.length());
				user = userRepository.findByPhoneNumber(phone);
				if (!user.isPresent()) {
					throw new CustomParameterizedException(RegisteredException.LOGIN_EXCEPTION.getException(),
							CustomExceptionCode.MOBILE_NUMBER_NOT_EXISTS.getErrMsg(),
							CustomExceptionCode.MOBILE_NUMBER_NOT_EXISTS.getErrCode());
				}
			}
		} else {
			user = userRepository.findById(lowercaseLogin);
			if (!user.isPresent()) {
				throw new CustomParameterizedException(RegisteredException.LOGIN_EXCEPTION.getException(),
						CustomExceptionCode.MOBILE_NUMBER_NOT_EXISTS.getErrMsg(),
						CustomExceptionCode.MOBILE_NUMBER_NOT_EXISTS.getErrCode());
			}
		}
		if (user.get().isAccountLocked()) {
			throw new CustomParameterizedException(RegisteredException.LOGIN_EXCEPTION.getException(),
					CustomExceptionCode.ACCOUNT_LOCKED.getErrMsg(), CustomExceptionCode.ACCOUNT_LOCKED.getErrCode());
		} else if (!user.get().isActiveFlag()) {
			throw new CustomParameterizedException(RegisteredException.LOGIN_EXCEPTION.getException(),
					CustomExceptionCode.ACCOUNT_DEACTIVE.getErrMsg(),
					CustomExceptionCode.ACCOUNT_DEACTIVE.getErrCode());
		} else if (!user.get().isLoginEnabled()) {
			throw new CustomParameterizedException(RegisteredException.LOGIN_EXCEPTION.getException(),
					CustomExceptionCode.LOGIN_NOT_ENABLE.getErrMsg(),
					CustomExceptionCode.LOGIN_NOT_ENABLE.getErrCode());
		} else if (user.get().isDeleteFlag()) {
			throw new CustomParameterizedException(RegisteredException.LOGIN_EXCEPTION.getException(),
					CustomExceptionCode.ACCOUNT_DELETED.getErrMsg(), CustomExceptionCode.ACCOUNT_DELETED.getErrCode());
		}
		authCategoryList.clear();
		log.info("before setPermissions method");
		List<CustomPermission> permission = setPermissions(user.get().getId(), user.get().getIsZoyloEmployee());
		authCategoryList.add(LookUp.RECIPIENT.getLookUpValue());

		String email = "";
		if (user.get().getEmailInfo() != null && user.get().getEmailInfo().getEmailAddress()!=null) {
			email = user.get().getEmailInfo().getEmailAddress();
		}
		String phoneNumber = "";
		if (user.get().getPhoneInfo() != null && user.get().getPhoneInfo().getPhoneNumber()!=null) {
			phoneNumber = user.get().getPhoneInfo().getPhoneNumber();
		}

		return new CustomUserDetails(user.get().getId(), email, user.get().getEncryptedPassword(), permission,
				phoneNumber, user.get().getIsZoyloEmployee(), authCategoryList);

	}

	private List<CustomPermission> setPermissions(String userId, Boolean isZoyloEmployee) {
		log.info("In setPermissions method ");
		List<CustomPermission> customPermissionList = new ArrayList<CustomPermission>();
		ZoyloUserAuth zoyloUserAuth = authRepository.findByUserId(userId);
		if (zoyloUserAuth != null) {
			List<ServiceProvidersList> providersList = zoyloUserAuth.getServiceProvidersList();
			if (providersList != null && !providersList.isEmpty()) {
				if (isZoyloEmployee != null && isZoyloEmployee) {
					log.info("LookUp.ADMIN.getLookUpValue()  "+LookUp.ADMIN.getLookUpValue());
					authCategoryList.add(LookUp.ADMIN.getLookUpValue());					
					List<CustomPermission> permissionList = mapPermission(providersList);
					log.info("permission List size is  "+permissionList);
					customPermissionList.addAll(permissionList);
				} else {					
					for (ServiceProvidersList serviceProvidersList : providersList) {
						if(serviceProvidersList.getProviderTypeCode().equals(Constants.DOCTOR_CLINIC)
								|| serviceProvidersList.getProviderTypeCode().equals(Constants.HOSPITAL)
								|| serviceProvidersList.getProviderTypeCode().equals(Constants.WELLNESS_CENTER)){
							authCategoryList.add(Constants.DOCTOR_CLINIC);
							List<CustomPermission> permissionList = mapPermission(providersList);
							log.info("permission List size is  "+permissionList);
							customPermissionList.addAll(permissionList);
							
						}else{
						authCategoryList.add(serviceProvidersList.getProviderTypeCode());
						}
					}
				}
			}
		} else {
			// recipient
			try {
				ResponseEntity<String> userPermission = recipientRestClient.getUserPermission(userId);
				String body = userPermission.getBody();
				JSONObject userJsonObj = new JSONObject(body);
				String userParseData = (String) userJsonObj.getString(DATA_CONSTANT);
				ObjectMapper mapper = new ObjectMapper();
				mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
				List<RecipientPermissionListVM> recipientPermissions = mapper.readValue(userParseData,
						mapper.getTypeFactory().constructCollectionType(List.class, RecipientPermissionListVM.class));
				for (RecipientPermissionListVM listVM : recipientPermissions) {
					for (PermissionsListVM permissionsListVM : listVM.getPermissionsList()) {
						CustomPermission customPermission = new CustomPermission();
						customPermission.setPermissionCode(permissionsListVM.getPermissionCode());
						customPermission.setPermissionId(permissionsListVM.getPermissionId());
						customPermissionList.add(customPermission);
					}
				}
			} catch (Exception e) {
				throw new CustomParameterizedException(RegisteredException.LOGIN_EXCEPTION.getException(),
						CustomExceptionCode.PERMISSION_NOT_FOUND_ERROR.getErrMsg(),
						CustomExceptionCode.PERMISSION_NOT_FOUND_ERROR.getErrCode());
			}
		}
		return customPermissionList;
	}

	/**
	 * @author diksha gupta
	 * @description to check whether email or mobile to login exists.
	 * @param userName
	 * @return
	 */
	public boolean checkCredentialsForLogin(String userName, String phone,String password) {
		String lowercaseLogin = userName.toLowerCase(Locale.ENGLISH);
		Optional<User> user = null;
		if (userName.contains("@")) {
			user = userRepository.findByEmailAddress(lowercaseLogin);
			if (!user.isPresent()) {
				throw new CustomParameterizedException(RegisteredException.LOGIN_EXCEPTION.getException(),
						CustomExceptionCode.EMAIL_NOT_EXISTS.getErrMsg(),
						CustomExceptionCode.EMAIL_NOT_EXISTS.getErrCode());
			}
			if (user.isPresent() && !user.get().getPhoneInfo().isVerifiedFlag()) {
				throw new CustomParameterizedException(RegisteredException.LOGIN_EXCEPTION.getException(),
						CustomExceptionCode.MOBILE_NOT_VERIFIED.getErrMsg(),
						CustomExceptionCode.MOBILE_NOT_VERIFIED.getErrCode());
			}
			return true;
		} else if (userName.startsWith(Constants.INDIA_MOBILE_CODE)) {
			user = userRepository.findByPhoneNumber(lowercaseLogin);
			if (!user.isPresent()) {
				user = userRepository.findByPhoneNumber(phone);
				if (!user.isPresent())
					throw new CustomParameterizedException(RegisteredException.LOGIN_EXCEPTION.getException(),
							CustomExceptionCode.MOBILE_NUMBER_NOT_EXISTS.getErrMsg(),
							CustomExceptionCode.MOBILE_NUMBER_NOT_EXISTS.getErrCode());
			}
			if (user.isPresent() && !user.get().getPhoneInfo().isVerifiedFlag()) {
				throw new CustomParameterizedException(RegisteredException.LOGIN_EXCEPTION.getException(),
						CustomExceptionCode.MOBILE_NOT_VERIFIED.getErrMsg(),
						CustomExceptionCode.MOBILE_NOT_VERIFIED.getErrCode());
			}
			return true;
		}
		return true;
	}

	private List<CustomPermission> mapPermission(List<ServiceProvidersList> providersList) {
		List<CustomPermission> customPermissionList = new ArrayList<CustomPermission>();
		log.info("providersList List size is  "+providersList.size());
		for (ServiceProvidersList serviceProvidersList : providersList) {
			List<GrantedRolesList> grantedRolesList = serviceProvidersList.getGrantedRolesList();
			for (GrantedRolesList rolesList : grantedRolesList) {
				log.info("Role code is   "+rolesList.getRoleCode());
				Role role = roleRepository.findOneByRoleCode(rolesList.getRoleCode());
				if (role != null) {
					for (GrantedPermissionsList permission : role.getPermissionList()) {
						CustomPermission customPermission = new CustomPermission();
						customPermission.setPermissionCode(permission.getPermissionCode());
						customPermission.setPermissionId(permission.getPermissionId());
						customPermissionList.add(customPermission);
					}
				}
			}
		}
		return customPermissionList;
	}
}
