package com.zoylo.gateway.security;

public class CustomRole {
	private String roleId;
	private String roleCode;

	public CustomRole() {
	}

	public CustomRole(String roleId, String roleCode) {
		this.roleId = roleId;
		this.roleCode = roleCode;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}


}
