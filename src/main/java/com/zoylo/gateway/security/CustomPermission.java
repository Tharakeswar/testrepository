package com.zoylo.gateway.security;

public class CustomPermission {

	private String permissionId;
	private String permissionCode;

	public CustomPermission() {
	}

	public CustomPermission(String permissionId, String permissionCode) {
		this.permissionId = permissionId;
		this.permissionCode = permissionCode;
	}

	public String getPermissionId() {
		return permissionId;
	}

	public void setPermissionId(String permissionId) {
		this.permissionId = permissionId;
	}

	public String getPermissionCode() {
		return permissionCode;
	}

	public void setPermissionCode(String permissionCode) {
		this.permissionCode = permissionCode;
	}

	@Override
	public int hashCode() {
	  final int prime = 31;
	  int result = 1;
	  result = prime * result + ((permissionCode == null) ? 0 : permissionCode.hashCode());
	  return result;
	}

	@Override
	public boolean equals(Object obj) {
	    if (this == obj)
	      return true;
	    if (obj == null)
	      return false;
	    if (getClass() != obj.getClass())
	      return false;
	    CustomPermission other = (CustomPermission) obj;
	    if (permissionCode == null) {
	      if (other.permissionCode != null)
	        return false;
	    } else if (!permissionCode.equals(other.permissionCode))
	      return false;
	    return true;
	  }

}
