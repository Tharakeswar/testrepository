package com.zoylo.gateway.security.jwt;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.GenericFilterBean;

import com.zoylo.gateway.web.rest.errors.CustomExceptionCode;
import com.zoylo.gateway.web.rest.errors.CustomParameterizedException;
import com.zoylo.gateway.web.rest.errors.RegisteredException;

/**
 * Filters incoming requests and installs a Spring Security principal if a header corresponding to a valid user is
 * found.
 */
	public class JWTFilter extends GenericFilterBean {
	private final Logger logger = LoggerFactory.getLogger(JWTFilter.class);

    private TokenProvider tokenProvider;

    public JWTFilter(TokenProvider tokenProvider) {
        this.tokenProvider = tokenProvider;
    }
    
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
        throws IOException, ServletException {
    	logger.info("In doFilter of JWTFilter");  
    	boolean ipRestrictionStatus = false;
    	try{
    		ipRestrictionStatus=tokenProvider.validateAuthorizedIpAddress(servletRequest,servletResponse);
    		if(!ipRestrictionStatus){
    			throw new CustomParameterizedException(
						RegisteredException.IP_ADDRESS_PERMISSION_ACCESS_REQUEST.getException(),
						CustomExceptionCode.IP_ADDRESS_PERMISSION_DENIED.getErrMsg(),
						CustomExceptionCode.IP_ADDRESS_PERMISSION_DENIED.getErrCode());    			
    		}
    	}catch(Exception exception){
    		((HttpServletResponse) servletResponse).sendError(HttpServletResponse.SC_UNAUTHORIZED, "IP address not allowed.");
    		
    	}  if(ipRestrictionStatus){
    		HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
    		String jwt = resolveToken(httpServletRequest);
    		if (StringUtils.hasText(jwt) && this.tokenProvider.validateToken(jwt)) {
    			Authentication authentication = this.tokenProvider.getAuthentication(jwt);
    			SecurityContextHolder.getContext().setAuthentication(authentication);
    		}
    		try {
    			filterChain.doFilter(servletRequest, servletResponse);
    		} catch (Exception exception) {
    			logger.info("Exception in doFilter while validating token {}", exception.getMessage());
    			((HttpServletResponse) servletResponse).sendError(HttpServletResponse.SC_UNAUTHORIZED, "Invalid token.");
    		}
    		
    	}
	
    }

    private String resolveToken(HttpServletRequest request){
        String bearerToken = request.getHeader(JWTConfigurer.AUTHORIZATION_HEADER);
        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7, bearerToken.length());
        }
        return null;
    }
}
