package com.zoylo.gateway.security.jwt;

import java.net.URI;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;


/***
 * @author arvindrawat
 * @version 1.0
 * @description Modify response header by refresh token
 *
 */

@ControllerAdvice
public class HeaderModifyAdvice implements ResponseBodyAdvice<Object> {
	@Autowired
	TokenProvider provider;
	
	private static final String MANAGEMENT_ON_URI = "management";
	private static final String V2_ON_URI = "v2";

	private final Logger log = LoggerFactory.getLogger(HeaderModifyAdvice.class);
	
	@Override
	public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
		return true;
	}

	@Override
	public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType,
			Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request,
			ServerHttpResponse response) {
		log.info("started execution of beforeBodyWrite method in HeaderModifyAdvice");
		if (request.getHeaders().containsKey("Switch-Profile")) {
			request.getHeaders().get("Switch-Profile").get(0);
		} else if (request.getHeaders().containsKey("Authorization")) {
			String token = request.getHeaders().get("Authorization").get(0);
			String jwt = resolveToken(token);

			URI uri = request.getURI();
			log.debug("current uri is : "+uri);
			String newtoken = "";
			// For This API ( "{host}:{port}/management/logs" ) no need to refresh Token
			log.debug("start of check condition for refresh token, if URI contains 'management' then token not refreshed");
			if (!(uri.toString().toLowerCase().contains(MANAGEMENT_ON_URI) || uri.toString().toLowerCase().contains(V2_ON_URI))) {
				log.debug("Start Trying For Refresh Token");
				newtoken = provider.refreshToken(jwt, request);
				log.debug("end of Refresh Token");
			}
			log.debug("end of check condition for refresh token");
			if (!newtoken.isEmpty()) {
				log.debug("Token Is refreshed");
				newtoken = "Bearer " + newtoken;
			} else {
				log.debug("Token Is Not refreshed");
				newtoken = token;
			}

			response.getHeaders().add("Authorization", newtoken);
			response.getHeaders().add("Access-Control-Expose-Headers", "Authorization");
		}
		log.info("end of beforeBodyWrite method in HeaderModifyAdvice");
		return body;
	}

	private String resolveToken(String token) {
		if (StringUtils.hasText(token) && token.startsWith("Bearer ")) {
			return token.substring(7, token.length());
		}
		return null;
	}

}