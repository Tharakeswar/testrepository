package com.zoylo.gateway.security.jwt;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.zoylo.gateway.domain.ServiceProvidersList;
import com.zoylo.gateway.domain.ZoyloAuthCategory;
import com.zoylo.gateway.domain.ZoyloUserAuth;
import com.zoylo.gateway.repository.AuthCategoryRepository;
import com.zoylo.gateway.repository.ZoyloUserAuthRepository;
import com.zoylo.gateway.security.CustomPermission;
import com.zoylo.gateway.security.CustomUserDetails;
import com.zoylo.gateway.security.DomainUserDetailsService;
import com.zoylo.gateway.security.SecurityUtils;
import com.zoylo.gateway.service.util.Constants;
import com.zoylo.gateway.web.rest.client.AdminRestClient;
import com.zoylo.gateway.web.rest.errors.CustomExceptionCode;
import com.zoylo.gateway.web.rest.errors.CustomInvalidTokenException;
import com.zoylo.gateway.web.rest.errors.RegisteredException;
import com.zoylo.gateway.web.rest.util.LookUp;

import io.github.jhipster.config.JHipsterProperties;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

@Component
public class TokenProvider {

	private final Logger log = LoggerFactory.getLogger(TokenProvider.class);

	private static final String AUTHORITIES_KEY = "auth";
	private static final String USER_ID = "uid";
	private static final String MOBILE_NO = "mobn";
	private static final String USER_NAME = "usr";
	private static final String AUTH_CATEGORY = "authCategory";
	private static final String RESOURCE_ID = "resourceId";
	private static final String AUTH_CATEGORY_LIST = "authCategoryList";
	private static final String REDIRECT_URL = "redirect_url";
	private static final String PLATFORM = "platform";
	private static final String MOBILE = "MOBILE";

	private String secretKey;

	private long tokenValidityInMilliseconds;

	private long tokenValidityInMillisecondsForRememberMe;

	private final JHipsterProperties jHipsterProperties;
	@Autowired
	private DomainUserDetailsService userDetailsService;
	@Autowired
	private AdminRestClient adminRestClient;

	@Autowired
	private ZoyloUserAuthRepository authRepository;

	@Autowired
	private AuthCategoryRepository authCategoryRepository;
	@Autowired
	private AdminAPIPublicAccessAllowed adminAPIPublicAccessAllowed;
	
	@Value("${app.tokenValidityInSecondsForMob}")
	private long tokenValidityInSecondsForMob;
	
	@Value("${app.security.active}")
  	private boolean securityActive;
	
	@Value("${app.authorizedIpAddresses}")
  	private String authorizedIpAddresses;
	private List<String> restrictedApis=new ArrayList<>();
	private List<String> authorizedIpAddressList=new ArrayList<>();
	

	public TokenProvider(JHipsterProperties jHipsterProperties) {
		this.jHipsterProperties = jHipsterProperties;
	}
	 {	
	    	restrictedApis.add("zoyloadmin-0.0.1-SNAPSHOT/api/codes/success");		 	
	    	restrictedApis.add("zoyloadmin-0.0.1-SNAPSHOT/api/codes/error");
	    	
	    }

	@PostConstruct
	public void init() {
		this.secretKey = jHipsterProperties.getSecurity().getAuthentication().getJwt().getSecret();

		this.tokenValidityInMilliseconds = 1000
				* jHipsterProperties.getSecurity().getAuthentication().getJwt().getTokenValidityInSeconds();
		this.tokenValidityInMillisecondsForRememberMe = 1000 * jHipsterProperties.getSecurity().getAuthentication()
				.getJwt().getTokenValidityInSecondsForRememberMe();
	}

	public String createToken(Authentication authentication, Boolean rememberMe,String roleCategory) {
		CustomUserDetails customUser = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		String mobileNo = customUser.getMobileNumber();
		String userId = customUser.getId();

		String roles = authentication.getAuthorities().stream().map(GrantedAuthority::getAuthority)
				.collect(Collectors.joining(","));
		String permissions = customUser.getPermission().stream().map(CustomPermission::getPermissionCode)
				.collect(Collectors.joining(","));
		String authrties = roles + "," + permissions;

		String authCategoryList = customUser.getAuthCategoryList().stream().collect(Collectors.joining(","));
		String resourceId = "";
		try {
			ResponseEntity<String> resourceData = adminRestClient.getServiceProviderResourceId(userId);
			String body = resourceData.getBody();
			JSONObject userJsonObj = new JSONObject(body);
			resourceId = (String) userJsonObj.getString(Constants.DATA);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		ZoyloUserAuth zoyloUserAuth = authRepository.findByUserId(userId);
		String authCategory = LookUp.RECIPIENT.getLookUpValue();
		if (zoyloUserAuth != null) {
			List<ServiceProvidersList> providersList = zoyloUserAuth.getServiceProvidersList();
			if (providersList != null && !providersList.isEmpty()) {
				if (providersList.get(0).getProviderTypeCode() == null) {
					authCategory = LookUp.ADMIN.getLookUpValue();
				} else if (providersList.get(0).getProviderTypeCode()
						.equalsIgnoreCase(LookUp.DIAGONSTIC.getLookUpValue())) {
					authCategory = LookUp.DIAGONSTIC.getLookUpValue();
				} else if (providersList.get(0).getProviderTypeCode()
						.equalsIgnoreCase(LookUp.DOCTOR.getLookUpValue())) {
					authCategory = LookUp.DOCTOR.getLookUpValue();
				} else if (providersList.get(0).getProviderTypeCode()
						.equalsIgnoreCase(LookUp.WELLNESS_CENTER.getLookUpValue())) {
					authCategory = LookUp.DOCTOR.getLookUpValue();
				}
			}
		}
		if (roleCategory != null && roleCategory.equalsIgnoreCase(LookUp.RECIPIENT.getLookUpValue())
				&& authCategory.equalsIgnoreCase(LookUp.ADMIN.getLookUpValue())) {
			authCategory = LookUp.RECIPIENT.getLookUpValue();
		}
		ZoyloAuthCategory category = authCategoryRepository.findByAuthCategoryCode(authCategory);
		String redirectUrl = "";
		if (category != null) {
			redirectUrl = category.getAuthCategoryLandingPageUrl();
		}

		long now = (new Date()).getTime();
		Date validity;
		if (rememberMe) {
			validity = new Date(now + this.tokenValidityInMillisecondsForRememberMe);
		} else {
			validity = new Date(now + this.tokenValidityInMilliseconds);
		}

		return Jwts.builder().setSubject(userId).claim(AUTHORITIES_KEY, authrties).claim(MOBILE_NO, mobileNo)
				.claim(AUTH_CATEGORY, authCategory).claim(USER_ID, userId).claim(USER_NAME, authentication.getName())
				.claim(RESOURCE_ID, resourceId).claim(AUTH_CATEGORY_LIST, authCategoryList)
				.claim(REDIRECT_URL, redirectUrl)
				// .claim(USER_PERMISSION, permissions)
				.signWith(SignatureAlgorithm.HS512, secretKey).setExpiration(validity).compact();
	}

	public Authentication getAuthentication(String token) {
		Claims claims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody();
		Collection<? extends GrantedAuthority> authorities = Arrays
				.stream(claims.get(AUTHORITIES_KEY).toString().split(",")).map(SimpleGrantedAuthority::new)
				.collect(Collectors.toList());
		CustomUserDetails principal = userDetailsService.loadUserByUsername(claims.getSubject());
		return new UsernamePasswordAuthenticationToken(principal, token, authorities);
	}

	public boolean validateToken(String authToken) {
		try {
			Jwts.parser().setSigningKey(secretKey).parseClaimsJws(authToken);
			return true;
		} catch (SignatureException exception) {
			log.info("Invalid JWT signature.");
			log.trace("Invalid JWT signature trace: {}", exception);			
		} catch (MalformedJwtException exception) {
			log.info("Invalid JWT token.");
			log.trace("Invalid JWT token trace: {}", exception);			
		} catch (ExpiredJwtException exception) {
			log.info("Expired JWT token.");
			log.trace("Expired JWT token trace: {}", exception);			
		} catch (UnsupportedJwtException exception) {
			log.info("Unsupported JWT token.");
			log.trace("Unsupported JWT token trace: {}", exception);		
		} catch (IllegalArgumentException exception) {
			log.info("JWT token compact of handler are invalid.");
			log.trace("JWT token compact of handler are invalid trace: {}", exception);			
		}
		return false;
	}

	/***
	 * @author arvind.rawat
	 * @description To refresh token
	 * @param token
	 * @return
	 */
	public String refreshToken(String token,ServerHttpRequest request) {
		log.info("Started execution of refreshToken method in TokenProvider");
		String mobPlatform = null;
		try {
			log.debug("started of check request object contains 'platform' header or not");
			if (request.getHeaders().containsKey(PLATFORM)) {
				mobPlatform = request.getHeaders().get(PLATFORM).get(0);
			}
			log.debug("ended of check request object contains 'platform' header or not");
			Claims claims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody();
			String mobileNumber = claims.get(MOBILE_NO).toString();
			String userId = claims.get(USER_ID).toString();
			String redirectUrl = "";
			String authCategoryList = "";
			log.debug("started of check claims object contains 'authCategoryList' key or not");
			if(claims.containsKey(AUTH_CATEGORY_LIST)) {
				authCategoryList = claims.get(AUTH_CATEGORY_LIST).toString();
			}
			log.debug("ended of check if claims object contains 'authCategoryList' key or not");
			log.debug("started of check claims object contains 'redirect_url' key or not");
			if(claims.containsKey(REDIRECT_URL)) {
				redirectUrl = claims.get(REDIRECT_URL).toString();
			}
			log.debug("ended of check if claims object contains 'redirect_url' key or not");
			Date validity;
			if(mobPlatform!=null && mobPlatform.equals(MOBILE)){				
				tokenValidityInMilliseconds=tokenValidityInSecondsForMob*1000;				
			}			
			long now = (new Date()).getTime();
			validity = new Date(now + this.tokenValidityInMilliseconds);
			log.info("ended execution of refreshToken method in TokenProvider");
			return Jwts.builder().setSubject(claims.getSubject()).claim(AUTHORITIES_KEY, claims.get(AUTHORITIES_KEY))
					.claim(MOBILE_NO, mobileNumber).claim(USER_ID, userId).claim(USER_NAME, claims.get(USER_NAME))
					.claim(RESOURCE_ID, claims.get(RESOURCE_ID)).claim(AUTH_CATEGORY, claims.get(AUTH_CATEGORY))
					.claim(AUTH_CATEGORY_LIST, authCategoryList).claim(REDIRECT_URL, redirectUrl)
					.signWith(SignatureAlgorithm.HS512, secretKey).setExpiration(validity).compact();
		} catch (Exception exception) {
			log.info("Exception occor while refreshing token {}", exception.getMessage());
			throw new CustomInvalidTokenException(RegisteredException.INVALID_TOKEN_EXCEPTION.getException(),
					CustomExceptionCode.INVALID_TOKEN_ERROR.getErrMsg(), exception,
					CustomExceptionCode.INVALID_TOKEN_ERROR.getErrCode());
		}

	}

	/***
	 * Token creation in case of switch profile
	 * 
	 * @param customUser
	 * @param authCategory
	 * @return
	 */
	public String createToken(CustomUserDetails customUser, String authCategory) {
		String mobileNo = customUser.getMobileNumber();
		String userId = customUser.getId();
		String permissions = customUser.getPermission().stream().map(CustomPermission::getPermissionCode)
				.collect(Collectors.joining(","));
		String authrties = "";
		if(permissions==null || permissions.isEmpty()) {
			 authrties = ",";
		}else {
			authrties = permissions;
		}
		log.info("authrtiesare >>>> "+authrties);
		String authCategoryList = customUser.getAuthCategoryList().stream().collect(Collectors.joining(","));
		String resourceId = "";
		try {
			ResponseEntity<String> resourceData = adminRestClient.getServiceProviderResourceId(userId);
			String body = resourceData.getBody();
			JSONObject userJsonObj = new JSONObject(body);
			resourceId = (String) userJsonObj.getString(Constants.DATA);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		ZoyloAuthCategory category = authCategoryRepository.findByAuthCategoryCode(authCategory);
		String redirectUrl = "";
		if (category != null) {
			redirectUrl = category.getAuthCategoryLandingPageUrl();
		}
		long now = (new Date()).getTime();
		Date validity;
		validity = new Date(now + this.tokenValidityInMilliseconds);

		return Jwts.builder().setSubject(userId).claim(AUTHORITIES_KEY, authrties).claim(MOBILE_NO, mobileNo)
				.claim(AUTH_CATEGORY, authCategory).claim(USER_ID, userId).claim(USER_NAME, customUser.getUsername())
				.claim(RESOURCE_ID, resourceId).claim(AUTH_CATEGORY_LIST, authCategoryList)
				.claim(REDIRECT_URL, redirectUrl).signWith(SignatureAlgorithm.HS512, secretKey).setExpiration(validity)
				.compact();
	}
	
	/**
     * @author arvind.rawat
     * @description To validate ip address is authorized or not
     * @param servletRequest
     * @param servletResponse
     * @throws IOException
     */
	public String getRequestIpAddress(ServletRequest servletRequest) {
		String ip=((HttpServletRequest) servletRequest).getHeader("x-forwarded-for");
        if(ip==null){
            ip=servletRequest.getRemoteAddr();
        } else {
          String ips[]=ip.split(",");
          ip=ips[0];
        }
        return ip;
	}
	
	public boolean validateAuthorizedIpAddress(ServletRequest servletRequest, ServletResponse servletResponse)
			throws IOException {
		try {
			String requestIpAddress = getRequestIpAddress(servletRequest);
			String[] restrictedIpAddresseArray = authorizedIpAddresses.split(",");
			authorizedIpAddressList = Arrays.asList(restrictedIpAddresseArray);	
			String uri = ((HttpServletRequest) servletRequest).getRequestURI().toString();		
			String requestedType=((HttpServletRequest) servletRequest).getMethod();
			if(securityActive) {
			  if (!authorizedIpAddressList.contains(requestIpAddress)) {
			   	// only for admin the check is added right now
				if(uri.contains("zoyloadmin")) {	
					 Map<String, String> populateAntMatchers = adminAPIPublicAccessAllowed.populateAntMatchers();
					 return SecurityUtils.validatedUserApiUrl(populateAntMatchers, uri, requestedType);
				}else {
					return true;
				}
			 }
			}
		} catch (Exception ex) {
			((HttpServletResponse) servletResponse).sendError(HttpServletResponse.SC_UNAUTHORIZED,
					"IP address not allowed.");
		}
		return true;
	}
    

}
