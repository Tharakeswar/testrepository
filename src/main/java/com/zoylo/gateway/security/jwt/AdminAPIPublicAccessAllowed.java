package com.zoylo.gateway.security.jwt;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class AdminAPIPublicAccessAllowed {

	private Map<String, String> antMatchersMap;
	
	public Map<String, String> getAntMatchersMap() {
	    return this.antMatchersMap;
	  }
	
	public Map<String, String> populateAntMatchers() {
	    antMatchersMap = new HashMap<String, String>();	
	    
	    
	    //MOBILE API 
	    
	    //PAYTM
	    antMatchersMap.put("api/paytm/generate-otp", "POST");
	    antMatchersMap.put("api/paytm/generate-token", "POST");
	    antMatchersMap.put("api/paytm/user-details", "GET");
	    antMatchersMap.put("api/paytm/balance-details", "GET");
	    antMatchersMap.put("api/paytm/withdraw-bal", "POST");
	    antMatchersMap.put("api/paytm/detach", "DELETE");
	    antMatchersMap.put("api/paytm/add-money", "POST");
	    
	    //CAMPAIGN
	    antMatchersMap.put("api/campaign", "POST,GET,PUT");
	    antMatchersMap.put("api/campaign/valid", "GET");
	    
	    //PROMOTION
	    antMatchersMap.put("api/promotions/public", "GET");
	    antMatchersMap.put("api/promotions/valid", "GET");
	    
	    //DOCTORPMS
	    antMatchersMap.put("api/doctor-pms/all-doctors", "GET");
	    antMatchersMap.put("api/doctor-pms/appointments", "POST,GET");
	    antMatchersMap.put("api/doctor-pms/appointments/counts", "GET");
	    antMatchersMap.put("api/doctor-details/{doctorId}/{serviceProviderId}", "GET");
	    antMatchersMap.put("api/doctor-pms/appointments/{appointmentId}", "GET");
	    antMatchersMap.put("api/doctor-appointments/reschedule", "POST");
	    antMatchersMap.put("api/doctor-appointments/{appointmentId}", "GET,PUT");
	    antMatchersMap.put("api/doctor-pms/appointments", "POST,GET");
	    antMatchersMap.put("api/fee-structures", "GET,POST,PUT");
	    antMatchersMap.put("api/doctor-pms/patients/{providerId}", "GET");
	    antMatchersMap.put("api/appointments/search", "GET");
	    antMatchersMap.put("api/doctor-registration/{providerId}", "GET");
	    antMatchersMap.put("api/doctor-appointments/invoices/{appointmentId}", "GET");
	    antMatchersMap.put("api/doctor-details", "GET");

	    antMatchersMap.put("api/doctor-registration/details", "PUT");
	    antMatchersMap.put("api/appointments/status-change", "PUT");
	    antMatchersMap.put("api/doctor-appointments/slots/{appointmentId}", "GET");
	    antMatchersMap.put("api/doctor-pms/{providerId}/doctors", "GET");
	    antMatchersMap.put("api/clinics/sorting", "GET");
	    antMatchersMap.put("api/clinics", "PUT");
	    antMatchersMap.put("api/link-doctors", "PUT,GET");
	    antMatchersMap.put("api/clinics/facilities", "GET");
	    antMatchersMap.put("api/clinics/save-facilities", "POST");
	    antMatchersMap.put("api/clinics/seo", "GET,POST");
	    antMatchersMap.put("api/local-service", "GET,POST");
	    antMatchersMap.put("api/master-tests", "GET");
	    antMatchersMap.put("api/pms/users", "POST");
	    antMatchersMap.put("api/clinics/images", "GET");
	    antMatchersMap.put("api/clinics/image", "POST");
	 
	    
	    antMatchersMap.put("api/providers/address/", "GET,PUT");
	    antMatchersMap.put("api/countries", "GET");
	    antMatchersMap.put("api/countries/states", "GET");
	    antMatchersMap.put("api/cities", "GET");
	    antMatchersMap.put("api/cities/stateId", "GET");
	    antMatchersMap.put("api/master-tests/{testName}", "GET");
	    antMatchersMap.put("api/diagnostics/{diagnosticid}/tests", "PUT,POST,GET");
	    antMatchersMap.put("api/diagnostics/{diagnosticId}/tests", "GET");
	    antMatchersMap.put("api/diagnostics/{diagnosticId}/appointments", "POST");
        antMatchersMap.put("api/diagnostic-pms/{diagnosticId}/appointments", "POST");
        antMatchersMap.put("api/appointments/service-provider/{diagnosticId}/filter", "GET");
        antMatchersMap.put("api/appointments/service-provider/{diagnosticId}/search-filter", "GET");
        antMatchersMap.put("api/appointments/service-provider/{diagnosticId}/search", "GET");


	    antMatchersMap.put("api/appointments/{id}", "GET");
	    antMatchersMap.put("api/appointments/{id}/cancel", "PUT");
	    antMatchersMap.put("api/appointments/reschedule", "PUT");
	    antMatchersMap.put("api/appointments", "PUT");
	    antMatchersMap.put("api/doctor-appointments/invoices", "GET");
	    
	    antMatchersMap.put("api/diagnostics/{diagnosticId}/resources/{resourceId}/profile", "GET,PUT");
	    antMatchersMap.put("api/appointments/search", "GET");
	    antMatchersMap.put("api/download-files", "GET");
	    antMatchersMap.put("api/diagnostics/appointments/patients", "GET");
	    
	    //DOCTOR API CONFIG
	    antMatchersMap.put("api/specializations/name", "GET");
	    antMatchersMap.put("api/doctor-details", "GET");
	    antMatchersMap.put("api/sales-reports", "GET");
	    
	    //API 
	    
	    antMatchersMap.put("api/appointments/payload-url", "POST");
	    
	    //USER
	    
	    antMatchersMap.put("api/appointments/recipient/", "GET");
	    antMatchersMap.put("api/appointments/recipient", "GET");
	    antMatchersMap.put("api/pms", "PUT");
	    antMatchersMap.put("api/doctor-pms", "GET");
	    antMatchersMap.put("api/pms/users", "POST");
	    antMatchersMap.put("api/all-appointments/auto-suggestion", "GET");

	    antMatchersMap.put("api/doctor-pms/patient-suggestions", "GET");
	    antMatchersMap.put("api/doctor-pms/patients", "GET");
	    antMatchersMap.put("api/pms/exist-users", "POST");
	    antMatchersMap.put("api/lookups/DAY_BREAKUP_CODE", "GET");
	    antMatchersMap.put("api/lookups/PAYMENT_GATEWAY", "GET");
	    antMatchersMap.put("api/countries", "GET");
	    antMatchersMap.put("api/cities", "GET");
	    antMatchersMap.put("api/cities/stateId/", "GET");
	    
	    
	    //Lookups
	    
	    antMatchersMap.put("api/lookups/", "GET");
	    antMatchersMap.put("api/lookups", "GET");
	    antMatchersMap.put("api/lookups/names", "GET");

	     
	     //WEB URLS

	     // admin urls (from config-new.txt)
	    antMatchersMap.put("api/pms/users", "POST,PUT");
	     antMatchersMap.put("api/pms/exist-users", "POST");
	     antMatchersMap.put("api/doctor-pms/all-doctors", "GET");
	     antMatchersMap.put("api/doctor-pms/patient-suggestions/{providerId}/{value}", "GET");
	     antMatchersMap.put("api/doctor-pms/patients/{providerId}", "GET");
	     antMatchersMap.put("api/doctor-pms/all-appointments/{providerId}", "GET");
	     antMatchersMap.put("api/doctor-pms/{providerId}", "GET");
	     
	     
	     // admin urls ( from constants.txt)
	     antMatchersMap.put("api/diagnostics", "PUT,POST");
	     antMatchersMap.put("api/diagnostics/{id}", "GET");
	     antMatchersMap.put("api/appointments/reschedule", "PUT");
	     antMatchersMap.put("api/appointments", "PUT");
	     antMatchersMap.put("api/diagnostics", "GET,POST");
	     antMatchersMap.put("api/daignostic-master-tests", "GET");
	     antMatchersMap.put("api/lookups/{code}", "GET");   // WEEK_DAY_LIST
	     antMatchersMap.put("api/diagnostics/slug/{id}", "GET");
	     antMatchersMap.put("api/appointments/window", "GET");
	     antMatchersMap.put("api/providers/awards", "GET,PUT");  
	     antMatchersMap.put("api/providers/about", "GET,PUT");
	     antMatchersMap.put("api/language", "GET");
	     antMatchersMap.put("api/providers/paymentmode/{id}", "GET");
	     antMatchersMap.put("api/providers/payment", "PUT");
	     antMatchersMap.put("api/providers/facilities", "PUT");
	     antMatchersMap.put("api/providers/facilities/{id}", "GET");
	     antMatchersMap.put("api/provider/awards", "GET");
	     antMatchersMap.put("api/provider/about", "GET");
	     antMatchersMap.put("api/provider/paymentmode", "GET");
	     antMatchersMap.put("api/provider/payment", "PUT");
	     antMatchersMap.put("api/clinics/get", "GET");
	     antMatchersMap.put("api/clinics/slug", "GET");
	     
	     
	     // admin urls (from config-new.txt)
	     antMatchersMap.put("api/campaign/valid", "GET");
	     antMatchersMap.put("api/countries", "GET");
	     antMatchersMap.put("api/doctor-details/search-info", "GET");
	     antMatchersMap.put("api/doctor-appointments", "GET,POST,PUT");
	     antMatchersMap.put("api/thridpartyserviceprovider/placeorder", "POST");
	     antMatchersMap.put("api/doctor-details", "GET");
	     antMatchersMap.put("api/campaign/resources", "GET");
	     antMatchersMap.put("api/daignostic-master-tests", "GET"); 
	     
	     antMatchersMap.put("api/doctor-appointments/package/filter", "GET");
	     antMatchersMap.put("api/appointments/recipient/", "GET");
	     antMatchersMap.put("api/user-profile", "POST");
	     antMatchersMap.put("api/doctor-appointments/reschedule", "POST");
	     antMatchersMap.put("api/cities/stateId/", "GET");
	     antMatchersMap.put("api/appointments/payload-url", "POST");
	     
	     antMatchersMap.put("api/transaction-paytm", "POST");
	     antMatchersMap.put("api/promotions/public", "GET");
	     antMatchersMap.put("api/promotions/valid", "GET");
	     antMatchersMap.put("api/paytm/balance-details", "GET");
	     antMatchersMap.put("api/paytm/generate-otp", "POST");
	     antMatchersMap.put("api/paytm/generate-token", "POST");
	     antMatchersMap.put("api/paytm/withdraw-bal", "POST");
	     antMatchersMap.put("api/paytm/add-money/", "GET");
	     antMatchersMap.put("api/paytm/detach", "DELETE");

	     antMatchersMap.put("api/serviceprovidingresourcelead", "POST");
	     antMatchersMap.put("api/serviceprovidingresourcelead/verifyotp", "POST");

	     return antMatchersMap;
	    
	   
    }
	
	
	
}