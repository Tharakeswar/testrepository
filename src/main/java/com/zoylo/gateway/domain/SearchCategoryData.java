package com.zoylo.gateway.domain;

/**
 * 
 * @author Devendra.Kumar
 * @version 1.0
 *
 */
public class SearchCategoryData {

	private String languageId;
	private String languageCode;
	private String searchCategoryName;

	public String getLanguageId() {
		return languageId;
	}

	public void setLanguageId(String languageId) {
		this.languageId = languageId;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getSearchCategoryName() {
		return searchCategoryName;
	}

	public void setSearchCategoryName(String searchCategoryName) {
		this.searchCategoryName = searchCategoryName;
	}

}
