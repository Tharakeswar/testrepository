package com.zoylo.gateway.domain;

import java.util.List;

/**
 * @author nagaraju
 * Class to hold default auth category info.
 *
 */
public class DefaultAuthCategoryInfo {
	
	private String authCategoryId;
	private String authCategoryCode;
	private String authCategoryLandingPageUrl;
	
	private List<AuthCategoryData> authCategoryData;

	public String getAuthCategoryId() {
		return authCategoryId;
	}

	public void setAuthCategoryId(String authCategoryId) {
		this.authCategoryId = authCategoryId;
	}

	public String getAuthCategoryCode() {
		return authCategoryCode;
	}

	public void setAuthCategoryCode(String authCategoryCode) {
		this.authCategoryCode = authCategoryCode;
	}

	public String getAuthCategoryLandingPageUrl() {
		return authCategoryLandingPageUrl;
	}

	public void setAuthCategoryLandingPageUrl(String authCategoryLandingPageUrl) {
		this.authCategoryLandingPageUrl = authCategoryLandingPageUrl;
	}

	public List<AuthCategoryData> getAuthCategoryData() {
		return authCategoryData;
	}

	public void setAuthCategoryData(List<AuthCategoryData> authCategoryData) {
		this.authCategoryData = authCategoryData;
	}

}
