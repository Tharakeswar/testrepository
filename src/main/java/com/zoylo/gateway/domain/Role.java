package com.zoylo.gateway.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "zoyloRole")
public class Role implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private String id;
	@Version
	private Integer versionId;
	private String roleCode;
	private List<RoleData> roleData;
	private List<GrantedPermissionsList> permissionList;
	
	private String authCategoryId;
	private String authCategoryCode;
	private List<AuthCategoryData> authCategoryData;
	/*private boolean deleteFlag;
	private DateTime deletedOn;*/
	private boolean activeFlag;
	private Date activeFromDate;
	private Date activeTillDate;
	private boolean systemDefined;
	@CreatedBy
	private String createdBy;
	@CreatedDate
	private DateTime createdOn;
	@LastModifiedBy
	private String lastUpdatedBy;
	@LastModifiedDate
	private DateTime lastUpdatedOn;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getVersionId() {
		return versionId;
	}

	public void setVersionId(Integer versionId) {
		this.versionId = versionId;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public List<RoleData> getRoleData() {
		return roleData;
	}

	public void setRoleData(List<RoleData> roleData) {
		this.roleData = roleData;
	}


	/*public boolean isDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(boolean deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public DateTime getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(DateTime deletedOn) {
		this.deletedOn = deletedOn;
	}*/

	public boolean isActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(boolean activeFlag) {
		this.activeFlag = activeFlag;
	}

	public Date getActiveFromDate() {
		return activeFromDate;
	}

	public void setActiveFromDate(Date activeFromDate) {
		this.activeFromDate = activeFromDate;
	}

	public Date getActiveTillDate() {
		return activeTillDate;
	}

	public void setActiveTillDate(Date activeTillDate) {
		this.activeTillDate = activeTillDate;
	}

	public boolean isSystemDefined() {
		return systemDefined;
	}

	public void setSystemDefined(boolean systemDefined) {
		this.systemDefined = systemDefined;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public DateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(DateTime createdOn) {
		this.createdOn = createdOn;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public DateTime getLastUpdatedOn() {
		return lastUpdatedOn;
	}

	public void setLastUpdatedOn(DateTime lastUpdatedOn) {
		this.lastUpdatedOn = lastUpdatedOn;
	}

	
	public List<GrantedPermissionsList> getPermissionList() {
		return permissionList;
	}

	public void setPermissionList(List<GrantedPermissionsList> permissionList) {
		this.permissionList = permissionList;
	}

	public String getAuthCategoryId() {
		return authCategoryId;
	}

	public void setAuthCategoryId(String authCategoryId) {
		this.authCategoryId = authCategoryId;
	}

	public String getAuthCategoryCode() {
		return authCategoryCode;
	}

	public void setAuthCategoryCode(String authCategoryCode) {
		this.authCategoryCode = authCategoryCode;
	}

	public List<AuthCategoryData> getAuthCategoryData() {
		return authCategoryData;
	}

	public void setAuthCategoryData(List<AuthCategoryData> authCategoryData) {
		this.authCategoryData = authCategoryData;
	}

	public Role() {
	}

	public Role(String roleCode, List<RoleData> roleData, List<GrantedPermissionsList> permissionsList, boolean systemDefined) {
		super();
		this.roleCode = roleCode;
		this.roleData = roleData;
		this.permissionList = permissionsList;
		this.systemDefined = systemDefined;
	}

}
