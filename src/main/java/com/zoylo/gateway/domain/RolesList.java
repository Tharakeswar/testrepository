package com.zoylo.gateway.domain;

import java.util.Date;
import java.util.List;

public class RolesList {
	private String roleId;
	private String roleCode;
	private List<RoleData> roleData;
	private String moduleId;
	private String moduleCode;
	private List<ModuleData> moduleData;
	private boolean defaultRoleFlag;
	private String landingPage;
	private boolean activeFlag;
	private Date activeFromDate;
	private Date activeTillDate;
	private List<PermissionList> permissionsList;
	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public List<RoleData> getRoleData() {
		return roleData;
	}

	public void setRoleData(List<RoleData> roleData) {
		this.roleData = roleData;
	}

	public String getModuleId() {
		return moduleId;
	}
	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}
	/*public ModuleInfo getModuleInfo() {
		return moduleInfo;
	}
	public void setModuleInfo(ModuleInfo moduleInfo) {
		this.moduleInfo = moduleInfo;
	}*/
	public boolean isDefaultRoleFlag() {
		return defaultRoleFlag;
	}
	public void setDefaultRoleFlag(boolean defaultRoleFlag) {
		this.defaultRoleFlag = defaultRoleFlag;
	}
	public String getLandingPage() {
		return landingPage;
	}
	public void setLandingPage(String landingPage) {
		this.landingPage = landingPage;
	}
	public boolean isActiveFlag() {
		return activeFlag;
	}
	public void setActiveFlag(boolean activeFlag) {
		this.activeFlag = activeFlag;
	}
	public Date getActiveFromDate() {
		return activeFromDate;
	}
	public void setActiveFromDate(Date activeFromDate) {
		this.activeFromDate = activeFromDate;
	}
	public Date getActiveTillDate() {
		return activeTillDate;
	}
	public void setActiveTillDate(Date activeTillDate) {
		this.activeTillDate = activeTillDate;
	}
	public List<PermissionList> getPermissionsList() {
		return permissionsList;
	}
	public void setPermissionsList(List<PermissionList> permissionsList) {
		this.permissionsList = permissionsList;
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	public List<ModuleData> getModuleData() {
		return moduleData;
	}

	public void setModuleData(List<ModuleData> moduleData) {
		this.moduleData = moduleData;
	}
	
}
