package com.zoylo.gateway.domain;

import java.io.Serializable;
import java.util.List;

public class LookupValueMV implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String lookupValueId;
	private String lookupValueCode;
	private Integer displaySequence;
	private List<LookupValueData> lookupValueData;
	private String imageInformation;
	private boolean activeFlag;

	public String getLookupValueId() {
		return lookupValueId;
	}

	public void setLookupValueId(String lookupValueId) {
		this.lookupValueId = lookupValueId;
	}

	public String getLookupValueCode() {
		return lookupValueCode;
	}

	public void setLookupValueCode(String lookupValueCode) {
		this.lookupValueCode = lookupValueCode;
	}

	public Integer getDisplaySequence() {
		return displaySequence;
	}

	public void setDisplaySequence(Integer displaySequence) {
		this.displaySequence = displaySequence;
	}

	public List<LookupValueData> getLookupValueData() {
		return lookupValueData;
	}

	public void setLookupValueData(List<LookupValueData> lookupValueData) {
		this.lookupValueData = lookupValueData;
	}

	public String getImageInformation() {
		return imageInformation;
	}

	public void setImageInformation(String imageInformation) {
		this.imageInformation = imageInformation;
	}

	public boolean isActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(boolean activeFlag) {
		this.activeFlag = activeFlag;
	}

}
