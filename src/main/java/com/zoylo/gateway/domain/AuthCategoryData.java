package com.zoylo.gateway.domain;
/**
 * 
 * @author Balram Sharma
 * @version 1.0
 * @description This class is used to hold language data
 *
 */
public class AuthCategoryData {

	private String languageId;
	private String languageCode;
	private String authCategoryName;
	private String authCategoryDescription;

	public String getLanguageId() {
		return languageId;
	}

	public void setLanguageId(String languageId) {
		this.languageId = languageId;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getAuthCategoryName() {
		return authCategoryName;
	}

	public void setAuthCategoryName(String authCategoryName) {
		this.authCategoryName = authCategoryName;
	}
	public String getAuthCategoryDescription() {
		return authCategoryDescription;
	}

	public void setAuthCategoryDescription(String authCategoryDescription) {
		this.authCategoryDescription = authCategoryDescription;
	}

}
