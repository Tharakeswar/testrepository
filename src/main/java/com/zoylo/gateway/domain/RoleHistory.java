package com.zoylo.gateway.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "zoyloRoleHistory")
public class RoleHistory implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private String id;
	private String roleId;
	private Integer versionId;
	private String roleCode;
	private List<RoleData> roleData;
	private List<PermissionList> permissionsList;
	private boolean deleteFlag;
	private DateTime deletedOn;
	private boolean activeFlag;
	private Date activeFromDate;
	private Date activeTillDate;
	private boolean systemDefined;
	@CreatedBy
	private String createdBy;
	@CreatedDate
	private DateTime createdOn;
	@LastModifiedBy
	private String lastUpdatedBy;
	@LastModifiedDate
	private DateTime lastUpdatedOn;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getVersionId() {
		return versionId;
	}

	public void setVersionId(Integer versionId) {
		this.versionId = versionId;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public List<RoleData> getRoleData() {
		return roleData;
	}

	public void setRoleData(List<RoleData> roleData) {
		this.roleData = roleData;
	}

	public List<PermissionList> getPermissionsList() {
		return permissionsList;
	}

	public void setPermissionsList(List<PermissionList> permissionsList) {
		this.permissionsList = permissionsList;
	}

	public boolean isDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(boolean deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public DateTime getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(DateTime deletedOn) {
		this.deletedOn = deletedOn;
	}

	public boolean isActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(boolean activeFlag) {
		this.activeFlag = activeFlag;
	}

	public Date getActiveFromDate() {
		return activeFromDate;
	}

	public void setActiveFromDate(Date activeFromDate) {
		this.activeFromDate = activeFromDate;
	}

	public Date getActiveTillDate() {
		return activeTillDate;
	}

	public void setActiveTillDate(Date activeTillDate) {
		this.activeTillDate = activeTillDate;
	}

	public boolean isSystemDefined() {
		return systemDefined;
	}

	public void setSystemDefined(boolean systemDefined) {
		this.systemDefined = systemDefined;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public DateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(DateTime createdOn) {
		this.createdOn = createdOn;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public DateTime getLastUpdatedOn() {
		return lastUpdatedOn;
	}

	public void setLastUpdatedOn(DateTime lastUpdatedOn) {
		this.lastUpdatedOn = lastUpdatedOn;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
