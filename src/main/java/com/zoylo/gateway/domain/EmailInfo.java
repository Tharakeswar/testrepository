package com.zoylo.gateway.domain;

import java.util.Date;

public class EmailInfo {

	private String emailAddress;
	private boolean verifiedFlag;
	private Date verifiedOn;
	// Added as part of JIRA - 
	private String verificationToken;
	private Date tokenSentAt;
	private Date tokenExpiresAt;
	
	public EmailInfo() {
	}

	public EmailInfo(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public EmailInfo(String emailAddress, boolean verifiedFlag, Date verifiedOn) {
		this.emailAddress = emailAddress;
		this.verifiedFlag = verifiedFlag;
		this.verifiedOn = verifiedOn;
	}

	public EmailInfo(String emailAddress, boolean verifiedFlag, Date verifiedOn, String verificationToken,
			Date tokenSentAt, Date tokenExpiresAt) {
		super();
		this.emailAddress = emailAddress;
		this.verifiedFlag = verifiedFlag;
		this.verifiedOn = verifiedOn;
		this.verificationToken = verificationToken;
		this.tokenSentAt = tokenSentAt;
		this.tokenExpiresAt = tokenExpiresAt;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public boolean isVerifiedFlag() {
		return verifiedFlag;
	}

	public void setVerifiedFlag(boolean verifiedFlag) {
		this.verifiedFlag = verifiedFlag;
	}

	public Date getVerifiedOn() {
		return verifiedOn;
	}

	public void setVerifiedOn(Date verifiedOn) {
		this.verifiedOn = verifiedOn;
	}

	public String getVerificationToken() {
		return verificationToken;
	}

	public void setVerificationToken(String verificationToken) {
		this.verificationToken = verificationToken;
	}

	public Date getTokenSentAt() {
		return tokenSentAt;
	}

	public void setTokenSentAt(Date tokenSentAt) {
		this.tokenSentAt = tokenSentAt;
	}

	public Date getTokenExpiresAt() {
		return tokenExpiresAt;
	}

	public void setTokenExpiresAt(Date tokenExpiresAt) {
		this.tokenExpiresAt = tokenExpiresAt;
	}

}
