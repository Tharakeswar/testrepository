package com.zoylo.gateway.domain;

import java.io.Serializable;
import java.util.List;

public class LookupMV implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;
	private Integer versionId;
	private String lookupCode;
	private List<LookupData> lookupData;
	private List<LookupValueMV> lookupValueList;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getVersionId() {
		return versionId;
	}

	public void setVersionId(Integer versionId) {
		this.versionId = versionId;
	}

	public String getLookupCode() {
		return lookupCode;
	}

	public void setLookupCode(String lookupCode) {
		this.lookupCode = lookupCode;
	}

	public List<LookupData> getLookupData() {
		return lookupData;
	}

	public void setLookupData(List<LookupData> lookupData) {
		this.lookupData = lookupData;
	}

	public List<LookupValueMV> getLookupValueList() {
		return lookupValueList;
	}

	public void setLookupValueList(List<LookupValueMV> lookupValueList) {
		this.lookupValueList = lookupValueList;
	}

}
