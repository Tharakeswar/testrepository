package com.zoylo.gateway.domain;

import java.util.Date;
import java.util.List;

public class GrantedPermissionsList {

	private String recipientId;
	private String recipientZoyloId;

	private String permissionId;
	private String permissionCode;
	private List<GrantedPermissionData> permissionData;
	private boolean activeFlag;
	private Date activeFromDate;
	private Date activeTillDate;
	private boolean systemDefined;
	public String getPermissionId() {
		return permissionId;
	}
	public void setPermissionId(String permissionId) {
		this.permissionId = permissionId;
	}
	public String getPermissionCode() {
		return permissionCode;
	}
	public void setPermissionCode(String permissionCode) {
		this.permissionCode = permissionCode;
	}
	public List<GrantedPermissionData> getPermissionData() {
		return permissionData;
	}
	public void setPermissionData(List<GrantedPermissionData> permissionData) {
		this.permissionData = permissionData;
	}
	public boolean isActiveFlag() {
		return activeFlag;
	}
	public void setActiveFlag(boolean activeFlag) {
		this.activeFlag = activeFlag;
	}
	public Date getActiveFromDate() {
		return activeFromDate;
	}
	public void setActiveFromDate(Date activeFromDate) {
		this.activeFromDate = activeFromDate;
	}
	public Date getActiveTillDate() {
		return activeTillDate;
	}
	public void setActiveTillDate(Date activeTillDate) {
		this.activeTillDate = activeTillDate;
	}
	public boolean isSystemDefined() {
		return systemDefined;
	}
	public void setSystemDefined(boolean systemDefined) {
		this.systemDefined = systemDefined;
	}
	public String getRecipientId() {
		return recipientId;
	}
	public void setRecipientId(String recipientId) {
		this.recipientId = recipientId;
	}
	public String getRecipientZoyloId() {
		return recipientZoyloId;
	}
	public void setRecipientZoyloId(String recipientZoyloId) {
		this.recipientZoyloId = recipientZoyloId;
	}
	
	
}
