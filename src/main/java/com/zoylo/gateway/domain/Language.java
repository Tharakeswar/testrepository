package com.zoylo.gateway.domain;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "zoyloLanguage")
public class Language {

	@Id
	private String id;
	@Version
	private Integer versionId;
	private String languageCode;
	private String languageName;
	private String languageNativeName;
	private boolean activeFlag;
	private Date activeFromDate;
	private boolean systemDefined;
	private boolean installedInZoylo;
	private boolean setAsDefaultInZoylo;
	private String createdBy;
	private Date createdOn;
	private String lastUpdatedBy;
	private Date lastUpdatedOn;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getVersionId() {
		return versionId;
	}

	public void setVersionId(Integer versionId) {
		this.versionId = versionId;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getLanguageName() {
		return languageName;
	}

	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}

	public String getLanguageNativeName() {
		return languageNativeName;
	}

	public void setLanguageNativeName(String languageNativeName) {
		this.languageNativeName = languageNativeName;
	}

	public boolean isActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(boolean activeFlag) {
		this.activeFlag = activeFlag;
	}

	public Date getActiveFromDate() {
		return activeFromDate;
	}

	public void setActiveFromDate(Date activeFromDate) {
		this.activeFromDate = activeFromDate;
	}

	public boolean isSystemDefined() {
		return systemDefined;
	}

	public void setSystemDefined(boolean systemDefined) {
		this.systemDefined = systemDefined;
	}

	public boolean isInstalledInZoylo() {
		return installedInZoylo;
	}

	public void setInstalledInZoylo(boolean installedInZoylo) {
		this.installedInZoylo = installedInZoylo;
	}

	public boolean isSetAsDefaultInZoylo() {
		return setAsDefaultInZoylo;
	}

	public void setSetAsDefaultInZoylo(boolean setAsDefaultInZoylo) {
		this.setAsDefaultInZoylo = setAsDefaultInZoylo;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedOn() {
		return lastUpdatedOn;
	}

	public void setLastUpdatedOn(Date lastUpdatedOn) {
		this.lastUpdatedOn = lastUpdatedOn;
	}

}
