package com.zoylo.gateway.domain;

public class ProviderTypeData {
	private String languageId; // String. Identifier of the language.
	private String languageCode; // String. Code associated with the language.
	private String providerTypeName; // String. Name of the authorization category.
	public String getLanguageId() {
		return languageId;
	}
	public void setLanguageId(String languageId) {
		this.languageId = languageId;
	}
	public String getLanguageCode() {
		return languageCode;
	}
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}
	public String getProviderTypeName() {
		return providerTypeName;
	}
	public void setProviderTypeName(String providerTypeName) {
		this.providerTypeName = providerTypeName;
	}
	
	
}
