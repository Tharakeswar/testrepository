package com.zoylo.gateway.domain;

import java.io.Serializable;

public class LookupData implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String languageId;
	private String languageCode;
	private String lookupDisplayName;
	private String lookupBriefDescription;

	public String getLanguageId() {
		return languageId;
	}

	public void setLanguageId(String languageId) {
		this.languageId = languageId;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getLookupDisplayName() {
		return lookupDisplayName;
	}

	public void setLookupDisplayName(String lookupDisplayName) {
		this.lookupDisplayName = lookupDisplayName;
	}

	public String getLookupBriefDescription() {
		return lookupBriefDescription;
	}

	public void setLookupBriefDescription(String lookupBriefDescription) {
		this.lookupBriefDescription = lookupBriefDescription;
	}

}
