package com.zoylo.gateway.domain;

/**
 * 
 * @author Devendra.Kumar
 * @version 1.0
 *
 */
public class SearchOperatorData {

	private String languageId;
	private String languageCode;
	private String searchOperatorName;

	public String getLanguageId() {
		return languageId;
	}

	public void setLanguageId(String languageId) {
		this.languageId = languageId;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getSearchOperatorName() {
		return searchOperatorName;
	}

	public void setSearchOperatorName(String searchOperatorName) {
		this.searchOperatorName = searchOperatorName;
	}

}
