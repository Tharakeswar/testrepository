package com.zoylo.gateway.domain;

import java.util.Date;
import java.util.List;

public class GrantedRolesList {
	private String roleId;
	private String roleCode;
	private List<GrantedRoleData> roleData;
	private boolean activeFlag;
	private Date activeFromDate;
	private Date activeTillDate;
	private boolean systemDefined;
	private List<GrantedPermissionsList> grantedPermissionsList;
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getRoleCode() {
		return roleCode;
	}
	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}
	public List<GrantedRoleData> getRoleData() {
		return roleData;
	}
	public void setRoleData(List<GrantedRoleData> roleData) {
		this.roleData = roleData;
	}
	public boolean isActiveFlag() {
		return activeFlag;
	}
	public void setActiveFlag(boolean activeFlag) {
		this.activeFlag = activeFlag;
	}
	public Date getActiveFromDate() {
		return activeFromDate;
	}
	public void setActiveFromDate(Date activeFromDate) {
		this.activeFromDate = activeFromDate;
	}
	public Date getActiveTillDate() {
		return activeTillDate;
	}
	public void setActiveTillDate(Date activeTillDate) {
		this.activeTillDate = activeTillDate;
	}
	public boolean isSystemDefined() {
		return systemDefined;
	}
	public void setSystemDefined(boolean systemDefined) {
		this.systemDefined = systemDefined;
	}
	public List<GrantedPermissionsList> getGrantedPermissionsList() {
		return grantedPermissionsList;
	}
	public void setGrantedPermissionsList(List<GrantedPermissionsList> grantedPermissionsList) {
		this.grantedPermissionsList = grantedPermissionsList;
	}
	
	
}
