package com.zoylo.gateway.domain;

public class ModuleData {

	private String languageId;
	private String languageCode;
	private String moduleDisplayName;
	private String moduleBriefDescription;

	public String getModuleDisplayName() {
		return moduleDisplayName;
	}
	

	public String getLanguageId() {
		return languageId;
	}


	public void setLanguageId(String languageId) {
		this.languageId = languageId;
	}


	public String getLanguageCode() {
		return languageCode;
	}


	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}


	public void setModuleDisplayName(String moduleDisplayName) {
		this.moduleDisplayName = moduleDisplayName;
	}

	public String getModuleBriefDescription() {
		return moduleBriefDescription;
	}

	public void setModuleBriefDescription(String moduleBriefDescription) {
		this.moduleBriefDescription = moduleBriefDescription;
	}

}
