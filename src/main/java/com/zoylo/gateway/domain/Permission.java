package com.zoylo.gateway.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "zoyloPermission")
public class Permission implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	private String id;

	@Version
	private Integer versionId;

	private String permissionCode;

	private List<PermissionData> permissionData;

	private String authCategoryId;
	private String authCategoryCode;
	private List<AuthCategoryData> authCategoryData;

	private String authCategoryLandingPageUrl;

	

	private boolean activeFlag;

	private Date activeFromDate;

	private Date activeTillDate;

	private boolean systemDefined;

	@CreatedBy
	private String createdBy;

	@CreatedDate
	private Date createdOn;

	@LastModifiedBy
	private String lastUpdateBy;

	@LastModifiedDate
	private DateTime lastUpdatedOn;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getVersionId() {
		return versionId;
	}

	public void setVersionId(Integer versionId) {
		this.versionId = versionId;
	}

	public String getPermissionCode() {
		return permissionCode;
	}

	public void setPermissionCode(String permissionCode) {
		this.permissionCode = permissionCode;
	}

	

	

	public List<PermissionData> getPermissionData() {
		return permissionData;
	}

	public void setPermissionData(List<PermissionData> permissionData) {
		this.permissionData = permissionData;
	}

	public boolean isActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(boolean activeFlag) {
		this.activeFlag = activeFlag;
	}

	public Date getActiveFromDate() {
		return activeFromDate;
	}

	public void setActiveFromDate(Date activeFromDate) {
		this.activeFromDate = activeFromDate;
	}

	public Date getActiveTillDate() {
		return activeTillDate;
	}

	public void setActiveTillDate(Date activeTillDate) {
		this.activeTillDate = activeTillDate;
	}

	public String getAuthCategoryLandingPageUrl() {
		return authCategoryLandingPageUrl;
	}

	public void setAuthCategoryLandingPageUrl(String authCategoryLandingPageUrl) {
		this.authCategoryLandingPageUrl = authCategoryLandingPageUrl;
	}

	public boolean isSystemDefined() {
		return systemDefined;
	}

	public void setSystemDefined(boolean systemDefined) {
		this.systemDefined = systemDefined;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getLastUpdateBy() {
		return lastUpdateBy;
	}

	public void setLastUpdateBy(String lastUpdateBy) {
		this.lastUpdateBy = lastUpdateBy;
	}

	public DateTime getLastUpdatedOn() {
		return lastUpdatedOn;
	}

	public void setLastUpdatedOn(DateTime lastUpdatedOn) {
		this.lastUpdatedOn = lastUpdatedOn;
	}

	public String getAuthCategoryId() {
		return authCategoryId;
	}

	public void setAuthCategoryId(String authCategoryId) {
		this.authCategoryId = authCategoryId;
	}

	public String getAuthCategoryCode() {
		return authCategoryCode;
	}

	public void setAuthCategoryCode(String authCategoryCode) {
		this.authCategoryCode = authCategoryCode;
	}

	public List<AuthCategoryData> getAuthCategoryData() {
		return authCategoryData;
	}

	public void setAuthCategoryData(List<AuthCategoryData> authCategoryData) {
		this.authCategoryData = authCategoryData;
	}

	public Permission() {

	}

	public Permission(String permissionCode, List<PermissionData> permissiondata, boolean systemDefined) {
		super();
		this.permissionCode = permissionCode;
		this.permissionData = permissiondata;
		this.systemDefined = systemDefined;
	}

}
