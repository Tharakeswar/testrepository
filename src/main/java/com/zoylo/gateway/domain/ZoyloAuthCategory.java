package com.zoylo.gateway.domain;

import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "zoyloAuthCategory")
public class ZoyloAuthCategory extends AbstractAuditingEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private String id;
	@Version
	private Integer versionId; // Integer. Mandatory. Auto-generated. Identifier of the document version.
	private String authCategoryCode;
	private List<AuthCategoryData> authCategoryData;
	private String authCategoryLandingPageUrl;
	private boolean allowRoleFlag;
	private boolean activeFlag;
	private Date activeFromDate;
	private Date activeTillDate;
	private boolean systemDefined;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getVersionId() {
		return versionId;
	}

	public void setVersionId(Integer versionId) {
		this.versionId = versionId;
	}

	public String getAuthCategoryCode() {
		return authCategoryCode;
	}

	public void setAuthCategoryCode(String authCategoryCode) {
		this.authCategoryCode = authCategoryCode;
	}

	public List<AuthCategoryData> getAuthCategoryData() {
		return authCategoryData;
	}

	public void setAuthCategoryData(List<AuthCategoryData> authCategoryData) {
		this.authCategoryData = authCategoryData;
	}

	public String getAuthCategoryLandingPageUrl() {
		return authCategoryLandingPageUrl;
	}

	public void setAuthCategoryLandingPageUrl(String authCategoryLandingPageUrl) {
		this.authCategoryLandingPageUrl = authCategoryLandingPageUrl;
	}

	public boolean isAllowRoleFlag() {
		return allowRoleFlag;
	}

	public void setAllowRoleFlag(boolean allowRoleFlag) {
		this.allowRoleFlag = allowRoleFlag;
	}

	public boolean isActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(boolean activeFlag) {
		this.activeFlag = activeFlag;
	}

	public Date getActiveFromDate() {
		return activeFromDate;
	}

	public void setActiveFromDate(Date activeFromDate) {
		this.activeFromDate = activeFromDate;
	}

	public Date getActiveTillDate() {
		return activeTillDate;
	}

	public void setActiveTillDate(Date activeTillDate) {
		this.activeTillDate = activeTillDate;
	}

	public boolean isSystemDefined() {
		return systemDefined;
	}

	public void setSystemDefined(boolean systemDefined) {
		this.systemDefined = systemDefined;
	}

}
