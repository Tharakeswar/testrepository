package com.zoylo.gateway.domain;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.validation.annotation.Validated;

@Validated
public class PermissionData {

	@NotNull
	@NotBlank
	private String languageId;

	@NotNull
	@NotBlank
	private String languageCode;

	@NotNull
	@NotBlank
	private String permissionName;

	private String permissionDescription;

	public String getLanguageId() {
		return languageId;
	}

	public void setLanguageId(String languageId) {
		this.languageId = languageId;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getPermissionName() {
		return permissionName;
	}

	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}

	public String getPermissionDescription() {
		return permissionDescription;
	}

	public void setPermissionDescription(String permissionDescription) {
		this.permissionDescription = permissionDescription;
	}

	public PermissionData() {

	}

	public PermissionData(String languageId, String languageCode, String permissionDisplayName,
			String permissionBriefDescription) {
		super();
		this.languageId = languageId;
		this.languageCode = languageCode;
		this.permissionName = permissionDisplayName;
		this.permissionDescription = permissionBriefDescription;
	}

}
