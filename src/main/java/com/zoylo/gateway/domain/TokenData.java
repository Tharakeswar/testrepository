package com.zoylo.gateway.domain;

public class TokenData {
	private String languageId;
	private String languageCode;
	private String tokenDisplayName;
	public String getLanguageId() {
		return languageId;
	}
	public void setLanguageId(String languageId) {
		this.languageId = languageId;
	}
	public String getLanguageCode() {
		return languageCode;
	}
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}
	public String getTokenDisplayName() {
		return tokenDisplayName;
	}
	public void setTokenDisplayName(String tokenDisplayName) {
		this.tokenDisplayName = tokenDisplayName;
	}



}