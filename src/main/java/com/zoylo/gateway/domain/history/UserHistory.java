package com.zoylo.gateway.domain.history;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.zoylo.gateway.domain.Authority;
import com.zoylo.gateway.domain.EmailInfo;
import com.zoylo.gateway.domain.PhoneInfo;

/***
 * Document to Maintain user History
 * 
 * @author Iti Gupta
 * @version 1.0
 *
 */
@Document(collection = "zoyloUserHistory")
public class UserHistory {

	@Id
	private String id;
	private String userId;
	private Integer versionId;
	private String userName;
	private String zoyloId;
	private String encryptedPassword;
	private Date passwordLastSetOn;
	private boolean activeFlag;
	private Date activeFromDate;
	private Date activeTillDate;
	private boolean loginEnabled;
	private boolean accountLocked;
	private EmailInfo emailInfo;
	private PhoneInfo phoneInfo;
	private String deviceId;
	private String deviceType;
	private Date deletedOn;
	private boolean deleteFlag;
	private boolean termsAndConditions;
	private Set<Authority> authorities = new HashSet<>();
	private String loginType;
	private String socialId;
	private String firstName;
	private String lastName;
	private String userType;

	public UserHistory() {
	}

	public UserHistory(String id, String userId, Integer versionId, String userName, String zoyloId,
			String encryptedPassword, Date passwordLastSetOn, boolean activeFlag, Date activeFromDate,
			Date activeTillDate, boolean loginEnabled, boolean accountLocked, EmailInfo emailInfo, PhoneInfo phoneInfo,
			String deviceId, String deviceType, Date deletedOn, boolean deleteFlag, 
			boolean termsAndConditions, Set<Authority> authorities, String loginType, String socialId, String firstName,
			String lastName, String userType) {
		super();
		this.id = id;
		this.userId = userId;
		this.versionId = versionId;
		this.userName = userName;
		this.zoyloId = zoyloId;
		this.encryptedPassword = encryptedPassword;
		this.passwordLastSetOn = passwordLastSetOn;
		this.activeFlag = activeFlag;
		this.activeFromDate = activeFromDate;
		this.activeTillDate = activeTillDate;
		this.loginEnabled = loginEnabled;
		this.accountLocked = accountLocked;
		this.emailInfo = emailInfo;
		this.phoneInfo = phoneInfo;
		this.deviceId = deviceId;
		this.deviceType = deviceType;
		this.deletedOn = deletedOn;
		this.deleteFlag = deleteFlag;
		this.termsAndConditions = termsAndConditions;
		this.authorities = authorities;
		this.loginType = loginType;
		this.socialId = socialId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.userType = userType;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Integer getVersionId() {
		return versionId;
	}

	public void setVersionId(Integer versionId) {
		this.versionId = versionId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getZoyloId() {
		return zoyloId;
	}

	public void setZoyloId(String zoyloId) {
		this.zoyloId = zoyloId;
	}

	public String getEncryptedPassword() {
		return encryptedPassword;
	}

	public void setEncryptedPassword(String encryptedPassword) {
		this.encryptedPassword = encryptedPassword;
	}

	public Date getPasswordLastSetOn() {
		return passwordLastSetOn;
	}

	public void setPasswordLastSetOn(Date passwordLastSetOn) {
		this.passwordLastSetOn = passwordLastSetOn;
	}

	public boolean isActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(boolean activeFlag) {
		this.activeFlag = activeFlag;
	}

	public Date getActiveFromDate() {
		return activeFromDate;
	}

	public void setActiveFromDate(Date activeFromDate) {
		this.activeFromDate = activeFromDate;
	}

	public Date getActiveTillDate() {
		return activeTillDate;
	}

	public void setActiveTillDate(Date activeTillDate) {
		this.activeTillDate = activeTillDate;
	}

	public boolean isLoginEnabled() {
		return loginEnabled;
	}

	public void setLoginEnabled(boolean loginEnabled) {
		this.loginEnabled = loginEnabled;
	}

	public boolean isAccountLocked() {
		return accountLocked;
	}

	public void setAccountLocked(boolean accountLocked) {
		this.accountLocked = accountLocked;
	}

	public EmailInfo getEmailInfo() {
		return emailInfo;
	}

	public void setEmailInfo(EmailInfo emailInfo) {
		this.emailInfo = emailInfo;
	}

	public PhoneInfo getPhoneInfo() {
		return phoneInfo;
	}

	public void setPhoneInfo(PhoneInfo phoneInfo) {
		this.phoneInfo = phoneInfo;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public boolean isDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(boolean deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	
	public boolean isTermsAndConditions() {
		return termsAndConditions;
	}

	public void setTermsAndConditions(boolean termsAndConditions) {
		this.termsAndConditions = termsAndConditions;
	}

	public Set<Authority> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(Set<Authority> authorities) {
		this.authorities = authorities;
	}

	public String getLoginType() {
		return loginType;
	}

	public void setLoginType(String loginType) {
		this.loginType = loginType;
	}

	public String getSocialId() {
		return socialId;
	}

	public void setSocialId(String socialId) {
		this.socialId = socialId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

}
