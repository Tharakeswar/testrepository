package com.zoylo.gateway.domain.history;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.zoylo.gateway.domain.ServiceProvidersList;
import com.zoylo.gateway.mv.BaseDomainMV;

/**
 * 
 * @author Devendra.Kumar
 * @version 1.0
 *
 */
@Document(collection = "zoyloUserAuthHistory")
public class UserAuthHistory extends BaseDomainMV implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	private String id;
	private String documentId;
	private Integer versionId;
	private String userId;
	private String zoyloId;
	private String emailId;
	private String phoneNumber;
	private List<ServiceProvidersList> serviceProvidersList;
	private boolean deleteFlag;
	private Date deletedOn;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDocumentId() {
		return documentId;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	public Integer getVersionId() {
		return versionId;
	}

	public void setVersionId(Integer versionId) {
		this.versionId = versionId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getZoyloId() {
		return zoyloId;
	}

	public void setZoyloId(String zoyloId) {
		this.zoyloId = zoyloId;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public List<ServiceProvidersList> getServiceProvidersList() {
		return serviceProvidersList;
	}

	public void setServiceProvidersList(List<ServiceProvidersList> serviceProvidersList) {
		this.serviceProvidersList = serviceProvidersList;
	}

	public boolean isDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(boolean deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}
}
