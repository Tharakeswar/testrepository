package com.zoylo.gateway.domain;

public class RoleData {

	private String languageId;
	private String languageCode;
	private String roleName;
	private String roleDescription;

	public String getLanguageId() {
		return languageId;
	}

	public void setLanguageId(String languageId) {
		this.languageId = languageId;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	
	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRoleDescription() {
		return roleDescription;
	}

	public void setRoleDescription(String roleDescription) {
		this.roleDescription = roleDescription;
	}

	public RoleData() {

	}

	public RoleData(String languageId, String languageCode, String roleDisplayName, String roleBriefDescription) {
		super();
		this.languageId = languageId;
		this.languageCode = languageCode;
		this.roleName = roleDisplayName;
		this.roleDescription = roleBriefDescription;
	}

}
