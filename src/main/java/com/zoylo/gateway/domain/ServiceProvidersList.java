package com.zoylo.gateway.domain;

import java.util.List;

public class ServiceProvidersList {
	private String providerTypeId;// String. Identifier of the provider type.
	private String providerTypeCode;
	private List<ProviderTypeData> providerTypeData;
	private String providerId;
	private String providerCode;                      // String. Mandatory. Code associated with the service provider.
    private String providerName;                      // String. Mandatory. Name of the service provider
    private String providerSearchName;
    private List<GrantedRolesList> grantedRolesList;
	public String getProviderTypeId() {
		return providerTypeId;
	}
	public void setProviderTypeId(String providerTypeId) {
		this.providerTypeId = providerTypeId;
	}
	public String getProviderTypeCode() {
		return providerTypeCode;
	}
	public void setProviderTypeCode(String providerTypeCode) {
		this.providerTypeCode = providerTypeCode;
	}
	public List<ProviderTypeData> getProviderTypeData() {
		return providerTypeData;
	}
	public void setProviderTypeData(List<ProviderTypeData> providerTypeData) {
		this.providerTypeData = providerTypeData;
	}
	public String getProviderId() {
		return providerId;
	}
	public void setProviderId(String providerId) {
		this.providerId = providerId;
	}
	public String getProviderCode() {
		return providerCode;
	}
	public void setProviderCode(String providerCode) {
		this.providerCode = providerCode;
	}
	public String getProviderName() {
		return providerName;
	}
	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}
	public String getProviderSearchName() {
		return providerSearchName;
	}
	public void setProviderSearchName(String providerSearchName) {
		this.providerSearchName = providerSearchName;
	}
	public List<GrantedRolesList> getGrantedRolesList() {
		return grantedRolesList;
	}
	public void setGrantedRolesList(List<GrantedRolesList> grantedRolesList) {
		this.grantedRolesList = grantedRolesList;
	}
    
    
    
}
