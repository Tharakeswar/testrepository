package com.zoylo.gateway.domain;

public class LookupCodeValueCodeVM {

	private String code;
	private String valueCode;
	
	public LookupCodeValueCodeVM() {
		super();
	}
	public LookupCodeValueCodeVM(String code, String valueCode) {
		super();
		this.code = code;
		this.valueCode = valueCode;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getValueCode() {
		return valueCode;
	}
	public void setValueCode(String valueCode) {
		this.valueCode = valueCode;
	}
	
}
