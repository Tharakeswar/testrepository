package com.zoylo.gateway.domain;

import java.util.ArrayList;
import java.util.List;

public class DefaultRoleInfo {

	private String roleId;
	private String roleCode;
	private List<RoleData> roleData;
	private String defaultLandingPage;

	public DefaultRoleInfo() {
		this.roleData = new ArrayList<>();
	}

	public DefaultRoleInfo(String roleId, String roleCode) {
		this.roleId = roleId;
		this.roleCode = roleCode;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public List<RoleData> getRoleData() {
		return roleData;
	}

	public void setRoleData(List<RoleData> roleData) {
		this.roleData = roleData;
	}

	public String getDefaultLandingPage() {
		return defaultLandingPage;
	}

	public void setDefaultLandingPage(String defaultLandingPage) {
		this.defaultLandingPage = defaultLandingPage;
	}

}
