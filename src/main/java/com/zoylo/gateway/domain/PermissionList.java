package com.zoylo.gateway.domain;

public class PermissionList {
	private String permissionId;
	private String permissionCode;

	public String getPermissionId() {
		return permissionId;
	}

	public void setPermissionId(String permissionId) {
		this.permissionId = permissionId;
	}

	public String getPermissionCode() {
		return permissionCode;
	}

	public void setPermissionCode(String permissionCode) {
		this.permissionCode = permissionCode;
	}

	public PermissionList() {

	}

	public PermissionList(String permissionId, String permissionCode) {
		super();
		this.permissionId = permissionId;
		this.permissionCode = permissionCode;
	}

}
