package com.zoylo.gateway.domain;

import java.io.Serializable;

public class LookupValueData implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String languageId;
	private String languageCode;
	private String valueDisplayName;
	private String valueBriefDescription;

	public String getLanguageId() {
		return languageId;
	}

	public void setLanguageId(String languageId) {
		this.languageId = languageId;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getValueDisplayName() {
		return valueDisplayName;
	}

	public void setValueDisplayName(String valueDisplayName) {
		this.valueDisplayName = valueDisplayName;
	}

	public String getValueBriefDescription() {
		return valueBriefDescription;
	}

	public void setValueBriefDescription(String valueBriefDescription) {
		this.valueBriefDescription = valueBriefDescription;
	}

}
