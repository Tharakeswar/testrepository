package com.zoylo.gateway.domain;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "zoyloUserToken")
public class ZoyloUserToken extends BaseDomain implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String _id;
	private String userId;
	private String twilioUserSid;
	private String userPhoneNumber;
	private String tokenId;
	private String tokenCode;
	private List<TokenData> tokenData;
	private String tokenValue;
	private String authCategory;
	private String appointmentId;
	
	public String get_id() {
		return _id;
	}
	public void set_id(String _id) {
		this._id = _id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getTwilioUserSid() {
		return twilioUserSid;
	}
	public void setTwilioUserSid(String twilioUserSid) {
		this.twilioUserSid = twilioUserSid;
	}
	public String getUserPhoneNumber() {
		return userPhoneNumber;
	}
	public void setUserPhoneNumber(String userPhoneNumber) {
		this.userPhoneNumber = userPhoneNumber;
	}
	public String getTokenId() {
		return tokenId;
	}
	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}
	public String getTokenCode() {
		return tokenCode;
	}
	public void setTokenCode(String tokenCode) {
		this.tokenCode = tokenCode;
	}
	public List<TokenData> getTokenData() {
		return tokenData;
	}
	public void setTokenData(List<TokenData> tokenData) {
		this.tokenData = tokenData;
	}
	public String getTokenValue() {
		return tokenValue;
	}
	public void setTokenValue(String tokenValue) {
		this.tokenValue = tokenValue;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getAuthCategory() {
		return authCategory;
	}
	public void setAuthCategory(String authCategory) {
		this.authCategory = authCategory;
	}
	public String getAppointmentId() {
		return appointmentId;
	}
	public void setAppointmentId(String appointmentId) {
		this.appointmentId = appointmentId;
	}
	
	
}