package com.zoylo.gateway.domain;

import java.util.Date;
import java.util.List;



/**
 * @author nagaraju
 * Class to hold permissions list.
 *
 */
public class ZoyloGrantedPermissionsList {
	
	private String recipientId;
	private String recipientZoyloId;
	private List<ZoyloPermissionsList> permissionsList;
	private boolean activeFlag;
	private Date activeFromDate;
	private Date activeTillDate;
	
	public String getRecipientId() {
		return recipientId;
	}
	public void setRecipientId(String recipientId) {
		this.recipientId = recipientId;
	}
	public String getRecipientZoyloId() {
		return recipientZoyloId;
	}
	public void setRecipientZoyloId(String recipientZoyloId) {
		this.recipientZoyloId = recipientZoyloId;
	}
	public List<ZoyloPermissionsList> getPermissionsList() {
		return permissionsList;
	}
	public void setPermissionsList(List<ZoyloPermissionsList> permissionsList) {
		this.permissionsList = permissionsList;
	}
	public boolean isActiveFlag() {
		return activeFlag;
	}
	public void setActiveFlag(boolean activeFlag) {
		this.activeFlag = activeFlag;
	}
	public Date getActiveFromDate() {
		return activeFromDate;
	}
	public void setActiveFromDate(Date activeFromDate) {
		this.activeFromDate = activeFromDate;
	}
	public Date getActiveTillDate() {
		return activeTillDate;
	}
	public void setActiveTillDate(Date activeTillDate) {
		this.activeTillDate = activeTillDate;
	}

}
