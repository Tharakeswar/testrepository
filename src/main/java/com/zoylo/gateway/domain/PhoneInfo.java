package com.zoylo.gateway.domain;

import java.util.Date;

public class PhoneInfo {
	private String phoneNumber;
	private boolean verifiedFlag;
	private Date verifiedOn;
	private int otp;
	private Date otpSentOn;
	private Integer resendOtpCount;

	public PhoneInfo() {
		this.verifiedOn = null;
	}

	public PhoneInfo(String phoneNumber) {
		this.phoneNumber = phoneNumber;
		this.verifiedOn = null;
		this.otpSentOn = new Date();

	}

	public PhoneInfo(String phoneNumber, boolean verifiedFlag, Date verifiedOn) {
		this.phoneNumber = phoneNumber;
		this.verifiedFlag = verifiedFlag;
		this.verifiedOn = new Date();

	}

	public PhoneInfo(String phoneNumber, boolean verifiedFlag, Date verifiedOn, Date otpSentOn) {
		this.phoneNumber = phoneNumber;
		this.verifiedFlag = verifiedFlag;
		this.verifiedOn = verifiedOn;
		this.otpSentOn = otpSentOn;
	}

	public Date getOtpSentOn() {
		return otpSentOn;
	}

	public void setOtpSentOn(Date otpSentOn) {
		this.otpSentOn = otpSentOn;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public boolean isVerifiedFlag() {
		return verifiedFlag;
	}

	public void setVerifiedFlag(boolean verifiedFlag) {
		this.verifiedFlag = verifiedFlag;
	}

	public Date getVerifiedOn() {
		return verifiedOn;
	}

	public void setVerifiedOn(Date verifiedOn) {
		this.verifiedOn = verifiedOn;
	}

	public void setOtp(int otp) {
		this.otp = otp;
	}

	public int getOtp() {
		return otp;
	}

	public Integer getResendOtpCount() {
		return resendOtpCount;
	}

	public void setResendOtpCount(Integer resendOtpCount) {
		this.resendOtpCount = resendOtpCount;
	}

}
