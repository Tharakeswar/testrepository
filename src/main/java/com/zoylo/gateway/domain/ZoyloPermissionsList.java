package com.zoylo.gateway.domain;

import java.util.List;


/**
 * @author nagaraju
 * Class to hold permission list
 *
 */
public class ZoyloPermissionsList {
	
	private String permissionId;
	private String permissionCode;
	private List<ZoyloPermissionData> permissionData;
	
	public String getPermissionId() {
		return permissionId;
	}
	public void setPermissionId(String permissionId) {
		this.permissionId = permissionId;
	}
	public String getPermissionCode() {
		return permissionCode;
	}
	public void setPermissionCode(String permissionCode) {
		this.permissionCode = permissionCode;
	}
	public List<ZoyloPermissionData> getPermissionData() {
		return permissionData;
	}
	public void setPermissionData(List<ZoyloPermissionData> permissionData) {
		this.permissionData = permissionData;
	}

}
