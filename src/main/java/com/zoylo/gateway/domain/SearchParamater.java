package com.zoylo.gateway.domain;

import java.util.List;

/**
 * 
 * @author Devendra.Kumar
 * @version 1.0
 *
 */
public class SearchParamater {

	private String searchParameterId;
	private String searchKeyId;
	private String searchKeyCode;
	private List<SearchKeyData> searchKeyData;

	private String searchOperatorId;
	private String searchOperatorCode;
	private List<SearchOperatorData> searchOperatorData;

	private String searchStartValue;
	private String searchEndValue;

	public String getSearchParameterId() {
		return searchParameterId;
	}

	public void setSearchParameterId(String searchParameterId) {
		this.searchParameterId = searchParameterId;
	}

	public String getSearchKeyId() {
		return searchKeyId;
	}

	public void setSearchKeyId(String searchKeyId) {
		this.searchKeyId = searchKeyId;
	}

	public String getSearchKeyCode() {
		return searchKeyCode;
	}

	public void setSearchKeyCode(String searchKeyCode) {
		this.searchKeyCode = searchKeyCode;
	}

	public List<SearchKeyData> getSearchKeyData() {
		return searchKeyData;
	}

	public void setSearchKeyData(List<SearchKeyData> searchKeyData) {
		this.searchKeyData = searchKeyData;
	}

	public String getSearchOperatorId() {
		return searchOperatorId;
	}

	public void setSearchOperatorId(String searchOperatorId) {
		this.searchOperatorId = searchOperatorId;
	}

	public String getSearchOperatorCode() {
		return searchOperatorCode;
	}

	public void setSearchOperatorCode(String searchOperatorCode) {
		this.searchOperatorCode = searchOperatorCode;
	}

	public List<SearchOperatorData> getSearchOperatorData() {
		return searchOperatorData;
	}

	public void setSearchOperatorData(List<SearchOperatorData> searchOperatorData) {
		this.searchOperatorData = searchOperatorData;
	}

	public String getSearchStartValue() {
		return searchStartValue;
	}

	public void setSearchStartValue(String searchStartValue) {
		this.searchStartValue = searchStartValue;
	}

	public String getSearchEndValue() {
		return searchEndValue;
	}

	public void setSearchEndValue(String searchEndValue) {
		this.searchEndValue = searchEndValue;
	}

}
