package com.zoylo.gateway.domain;

import java.util.List;

/**
 * @author nagaraju
 * Class for holding input data for creating user profile
 *
 */
public class UserProfile {
	private String id;
	private String userId;
	private String zoyloId;
	private String firstName;
	private String middleName;
	private String lastName;
	private List<ZoyloGrantedPermissionsList> grantedPermissionsList;
	private DefaultAuthCategoryInfo defaultAuthCategoryInfo;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getZoyloId() {
		return zoyloId;
	}
	public void setZoyloId(String zoyloId) {
		this.zoyloId = zoyloId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public List<ZoyloGrantedPermissionsList> getGrantedPermissionsList() {
		return grantedPermissionsList;
	}
	public void setGrantedPermissionsList(List<ZoyloGrantedPermissionsList> grantedPermissionsList) {
		this.grantedPermissionsList = grantedPermissionsList;
	}
	public DefaultAuthCategoryInfo getDefaultAuthCategoryInfo() {
		return defaultAuthCategoryInfo;
	}
	public void setDefaultAuthCategoryInfo(DefaultAuthCategoryInfo defaultAuthCategoryInfo) {
		this.defaultAuthCategoryInfo = defaultAuthCategoryInfo;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

}
