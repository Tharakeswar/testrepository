package com.zoylo.gateway.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * A user.
 */

@Document(collection = "zoyloUser")
public class User extends AbstractAuditingEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private String id;

	@Version
	private Integer versionId;

	private String zoyloId;
	
	private String userName;

	private Boolean isZoyloEmployee;

	private String encryptedPassword;

	// @Field("password_last_set_on")
	private Date passwordLastSetOn;

	// @Field("activeFlag")
	private boolean activeFlag;

	// @Field("active_from_date")
	private Date activeFromDate;

	// @Field("active_till_date")
	private Date activeTillDate;

	// @Field("loginEnabled")
	private boolean loginEnabled;

	// @Field("accountLocked")
	private boolean accountLocked;

	private EmailInfo emailInfo;
	private PhoneInfo phoneInfo;

	private String deviceId;

	private String deviceType;

	private Date deletedOn;

	// @Field("deleteFlag")
	private boolean deleteFlag;

	// private Permissions permissions;

	// private boolean termsAndConditions;

	@JsonIgnore
	private Set<Authority> authorities = new HashSet<>();

	// Social Media
	// private String loginType;

	// private String socialId;

	private String firstName;
	private String middleName;
	private String lastName;
	private String ecommId;
	private String ecommToken;

	// private String userType;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Integer getVersionId() {
		return versionId;
	}

	public void setVersionId(Integer versionId) {
		this.versionId = versionId;
	}

	public String getZoyloId() {
		return zoyloId;
	}

	public void setZoyloId(String zoyloId) {
		this.zoyloId = zoyloId;
	}

	public String getEncryptedPassword() {
		return encryptedPassword;
	}

	public void setEncryptedPassword(String encryptedPassword) {
		this.encryptedPassword = encryptedPassword;
	}

	public Date getPasswordLastSetOn() {
		return passwordLastSetOn;
	}

	public void setPasswordLastSetOn(Date passwordLastSetOn) {
		this.passwordLastSetOn = passwordLastSetOn;
	}

	public Date getActiveFromDate() {
		return activeFromDate;
	}

	public void setActiveFromDate(Date activeFromDate) {
		this.activeFromDate = activeFromDate;
	}

	public Date getActiveTillDate() {
		return activeTillDate;
	}

	public void setActiveTillDate(Date activeTillDate) {
		this.activeTillDate = activeTillDate;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public EmailInfo getEmailInfo() {
		return emailInfo;
	}

	public void setEmailInfo(EmailInfo emailInfo) {
		this.emailInfo = emailInfo;
	}

	public PhoneInfo getPhoneInfo() {
		return phoneInfo;
	}

	public void setPhoneInfo(PhoneInfo phoneInfo) {
		this.phoneInfo = phoneInfo;
	}

	public Set<Authority> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(Set<Authority> authorities) {
		this.authorities = authorities;
	}

	public boolean isActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(boolean activeFlag) {
		this.activeFlag = activeFlag;
	}

	public boolean isLoginEnabled() {
		return loginEnabled;
	}

	public void setLoginEnabled(boolean loginEnabled) {
		this.loginEnabled = loginEnabled;
	}

	public boolean isAccountLocked() {
		return accountLocked;
	}

	public void setAccountLocked(boolean accountLocked) {
		this.accountLocked = accountLocked;
	}

	public boolean isDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(boolean deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLastName() {
		return lastName;
	}

	public Boolean getIsZoyloEmployee() {
		return isZoyloEmployee;
	}

	public void setIsZoyloEmployee(Boolean isZoyloEmployee) {
		this.isZoyloEmployee = isZoyloEmployee;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}	
	
	public String getEcommId() {
		return ecommId;
	}

	public void setEcommId(String ecommId) {
		this.ecommId = ecommId;
	}

	public String getEcommToken() {
		return ecommToken;
	}

	public void setEcommToken(String ecommToken) {
		this.ecommToken = ecommToken;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		User user = (User) o;
		return !(user.getId() == null || getId() == null) && Objects.equals(getId(), user.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	/*
	 * @Override public String toString() { return "User{" + "login='" + userName +
	 * '\'' + ", email='" + email + '\'' + ", imageUrl='" + imageUrl + '\'' +
	 * ", activated='" + activeFlag + '\'' + ", langKey='" + langKey + '\'' +
	 * ", activationKey='" + activationKey + '\'' + "}"; }
	 */
}
