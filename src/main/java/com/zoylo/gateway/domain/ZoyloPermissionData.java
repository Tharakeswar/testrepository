package com.zoylo.gateway.domain;

/**
 * @author nagaraju
 * Class to hold permission data
 *
 */
public class ZoyloPermissionData {
	
	private String languageId;
	private String languageCode;
	private String permissionName;

	public ZoyloPermissionData() {
	}

	public ZoyloPermissionData(String languageId, String languageCode, String permissionName) {
		super();
		this.languageId = languageId;
		this.languageCode = languageCode;
		this.permissionName = permissionName;
	}

	public String getLanguageId() {
		return languageId;
	}

	public void setLanguageId(String languageId) {
		this.languageId = languageId;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getPermissionName() {
		return permissionName;
	}

	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}


}
