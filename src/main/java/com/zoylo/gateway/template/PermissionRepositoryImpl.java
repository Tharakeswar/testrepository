package com.zoylo.gateway.template;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.stereotype.Component;
import com.zoylo.gateway.domain.Permission;
import com.zoylo.gateway.domain.PermissionData;
import com.zoylo.gateway.repository.ZoyloPermissionCustomRepository;
/**
 * 
 * @author Balram Sharma
 * @version 1.0
 * @description Class to implement repository method 
 *
 */
@Component
public class PermissionRepositoryImpl implements ZoyloPermissionCustomRepository{
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Override
	public Page<Permission> searchPermissionProperties(Pageable pageable, String permissionCode, String permissionName,
			boolean activeFlag, String authCategoryCode, String languageCode) {
		Query query = new Query();
		query.with(pageable);
		query.addCriteria(Criteria.where("activeFlag").is(activeFlag));
		
	
		if (permissionCode != null && permissionCode.length() != 0) {
			query.addCriteria(Criteria.where("permissionCode").is(permissionCode));
		}
		if (permissionName != null && permissionName.length() != 0) {
			permissionCode = permissionCode.toUpperCase();
			query.addCriteria(Criteria.where("permissionData.permissionName").is(permissionName));
		}
		if (authCategoryCode != null && authCategoryCode.length() != 0) {
			authCategoryCode = authCategoryCode.toUpperCase();
			query.addCriteria(Criteria.where("authCategoryCode").is(authCategoryCode));
		}
	
		List<Permission> permissionListFromDb = mongoTemplate.find(query, Permission.class);
	
		for (Permission zoyloPermission : permissionListFromDb) {
			List<PermissionData> PermissionDataList = zoyloPermission.getPermissionData();
			// filtered Permission based on languageCode and setting zoyloPermission
			List<PermissionData> filteredPermissionDataList = PermissionDataList.stream()
					.filter(permissionData -> languageCode.equals(permissionData.getLanguageCode()))
					.collect(Collectors.toList());
			zoyloPermission.setPermissionData(filteredPermissionDataList);
		}
		Page<Permission> pages = PageableExecutionUtils.getPage(
				permissionListFromDb, 
				                pageable, 
				                () -> mongoTemplate.count(query, Permission.class));

		return pages;
	}

}
