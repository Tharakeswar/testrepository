package com.zoylo.gateway.template;

import java.util.List;

import com.zoylo.gateway.model.ZoyloAuthVM;
import com.zoylo.gateway.mv.ZoyloUserAuthVM;

public interface ZoyloAuthRepo {
	public List<ZoyloAuthVM> findAuthByProviderId(String userId, String providerId);
	
	public List<ZoyloAuthVM> findAuthByProviderId(String userId, String providerId,String providerType);

	public List<ZoyloAuthVM> findAuthByProviderIdAndPermissonCode(String userId, String providerId,
			String permissionCode);
	
	public List<ZoyloUserAuthVM> findAuthByProviderIdAndRoleCode(String providerId,String providerTypeCode,
			String roleCode);

	public List<ZoyloUserAuthVM> findAuthByProviderIdAndRoleCode(String providerId, String roleCode);
}
