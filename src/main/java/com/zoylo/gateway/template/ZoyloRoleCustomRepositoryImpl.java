package com.zoylo.gateway.template;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.stereotype.Repository;

import com.zoylo.gateway.domain.Role;
import com.zoylo.gateway.domain.RoleData;
import com.zoylo.gateway.repository.ZoyloRoleCustomRepository;
@Repository
public class ZoyloRoleCustomRepositoryImpl implements ZoyloRoleCustomRepository{
	
	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public Page<Role> findByRoleProperties(Pageable pageable, String roleCode, String roleName, String authCategoryCode,
			String authCategoryName, boolean activeFlag, String languageCode) {
		
		Query query = new Query();
		query.with(pageable);
		query.addCriteria(Criteria.where("activeFlag").is(activeFlag));
		if (roleCode != null && roleCode.length() != 0) {
			roleCode = roleCode.toUpperCase();
			query.addCriteria(Criteria.where("roleCode").is(roleCode));
		}
		if (roleName != null && roleName.length() != 0) {
			query.addCriteria(Criteria.where("roleData.roleName").is(roleName));
		}
		if (authCategoryCode != null && authCategoryCode.length() != 0) {
			authCategoryCode = authCategoryCode.toUpperCase();
			query.addCriteria(Criteria.where("authCategoryCode").is(authCategoryCode));
		}
		if (authCategoryName != null && authCategoryName.length() != 0) {
			query.addCriteria(Criteria.where("authCategoryData.authCategoryName").is(authCategoryName));
		}
		List<Role> rolesListFromDb = mongoTemplate.find(query, Role.class);
		
		for (Role zoyloRole : rolesListFromDb) {
			List<RoleData> roleDataList = zoyloRole.getRoleData();
			// filtered Permission based on languageCode and setting zoyloPermission
			List<RoleData> filteredRoleDataList = roleDataList.stream()
					.filter(roleData -> roleData.getLanguageCode().equals(languageCode))
					.collect(Collectors.toList());
			zoyloRole.setRoleData(filteredRoleDataList);
		}
		Page<Role> pages = PageableExecutionUtils.getPage(
				rolesListFromDb, 
                pageable, 
                () -> mongoTemplate.count(query, Role.class));
		
		return pages;

	}

}
