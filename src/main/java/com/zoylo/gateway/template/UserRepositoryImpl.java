package com.zoylo.gateway.template;

import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.google.common.base.Strings;
import com.zoylo.gateway.domain.User;
import com.zoylo.gateway.service.util.Constants;

@Repository
public class UserRepositoryImpl implements ZoyloUserRepositoryRepo {
	@Autowired
	private MongoOperations mongoOperations;
	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public User findByPhoneNumberAndDeleteFlag(String phoneNumber, boolean deleteFlag) {
		String mobile = phoneNumber;
		if (!phoneNumber.startsWith(Constants.INDIA_MOBILE_CODE)) {
			phoneNumber = Constants.INDIA_MOBILE_CODE + phoneNumber;
		} else {
			mobile = phoneNumber.substring(3, phoneNumber.length());
		}
		Query query = new Query(new Criteria()
				.orOperator(Criteria.where("phoneInfo.phoneNumber").is(mobile),
						Criteria.where("phoneInfo.phoneNumber").is(phoneNumber))
				.andOperator(Criteria.where("deleteFlag").is(deleteFlag)));
		return mongoOperations.findOne(query, User.class);
	}

	@Override
	public User findByPhoneNumberAndVerifiedFlagAndDeleteFlag(String phoneNumber, boolean verifiedFlag,
			boolean deleteFlag) {
		String mobile = phoneNumber;
		if (!phoneNumber.startsWith(Constants.INDIA_MOBILE_CODE)) {
			phoneNumber = Constants.INDIA_MOBILE_CODE + phoneNumber;
		} else {
			mobile = phoneNumber.substring(3, phoneNumber.length());
		}
		Query query = new Query(new Criteria()
				.orOperator(Criteria.where("phoneInfo.phoneNumber").is(mobile),
						Criteria.where("phoneInfo.phoneNumber").is(phoneNumber))
				.and("deleteFlag").is(deleteFlag).and("phoneInfo.verifiedFlag").is(verifiedFlag));
		return mongoOperations.findOne(query, User.class);
	}

	@Override
	public User findByEmailInfoEmailAddressAndPhoneInfoPhoneNumber(String emailId, String phoneNumber) {
		String mobile = phoneNumber;
		if (!phoneNumber.startsWith(Constants.INDIA_MOBILE_CODE)) {
			phoneNumber = Constants.INDIA_MOBILE_CODE + phoneNumber;
		} else {
			mobile = phoneNumber.substring(3, phoneNumber.length());
		}
		Query query = new Query(new Criteria()
				.orOperator(Criteria.where("phoneInfo.phoneNumber").is(mobile),
						Criteria.where("phoneInfo.phoneNumber").is(phoneNumber))
				.andOperator(Criteria.where("emailInfo.emailAddress").is(emailId)));
		return mongoOperations.findOne(query, User.class);
	}

	@Override
	public User findByMobileNumber(String phoneNumber) {
		String mobile = phoneNumber;
		if (!phoneNumber.startsWith(Constants.INDIA_MOBILE_CODE)) {
			phoneNumber = Constants.INDIA_MOBILE_CODE + phoneNumber;
		} else {
			mobile = phoneNumber.substring(3, phoneNumber.length());
		}
		Query query = new Query(new Criteria().orOperator(Criteria.where("phoneInfo.phoneNumber").is(mobile),
				Criteria.where("phoneInfo.phoneNumber").is(phoneNumber)));
		return mongoOperations.findOne(query, User.class);
	}

	@Override
	public List<User> findByUserName(String value) {
		Query query = new Query();
		query.addCriteria(
				Criteria.where(Constants.USERNAME).is(value).regex(Pattern.compile(value, Pattern.CASE_INSENSITIVE))
						.andOperator(Criteria.where(Constants.DELETEFLAG).is(Boolean.FALSE)));

		List<User> user = mongoTemplate.find(query, User.class);

		return user;
	}

	@Override
	public List<User> findByEmail(String value) {
		Query query = new Query();
		query.addCriteria(
				Criteria.where(Constants.EMAIL).is(value).regex(Pattern.compile(value, Pattern.CASE_INSENSITIVE))
						.andOperator(Criteria.where(Constants.DELETEFLAG).is(Boolean.FALSE)));

		List<User> user = mongoTemplate.find(query, User.class);

		return user;
	}

	@Override
	public List<User> findByPhone(String value) {
		Query query = new Query();
		query.addCriteria(
				Criteria.where(Constants.PHONE).is(value).regex(Pattern.compile(value, Pattern.CASE_INSENSITIVE))
						.andOperator(Criteria.where(Constants.DELETEFLAG).is(Boolean.FALSE)));

		List<User> user = mongoTemplate.find(query, User.class);

		return user;
	}

	

	@Override
	public List<User> findByName(String value) {

		String[] nameArray = null;
		String firstName = "", middleName = "", LastName = "";

		Query query = null;
		if (!Strings.isNullOrEmpty(value)) {
			if (value != null && value.contains(" ")) {
				nameArray = value.split(" ");
				for (int count = 0; count < nameArray.length; count++) {
					if (count == 0 && nameArray[0] != null && nameArray[0] != "") {
						firstName = nameArray[0];
					}
					if (count == 1 && nameArray[1] != null && nameArray[1] != "") {
						middleName = nameArray[1];
					}
					if (count == 2 && nameArray[2] != null && nameArray[2] != "") {
						LastName = nameArray[2];
					}
				}
			}else if (!Strings.isNullOrEmpty(value)) {
				
				nameArray=new String[1];
				nameArray[0]=value;
				firstName = nameArray[0];
				
				
			}

			if (nameArray.length == 1) {
				query = new Query(new Criteria().orOperator(Criteria.where("firstName").is(firstName),
						Criteria.where("middleName").is(firstName), Criteria.where("lastName").is(firstName)

				).andOperator(Criteria.where(Constants.DELETEFLAG).is(Boolean.FALSE)));
			} else if (nameArray.length == 2) {
				query = new Query(new Criteria()
						.orOperator(Criteria.where("firstName").is(firstName),
								Criteria.where("middleName").is(middleName), Criteria.where("lastName").is(middleName))
						.andOperator(Criteria.where(Constants.DELETEFLAG).is(Boolean.FALSE)));
			} else if (nameArray.length == 3) {
				query = new Query(new Criteria()
						.orOperator(Criteria.where("firstName").is(firstName),
								Criteria.where("middleName").is(middleName), Criteria.where("lastName").is(LastName))
						.andOperator(Criteria.where(Constants.DELETEFLAG).is(Boolean.FALSE)));
			}
		}

		List<User> user = mongoTemplate.find(query, User.class);

		return user;
	}

	@Override
	public List<User> findByPhoneInfoPhoneNumberIn(List<String> phoneNumbers) {
		Criteria criterias[] = new Criteria[phoneNumbers.size()];
		int index = -1;
		for (String phoneNumber : phoneNumbers) {
			if(phoneNumber.contains("+91")){
				phoneNumber = phoneNumber.replace("+91", "");
			}
			index++;
			Criteria phoneCriteria = new Criteria();
			phoneCriteria.andOperator(Criteria.where(Constants.PHONE).regex(Pattern.compile(phoneNumber, Pattern.CASE_INSENSITIVE)),
					Criteria.where("phoneInfo").ne(null));
			criterias[index] = phoneCriteria;
		}
		Criteria criteria = new Criteria();
		criteria.orOperator(criterias);
		Query query = new Query(criteria);
		List<User> user = mongoTemplate.find(query, User.class);
		return user;
	}
}
