package com.zoylo.gateway.template;


import java.util.List;
import java.util.Optional;

import com.zoylo.gateway.domain.User;

public interface ZoyloUserRepositoryRepo {
	User findByPhoneNumberAndDeleteFlag(String phoneNumber,boolean deleteFlag);
	User findByPhoneNumberAndVerifiedFlagAndDeleteFlag(String phoneNumber ,boolean verifiedFlag,
			boolean deleteFlag);
	User findByEmailInfoEmailAddressAndPhoneInfoPhoneNumber(String emailId, String mobileNumber);
	
	User findByMobileNumber(String phoneNumber);
	
	List<User> findByUserName(String value);
	
	List<User> findByEmail(String value);
	
	List<User> findByPhone(String value);
	
	List<User> findByName(String value);
	
	public List<User> findByPhoneInfoPhoneNumberIn(List<String> phoneNumbers);

}
