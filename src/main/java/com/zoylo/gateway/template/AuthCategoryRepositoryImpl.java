package com.zoylo.gateway.template;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.stereotype.Repository;

import com.zoylo.gateway.domain.AuthCategoryData;
import com.zoylo.gateway.domain.ZoyloAuthCategory;
import com.zoylo.gateway.repository.AuthCategoryCustomRepository;
/**
 * 
 * @author Balram Sharma
 * @version 1.0
 * @description This class is used to implement AuthCategory Repository method
 */
@Repository
public class AuthCategoryRepositoryImpl implements AuthCategoryCustomRepository{
	
	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public Page<ZoyloAuthCategory> searchAuthCategory(Pageable pageable, String authCategoryCode,
			String authCategoryName, boolean activeFlag, String languageCode) {

		Query query = new Query();
		query.with(pageable);
		query.addCriteria(Criteria.where("activeFlag").is(activeFlag));

		if (authCategoryCode != null && authCategoryCode.length() != 0) {
			authCategoryCode = authCategoryCode.toUpperCase();
			query.addCriteria(Criteria.where("authCategoryCode").is(authCategoryCode));
		}
		if (authCategoryName != null && authCategoryName.length() != 0) {
			query.addCriteria(Criteria.where("authCategoryData.authCategoryName").is(authCategoryName));
		}
		query.limit(pageable.getPageSize());
		query.skip(pageable.getPageNumber() * pageable.getPageSize());

		List<ZoyloAuthCategory> authCategoryListFromDb = mongoTemplate.find(query, ZoyloAuthCategory.class);

		for (ZoyloAuthCategory zoyloAuthCategory : authCategoryListFromDb) {
			List<AuthCategoryData> authCategoryDataList = zoyloAuthCategory.getAuthCategoryData();
			// filtered Permission based on languageCode and setting
			// zoyloPermission
			List<AuthCategoryData> filteredAuthCategoryDataList = authCategoryDataList.stream()
					.filter(authCategoryData -> authCategoryData.getLanguageCode().equals(languageCode))
					.collect(Collectors.toList());
			zoyloAuthCategory.setAuthCategoryData(filteredAuthCategoryDataList);
		}
		Page<ZoyloAuthCategory> pages = PageableExecutionUtils.getPage(
				authCategoryListFromDb, 
				                pageable, 
				                () -> mongoTemplate.count(query, ZoyloAuthCategory.class));
		return pages;
	}

}
