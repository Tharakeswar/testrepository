package com.zoylo.gateway.template;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.stereotype.Repository;

import com.google.common.base.Strings;
import com.zoylo.gateway.domain.User;
import com.zoylo.gateway.repository.CustomUserRepository;

/**
 * 
 * @author arvind.rawat
 * @version 1.0
 *
 */
@Repository
public class CustomUserRepositoryImpl implements CustomUserRepository {

	@Autowired
	private MongoOperations mongoOperations;

	
	
	/* (non-Javadoc)
	 * @see com.zoylo.gateway.repository.CustomUserRepository#fetchUserList(boolean, boolean)
	 */
	@Override
	public List<User> fetchUserList(boolean isZoyloEmployee, boolean activeFlag){
		Query query = new Query();
		
		query.addCriteria(Criteria.where("activeFlag").is(activeFlag));
		query.addCriteria(Criteria.where("isZoyloEmployee").is(isZoyloEmployee));
		query.addCriteria(Criteria.where("firstName").ne(null));
		query.fields().include("id");
		query.fields().include("isZoyloEmployee");
		query.fields().include("activeFlag");
		query.fields().include("firstName");
		query.fields().include("middleName");
		query.fields().include("lastName");
		query.with(new Sort(new Order(Direction.ASC, "firstName")));
		List<User> usersList = mongoOperations.find(query, User.class);
		return usersList;
	}
	
	
	/***
	 * @author arvind.rawat
	 * @param emailAddress
	 * @param phoneNumber
	 * @param deleteFlag
	 * @return
	 */
	@Override
	public User findByEmailAddressOrPhoneNumber(String emailAddress, String phoneNumber, boolean deleteFlag) {
		Query query = new Query(new Criteria()
				.orOperator(Criteria.where("emailInfo.emailAddress").is(emailAddress),
						Criteria.where("phoneInfo.phoneNumber").is(phoneNumber))
				.andOperator(Criteria.where("deleteFlag").is(deleteFlag)));
		return mongoOperations.findOne(query, User.class);
	}

	/**
	 * @author arvind.rawat
	 * @description get all admin user
	 */
	@Override
	public Map<Long, List<User>> getAllAdminUsers(Pageable pageable, String searchField) {
		String[] nameArray = null;
		String firstName = "", middleName = "", LastName = "";
		Query query = null;
		if (!Strings.isNullOrEmpty(searchField)) {
			if (searchField != null && searchField.contains(" ")) {
				nameArray = searchField.split(" ");
				for (int count = 0; count < nameArray.length; count++) {
					if (count == 0 && nameArray[0] != null && nameArray[0] != "") {
						firstName = nameArray[0];
					}
					if (count == 1 && nameArray[1] != null && nameArray[1] != "") {
						middleName = nameArray[1];
					}
					if (count == 2 && nameArray[2] != null && nameArray[2] != "") {
						LastName = nameArray[2];
					}
				}
			} else if (!Strings.isNullOrEmpty(searchField)) {

				nameArray = new String[1];
				nameArray[0] = searchField;
			}

			String phoneNumber = searchField.replaceAll("\\+", "");
			if (nameArray.length == 1) {
				firstName = searchField;
				query = new Query(new Criteria()
						.orOperator(Criteria.where("emailInfo.emailAddress").is(searchField.trim()),
								Criteria.where("firstName").is(firstName),
								Criteria.where("middleName").is(firstName),
								Criteria.where("lastName").is(firstName),
								Criteria.where("phoneInfo.phoneNumber").regex(phoneNumber))
						.andOperator(Criteria.where("deleteFlag").is(Boolean.FALSE)/*,
								Criteria.where("isZoyloEmployee").is(Boolean.TRUE)*/));
			} else if (nameArray.length == 2) {
				query = new Query(new Criteria()
						.orOperator(Criteria.where("emailInfo.emailAddress").is(searchField.trim()),
								Criteria.where("firstName").is(firstName),
								Criteria.where("middleName").is(firstName),
								Criteria.where("lastName").is(firstName),
								Criteria.where("middleName").is(middleName),
								Criteria.where("lastName").is(middleName),
								Criteria.where("phoneInfo.phoneNumber").regex(phoneNumber))
						.andOperator(Criteria.where("deleteFlag").is(Boolean.FALSE)/*,
								Criteria.where("isZoyloEmployee").is(Boolean.TRUE)*/));
			} else if (nameArray.length == 3) {
				query = new Query(new Criteria()
						.orOperator(Criteria.where("emailInfo.emailAddress").is(searchField.trim()),
								Criteria.where("firstName").is(firstName),
								Criteria.where("middleName").is(middleName),
								Criteria.where("lastName").is(middleName),
								Criteria.where("lastName").is(LastName),
								Criteria.where("phoneInfo.phoneNumber").regex(phoneNumber))
						.andOperator(Criteria.where("deleteFlag").is(Boolean.FALSE)/*,
								Criteria.where("isZoyloEmployee").is(Boolean.TRUE)*/));
			}
		} else {
			query = new Query(/*Criteria.where("isZoyloEmployee").is(Boolean.TRUE)
					.andOperator*/(Criteria.where("deleteFlag").is(false)));
		}
		query = query.with(pageable);
		Long total = mongoOperations.count(query, User.class);
		List<User> userList = mongoOperations.find(query, User.class);
		Map<Long, List<User>> map = new HashMap<>();
		map.put(total, userList);
		return map;
	}
	
	/**
	 * This is used in Application Users Search
	 * @author RakeshH
	 * @param pageable
	 * @param activeFlag
	 * @param isZoyloEmployee
	 * @param firstName
	 * @param lastName
	 * @param emailId
	 * @param mobileNumber
	 */
	
	public Page<User> searchUser (Pageable pageable, boolean activeFlag, boolean isZoyloEmployee, 
			String firstName, String lastName, String emailId, String mobileNumber) {
		
		Query query = new Query();
		query.with(pageable);
		
		query.addCriteria(Criteria.where("activeFlag").is(activeFlag));
		query.addCriteria(Criteria.where("isZoyloEmployee").is(isZoyloEmployee));
		
		if (firstName != null)
		query.addCriteria(Criteria.where("firstName").is(firstName));
		
		if (lastName != null)
		query.addCriteria(Criteria.where("lastName").is(lastName));
		
		if (emailId != null)
		query.addCriteria(Criteria.where("emailInfo.emailAddress").is(emailId));
		
		if (mobileNumber != null)
		query.addCriteria(Criteria.where("phoneInfo.phoneNumber").is(mobileNumber));

		List<User> usersList = mongoOperations.find(query, User.class);
		Page<User> pages = PageableExecutionUtils.getPage(
				usersList, 
				                pageable, 
				                () -> mongoOperations.count(query, User.class));
		
		return pages;

		
	}
}
