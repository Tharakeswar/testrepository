package com.zoylo.gateway.template;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.aggregation.ProjectionOperation;
import org.springframework.data.mongodb.core.aggregation.UnwindOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import com.zoylo.gateway.model.ZoyloAuthVM;
import com.zoylo.gateway.mv.ZoyloUserAuthVM;

@Repository
public class ZoyloUserAuthRepositoryImpl implements ZoyloAuthRepo {

	private static final String SERVICE_PROVIDERS_LIST = "serviceProvidersList";
	private static final String PROVIDER_ID = "providerId";
	private static final String USER_ID = "userId";
	private static final String GRANTED_ROLES_LIST = "grantedRolesList";
	private static final String GRANTED_PERMISSIONS_LIST = "grantedPermissionsList";
	private static final String PERMISSION_CODE = "permissionCode";
	private static final String PROVIDER_TYPE_CODE = "providerTypeCode";
	private static final String ROLE_CODE = "roleCode";
	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public List<ZoyloAuthVM> findAuthByProviderId(String userId, String providerId) {
		UnwindOperation unWind = Aggregation.unwind(SERVICE_PROVIDERS_LIST);
		MatchOperation matchOperation = Aggregation.match(
				Criteria.where(SERVICE_PROVIDERS_LIST + "." + PROVIDER_ID).is(providerId).and(USER_ID).is(userId));
		ProjectionOperation project = Aggregation.project(SERVICE_PROVIDERS_LIST);
		Aggregation aggregation = Aggregation.newAggregation(unWind, matchOperation, project);
		AggregationResults<ZoyloAuthVM> aggregationResults = mongoTemplate.aggregate(aggregation, "zoyloUserAuth",
				ZoyloAuthVM.class);
		List<ZoyloAuthVM> comboServicesLists = aggregationResults.getMappedResults();
		return comboServicesLists;
	}

	@Override
	public List<ZoyloAuthVM> findAuthByProviderIdAndPermissonCode(String userId, String providerId,
			String permissionCode) {
		UnwindOperation unWind = Aggregation.unwind(SERVICE_PROVIDERS_LIST);
		MatchOperation matchOperation = Aggregation.match(Criteria.where(SERVICE_PROVIDERS_LIST + "." + PROVIDER_ID)
				.is(providerId).and(USER_ID).is(userId).and(SERVICE_PROVIDERS_LIST + "." + GRANTED_ROLES_LIST + "."
						+ GRANTED_PERMISSIONS_LIST + "." + PERMISSION_CODE)
				.is(permissionCode));
		ProjectionOperation project = Aggregation.project(SERVICE_PROVIDERS_LIST);
		Aggregation aggregation = Aggregation.newAggregation(unWind, matchOperation, project);
		AggregationResults<ZoyloAuthVM> aggregationResults = mongoTemplate.aggregate(aggregation, "zoyloUserAuth",
				ZoyloAuthVM.class);
		List<ZoyloAuthVM> comboServicesLists = aggregationResults.getMappedResults();
		return comboServicesLists;
	}

	@Override
	public List<ZoyloAuthVM> findAuthByProviderId(String userId, String providerId, String providerType) {
		UnwindOperation unWind = Aggregation.unwind(SERVICE_PROVIDERS_LIST);
		MatchOperation matchOperation = Aggregation
				.match(Criteria.where(SERVICE_PROVIDERS_LIST + "." + PROVIDER_ID).is(providerId).and(USER_ID).is(userId)
						/*.and(SERVICE_PROVIDERS_LIST + "." + PROVIDER_TYPE_CODE).is(providerType)*/);
		ProjectionOperation project = Aggregation.project(SERVICE_PROVIDERS_LIST);
		Aggregation aggregation = Aggregation.newAggregation(unWind, matchOperation, project);
		AggregationResults<ZoyloAuthVM> aggregationResults = mongoTemplate.aggregate(aggregation, "zoyloUserAuth",
				ZoyloAuthVM.class);
		List<ZoyloAuthVM> comboServicesLists = aggregationResults.getMappedResults();
		return comboServicesLists;
	}

	@Override
	public List<ZoyloUserAuthVM> findAuthByProviderIdAndRoleCode(String providerId, String providerTypeCode,
			String roleCode) {
		UnwindOperation unWind = Aggregation.unwind(SERVICE_PROVIDERS_LIST);
		MatchOperation matchOperation = Aggregation.match(Criteria.where(SERVICE_PROVIDERS_LIST + "." + PROVIDER_ID)
				.is(providerId).and(SERVICE_PROVIDERS_LIST + "." + GRANTED_ROLES_LIST + "." + ROLE_CODE).is(roleCode)
				.and(SERVICE_PROVIDERS_LIST + "." + PROVIDER_TYPE_CODE).is(providerTypeCode));
		Aggregation aggregation = Aggregation.newAggregation(unWind, matchOperation);
		AggregationResults<ZoyloUserAuthVM> aggregationResults = mongoTemplate.aggregate(aggregation, "zoyloUserAuth",
				ZoyloUserAuthVM.class);
		List<ZoyloUserAuthVM> comboServicesLists = aggregationResults.getMappedResults();
		return comboServicesLists;
	}

	@Override
	public List<ZoyloUserAuthVM> findAuthByProviderIdAndRoleCode(String providerId, String roleCode) {
		UnwindOperation unWind = Aggregation.unwind(SERVICE_PROVIDERS_LIST);
		MatchOperation matchOperation = Aggregation.match(Criteria.where(SERVICE_PROVIDERS_LIST + "." + PROVIDER_ID)
				.is(providerId).and(SERVICE_PROVIDERS_LIST + "." + GRANTED_ROLES_LIST + "." + ROLE_CODE).is(roleCode));
		Aggregation aggregation = Aggregation.newAggregation(unWind, matchOperation);
		AggregationResults<ZoyloUserAuthVM> aggregationResults = mongoTemplate.aggregate(aggregation, "zoyloUserAuth",
				ZoyloUserAuthVM.class);
		List<ZoyloUserAuthVM> comboServicesLists = aggregationResults.getMappedResults();
		return comboServicesLists;
	}

}
