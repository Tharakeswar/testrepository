package com.zoylo.gateway.model;

/**
 * 
 * @author ankur
 * @version 1.0
 */
public class DoctorVM {
	private String id;
	private String spacializationCode;
	private String gendar;
	private String qualification;
	private String medicalRegistrationNumber;
	private String associatedHospitalName;
	private double latitude;
	private double longitude;
	private String cityCode;
	private String stateCode;

	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public String getSpacializationCode() {
		return spacializationCode;
	}

	public void setSpacializationCode(String spacializationCode) {
		this.spacializationCode = spacializationCode;
	}

	public String getQualification() {
		return qualification;
	}

	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	public String getGendar() {
		return gendar;
	}

	public void setGendar(String gendar) {
		this.gendar = gendar;
	}

	public String getMedicalRegistrationNumber() {
		return medicalRegistrationNumber;
	}

	public void setMedicalRegistrationNumber(String medicalRegistrationNumber) {
		this.medicalRegistrationNumber = medicalRegistrationNumber;
	}

	public String getAssociatedHospitalName() {
		return associatedHospitalName;
	}

	public void setAssociatedHospitalName(String associatedHospitalName) {
		this.associatedHospitalName = associatedHospitalName;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

}
