package com.zoylo.gateway.model;

import com.zoylo.gateway.domain.ServiceProvidersList;

public class ZoyloAuthVM {
	private ServiceProvidersList serviceProvidersList;

	public ServiceProvidersList getServiceProvidersList() {
		return serviceProvidersList;
	}

	public void setServiceProvidersList(ServiceProvidersList serviceProvidersList) {
		this.serviceProvidersList = serviceProvidersList;
	}

}
