package com.zoylo.gateway.model;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.validation.annotation.Validated;

import com.zoylo.gateway.web.rest.errors.CustomValidationConstant;
import com.zoylo.validator.MobileNumber;

@Validated
public class ChangeMobileEmailVM {
	private String id;
	@NotEmpty(message = CustomValidationConstant.EMAIL_NOT_EMPTY)
	@Email(message = CustomValidationConstant.EMAIL_CORE)
	private String emailAddress;
	@Size(min = 10, max = 10, message = CustomValidationConstant.MOBILE_LENGTH)
	@MobileNumber(message = CustomValidationConstant.MOBILE_DIGIT)
	private String phoneNumber;

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

}
