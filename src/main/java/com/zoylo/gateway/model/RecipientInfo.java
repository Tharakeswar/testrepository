package com.zoylo.gateway.model;
/**
 * @author damini.arora
 * @version 1.0
 */
public class RecipientInfo {
	private String recipientId;
	private String recipientZoyloId;
	private String recipientFirstName;
	private String recipientLastName;
	private String recipientPhone;
	private String recipientEmail;
	public String getRecipientId() {
		return recipientId;
	}
	public void setRecipientId(String recipientId) {
		this.recipientId = recipientId;
	}
	public String getRecipientZoyloId() {
		return recipientZoyloId;
	}
	public void setRecipientZoyloId(String recipientZoyloId) {
		this.recipientZoyloId = recipientZoyloId;
	}
	public String getRecipientFirstName() {
		return recipientFirstName;
	}
	public void setRecipientFirstName(String recipientFirstName) {
		this.recipientFirstName = recipientFirstName;
	}
	public String getRecipientLastName() {
		return recipientLastName;
	}
	public void setRecipientLastName(String recipientLastName) {
		this.recipientLastName = recipientLastName;
	}
	public String getRecipientPhone() {
		return recipientPhone;
	}
	public void setRecipientPhone(String recipientPhone) {
		this.recipientPhone = recipientPhone;
	}
	public String getRecipientEmail() {
		return recipientEmail;
	}
	public void setRecipientEmail(String recipientEmail) {
		this.recipientEmail = recipientEmail;
	}
	

}
