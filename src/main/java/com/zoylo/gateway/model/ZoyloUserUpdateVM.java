package com.zoylo.gateway.model;

/**
 * @author nagaraju
 * Class to hold data for updating user.
 *
 */
public class ZoyloUserUpdateVM {
	
	private String id;
	private String zoyloId;
	private String userName;
	private String firstName;
	private String middleName;
	private String lastName;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getZoyloId() {
		return zoyloId;
	}
	public void setZoyloId(String zoyloId) {
		this.zoyloId = zoyloId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

}
