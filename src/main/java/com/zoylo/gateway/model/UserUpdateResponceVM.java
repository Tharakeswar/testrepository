package com.zoylo.gateway.model;

import com.zoylo.gateway.domain.User;

public class UserUpdateResponceVM {
	private boolean isEmailExits;
	private boolean isMobileExits;
	private boolean isUpdatedSuccessfully;
	private String userId;

	public boolean isEmailExits() {
		return isEmailExits;
	}

	public void setEmailExits(boolean isEmailExits) {
		this.isEmailExits = isEmailExits;
	}

	public boolean isMobileExits() {
		return isMobileExits;
	}

	public void setMobileExits(boolean isMobileExits) {
		this.isMobileExits = isMobileExits;
	}

	public boolean isUpdatedSuccessfully() {
		return isUpdatedSuccessfully;
	}

	public void setUpdatedSuccessfully(boolean isUpdatedSuccessfully) {
		this.isUpdatedSuccessfully = isUpdatedSuccessfully;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
}
