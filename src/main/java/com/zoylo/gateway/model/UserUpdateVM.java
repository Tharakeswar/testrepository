package com.zoylo.gateway.model;
/***
 * Model to update the User
 * @author Iti Gupta
 * @version 1.0
 *
 */
public class UserUpdateVM {
	
	private String userId;
	private String zoyloId;
	private String firstName;
	private String middleName;
	private String lastName;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getZoyloId() {
		return zoyloId;
	}
	public void setZoyloId(String zoyloId) {
		this.zoyloId = zoyloId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	

}
