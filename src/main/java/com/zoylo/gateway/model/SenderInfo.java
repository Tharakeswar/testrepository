package com.zoylo.gateway.model;
/**
 * @author damini.arora
 * @version 1.0
 */
public class SenderInfo {
	private String senderId;
	private String senderZoyloId;
	private String senderFirstname;
	private String senderLastName;
	private String senderPhone;
	private String senderEmail;
	public String getSenderId() {
		return senderId;
	}
	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}
	public String getSenderZoyloId() {
		return senderZoyloId;
	}
	public void setSenderZoyloId(String senderZoyloId) {
		this.senderZoyloId = senderZoyloId;
	}
	public String getSenderFirstname() {
		return senderFirstname;
	}
	public void setSenderFirstname(String senderFirstname) {
		this.senderFirstname = senderFirstname;
	}
	public String getSenderLastName() {
		return senderLastName;
	}
	public void setSenderLastName(String senderLastName) {
		this.senderLastName = senderLastName;
	}
	public String getSenderPhone() {
		return senderPhone;
	}
	public void setSenderPhone(String senderPhone) {
		this.senderPhone = senderPhone;
	}
	public String getSenderEmail() {
		return senderEmail;
	}
	public void setSenderEmail(String senderEmail) {
		this.senderEmail = senderEmail;
	}

}
