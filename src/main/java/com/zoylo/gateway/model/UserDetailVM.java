/**
 * 
 */
package com.zoylo.gateway.model;

import java.util.Date;
import java.util.List;

import com.zoylo.gateway.domain.EmailInfo;
import com.zoylo.gateway.domain.PhoneInfo;

/**
 * @author user
 * @version 1.0
 */
public class UserDetailVM {

	private String id;

	private String firstName;
	private String lastName;

	private String zoyloId;

	private boolean activeFlag;

	private Date activeFromDate;

	private Date activeTillDate;

	private boolean loginEnabled;

	private boolean accountLocked;

	private EmailInfo emailInfo;
	private PhoneInfo phoneInfo;

	private String userType;
	private List<UserRoleVM> userRoleVM;
	private Boolean isZoyloEmployee;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getZoyloId() {
		return zoyloId;
	}

	public void setZoyloId(String zoyloId) {
		this.zoyloId = zoyloId;
	}

	public boolean isActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(boolean activeFlag) {
		this.activeFlag = activeFlag;
	}

	public Date getActiveFromDate() {
		return activeFromDate;
	}

	public void setActiveFromDate(Date activeFromDate) {
		this.activeFromDate = activeFromDate;
	}

	public Date getActiveTillDate() {
		return activeTillDate;
	}

	public void setActiveTillDate(Date activeTillDate) {
		this.activeTillDate = activeTillDate;
	}

	public boolean isLoginEnabled() {
		return loginEnabled;
	}

	public void setLoginEnabled(boolean loginEnabled) {
		this.loginEnabled = loginEnabled;
	}

	public boolean isAccountLocked() {
		return accountLocked;
	}

	public void setAccountLocked(boolean accountLocked) {
		this.accountLocked = accountLocked;
	}

	public EmailInfo getEmailInfo() {
		return emailInfo;
	}

	public void setEmailInfo(EmailInfo emailInfo) {
		this.emailInfo = emailInfo;
	}

	public PhoneInfo getPhoneInfo() {
		return phoneInfo;
	}

	public void setPhoneInfo(PhoneInfo phoneInfo) {
		this.phoneInfo = phoneInfo;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getUserType() {
		return userType;
	}

	public List<UserRoleVM> getUserRoleVM() {
		return userRoleVM;
	}

	public void setUserRoleVM(List<UserRoleVM> userRoleVM) {
		this.userRoleVM = userRoleVM;
	}

	public Boolean getIsZoyloEmployee() {
		return isZoyloEmployee;
	}

	public void setIsZoyloEmployee(Boolean isZoyloEmployee) {
		this.isZoyloEmployee = isZoyloEmployee;
	}

}
