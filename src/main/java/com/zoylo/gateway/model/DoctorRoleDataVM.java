package com.zoylo.gateway.model;

import java.util.List;

import com.zoylo.gateway.domain.ProviderTypeData;

/**
 * 
 * @author Diksha Gupta
 * @version 1.0
 * @description Model for add doctor in doctor pms
 *
 */
public class DoctorRoleDataVM {
	private String role;
	private String providerTypeId;// String. Identifier of the provider type.
	private String providerTypeCode;
	private List<ProviderTypeData> providerTypeData;
	private List<String> providerIdList;
	private String providerCode; // String. Mandatory. Code associated with the
									// service provider.
	private List<String> providerNameList; // String. Mandatory. Name of the service
									// provider
	private String providerSearchName;
	private String userId;

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getProviderTypeId() {
		return providerTypeId;
	}

	public void setProviderTypeId(String providerTypeId) {
		this.providerTypeId = providerTypeId;
	}

	public String getProviderTypeCode() {
		return providerTypeCode;
	}

	public void setProviderTypeCode(String providerTypeCode) {
		this.providerTypeCode = providerTypeCode;
	}

	public List<ProviderTypeData> getProviderTypeData() {
		return providerTypeData;
	}

	public void setProviderTypeData(List<ProviderTypeData> providerTypeData) {
		this.providerTypeData = providerTypeData;
	}

	public String getProviderCode() {
		return providerCode;
	}

	public void setProviderCode(String providerCode) {
		this.providerCode = providerCode;
	}

	public String getProviderSearchName() {
		return providerSearchName;
	}

	public void setProviderSearchName(String providerSearchName) {
		this.providerSearchName = providerSearchName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public List<String> getProviderIdList() {
		return providerIdList;
	}

	public void setProviderIdList(List<String> providerIdList) {
		this.providerIdList = providerIdList;
	}

	public List<String> getProviderNameList() {
		return providerNameList;
	}

	public void setProviderNameList(List<String> providerNameList) {
		this.providerNameList = providerNameList;
	}

}
