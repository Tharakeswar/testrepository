package com.zoylo.gateway.model;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Constraint(validatedBy = UserTypeValidator.class)
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface IsValidUserType {

	String listOfUserType();
	String message() default "Provide valid User Type";
	
	Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
