package com.zoylo.gateway.model;

/***
 * 
 * @author Iti gupta
 * @version 1.0
 *
 */
public class PermissionData {

	private String languageId;
	private String languageCode;
	private String permissionName;

	public PermissionData() {
	}

	public PermissionData(String languageId, String languageCode, String permissionName) {
		super();
		this.languageId = languageId;
		this.languageCode = languageCode;
		this.permissionName = permissionName;
	}

	public String getLanguageId() {
		return languageId;
	}

	public void setLanguageId(String languageId) {
		this.languageId = languageId;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getPermissionName() {
		return permissionName;
	}

	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}

}
