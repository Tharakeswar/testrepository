package com.zoylo.gateway.model;

import java.util.List;

public class PMSUserRoleMVList {

	private List<PMSUserRoleMV> pMSUserRoleMVs;

	public List<PMSUserRoleMV> getpMSUserRoleMVs() {
		return pMSUserRoleMVs;
	}

	public void setpMSUserRoleMVs(List<PMSUserRoleMV> pMSUserRoleMVs) {
		this.pMSUserRoleMVs = pMSUserRoleMVs;
	}
}
