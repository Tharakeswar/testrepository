package com.zoylo.gateway.model;

/**
 * 
 * @author salaria
 *
 */
public class ZoyloLeadGenerationVM {
	private String firstName;
	private String lastName;
	private String mobileNumber;
	private String emailId;
	private String gender;
	private String medicalRegistrationNumber;
	private String associatedHospitalName;
	private double latitude;
	private double longitude;
	private String cityCode;
	private String stateCode;
	private String userType;// Doctor/Diagnostic
	private String diagnosticCenterName;
	private AddressVM diagnosticCenterAddress;
	private String specializationCode;
	private String qualification;
	private String deviceType;
	private String deviceAgent;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMedicalRegistrationNumber() {
		return medicalRegistrationNumber;
	}

	public void setMedicalRegistrationNumber(String medicalRegistrationNumber) {
		this.medicalRegistrationNumber = medicalRegistrationNumber;
	}

	public String getAssociatedHospitalName() {
		return associatedHospitalName;
	}

	public void setAssociatedHospitalName(String associatedHospitalName) {
		this.associatedHospitalName = associatedHospitalName;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getDiagnosticCenterName() {
		return diagnosticCenterName;
	}

	public void setDiagnosticCenterName(String diagnosticCenterName) {
		this.diagnosticCenterName = diagnosticCenterName;
	}

	public AddressVM getDiagnosticCenterAddress() {
		return diagnosticCenterAddress;
	}

	public void setDiagnosticCenterAddress(AddressVM diagnosticCenterAddress) {
		this.diagnosticCenterAddress = diagnosticCenterAddress;
	}

	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	public String getQualification() {
		return qualification;
	}

	public void setSpecializationCode(String specializationCode) {
		this.specializationCode = specializationCode;
	}

	public String getSpecializationCode() {
		return specializationCode;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getDeviceAgent() {
		return deviceAgent;
	}

	public void setDeviceAgent(String deviceAgent) {
		this.deviceAgent = deviceAgent;
	}
	

}
