package com.zoylo.gateway.model;

import java.util.Date;
import java.util.List;

/**
 * 
 * @author Diksha Gupta
 * @version 1.0
 * @description Model for patient info in book appointment
 *
 */
public class UserPMSDataVM {

	private String userId;
	private String zoyloId;
	private String firstName;
	private String middleName;
	private String lastName;
	private String gender;
	private Date dateOfBirth;
	private String phoneNumber;
	private String emailAddress;
	private boolean existsEmail;
	private boolean existsMobile;

	public boolean isExistsEmail() {
		return existsEmail;
	}

	public void setExistsEmail(boolean existsEmail) {
		this.existsEmail = existsEmail;
	}

	public boolean isExistsMobile() {
		return existsMobile;
	}

	public void setExistsMobile(boolean existsMobile) {
		this.existsMobile = existsMobile;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getZoyloId() {
		return zoyloId;
	}

	public void setZoyloId(String zoyloId) {
		this.zoyloId = zoyloId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

}
