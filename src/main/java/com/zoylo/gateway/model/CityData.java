package com.zoylo.gateway.model;

/***
 * 
 * @author Iti Gupta
 * @version 1.0
 * 
 *
 */
public class CityData {

	private String languageId;
	private String languageCode;
	private String cityName;

	public CityData() {
	}

	public CityData(String languageId, String languageCode, String cityName) {
		super();
		this.languageId = languageId;
		this.languageCode = languageCode;
		this.cityName = cityName;
	}

	public String getLanguageId() {
		return languageId;
	}

	public void setLanguageId(String languageId) {
		this.languageId = languageId;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

}
