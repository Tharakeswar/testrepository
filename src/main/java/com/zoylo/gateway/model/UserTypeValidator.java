package com.zoylo.gateway.model;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UserTypeValidator implements ConstraintValidator<IsValidUserType,String>{

	private String listOfUserType;
	
	@Override
	public void initialize(IsValidUserType constraintAnnotation) {
		
		listOfUserType = constraintAnnotation.listOfUserType();
	}

	@Override
	public boolean isValid(String userType, ConstraintValidatorContext context) {
		if(userType == null) {
			return false;
		}
		 if(userType.matches(listOfUserType)) {
			 return true;
		 }
		return false;
	}

	
}
