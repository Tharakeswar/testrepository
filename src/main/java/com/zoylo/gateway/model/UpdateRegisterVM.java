package com.zoylo.gateway.model;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.validation.annotation.Validated;

import com.zoylo.gateway.web.rest.errors.CustomValidationConstant;
import com.zoylo.validator.MobileNumber;

@Validated
public class UpdateRegisterVM {
	private String id;
	@Size(min = 3, max = 50, message = CustomValidationConstant.FIRST_NAME_LENGTH)
	private String firstName;
	@Size(min = 3, max = 50, message = CustomValidationConstant.LAST_NAME_LENGTH)
	private String lastName;
	private String zoyloId;
	@NotEmpty(message = CustomValidationConstant.EMAIL_NOT_EMPTY)
	@Email(message = CustomValidationConstant.EMAIL_CORE)
	private String emailAddress;

	@Size(min = 10, max = 13, message = CustomValidationConstant.MOBILE_LENGTH)
	@MobileNumber(message = CustomValidationConstant.MOBILE_DIGIT)
	private String phoneNumber;

	private String encryptedPassword;
	private boolean isTermsAndCondtionAccpected;

	private String deviceId;
	private String deviceType;

	@NotEmpty(message = CustomValidationConstant.USER_TYPE_NOT_EMPTY)
	private String userType;

	private AdminVM adminDetail;
	private boolean activeFlag;
	private Boolean isZoyloEmployee;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public boolean getTermsAndCondtionAccpected() {
		return isTermsAndCondtionAccpected;
	}

	public void setTermsAndCondtionAccpected(boolean isTermsAndCondtionAccpected) {
		this.isTermsAndCondtionAccpected = isTermsAndCondtionAccpected;
	}

	public String getZoyloId() {
		return zoyloId;
	}

	public void setZoyloId(String zoyloId) {
		this.zoyloId = zoyloId;
	}

	public String getEncryptedPassword() {
		return encryptedPassword;
	}

	public void setEncryptedPassword(String encryptedPassword) {
		this.encryptedPassword = encryptedPassword;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public AdminVM getAdminDetail() {
		return adminDetail;
	}

	public void setAdminDetail(AdminVM adminDetail) {
		this.adminDetail = adminDetail;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}
	public boolean getIsActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(boolean activeFlag) {
		this.activeFlag = activeFlag;
	}

	public Boolean getIsZoyloEmployee() {
		return isZoyloEmployee;
	}

	public void setIsZoyloEmployee(Boolean isZoyloEmployee) {
		this.isZoyloEmployee = isZoyloEmployee;
	}
	
}
