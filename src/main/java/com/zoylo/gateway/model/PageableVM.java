package com.zoylo.gateway.model;

import java.util.List;

public class PageableVM<T> {
	private List<T> content;
	private PageInfoVM pageInfo;

	public List<T> getContent() {
		return content;
	}

	public void setContent(List<T> content) {
		this.content = content;
	}

	public PageInfoVM getPageInfo() {
		return pageInfo;
	}

	public void setPageInfo(PageInfoVM pageInfo) {
		this.pageInfo = pageInfo;
	}
}
