package com.zoylo.gateway.model;

import java.util.List;

public class EcommCustomer {
	private String email;
	private String firstname;
	private String lastname;
	private List<CustomerAttribute> custom_attributes;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public List<CustomerAttribute> getCustom_attributes() {
		return custom_attributes;
	}

	public void setCustom_attributes(List<CustomerAttribute> custom_attributes) {
		this.custom_attributes = custom_attributes;
	}

	@Override
	public String toString() {
		return "EcommCustomer [email=" + email + ", firstname=" + firstname + ", lastname=" + lastname
				+ ", custom_attributes=" + custom_attributes + "]";
	}
}
