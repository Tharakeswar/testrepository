package com.zoylo.gateway.model;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.zoylo.gateway.web.rest.errors.CustomValidationConstant;

public class ForgotPasswordVM {
	@NotNull
	@NotEmpty( message = CustomValidationConstant.OLD_PD )
	private String newPassword;
	@NotNull
	@NotEmpty( message = CustomValidationConstant.NEW_PD )
	private String newPasswordCheck;
	@NotNull
	@NotEmpty( message = CustomValidationConstant.EMAIL_ID )
	private String emailId;
	
	@NotEmpty( message = CustomValidationConstant.TOKEN )
	private String token;

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getNewPasswordCheck() {
		return newPasswordCheck;
	}

	public void setNewPasswordCheck(String newPasswordCheck) {
		this.newPasswordCheck = newPasswordCheck;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
