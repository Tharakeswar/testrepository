package com.zoylo.gateway.model;

/**
 * @author nagaraju
 * Class to hold input data for assigning/un assigning a role to user
 *
 */
public class UserRoleDataVM {
	
	private String id; // user id
	private String zoyloId;
	private String phoneNumber;
	private String emailId;
	private String roleCode;
	private String roleName;
	private boolean activeFlag;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getZoyloId() {
		return zoyloId;
	}
	public void setZoyloId(String zoyloId) {
		this.zoyloId = zoyloId;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getRoleCode() {
		return roleCode;
	}
	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}
	public boolean isActiveFlag() {
		return activeFlag;
	}
	public void setActiveFlag(boolean activeFlag) {
		this.activeFlag = activeFlag;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

}
