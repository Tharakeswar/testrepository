package com.zoylo.gateway.model;

import java.security.Permissions;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.zoylo.gateway.domain.Authority;
import com.zoylo.gateway.domain.EmailInfo;
import com.zoylo.gateway.domain.PhoneInfo;

public class UserResponseVM {
	
	private String id;

	@JsonIgnore
	private Integer versionId;

	private String userName;

	private String zoyloId;

	private String encryptedPassword;

	// @Field("password_last_set_on")
	private Date passwordLastSetOn;

	//@Field("activeFlag")
	private boolean activeFlag;

	// @Field("active_from_date")
	private Date activeFromDate;

	// @Field("active_till_date")
	private Date activeTillDate;

	//@Field("loginEnabled")
	private boolean loginEnabled;

	//@Field("accountLocked")
	private boolean accountLocked;

	@JsonIgnore
	private EmailInfo emailInfo;
	
	@JsonIgnore
	private PhoneInfo phoneInfo;

	private String deviceId;

	private String deviceType;
	
	private Boolean emailVerified;

	private Date deletedOn;

	//@Field("deleteFlag")
	private boolean deleteFlag;
	@JsonIgnore
	private Permissions permissions;
	
	//private boolean termsAndConditions;

	@JsonIgnore
	private Set<Authority> authorities = new HashSet<>();
	
	
	//Social Media
	//private String loginType;
	
	//private String socialId;
	
	private String firstName;
	private String lastName;
	
	private String userType;
	private Boolean isUserExist;
	private String registerType;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getVersionId() {
		return versionId;
	}

	public void setVersionId(Integer versionId) {
		this.versionId = versionId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getZoyloId() {
		return zoyloId;
	}

	public void setZoyloId(String zoyloId) {
		this.zoyloId = zoyloId;
	}

	public String getEncryptedPassword() {
		return encryptedPassword;
	}

	public void setEncryptedPassword(String encryptedPassword) {
		this.encryptedPassword = encryptedPassword;
	}

	public Date getPasswordLastSetOn() {
		return passwordLastSetOn;
	}

	public void setPasswordLastSetOn(Date passwordLastSetOn) {
		this.passwordLastSetOn = passwordLastSetOn;
	}

	public boolean isActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(boolean activeFlag) {
		this.activeFlag = activeFlag;
	}

	public Date getActiveFromDate() {
		return activeFromDate;
	}

	public void setActiveFromDate(Date activeFromDate) {
		this.activeFromDate = activeFromDate;
	}

	public Date getActiveTillDate() {
		return activeTillDate;
	}

	public void setActiveTillDate(Date activeTillDate) {
		this.activeTillDate = activeTillDate;
	}

	public boolean isLoginEnabled() {
		return loginEnabled;
	}

	public void setLoginEnabled(boolean loginEnabled) {
		this.loginEnabled = loginEnabled;
	}

	public boolean isAccountLocked() {
		return accountLocked;
	}

	public void setAccountLocked(boolean accountLocked) {
		this.accountLocked = accountLocked;
	}

	public EmailInfo getEmailInfo() {
		return emailInfo;
	}

	public void setEmailInfo(EmailInfo emailInfo) {
		this.emailInfo = emailInfo;
	}

	public PhoneInfo getPhoneInfo() {
		return phoneInfo;
	}

	public void setPhoneInfo(PhoneInfo phoneInfo) {
		this.phoneInfo = phoneInfo;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public boolean isDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(boolean deleteFlag) {
		this.deleteFlag = deleteFlag;
	}


	

	public Set<Authority> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(Set<Authority> authorities) {
		this.authorities = authorities;
	}

	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public Boolean getIsUserExist() {
		return isUserExist;
	}

	public void setIsUserExist(Boolean isUserExist) {
		this.isUserExist = isUserExist;
	}

	public String getRegisterType() {
		return registerType;
	}

	public void setRegisterType(String registerType) {
		this.registerType = registerType;
	}

	public Boolean getEmailVerified() {
		return emailVerified;
	}

	public void setEmailVerified(Boolean emailVerified) {
		this.emailVerified = emailVerified;
	}
	
}
