package com.zoylo.gateway.model;

/**
 * 
 * @author Devendra.Kumar
 * @version 1.0
 *
 */
public class PasswordErrorVM {

	private String errorCode;
	private String errorMsg;
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	
	
}
