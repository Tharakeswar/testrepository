package com.zoylo.gateway.model;

public class SocialMediaVM {
	private String id;
	private boolean isUserExist;
	private String mobileNumber;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isUserExist() {
		return isUserExist;
	}

	public void setUserExist(boolean isUserExist) {
		this.isUserExist = isUserExist;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

}
