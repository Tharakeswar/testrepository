package com.zoylo.gateway.model;

/**
 * 
 * @author ankur
 * @version 1.0
 *
 */
public class DiagnosticVM {
	private String id;
	private String diagnosticCenterName;
	private double latitude;
	private double longitude;
	private AddressVM diagnosticCenterAddress;
	private String cityCode;
	private String stateCode;

	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public String getDiagnosticCenterName() {
		return diagnosticCenterName;
	}

	public void setDiagnosticCenterName(String diagnosticCenterName) {
		this.diagnosticCenterName = diagnosticCenterName;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public AddressVM getDiagnosticCenterAddress() {
		return diagnosticCenterAddress;
	}

	public void setDiagnosticCenterAddress(AddressVM diagnosticCenterAddress) {
		this.diagnosticCenterAddress = diagnosticCenterAddress;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

}
