package com.zoylo.gateway.model;

import java.util.List;
/**
 * @author damini.arora
 * @version 1.0
 */
public class MessageInfo {
	private String messageText;
	private List<MessageAttachmentList> messageAttachmentList;
	public String getMessageText() {
		return messageText;
	}
	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}
	public List<MessageAttachmentList> getMessageAttachmentList() {
		return messageAttachmentList;
	}
	public void setMessageAttachmentList(List<MessageAttachmentList> messageAttachmentList) {
		this.messageAttachmentList = messageAttachmentList;
	}
	

}
