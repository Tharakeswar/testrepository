package com.zoylo.gateway.model;
/**
 * 
 * @author Balram Sharma
 * @version 1.0
 * @description This class is used to hold id,name,code of AuthCategory entity
 *
 */
public class AuthCategoryNameList {
	
	private String id;
	private boolean allowRoleFlag;
	private boolean activeFlag;
	private String authCategoryCode;
	private String authCategoryName;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public boolean isActiveFlag() {
		return activeFlag;
	}
	public void setActiveFlag(boolean activeFlag) {
		this.activeFlag = activeFlag;
	}
	public String getAuthCategoryCode() {
		return authCategoryCode;
	}
	public void setAuthCategoryCode(String authCategoryCode) {
		this.authCategoryCode = authCategoryCode;
	}
	public String getAuthCategoryName() {
		return authCategoryName;
	}
	public void setAuthCategoryName(String authCategoryName) {
		this.authCategoryName = authCategoryName;
	}
	public boolean isAllowRoleFlag() {
		return allowRoleFlag;
	}
	public void setAllowRoleFlag(boolean allowRoleFlag) {
		this.allowRoleFlag = allowRoleFlag;
	}
	
	
	

}
