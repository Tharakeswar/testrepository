package com.zoylo.gateway.model;

import java.io.Serializable;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.validation.annotation.Validated;

import com.zoylo.gateway.web.rest.errors.CustomValidationConstant;

@Validated
public class MobileVerificationVM  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6260263700518015170L;
	@NotEmpty(message = CustomValidationConstant.USER_ID_NOT_EMPTY)
	private String id;
	private int otp;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getOtp() {
		return otp;
	}

	public void setOtp(int otp) {
		this.otp = otp;
	}

}
