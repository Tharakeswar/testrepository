package com.zoylo.gateway.model;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.validation.annotation.Validated;

import com.zoylo.gateway.web.rest.errors.CustomValidationConstant;
import com.zoylo.validator.MobileNumber;

@Validated
public class RecipientVM {
	private String id;
	@NotEmpty(message = CustomValidationConstant.FIRST_NAME_NOT_BLANK)
	@Size(min = 3, max = 50, message = CustomValidationConstant.FIRST_NAME_LENGTH)
	private String firstName;
	private String middleName;
	@NotEmpty(message = CustomValidationConstant.LAST_NAME_NOT_BLANK)
	@Size(min = 3, max = 50, message = CustomValidationConstant.LAST_NAME_LENGTH)
	private String lastName;
	private String zoyloId;
	@NotEmpty(message = CustomValidationConstant.EMAIL_NOT_EMPTY)
	@Email(message = CustomValidationConstant.EMAIL_CORE)
	private String emailAddress;

	@Size(min = 10, max = 13, message = CustomValidationConstant.MOBILE_LENGTH)
	@MobileNumber(message = CustomValidationConstant.MOBILE_DIGIT)
	private String phoneNumber;
	@NotEmpty(message = CustomValidationConstant.PD_NOT_EMPTY)
	private String encryptedPassword;



	private String deviceId;
	private String deviceType;

	@NotEmpty(message = CustomValidationConstant.USER_TYPE_NOT_EMPTY)
	@IsValidUserType(listOfUserType = "admin|subadmin|recipient|doctor|diagnostic", message = CustomValidationConstant.USER_TYPE_NOT_VALID)
	private String userType;
	
	private String userId;
	
	private String ecommAdminToken;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getZoyloId() {
		return zoyloId;
	}

	public void setZoyloId(String zoyloId) {
		this.zoyloId = zoyloId;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEncryptedPassword() {
		return encryptedPassword;
	}

	public void setEncryptedPassword(String encryptedPassword) {
		this.encryptedPassword = encryptedPassword;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}


	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getEcommAdminToken() {
		return ecommAdminToken;
	}

	public void setEcommAdminToken(String ecommAdminToken) {
		this.ecommAdminToken = ecommAdminToken;
	}

	@Override
	public String toString() {
		return "RecipientVM [id=" + id + ", firstName=" + firstName + ", middleName=" + middleName + ", lastName="
				+ lastName + ", zoyloId=" + zoyloId + ", emailAddress=" + emailAddress + ", phoneNumber=" + phoneNumber
				+ ", encryptedPassword=" + encryptedPassword + ", deviceId=" + deviceId + ", deviceType=" + deviceType
				+ ", userType=" + userType + ", userId=" + userId + ", ecommAdminToken=" + ecommAdminToken + "]";
	}
}
