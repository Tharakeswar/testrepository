package com.zoylo.gateway.model;

import java.util.Date;
import com.zoylo.gateway.domain.AuthCategoryData;
/**
 * 
 * @author Balram Sharma
 * @version 1.0
 * @description This class is used to hold user data
 *
 */
public class ZoyloAuthCategoryVM {
	
	private String id;
	private Integer versionId; 
	private String authCategoryCode;
	private AuthCategoryData authCategoryData;
	private String authCategoryLandingPageUrl;
	private boolean allowRoleFlag;
	private boolean activeFlag;
	private Date activeFromDate;
	private Date activeTillDate;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Integer getVersionId() {
		return versionId;
	}
	public void setVersionId(Integer versionId) {
		this.versionId = versionId;
	}
	public String getAuthCategoryCode() {
		return authCategoryCode;
	}
	public void setAuthCategoryCode(String authCategoryCode) {
		this.authCategoryCode = authCategoryCode;
	}
	public AuthCategoryData getAuthCategoryData() {
		return authCategoryData;
	}
	public void setAuthCategoryData(AuthCategoryData authCategoryData) {
		this.authCategoryData = authCategoryData;
	}
	public String getAuthCategoryLandingPageUrl() {
		return authCategoryLandingPageUrl;
	}
	public void setAuthCategoryLandingPageUrl(String authCategoryLandingPageUrl) {
		this.authCategoryLandingPageUrl = authCategoryLandingPageUrl;
	}
	public boolean isAllowRoleFlag() {
		return allowRoleFlag;
	}
	public void setAllowRoleFlag(boolean allowRoleFlag) {
		this.allowRoleFlag = allowRoleFlag;
	}
	public boolean isActiveFlag() {
		return activeFlag;
	}
	public void setActiveFlag(boolean activeFlag) {
		this.activeFlag = activeFlag;
	}
	public Date getActiveFromDate() {
		return activeFromDate;
	}
	public void setActiveFromDate(Date activeFromDate) {
		this.activeFromDate = activeFromDate;
	}
	public Date getActiveTillDate() {
		return activeTillDate;
	}
	public void setActiveTillDate(Date activeTillDate) {
		this.activeTillDate = activeTillDate;
	}
}
