package com.zoylo.gateway.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.zoylo.gateway.domain.ModuleData;

/**
 * @author damini.arora
 * @version 1.0
 *
 */
public class ModuleDataVM {

	private String id;

	@JsonIgnore
	private Integer versionId;

	private String moduleCode;

	private int displaySequence;

	private List<ModuleData> moduleData;

	private String imageInformation;

	private boolean activeFlag;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	public int getDisplaySequence() {
		return displaySequence;
	}

	public void setDisplaySequence(int displaySequence) {
		this.displaySequence = displaySequence;
	}

	public List<ModuleData> getModuleData() {
		return moduleData;
	}

	public void setModuleData(List<ModuleData> moduleData) {
		this.moduleData = moduleData;
	}

	public String getImageInformation() {
		return imageInformation;
	}

	public void setImageInformation(String imageInformation) {
		this.imageInformation = imageInformation;
	}

	public boolean isActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(boolean activeFlag) {
		this.activeFlag = activeFlag;
	}

}
