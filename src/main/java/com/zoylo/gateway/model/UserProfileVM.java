package com.zoylo.gateway.model;

/**
 * 
 * @author arvind.rawat
 * @version 1.0
 *
 */
public class UserProfileVM {

	private String userId;
	private String zoyloId;
	private String firstName;
	private String lastName;
	private String gender;
	private RecipientPermissionListVM grantedPermissionsList;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getZoyloId() {
		return zoyloId;
	}

	public void setZoyloId(String zoyloId) {
		this.zoyloId = zoyloId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public RecipientPermissionListVM getGrantedPermissionsList() {
		return grantedPermissionsList;
	}

	public void setGrantedPermissionsList(RecipientPermissionListVM grantedPermissionsList) {
		this.grantedPermissionsList = grantedPermissionsList;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
	
}