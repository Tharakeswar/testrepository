package com.zoylo.gateway.model;

public class EcommUserRegistrationVM {
	private EcommCustomer customer;
	private String password;

	public EcommCustomer getCustomer() {
		return customer;
	}

	public void setCustomer(EcommCustomer customer) {
		this.customer = customer;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "EcommUserRegistrationVM [customer=" + customer + ", password=" + password + "]";
	}
	
	
}
