package com.zoylo.gateway.model;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.validation.annotation.Validated;
import com.zoylo.gateway.web.rest.errors.CustomValidationConstant;

@Validated
public class ZoyloPermissionVM {

	private String roleCode;
	@NotEmpty(message = CustomValidationConstant.PERMISSION_CODE)
	private String permissionCode;
	private boolean activeFlag;
	
	public String getRoleCode() {
		return roleCode;
	}
	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}
	public String getPermissionCode() {
		return permissionCode;
	}
	public void setPermissionCode(String permissionCode) {
		this.permissionCode = permissionCode;
	}
	public boolean isActiveFlag() {
		return activeFlag;
	}
	public void setActiveFlag(boolean activeFlag) {
		this.activeFlag = activeFlag;
	}
	
	
	
}
