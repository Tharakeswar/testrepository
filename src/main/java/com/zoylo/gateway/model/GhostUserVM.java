package com.zoylo.gateway.model;

import javax.validation.constraints.Size;

import com.zoylo.gateway.web.rest.errors.CustomValidationConstant;

/**
 * @version 1.0
 * @author diksha gupta
 * @descrition ghost user entry model
 *
 */
public class GhostUserVM {
	private String userId;
	private String zoyloId;
	@Size(min = 3, max = 50, message=CustomValidationConstant.FIRST_NAME_LENGTH)
	private String firstName;
	private String lastName;
	private Integer age;
	private String gender;
	
	private String mobileNumber;
	
	private String emailId;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getZoyloId() {
		return zoyloId;
	}

	public void setZoyloId(String zoyloId) {
		this.zoyloId = zoyloId;
	}

}
