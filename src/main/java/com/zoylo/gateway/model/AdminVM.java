/**
 * 
 */
package com.zoylo.gateway.model;

import java.util.List;

/**
 * @author user
 * @version 1.0
 *
 */
public class AdminVM {
	private List<String> roleCode;

	public List<String> getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(List<String> roleCode) {
		this.roleCode = roleCode;
	}

}
