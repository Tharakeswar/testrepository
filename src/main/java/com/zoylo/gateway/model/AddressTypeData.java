package com.zoylo.gateway.model;

/***
 * 
 * @author Iti Gupta
 * @version 1.0
 *
 */
public class AddressTypeData {

	private String languageId;
	private String languageCode;
	private String addressTypeName;

	public AddressTypeData() {
	}

	public AddressTypeData(String languageId, String languageCode, String addressTypeName) {
		super();
		this.languageId = languageId;
		this.languageCode = languageCode;
		this.addressTypeName = addressTypeName;
	}

	public String getLanguageId() {
		return languageId;
	}

	public void setLanguageId(String languageId) {
		this.languageId = languageId;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getAddressTypeName() {
		return addressTypeName;
	}

	public void setAddressTypeName(String addressTypeName) {
		this.addressTypeName = addressTypeName;
	}

}
