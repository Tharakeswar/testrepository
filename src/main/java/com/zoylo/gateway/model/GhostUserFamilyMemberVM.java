package com.zoylo.gateway.model;

/***
 * View Model to add a Ghost User
 * 
 * @author Iti Gupta
 * @version 1.0
 *
 */
public class GhostUserFamilyMemberVM {

	private String userId;
	private String zoyloId;
	private String firstName;
	private String lastName;
	private String gender;
	private String mobileNumber;
	private String emailId;

	public GhostUserFamilyMemberVM() {
	}

	public GhostUserFamilyMemberVM(String userId, String zoyloId, String firstName, String lastName, String gender,
			String mobileNumber, String emailId) {
		super();
		this.userId = userId;
		this.zoyloId = zoyloId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
		this.mobileNumber = mobileNumber;
		this.emailId = emailId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getZoyloId() {
		return zoyloId;
	}

	public void setZoyloId(String zoyloId) {
		this.zoyloId = zoyloId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

}
