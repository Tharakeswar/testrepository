package com.zoylo.gateway.model;

import java.util.List;

/***
 * 
 * @author Iti Gupta
 * @version 1.0
 *
 */
public class AddressData {

	private String countryId;
	private String countryCode;
	private List<CountryData> countryData;
	private String stateId;
	private String stateCode;
	private List<StateData> stateData;
	private String cityId;
	private String cityCode;
	private List<CityData> cityData;
	private String addressLineOne;
	private String addressLineTwo;
	private String addressLocality;
	private String nearbyLandmark;
	private String pinCode;
	private String gpsLatitude;
	private String gpsLongitude;

	public AddressData() {
	}

	public AddressData(String countryId, String countryCode, List<CountryData> countryData, String stateId,
			String stateCode, List<StateData> stateData, String cityId, String cityCode, List<CityData> cityData,
			String addressLineOne, String addressLineTwo, String addressLocality, String nearbyLandmark, String pinCode,
			String gpsLatitude, String gpsLongitude) {
		super();
		this.countryId = countryId;
		this.countryCode = countryCode;
		this.countryData = countryData;
		this.stateId = stateId;
		this.stateCode = stateCode;
		this.stateData = stateData;
		this.cityId = cityId;
		this.cityCode = cityCode;
		this.cityData = cityData;
		this.addressLineOne = addressLineOne;
		this.addressLineTwo = addressLineTwo;
		this.addressLocality = addressLocality;
		this.nearbyLandmark = nearbyLandmark;
		this.pinCode = pinCode;
		this.gpsLatitude = gpsLatitude;
		this.gpsLongitude = gpsLongitude;
	}

	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public List<CountryData> getCountryData() {
		return countryData;
	}

	public void setCountryData(List<CountryData> countryData) {
		this.countryData = countryData;
	}

	public String getStateId() {
		return stateId;
	}

	public void setStateId(String stateId) {
		this.stateId = stateId;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public List<StateData> getStateData() {
		return stateData;
	}

	public void setStateData(List<StateData> stateData) {
		this.stateData = stateData;
	}

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public List<CityData> getCityData() {
		return cityData;
	}

	public void setCityData(List<CityData> cityData) {
		this.cityData = cityData;
	}

	public String getAddressLineOne() {
		return addressLineOne;
	}

	public void setAddressLineOne(String addressLineOne) {
		this.addressLineOne = addressLineOne;
	}

	public String getAddressLineTwo() {
		return addressLineTwo;
	}

	public void setAddressLineTwo(String addressLineTwo) {
		this.addressLineTwo = addressLineTwo;
	}

	public String getAddressLocality() {
		return addressLocality;
	}

	public void setAddressLocality(String addressLocality) {
		this.addressLocality = addressLocality;
	}

	public String getNearbyLandmark() {
		return nearbyLandmark;
	}

	public void setNearbyLandmark(String nearbyLandmark) {
		this.nearbyLandmark = nearbyLandmark;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getGpsLatitude() {
		return gpsLatitude;
	}

	public void setGpsLatitude(String gpsLatitude) {
		this.gpsLatitude = gpsLatitude;
	}

	public String getGpsLongitude() {
		return gpsLongitude;
	}

	public void setGpsLongitude(String gpsLongitude) {
		this.gpsLongitude = gpsLongitude;
	}

}
