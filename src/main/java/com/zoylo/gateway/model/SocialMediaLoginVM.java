package com.zoylo.gateway.model;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.validation.annotation.Validated;

import com.zoylo.gateway.web.rest.errors.CustomValidationConstant;

/**
 * Social media login model
 * 
 * @version 1.0
 * @author user
 *
 */
@Validated
public class SocialMediaLoginVM {
	@NotEmpty(message = CustomValidationConstant.LOGIN_TYPE_NOT_EMPTY)
	private String loginType;
	@NotEmpty(message = CustomValidationConstant.SOCIAL_ID_NOT_EMPTY)
	private String socialId;

	private String firstName;

	private String lastName;

	private String emailAddress;

	private String phoneNumber;
	private boolean isTermsAndCondtionAccpected;
	@NotEmpty(message = CustomValidationConstant.USER_TYPE_NOT_EMPTY)
	private String userType;

	public String getLoginType() {
		return loginType;
	}

	public void setLoginType(String loginType) {
		this.loginType = loginType;
	}

	public String getSocialId() {
		return socialId;
	}

	public void setSocialId(String socialId) {
		this.socialId = socialId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public boolean isTermsAndCondtionAccpected() {
		return isTermsAndCondtionAccpected;
	}

	public void setTermsAndCondtionAccpected(boolean isTermsAndCondtionAccpected) {
		this.isTermsAndCondtionAccpected = isTermsAndCondtionAccpected;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

}
