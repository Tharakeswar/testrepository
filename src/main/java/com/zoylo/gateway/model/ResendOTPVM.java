package com.zoylo.gateway.model;

/**
 * @author Mehraj Malik
 *
 */
public class ResendOTPVM {

	private String mobileNo;

	public ResendOTPVM() {
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
}
