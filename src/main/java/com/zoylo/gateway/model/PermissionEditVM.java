package com.zoylo.gateway.model;

import java.util.List;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.validation.annotation.Validated;

import com.zoylo.gateway.domain.AuthCategoryData;
import com.zoylo.gateway.domain.PermissionData;
import com.zoylo.gateway.web.rest.errors.CustomValidationConstant;

@Validated
public class PermissionEditVM {

	@NotEmpty(message = CustomValidationConstant.PERMISSION_ID)
	private String id;

	private String authCategoryId;
	private String authCategoryCode;
	private List<AuthCategoryData> authCategoryData;

	private String authCategoryLandingPageUrl;

	@NotEmpty(message = CustomValidationConstant.PERMISSION_CODE)
	private String permissionCode;

	private List<PermissionData> permissiondata;

	private boolean activeFlag;

	private String imageInformation;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPermissionCode() {
		return permissionCode;
	}

	public void setPermissionCode(String permissionCode) {
		this.permissionCode = permissionCode;
	}

	public List<PermissionData> getPermissiondata() {
		return permissiondata;
	}

	public void setPermissiondata(List<PermissionData> permissiondata) {
		this.permissiondata = permissiondata;
	}

	public String getAuthCategoryId() {
		return authCategoryId;
	}

	public void setAuthCategoryId(String authCategoryId) {
		this.authCategoryId = authCategoryId;
	}

	public String getAuthCategoryCode() {
		return authCategoryCode;
	}

	public void setAuthCategoryCode(String authCategoryCode) {
		this.authCategoryCode = authCategoryCode;
	}

	public List<AuthCategoryData> getAuthCategoryData() {
		return authCategoryData;
	}

	public void setAuthCategoryData(List<AuthCategoryData> authCategoryData) {
		this.authCategoryData = authCategoryData;
	}

	public String getAuthCategoryLandingPageUrl() {
		return authCategoryLandingPageUrl;
	}

	public void setAuthCategoryLandingPageUrl(String authCategoryLandingPageUrl) {
		this.authCategoryLandingPageUrl = authCategoryLandingPageUrl;
	}

	public boolean isActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(boolean activeFlag) {
		this.activeFlag = activeFlag;
	}

	public String getImageInformation() {
		return imageInformation;
	}

	public void setImageInformation(String imageInformation) {
		this.imageInformation = imageInformation;
	}

}
