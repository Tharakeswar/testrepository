package com.zoylo.gateway.model;
/**
 * @author damini.arora
 * @version 1.0
 */
public class MessageAttachmentList {
	private String attachmentId;
	private String attachmentMimeType;
	private String attachmentUrl;
	public String getAttachmentId() {
		return attachmentId;
	}
	public void setAttachmentId(String attachmentId) {
		this.attachmentId = attachmentId;
	}
	public String getAttachmentMimeType() {
		return attachmentMimeType;
	}
	public void setAttachmentMimeType(String attachmentMimeType) {
		this.attachmentMimeType = attachmentMimeType;
	}
	public String getAttachmentUrl() {
		return attachmentUrl;
	}
	public void setAttachmentUrl(String attachmentUrl) {
		this.attachmentUrl = attachmentUrl;
	}
	

}
