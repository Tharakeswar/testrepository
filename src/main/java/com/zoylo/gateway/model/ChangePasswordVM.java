package com.zoylo.gateway.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.validation.annotation.Validated;

/**
 * 
 * @author Devendra.Kumar
 *
 */
@Validated
public class ChangePasswordVM {
	@NotNull
	@NotEmpty
	private String oldPassword;

	@NotNull
	@NotEmpty
	@Size(min = 6)
	private String newPassword;

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
}
