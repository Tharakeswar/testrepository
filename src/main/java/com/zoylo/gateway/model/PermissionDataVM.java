package com.zoylo.gateway.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.zoylo.gateway.domain.PermissionData;

public class PermissionDataVM {

	private String id;

	@JsonIgnore
	private Integer versionId;

	private String permissionCode;

	private List<PermissionData> permissiondata;

	private boolean activeFlag;

	private String imageInformation;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPermissionCode() {
		return permissionCode;
	}

	public void setPermissionCode(String permissionCode) {
		this.permissionCode = permissionCode;
	}

	public List<PermissionData> getPermissiondata() {
		return permissiondata;
	}

	public void setPermissiondata(List<PermissionData> permissiondata) {
		this.permissiondata = permissiondata;
	}

	

	public boolean isActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(boolean activeFlag) {
		this.activeFlag = activeFlag;
	}

	public String getImageInformation() {
		return imageInformation;
	}

	public void setImageInformation(String imageInformation) {
		this.imageInformation = imageInformation;
	}
}
