package com.zoylo.gateway.model;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.validation.annotation.Validated;

import com.zoylo.gateway.domain.AuthCategoryData;
import com.zoylo.gateway.domain.PermissionData;
import com.zoylo.gateway.web.rest.errors.CustomValidationConstant;

/**
 * @author Balram Sharma
 * @version 1.0 This class is used to hold user given data.
 */
@Validated
public class PermissionZoyloVM {
	private String id;
	@NotEmpty(message = CustomValidationConstant.PERMISSION_CODE)
	@NotNull
	private String permissionCode;
	@NotNull
	private PermissionData permissionData;
	@NotEmpty(message = CustomValidationConstant.AUTH_CATEGORY_CODE)
	private String authCategoryCode;
	private AuthCategoryData authCategoryData;
	private String authCategoryLandingPageUrl;
	private boolean activeFlag;

	public PermissionZoyloVM() {
		super();
	}

	public PermissionZoyloVM(String id, String permissionCode, PermissionData permissionData, String authCategoryCode,
			AuthCategoryData authCategoryData, String authCategoryLandingPageUrl) {
		super();
		this.id = id;
		this.permissionCode = permissionCode;
		this.permissionData = permissionData;
		this.authCategoryCode = authCategoryCode;
		this.authCategoryData = authCategoryData;
		this.authCategoryLandingPageUrl = authCategoryLandingPageUrl;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPermissionCode() {
		return permissionCode;
	}

	public void setPermissionCode(String permissionCode) {
		this.permissionCode = permissionCode;
	}

	public PermissionData getPermissionData() {
		return permissionData;
	}

	public void setPermissionData(PermissionData permissionData) {
		this.permissionData = permissionData;
	}

	public String getAuthCategoryCode() {
		return authCategoryCode;
	}

	public void setAuthCategoryCode(String authCategoryCode) {
		this.authCategoryCode = authCategoryCode;
	}

	public AuthCategoryData getAuthCategoryData() {
		return authCategoryData;
	}

	public void setAuthCategoryData(AuthCategoryData authCategoryData) {
		this.authCategoryData = authCategoryData;
	}

	public String getAuthCategoryLandingPageUrl() {
		return authCategoryLandingPageUrl;
	}

	public void setAuthCategoryLandingPageUrl(String authCategoryLandingPageUrl) {
		this.authCategoryLandingPageUrl = authCategoryLandingPageUrl;
	}

	public boolean isActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(boolean activeFlag) {
		this.activeFlag = activeFlag;
	}

}
