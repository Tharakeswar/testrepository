package com.zoylo.gateway.model;

import java.util.List;

import com.zoylo.gateway.domain.GrantedRoleData;

public class PMSUserRoleMV {
	
	private String userId;
	private String providerId;
	private String roleId;
	private String roleCode;
	private List<GrantedRoleData> grantedRoleData;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getProviderId() {
		return providerId;
	}
	public void setProviderId(String providerId) {
		this.providerId = providerId;
	}
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getRoleCode() {
		return roleCode;
	}
	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}
	public List<GrantedRoleData> getGrantedRoleData() {
		return grantedRoleData;
	}
	public void setGrantedRoleData(List<GrantedRoleData> grantedRoleData) {
		this.grantedRoleData = grantedRoleData;
	}
		
}
