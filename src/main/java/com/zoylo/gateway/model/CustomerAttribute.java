package com.zoylo.gateway.model;

public class CustomerAttribute {
	private String attribute_code;
	private String value;

	public String getAttribute_code() {
		return attribute_code;
	}

	public void setAttribute_code(String attribute_code) {
		this.attribute_code = attribute_code;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	@Override
	public String toString() {
		return "CustomerAttribute [attribute_code=" + attribute_code + ", value=" + value + "]";
	}
}
