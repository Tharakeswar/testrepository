package com.zoylo.gateway.model;

import org.springframework.validation.annotation.Validated;

@Validated
public class RegisterVM extends RecipientVM{
	
	private AdminVM adminDetail;
	private boolean activeFlag;
	private Boolean isZoyloEmployee;


	public AdminVM getAdminDetail() {
		return adminDetail;
	}

	public void setAdminDetail(AdminVM adminDetail) {
		this.adminDetail = adminDetail;
	}

	public boolean getIsActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(boolean activeFlag) {
		this.activeFlag = activeFlag;
	}

	public Boolean getIsZoyloEmployee() {
		return isZoyloEmployee;
	}

	public void setIsZoyloEmployee(Boolean isZoyloEmployee) {
		this.isZoyloEmployee = isZoyloEmployee;
	}

	

}
