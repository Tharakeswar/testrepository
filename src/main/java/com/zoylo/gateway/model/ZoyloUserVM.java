package com.zoylo.gateway.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author nagaraju
 * Class to hold input data for creating a user
 */
public class ZoyloUserVM {
	
	private String id;
	private String zoyloId;
	private String userName;
	private String firstName;
	private String middleName;
	private String lastName;
	private String mobileNumber;
	private String emailId;
	private boolean isZoyloEmployee;
	private String encryptedPassword;
	private boolean loginEnabled;
	private boolean accountLocked;
	private boolean activeFlag;
	
	/**
	 *  used only in UserBllImpl#fetchUserRoleDetail
	 */
	private List<UserRoleDataVM> userRole;
	
	public List<UserRoleDataVM> getUserRole() {
		return userRole;
	}
	public void setUserRole(List<UserRoleDataVM> userRole) {
		this.userRole = userRole;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getZoyloId() {
		return zoyloId;
	}
	public void setZoyloId(String zoyloId) {
		this.zoyloId = zoyloId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	@JsonProperty("isZoyloEmployee")
	public boolean getIsZoyloEmployee() {
		return isZoyloEmployee;
	}
	public void setIsZoyloEmployee(boolean isZoyloEmployee) {
		this.isZoyloEmployee = isZoyloEmployee;
	}
	public String getEncryptedPassword() {
		return encryptedPassword;
	}
	public void setEncryptedPassword(String encryptedPassword) {
		this.encryptedPassword = encryptedPassword;
	}
	public boolean isLoginEnabled() {
		return loginEnabled;
	}
	public void setLoginEnabled(boolean loginEnabled) {
		this.loginEnabled = loginEnabled;
	}
	public boolean isAccountLocked() {
		return accountLocked;
	}
	public void setAccountLocked(boolean accountLocked) {
		this.accountLocked = accountLocked;
	}
	public boolean isActiveFlag() {
		return activeFlag;
	}
	public void setActiveFlag(boolean activeFlag) {
		this.activeFlag = activeFlag;
	}
	
	

}
