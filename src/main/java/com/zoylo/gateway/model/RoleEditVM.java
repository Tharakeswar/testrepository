package com.zoylo.gateway.model;

import java.util.List;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.zoylo.gateway.domain.PermissionList;
import com.zoylo.gateway.domain.RoleData;
import com.zoylo.gateway.web.rest.errors.CustomValidationConstant;

@Validated
public class RoleEditVM {
	@NotEmpty(message = CustomValidationConstant.ROLE_ID)
	private String id;
	@JsonIgnore
	private Integer versionId;
	@NotEmpty(message = CustomValidationConstant.ROLE_CODE)
	private String roleCode;
	private List<RoleData> roleData;
	private List<PermissionList> permissionsList;
	private boolean activeFlag;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public List<RoleData> getRoleData() {
		return roleData;
	}

	public void setRoleData(List<RoleData> roleData) {
		this.roleData = roleData;
	}

	public List<PermissionList> getPermissionsList() {
		return permissionsList;
	}

	public void setPermissionsList(List<PermissionList> permissionsList) {
		this.permissionsList = permissionsList;
	}

	public boolean isActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(boolean activeFlag) {
		this.activeFlag = activeFlag;
	}

}
