package com.zoylo.gateway.model;

/***
 * 
 * @author Iti Gupta
 * @version 1.0
 * 
 *
 */
public class CountryData {

	private String languageId;
	private String languageCode;
	private String countryName;

	public CountryData() {
	}

	public CountryData(String languageId, String languageCode, String countryName) {
		super();
		this.languageId = languageId;
		this.languageCode = languageCode;
		this.countryName = countryName;
	}

	public String getLanguageId() {
		return languageId;
	}

	public void setLanguageId(String languageId) {
		this.languageId = languageId;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

}
