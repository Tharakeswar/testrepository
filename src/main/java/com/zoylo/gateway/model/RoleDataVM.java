package com.zoylo.gateway.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.zoylo.gateway.domain.PermissionList;
import com.zoylo.gateway.domain.RoleData;

public class RoleDataVM {
	private String id;
	@JsonIgnore
	private Integer versionId;
	private String roleCode;
	private List<RoleData> roleData;
	private List<PermissionList> permissionsList;
	private boolean activeFlag;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public List<RoleData> getRoleData() {
		return roleData;
	}

	public void setRoleData(List<RoleData> roleData) {
		this.roleData = roleData;
	}

	public List<PermissionList> getPermissionsList() {
		return permissionsList;
	}

	public void setPermissionsList(List<PermissionList> permissionsList) {
		this.permissionsList = permissionsList;
	}

	public boolean isActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(boolean activeFlag) {
		this.activeFlag = activeFlag;
	}

}
