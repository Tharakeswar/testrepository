package com.zoylo.gateway.model;
/**
 * @author damini.arora
 * @version 1.0
 */
import java.util.List;

public class ZoyloNotificationVM {

	private String id;
	private String notificationModeCode;
	private String notificationStatusCode;
	private String notificationMessageId;
	private Integer retryCounter;
	private SenderInfo senderInfo;
	private RecipientInfo recipientInfo;
	private MessageInfo messageInfo;
	private List<DeliveryTrail> deliveryTrail;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNotificationModeCode() {
		return notificationModeCode;
	}

	public void setNotificationModeCode(String notificationModeCode) {
		this.notificationModeCode = notificationModeCode;
	}

	public String getNotificationStatusCode() {
		return notificationStatusCode;
	}

	public void setNotificationStatusCode(String notificationStatusCode) {
		this.notificationStatusCode = notificationStatusCode;
	}

	public String getNotificationMessageId() {
		return notificationMessageId;
	}

	public void setNotificationMessageId(String notificationMessageId) {
		this.notificationMessageId = notificationMessageId;
	}

	public Integer getRetryCounter() {
		return retryCounter;
	}

	public void setRetryCounter(Integer retryCounter) {
		this.retryCounter = retryCounter;
	}

	public SenderInfo getSenderInfo() {
		return senderInfo;
	}

	public void setSenderInfo(SenderInfo senderInfo) {
		this.senderInfo = senderInfo;
	}

	public RecipientInfo getRecipientInfo() {
		return recipientInfo;
	}

	public void setRecipientInfo(RecipientInfo recipientInfo) {
		this.recipientInfo = recipientInfo;
	}

	public MessageInfo getMessageInfo() {
		return messageInfo;
	}

	public void setMessageInfo(MessageInfo messageInfo) {
		this.messageInfo = messageInfo;
	}

	public List<DeliveryTrail> getDeliveryTrail() {
		return deliveryTrail;
	}

	public void setDeliveryTrail(List<DeliveryTrail> deliveryTrail) {
		this.deliveryTrail = deliveryTrail;
	}

}
