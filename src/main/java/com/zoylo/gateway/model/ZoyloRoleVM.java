package com.zoylo.gateway.model;

import java.util.List;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.validation.annotation.Validated;
import com.zoylo.gateway.domain.RoleData;
import com.zoylo.gateway.web.rest.errors.CustomValidationConstant;

@Validated
public class ZoyloRoleVM {

	@NotEmpty(message=CustomValidationConstant.ROLE_CODE)
	private String roleCode;
	private List<RoleData> roleData;
	private boolean activeFlag;
	private String authCategoryCode;
	
	public String getRoleCode() {
		return roleCode;
	}
	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}
	public List<RoleData> getRoleData() {
		return roleData;
	}
	public void setRoleData(List<RoleData> roleData) {
		this.roleData = roleData;
	}
	public boolean isActiveFlag() {
		return activeFlag;
	}
	public void setActiveFlag(boolean activeFlag) {
		this.activeFlag = activeFlag;
	}
	public String getAuthCategoryCode() {
		return authCategoryCode;
	}
	public void setAuthCategoryCode(String authCategoryCode) {
		this.authCategoryCode = authCategoryCode;
	}
	
	
}
