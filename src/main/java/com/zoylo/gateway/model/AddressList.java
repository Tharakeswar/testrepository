package com.zoylo.gateway.model;

import java.util.List;


/***
 * 
 * @author Iti Gupta
 * @version 1.0
 *
 */
public class AddressList {

	private String addressId;
	private String addressCustomName;
	private String addressTypeId;
	private String addressTypeCode;
	private List<AddressTypeData> addressTypeData;
	private boolean activeFlag;
	private boolean defaultFlag;
	private AddressData addressData;

	public AddressList() {
	}

	public AddressList(String addressId, String addressCustomName, String addressTypeId, String addressTypeCode,
			List<AddressTypeData> addressTypeData, boolean activeFlag, boolean defaultFlag, AddressData addressData) {
		super();
		this.addressId = addressId;
		this.addressCustomName = addressCustomName;
		this.addressTypeId = addressTypeId;
		this.addressTypeCode = addressTypeCode;
		this.addressTypeData = addressTypeData;
		this.activeFlag = activeFlag;
		this.defaultFlag = defaultFlag;
		this.addressData = addressData;
	}

	public String getAddressId() {
		return addressId;
	}

	public void setAddressId(String addressId) {
		this.addressId = addressId;
	}

	public String getAddressCustomName() {
		return addressCustomName;
	}

	public void setAddressCustomName(String addressCustomName) {
		this.addressCustomName = addressCustomName;
	}

	public String getAddressTypeId() {
		return addressTypeId;
	}

	public void setAddressTypeId(String addressTypeId) {
		this.addressTypeId = addressTypeId;
	}

	public String getAddressTypeCode() {
		return addressTypeCode;
	}

	public void setAddressTypeCode(String addressTypeCode) {
		this.addressTypeCode = addressTypeCode;
	}

	public List<AddressTypeData> getAddressTypeData() {
		return addressTypeData;
	}

	public void setAddressTypeData(List<AddressTypeData> addressTypeData) {
		this.addressTypeData = addressTypeData;
	}

	public boolean isActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(boolean activeFlag) {
		this.activeFlag = activeFlag;
	}

	public boolean isDefaultFlag() {
		return defaultFlag;
	}

	public void setDefaultFlag(boolean defaultFlag) {
		this.defaultFlag = defaultFlag;
	}

	public AddressData getAddressData() {
		return addressData;
	}

	public void setAddressData(AddressData addressData) {
		this.addressData = addressData;
	}

}
