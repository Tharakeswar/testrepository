package com.zoylo.gateway.model;

import java.util.Date;
/**
 * @author damini.arora
 * @version 1.0
 */
public class DeliveryTrail {
	private Date deliveryAttemptedAt;
	private String deliveryStatusCode;

	public String getDeliveryStatusCode() {
		return deliveryStatusCode;
	}

	public void setDeliveryStatusCode(String deliveryStatusCode) {
		this.deliveryStatusCode = deliveryStatusCode;
	}

	public Date getDeliveryAttemptedAt() {
		return deliveryAttemptedAt;
	}

	public void setDeliveryAttemptedAt(Date deliveryAttemptedAt) {
		this.deliveryAttemptedAt = deliveryAttemptedAt;
	}

}
