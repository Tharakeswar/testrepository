package com.zoylo.gateway.model;

import java.util.List;

import org.hibernate.validator.constraints.NotEmpty;

import com.zoylo.gateway.domain.ProviderTypeData;
import com.zoylo.gateway.web.rest.errors.CustomValidationConstant;


/**
 * 
 * @author Diksha Gupta
 * @version 1.0
 * @description Model for add user in doctor pms
 *
 */
public class UserDataVM {
	private String firstName;
	private String middleName;
	private String lastName;
	private String phoneNumber;
	private String emailAddress;
	private String password;
	private String role;
	private String providerTypeId;// String. Identifier of the provider type.
	private String providerTypeCode;
	private List<ProviderTypeData> providerTypeData;
	private String providerId;
	private String providerCode;                      // String. Mandatory. Code associated with the service provider.
    private String providerName;                      // String. Mandatory. Name of the service provider
    private String providerSearchName;
    private String userId;
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getProviderTypeId() {
		return providerTypeId;
	}

	public void setProviderTypeId(String providerTypeId) {
		this.providerTypeId = providerTypeId;
	}

	public String getProviderTypeCode() {
		return providerTypeCode;
	}

	public void setProviderTypeCode(String providerTypeCode) {
		this.providerTypeCode = providerTypeCode;
	}

	public List<ProviderTypeData> getProviderTypeData() {
		return providerTypeData;
	}

	public void setProviderTypeData(List<ProviderTypeData> providerTypeData) {
		this.providerTypeData = providerTypeData;
	}

	public String getProviderId() {
		return providerId;
	}

	public void setProviderId(String providerId) {
		this.providerId = providerId;
	}

	public String getProviderCode() {
		return providerCode;
	}

	public void setProviderCode(String providerCode) {
		this.providerCode = providerCode;
	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public String getProviderSearchName() {
		return providerSearchName;
	}

	public void setProviderSearchName(String providerSearchName) {
		this.providerSearchName = providerSearchName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
