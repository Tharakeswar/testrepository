package com.zoylo.gateway.model;

import java.util.List;


public class PermissionsListVM {
	private String permissionId;
	private String permissionCode;
	private List<PermissionData> permissionData;
	public String getPermissionId() {
		return permissionId;
	}
	public void setPermissionId(String permissionId) {
		this.permissionId = permissionId;
	}
	public String getPermissionCode() {
		return permissionCode;
	}
	public void setPermissionCode(String permissionCode) {
		this.permissionCode = permissionCode;
	}
	public List<PermissionData> getPermissionData() {
		return permissionData;
	}
	public void setPermissionData(List<PermissionData> permissionData) {
		this.permissionData = permissionData;
	}
	
	
}
