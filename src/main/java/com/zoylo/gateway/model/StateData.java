package com.zoylo.gateway.model;

/***
 * 
 * @author Iti Gupta
 * @version 1.0
 * 
 *
 */
public class StateData {

	private String languageId;
	private String languageCode;
	private String stateName;

	public StateData() {
	}

	public StateData(String languageId, String languageCode, String stateName) {
		super();
		this.languageId = languageId;
		this.languageCode = languageCode;
		this.stateName = stateName;
	}

	public String getLanguageId() {
		return languageId;
	}

	public void setLanguageId(String languageId) {
		this.languageId = languageId;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

}
