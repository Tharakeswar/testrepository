package com.zoylo.gateway.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.zoylo.gateway.domain.Role;

public interface ZoyloRoleCustomRepository {

	Page<Role> findByRoleProperties(Pageable pageable, String roleCode, String roleName, String authCategoryCode,
			String authCategoryName, boolean activeFlag,String languageCode);

}
