package com.zoylo.gateway.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.zoylo.gateway.domain.Permission;

/**
 * 
 * @author Diksha Gupta
 * @version 1.0
 * @description Interface for User permission collection
 *
 */
public interface PermissionRepository extends MongoRepository<Permission, String>,ZoyloPermissionCustomRepository{
	public List<Permission> findByPermissionCode(String permissionCode);
	public Permission findOneByPermissionCode(String permissionCode);
	public List<Permission> findByAuthCategoryCode(String authCode);
}
