package com.zoylo.gateway.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class CustomRepositoryImpl implements CustomRepository {
	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public void save(Object objectToSave, String collectionName) {
		mongoTemplate.insert(objectToSave, collectionName);
	}

}
