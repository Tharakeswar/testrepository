package com.zoylo.gateway.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.zoylo.gateway.domain.ZoyloUserAuth;
import com.zoylo.gateway.template.ZoyloAuthRepo;

import java.lang.String;

public interface ZoyloUserAuthRepository extends MongoRepository<ZoyloUserAuth, String>, ZoyloAuthRepo {
	public ZoyloUserAuth findByUserId(String userid);

	@Query("{'$and':[{'serviceProvidersList.providerTypeCode':?2} , {'userId':?0}, {'serviceProvidersList.providerId':?1} ]}")
	public ZoyloUserAuth findZoyloUserAuth(String userId, String providerId, String providerTypeCode);
}
