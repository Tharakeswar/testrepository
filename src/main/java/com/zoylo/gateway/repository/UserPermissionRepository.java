package com.zoylo.gateway.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.zoylo.gateway.domain.UserPermission;


@Repository
public interface UserPermissionRepository extends MongoRepository<UserPermission, String>{

	 public List<UserPermission> findByUserId(String userId);
	 public List<UserPermission> findUserPermissionByUserZoyloId(String userId);
	 public List<UserPermission> findAll();
}
