package com.zoylo.gateway.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.zoylo.gateway.domain.ZoyloUserToken;

@Repository
public interface ZoyloUserTokenRepository extends MongoRepository<ZoyloUserToken, String> {

	List<ZoyloUserToken> findByUserId(String userId);

}