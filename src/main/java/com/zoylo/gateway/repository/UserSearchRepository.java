package com.zoylo.gateway.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.zoylo.gateway.domain.UserSearch;

/**
 * 
 * @author Devendra.Kumar
 * @version 1.0
 *
 */
@Repository
public interface UserSearchRepository extends MongoRepository<UserSearch, String>,UserSearchCustomRepository {

}
