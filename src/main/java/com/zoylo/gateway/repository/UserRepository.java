package com.zoylo.gateway.repository;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.zoylo.gateway.domain.User;
import com.zoylo.gateway.template.ZoyloUserRepositoryRepo;


/**
 * Spring Data MongoDB repository for the User entity.
 */
@Repository
public interface UserRepository extends MongoRepository<User, String> ,ZoyloUserRepositoryRepo{

	// Optional<User> findOneByActivationKey(String activationKey);

	List<User> findAllByActiveFlagIsFalseAndCreatedDateBefore(Instant dateTime);

	// Optional<User> findOneByResetKey(String resetKey);

	@Query(value = "{emailInfo.emailAddress:?0}")
	Optional<User> findByEmailAddress(String emailAddress);

	@Query(value = "{phoneInfo.phoneNumber:?0}")
	Optional<User> findByPhoneNumber(String phoneNumber);

	Optional<User> findOneByUserName(String userName);

	Page<User> findAllByUserNameNot(Pageable pageable, String userName);

	User findOneById(String id);

	Optional<User> findById(String id);

	Page<User> findByDeleteFlagAndIsZoyloEmployee(Pageable pageable, Boolean deleteFlag, Boolean isZoyloEmployee);

//	@Query("{'$and':[{'deleteFlag':?2} , {'phoneInfo.phoneNumber':?0}, {'phoneInfo.verifiedFlag':?1} ]}")
//	Optional<User> findByPhoneNumberAndVerifiedFlagAndDeleteFlag(String phoneNumber, boolean verifiedFlag,
//			boolean deleteFlag);

	@Query("{'$and':[ {'emailInfo.emailAddress':?0}, {'emailInfo.verifiedFlag':?1},{'deleteFlag':?2} ]}")
	Optional<User> findByEmailAddressAndVerifiedFlag(String emailAddress, boolean verifiedFlag, boolean deleteFlag);

//	@Query("{'$and':[{'deleteFlag':?1} , {'phoneInfo.phoneNumber':?0}]}")
//	Optional<User> findByPhoneNumberAndDeleteFlag(String phoneNumber, boolean deleteFlag);

	@Query("{'$and':[ {'deleteFlag':?1},{'emailInfo.emailAddress':?0}]}")
	User findByEmailAddressAndDeleteFlag(String emailAddress, boolean deleteFlag);

	@Query("{'$and':[ {'emailInfo.emailAddress':?0}, {'deleteFlag':?1} ]}")
	Optional<User> findByEmailAddressAndVerifiedFlag(String emailAddress, boolean deleteFlag);

	/**
	 * Provides a list of User's who has a particular device type
	 * 
	 * @author Mehraj Malik
	 * 
	 */
	Page<User> findByDeviceTypeIgnoreCase(String deviceType, Pageable pageable);

	Page<User> findByDeleteFlagAndFirstNameIgnoreCaseAndIsZoyloEmployee(Pageable pageable, Boolean deleteFlag,
			String firstName, Boolean isZoyloEmployee);

	Page<User> findByDeleteFlagAndLastNameIgnoreCaseAndIsZoyloEmployee(Pageable pageable, Boolean deleteFlag,
			String lastName, Boolean isZoyloEmployee);
	
	Page<User> findByDeleteFlag(boolean deletedFlag,Pageable pageable);
	
	User findByEmailInfoEmailAddressAndDeleteFlag(String emailAddress,boolean deletedFlag);
	
	User findByPhoneInfoPhoneNumberAndDeleteFlag(String phoneNumber,boolean deletedFlag);

	Page<User> findByActiveFlagAndIsZoyloEmployee(Pageable pageable, boolean activeFalg, boolean isZoyloEmployee);

	public List<User> findByUserNameStartsWithIgnoreCaseAndDeleteFlag(String name,Boolean deleteFlag);
	
	public List<User> findByPhoneInfoPhoneNumberStartsWithAndDeleteFlag(String phone,Boolean flag);

	@Query("{'$or':[ {'phoneInfo.phoneNumber':?0}, {'phoneInfo.phoneNumber':?1} ]}")
	public User findOneByPhoneInfoPhoneNumber(String phoneNumber,String number);
	
	public User findOneByEmailInfoEmailAddress(String email);
	
	public List<User> findByPhoneInfoPhoneNumberIn(List<String> phoneNumbers);

	public List<User> findByCreatedDateBetween(Date startDate, Date endDate);

}
