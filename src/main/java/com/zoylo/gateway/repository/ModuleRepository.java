package com.zoylo.gateway.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.zoylo.gateway.domain.Module;

/**
 * @author damini.arora
 * @version 1.0
 *
 */
public interface ModuleRepository extends MongoRepository<Module, String> {
	public List<Module> findByDeleteFlag(boolean deleteFlag);
	@Query("{'$and':[ {'moduleCode':?0}, {'deleteFlag':?1} ]}")
	public Module findByModuleCode(String moduleCode,boolean isDeleted);

}
