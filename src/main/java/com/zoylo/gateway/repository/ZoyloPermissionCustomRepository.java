package com.zoylo.gateway.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.zoylo.gateway.domain.Permission;

/**
 * 
 * @author Balram Sharma
 * @version 1.0
 * @description Interface for declare permission repository method.
 *
 */
public interface ZoyloPermissionCustomRepository {

	Page<Permission> searchPermissionProperties(Pageable pageable, String permissionCode, String permissionName,
			boolean activeFlag, String authCategoryCode, String languageCode);

}
