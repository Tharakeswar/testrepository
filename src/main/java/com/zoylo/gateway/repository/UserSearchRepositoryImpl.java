package com.zoylo.gateway.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.zoylo.gateway.domain.UserSearch;

@Repository
public class UserSearchRepositoryImpl implements UserSearchCustomRepository {


	@Autowired
	private MongoOperations mongoOperations;
	
	@Override
	public List<UserSearch> findTopFiveByUserId(String userId,String providerType) {
		Pageable pageable = new PageRequest(0, 20);
		Criteria criteria = new Criteria();
		criteria.and("userId").is(userId);
		criteria.and("providerTypeCode").is(providerType.toUpperCase());
		Query query = new Query(criteria);
		query.with(new Sort(Sort.Direction.DESC, "createdOn")).with(pageable);
		List<UserSearch> userSearchs = mongoOperations.find(query, UserSearch.class);
		return userSearchs;
	}

	
}
