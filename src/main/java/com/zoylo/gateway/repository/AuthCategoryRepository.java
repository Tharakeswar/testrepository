package com.zoylo.gateway.repository;

import com.zoylo.gateway.domain.ZoyloAuthCategory;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the ZoyloAuthCategory entity.
 */
@Repository
public interface AuthCategoryRepository extends MongoRepository<ZoyloAuthCategory, String> ,AuthCategoryCustomRepository{
	public ZoyloAuthCategory findByAuthCategoryCode(String authCode);
	/**
	 * @author Balram Sharma
	 * @description get all ZoyloAuthCategory those ActiveFlag is true
	 * @return List<AuthCategoryNameList>
	 */
	public List<ZoyloAuthCategory> findByActiveFlagIsTrue();
}
