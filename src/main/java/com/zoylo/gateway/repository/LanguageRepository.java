package com.zoylo.gateway.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import com.zoylo.gateway.domain.Language;

/**
 * 
 * @author Diksha Gupta
 * @version 1.0
 * @description Interface for Language collection
 *
 */
@Repository
public interface LanguageRepository extends MongoRepository<Language, String> {
    Language findOneByLanguageCode(String languageCode);
}
