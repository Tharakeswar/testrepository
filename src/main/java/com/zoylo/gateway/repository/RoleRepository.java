package com.zoylo.gateway.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.zoylo.gateway.domain.Role;

/**
 * 
 * @author Diksha Gupta
 * @version 1.0
 * @description Interface for User role collection
 *
 */
@Repository
public interface RoleRepository extends MongoRepository<Role, String> {
	public Role findOneByRoleCode(String roleCode);
	
	public List<Role> findByRoleCode(String roleCode);
	public List<Role> findByAuthCategoryCode(String authCategoryCode);
	
	

}
