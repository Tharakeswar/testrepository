package com.zoylo.gateway.repository;

import java.util.List;

import com.zoylo.gateway.domain.UserSearch;

public interface UserSearchCustomRepository {

	public List<UserSearch> findTopFiveByUserId(String userId,String providerType);
}
