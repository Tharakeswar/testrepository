package com.zoylo.gateway.repository;

public interface CustomRepository {

	void save(Object objectToSave, String collectionName);

}
