package com.zoylo.gateway.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.zoylo.gateway.domain.ZoyloAuthCategory;
/**
 * 
 * @author Balram Sharma
 * @version 1.0
 * @description This class is used to declare repository method
 */
public interface AuthCategoryCustomRepository {

	Page<ZoyloAuthCategory> searchAuthCategory(Pageable pageable, String authCategoryCode, String authCategoryName,
			boolean activeFlag,String languageCode);
}
