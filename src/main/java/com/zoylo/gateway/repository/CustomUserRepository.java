package com.zoylo.gateway.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.zoylo.gateway.domain.User;

/**
 * 
 * @author arvind.rawat
 * @version 1.0
 *
 */
public interface CustomUserRepository {
	public User findByEmailAddressOrPhoneNumber(String emailAddress,String phoneNumber,boolean deleteFlag);
	
	public Map<Long, List<User>>  getAllAdminUsers(Pageable pageable,String searchField);
	
	public Page<User> searchUser (Pageable pageable, boolean activeFlag, boolean isZoyloEmployee, 
			String firstName, String lastName, String emailId, String mobileNumber);

	/**
	 * @author RakeshH
	 * @param isZoyloEmployee
	 * @param activeFlag
	 * @return {@link User}
	 */
	public List<User> fetchUserList(boolean isZoyloEmployee, boolean activeFlag);
}
