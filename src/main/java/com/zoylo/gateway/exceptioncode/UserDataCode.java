package com.zoylo.gateway.exceptioncode;

public enum UserDataCode {
	NO_DATA_FOUND("ERR1000","No data found for username: {0} ."),
	PASSWORD_WRONG("ERR1010","Password does not match for username: {0} ."),
	REGISTERATION_FAIL("ERR1020","Registration failed for username: {0}"),
	EMAIL_EXISTS("ERR1030","Email alreday use by other user: {0}"),
	MOBILE_NUMBER_EXISTS("ERR1040","Mobile number already use by other user: {0}");
    private final String errCode;
    private final String errMsg;

    /**
     * @param text
     */
    private UserDataCode(final String errCode,final String errMsg) {
        this.errCode = errCode;
        this.errMsg = errMsg;
    }

    /**
	 * Return a string representation of this status code.
	 */
	@Override
	public String toString() {
		return  errCode + ": " + errMsg;
	}

	/**
	 * Return the enum constant of this type with the specified numeric value.
	 * 
	 * @param statusCode
	 *            the numeric value of the enum to be returned
	 * @return the enum constant with the specified numeric value
	 * @throws IllegalArgumentException
	 *             if this enum has no constant for the specified numeric value
	 */

	public String getErrCode() {
		return errCode;
	}

	public String getErrMsg() {
		return errMsg;
	}
}