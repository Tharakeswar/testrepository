package com.zoylo.gateway.service;

import java.util.UUID;

import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Component;


@Component
public class EmailTokenProvider {
	/**
	 * Returns a random hashed string which needs to be used as token
	 * @return
	 */
	public String generateEmailVerificationToken(String salt){
		if(salt == null){
		  salt = "";
		}
		String token = UUID.randomUUID().toString();
		return hashedString(token, salt);
	}

	private String hashedString(String token, String salt){
		return new ShaPasswordEncoder(512).encodePassword(token, salt);
	}

}
