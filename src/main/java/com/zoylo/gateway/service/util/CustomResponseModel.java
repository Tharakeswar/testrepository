package com.zoylo.gateway.service.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CustomResponseModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1914248772208627534L;
	private boolean success;
	private String infoCode;
	private String infoMsg;
	private Object data;
	private String exception;
	private List<String> errorCodeList = new ArrayList<String>();
	HashMap<String, Object> metaInfo = new HashMap<>();

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getInfoCode() {
		return infoCode;
	}

	public void setInfoCode(String infoCode) {
		this.infoCode = infoCode;
	}
	
	public String getInfoMsg() {
		return infoMsg;
	}
	
	public void setInfoMsg(String infoMsg) {
		this.infoMsg = infoMsg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public HashMap<String, Object> getMetaInfo() {
		return metaInfo;
	}

	public void setMetaInfo(HashMap<String, Object> metaInfo) {
		this.metaInfo = metaInfo;
	}

	public String getException() {
		return exception;
	}

	public void setException(String exception) {
		this.exception = exception;
	}

	public List<String> getErrorCodeList() {
		return errorCodeList;
	}

	public void setErrorCodeList(List<String> errorCodeList) {
		this.errorCodeList = errorCodeList;
	}

}
