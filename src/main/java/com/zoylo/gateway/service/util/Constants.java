package com.zoylo.gateway.service.util;

/**
 * @author user
 *
 */
public final class Constants {

	private Constants() {
	}

	public static final String REGISTRATION_OTP_TITLE = "Zoylo registration OTP - ";
	public static final String INDIA_MOBILE_CODE = "+91";
	public static final String OTP_SENT = "OTP sent successfully";
	public static final String DATA = "data";
	public static final String CHANGE_PD_MESSAGE = "Your password has been changed successfully.";
	public static final String UPDATE = "update";
	public static final String DELETE = "delete";
	public static final String ZOYLO_USER = "zoyloUser";
	public static final String ZOYLO_USER_HISTORY = "zoyloUserHistory";
	public static final String MOBILE_VERIFICATION = "mobile-verification";
	public static final String RESEND_OTP = "resend-otp";
	public static final String CHANGE_PD = "change-password";
	public static final String DELETEFLAG = "deleteFlag";
	public static final String USERNAME = "userName";
	public static final String EMAIL="emailInfo.emailAddress";
	public static final String PHONE="phoneInfo.phoneNumber";
	public static final String DOCTOR_CLINIC = "DOCTOR_CLINIC";
	public static final String WELLNESS_CENTER = "WELLNESS_CENTER";
	public static final String HOSPITAL = "HOSPITAL";
	public static final String DOCTOR = "DOCTOR";
	public final static String VALUE_CODE = "lookupValueList.lookupValueCode";
	public final static String VALUE_LIST = "lookupValueList.$";
	public final static String LOOKUP_CODE = "lookupCode";
	public final static String LOOKUP_DATA = "lookupData";
	public final static String DELETED_ON = "deletedOn";
	public final static String ACTIVE_FROM_DATE = "activeFromDate";
	public final static String ACTIVE_TILL_DATE = "activeTillDate";
	public final static String CREATED_ON = "createdOn";
	public final static String CREATED_BY = "createdBy";
	public final static String LAST_UPDATED_ON = "lastUpdatedOn";
	public final static String LAST_UPDATED_BY = "lastUpdatedBy";
	public final static String VERSION_ID = "versionId";
	public final static String TWILIO_CODE ="TWILIO_TOKEN";
	public final static String TWILIO_CHAT_VALUE ="TWILIO_CHAT_TOKEN";
	public final static String TWILIO_VIDEO_VALUE ="TWILIO_VIDEO_TOKEN";
	public final static String LANGUAGE_CODE = "EN";
	public final static Integer INITIAL_RESEND_OTP_COUNT = 1;
	public final static Integer DEFAULT_RESEND_OTP_COUNT = 0;
	public final static String TRUE = "true";
	public final static String SUCCESS = "success";
	



}
