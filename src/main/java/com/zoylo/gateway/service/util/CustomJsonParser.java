package com.zoylo.gateway.service.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zoylo.gateway.successcode.CustomResponse;
import com.zoylo.gateway.web.rest.errors.CustomExceptionCode;
import com.zoylo.gateway.web.rest.errors.CustomParameterizedException;
import com.zoylo.gateway.web.rest.errors.RegisteredException;


/***
 * @version 1.0
 * @author arvindrawat
 */
public class CustomJsonParser<T> {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	/***
	 * @author arvindrawat
	 * @description To parse the custom json response
	 * @param jsonObj
	 * @return
	 */
	public CustomResponse parseJsonResponse(ResponseEntity<String> jsonObj) {
		try {
			String body = jsonObj.getBody().toString();
			ObjectMapper mapper = new ObjectMapper();
			CustomResponseModel CustomResponseModel = mapper.readValue(body, CustomResponseModel.class);
			
			return new CustomResponse(CustomResponseModel.getInfoCode(),
					CustomResponseModel.getInfoMsg(),
					CustomResponseModel.getData(), CustomResponseModel.getMetaInfo());
		} catch (Exception exception) {
			logger.error("Exception occor while parsing Response json {} " , exception.getMessage());
			throw new CustomParameterizedException(RegisteredException.RESPONSE_PARSER_EXCEPTION.getException(),
					CustomExceptionCode.RESPONSE_PARSER_ERROR.getErrMsg(),exception,
					CustomExceptionCode.RESPONSE_PARSER_ERROR.getErrCode());
		}

	}

	public T parseResponseData(String jsonObj, Class<T> destination) {
		logger.info("In parseStateDataData method... {}" , jsonObj);
		try {
			ObjectMapper mapper = new ObjectMapper();
			return mapper.readValue(jsonObj, destination);
		} catch (Exception exception) {
			logger.error("Exception occor while parsing Response data {}" , exception.getMessage());
			throw new CustomParameterizedException(RegisteredException.RESPONSE_PARSER_EXCEPTION.getException(),
					CustomExceptionCode.RESPONSE_PARSER_ERROR.getErrMsg(),exception,
					CustomExceptionCode.RESPONSE_PARSER_ERROR.getErrCode());
		}
	}

}
