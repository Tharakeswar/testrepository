package com.zoylo.gateway.service.util;
/**
 * @version1.0
 * @author user
 * @description Meta info for response 
 */
public class MetaInfo {
	private String serviceName;
	
	public MetaInfo(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
}
