package com.zoylo.gateway.successcode;

/**
 * Succss codes for the Controller layer. These success will be sent from the
 * controller layer for individual CRUD Opertaion.
 * 
 * 
 * @author Manoj Narwal
 * @version 1.0
 *
 */
public enum SuccessCode {

	SAVED_SUCCESSFULLY("INFO2051", "Role saved Successfully", "Role add Service"), FETCHED_SUCCESSFULLY("INFO2052",
			"Role view Successfully", "Role view Service"), UPDATED_SUCCESSFULLY("INFO2053",
					"Role updated Successfully", "Role update Service"), DELETED_SUCCESSFULLY("INFO2054",
							"Role deleted Successfully", "Role delete Service"), PERMISSION_SAVE("INFO2000",
									"Permission add Service saved Successfully",
									"Permission add Service save Service"), PERMISSION_DELETE("INFO2001",
											"Permission add Service delete Successfully",
											"Permission delete Service"), PERMISSION_UPDATE("INFO2002",
													"Permission add Service update update Successfully",
													"Permission edit  Service"), PERMISSION_FOUND("INFO2003",
															"Permission add Service  View Successfully",
															"Permission view Service"), MODULE_SAVE("INFO6009",
																	"Module added Successfully",
																	"Module add service"), MODULE_DELETE("INFO6010",
																			"Module deleted Successfully",
																			"Module Delete service"), MODULE_UPDATE(
																					"INFO6011",
																					"Module updated Successfully",
																					"Module Update Service"), FOUND_ALL_MODULE(
																							"INFO6012",
																							"Module viewd Successfully",
																							"Module View All Service"), FOUND_MODULE(
																									"INF6013",
																									"Module viewed by moduleCode Successfully",
																									"Module View Service by moduleCode"), LOGIN_SUCCESSFULLY(
																											"INFO0010",
																											"Login Successfully",
																											"Login Service"), REGISTRATION_SUCCESSFULLY(
																													"INFO0011",
																													"Registration Successfully",
																													"Registration Service"), GET_USER_TYPE(
																															"INFO0012",
																															"User types got Successfully",
																															"Get user type Service"), DELETE_USER_SUCCESSFULLY(
																																	"INFO0013",
																																	"User deleted Successfully",
																																	"Delete user Service"), UPDATE_USER_SUCCESSFULLY(
																																			"INFO0014",
																																			"User upadted Successfully",
																																			"Update user Service"), GET_USER_SUCCESSFULLY(
																																					"INFO0015",
																																					"User fetched Successfully",
																																					"Get user Service"), SOCIAL_REGISTRATION_SUCCESSFULLY(
																																							"SCS0016",
																																							"Social  deleted Successfully",
																																							"Social Registration Service"), SOCIAL_MOBILE_AND_EMAIL_UPDATE_SUCCESSFULLY(
																																									"SCS0017",
																																									"User deleted Successfully",
																																									"Set email and mobile"), MOBILE_VERIFY_SUCCESSFULLY(
																																											"SCS0018",
																																											"Mobile verification done Successfully",
																																											"Mobile verification service"), SOCIAL_MEDIA_LOGIN_SUCCESSFULLY(
																																													"SCS0019",
																																													"Social media Login done Successfully",
																																													"Social media Login Service"), ADMIN_REGISTRATION_SUCCESSFULLY(
																																															"INFO0020",
																																															"Admin Registration done Successfully",
																																															"Admin Registration Service"), DOCTOR_SAVE_SUCCESSFULLY(
																																																	"INFO0021",
																																																	"Doctor saved Successfully",
																																																	"Doctor save Service"), DIANOSTIC_SAVE_SUCCESSFULLY(
																																																			"INFO0022",
																																																			"Diagnostic saved Successfully",
																																																			"Diagnostic save Service"), EMAIL_VERIFICATION(
																																																					"INFO0023",
																																																					"Email verification done Successfully",
																																																					"Email verification Service"), MOBILE_UPDATE_SUCCESS(
																																																							"INFO2000",
																																																							"Mobile updated Successfully",
																																																							"User Profile : Mobile update Service"), EMAIL_UPDATE_SUCCESS(
																																																									"INFO2010",
																																																									"Email updated  Successfully",
																																																									"User Profile : Email update Service"), PASSWORD_UPDATE_SUCCESS(
																																																											"INFO2020",
																																																											"Password updated  Successfully",
																																																											"User Profile: Password update Service"),
	/**
	 * Created by piyush singh
	 * 
	 */
	TOKEN_CHECK_SUCCESS("INFO2030", "Email checked Successfully", "Token check service"), EMAIL_SEND_SUCCESS("INFO2040", "Email sent  Successfully", "Email send service "), OTP_SEND_SUCCESS("INFO2041", "OTP sent Successfully", "OTP send ervice "), OTP_VERIFICATION_SUCCESS("INFO2042", "OTP verification done Successfully", "OTP verify service"),
	ERROR_CODES_FETCH_SUCCESS("SCS9991", "All Zoylo Gateway Error Codes Fetched Successfully", "Get Zoylo Gateway Error Codes Service"),	
	SUCCESS_CODES_FETCH_SUCCESS("SCS9992", "All Zoylo Gateway Success Codes Fetched Successfully", "Get Zoylo Gateway Success Codes Service"),
	FILE_UPLOAD_SUCCESSFULLY("SCS9993", "Upload file Successfully", "Upload File Service"),
	FILES_DOWNLOAD_SUCCESSFULLY("SCS9994", "Download files Successfully", "Download File Service"),
	SWITCH_PROFILE_SUCCESSFULLY("SCS9995", "Switch Profile Successfully", "Switch Profile Service"),
	SET_PMS_ROLE_SUCCESSFULLY("SCS9996", "Set pms user role Successfully", "Set pms user role"),
	GET_PMS_ROLE_SUCCESSFULLY("SCS9997", "Get pms user role Successfully", "Get pms user role"),
	PMS_AUTHRIZATION_SUCCESSFULLY("SCS9998", "PMS user authrization Successfully", "check pms user role"), 
	GET_PMS_USER_ROLE_SUCCESSFULLY("SCS9999", "PMS user role fetched Successfully", "fetch pms user role"),
	AUTHCATEGORY_SAVE("INFO2000","AuthCategory add Service saved Successfully","AuthCategory add Service save Service"),
	AUTHCATEGORY_UPDATE("INFO2001","AuthCategory Service update Successfully","AuthCategory update Service"),
	GET_AUTH_CODE_SUCCESSFULLY("SCS2002", "Authcode fetched Successfully", "fetch Authcode"),
	AUTH_CATEGORY_FETCHED_SUCCESSFULLY("INFO2003","AuthCategory fetched Successfully", "AuthCategory fetched Service"),

	PERMISSION_FETCHED_SUCCESSFULLY("SCS7000", "Permission fetched Successfully", "fetch permission"),

	USER_CREATED_SUCCESSFULLY("SCS7001", "User created successfully", "User Register Service"),
	USER_UPDATED_SUCCESSFULLY("SCS7002", "User updated successfully", "User Register Service"),
	ADD_USER_ROLE("SCS7003", "Role added to the user successfully", "User Register Service"),
	UPDATE_USER_ROLE("SCS7004",  "user role successfully updated sucessfull", "User Register Service"),
	SEARCH_USER_ROLE("SCS7005", "User role search successfull", "User Register Service"),
	
	
	EMAIL_VERIFIED_SUCCESSFULLY("SCS7006","Email Verified Successfully", "Email Verification Service"),
	EMAIL_VERFICATION_TOKEN_GENERATED_SUCCESSFULLY("SCS7007","Email Verification Token generated Successfully", "Email Verification Service"),
	USERSEARCH_DAIGNOSTIC_SAVED_SUCCESSFULLY("SCS7450","Diagnostic UserSearch data saved Successfully", "UserSearch Service"),
	USERSEARCH_TOP5_GET_SUCCESSFULLY("SCS7451","Get top 5 UserSearch Successfully", "UserSearch Service"),
	GET_POPULAR_SEARCH("SCS7452","Get Popular Search Successfully", "UserSearch Service"),
	USERSEARCH_DOCTOR_SAVED_SUCCESSFULLY("SCS7453","Doctor UserSearch data saved Successfully", "UserSearch Service"),

	USER_IDS_FETCHED("SCS7454","UserIds fetched sucessfully", "UserIds fetched Service"),

	UPDATE_ECOMM_USER("SCS7454",  "E-commerce user updated successfully", "E-commerce user update Service");
	
	
	private final String successCode;
	private final String successMessage;
	private final String serviceName;

	private SuccessCode(String successCode, String successMessage, String serviceName) {
		this.successCode = successCode;
		this.successMessage = successMessage;
		this.serviceName = serviceName;
	}

	public String getSuccessCode() {
		return successCode;
	}

	public String getSuccessMessage() {
		return successMessage;
	}

	public String getServiceName() {
		return serviceName;
	}

}
