package com.zoylo.gateway.successcode;

import java.util.Map;
import java.util.Objects;

/**
 * @author Manoj Narwal
 *         <p>
 *         A customized response class which is used to translate the response
 *         on the view. Format: <code>
 * <pre>
 *  success : true,
 *  infoCode : SCS1010,
 *  data : [My Data]
 *  </pre>
 * </code>
 *         </p>
 */
public class CustomResponse {
	private final boolean  success;
	private final Object infoCode;
	private final String infoMsg;
	private final Map<String,Object> metaInfo;
	private final Object data;
	
	public CustomResponse(Object infoCode, String infoMsg, Map<String,Object> metaInfo) {
		this(infoCode, infoMsg, null, metaInfo);
	}

	public CustomResponse(Object infoCode, String infoMsg, Object data, Map<String,Object> metaInfo) {
		this.success = true;
		this.infoCode = infoCode;
		this.infoMsg = infoMsg;
		this.data = data;
		this.metaInfo = metaInfo;
	}

	public CustomResponse(Object infoCode, String infoMsg) {
		this(infoCode, infoMsg, null);
	}

	public boolean isSuccess() {
		return success;
	}

	public Object getInfoCode() {
		return infoCode;
	}
	
	public Object getInfoMsg() {
		return infoMsg;
	}

	public Object getData() {
		return data;
	}
	
	public Map<String,Object> getMetaInfo() {
		return metaInfo;
	}

	@Override
	public String toString() {
		return  "Success : "+Objects.toString(success)+
				"Info Code : "+Objects.toString(infoCode,"InfoCode is null")+
				"Info Message : "+Objects.toString(infoMsg,"InfoMsg is null")+
				"Meta Info : "+Objects.toString(metaInfo,"InfoCode is null")+
				"Data : "+Objects.toString(data,"Data is null");
	}
	
}
