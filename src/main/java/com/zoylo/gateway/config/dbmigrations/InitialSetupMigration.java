package com.zoylo.gateway.config.dbmigrations;

import java.time.Instant;

import org.springframework.data.mongodb.core.MongoTemplate;

import com.github.mongobee.changeset.ChangeLog;
import com.github.mongobee.changeset.ChangeSet;
import com.zoylo.gateway.domain.Authority;
import com.zoylo.gateway.domain.User;
import com.zoylo.gateway.security.AuthoritiesConstants;

/**
 * Creates the initial database setup
 */
@ChangeLog(order = "001")
public class InitialSetupMigration {

    @ChangeSet(order = "01", author = "initiator", id = "01-addAuthorities")
    public void addAuthorities(MongoTemplate mongoTemplate) {
        Authority adminAuthority = new Authority();
        adminAuthority.setName(AuthoritiesConstants.ADMIN);
        Authority userAuthority = new Authority();
        userAuthority.setName(AuthoritiesConstants.USER);
        mongoTemplate.save(adminAuthority);
        mongoTemplate.save(userAuthority);
    }

    @ChangeSet(order = "02", author = "initiator", id = "02-addUsers")
    public void addUsers(MongoTemplate mongoTemplate) {
        Authority adminAuthority = new Authority();
        adminAuthority.setName(AuthoritiesConstants.ADMIN);
        Authority userAuthority = new Authority();
        userAuthority.setName(AuthoritiesConstants.USER);

        User systemUser = new User();
        systemUser.setId("user-0");
        systemUser.setUserName("system");
        systemUser.setEncryptedPassword("$2a$10$mE.qmcV0mFU5NcKh73TZx.z4ueI/.bDWbj0T1BYyqP481kGGarKLG");	//NOSONAR
        //systemUser.setFirstName("");
       // systemUser.setLastName("System");
       // systemUser.setEmail("system@localhost");
        systemUser.setActiveFlag(true);
       // systemUser.setLangKey("en");
        systemUser.setCreatedBy(systemUser.getUserName());
        systemUser.setCreatedDate(Instant.now());
        systemUser.getAuthorities().add(adminAuthority);
        systemUser.getAuthorities().add(userAuthority);
        mongoTemplate.save(systemUser);

        User anonymousUser = new User();
        anonymousUser.setId("user-1");
        anonymousUser.setUserName("anonymoususer");
        anonymousUser.setEncryptedPassword("$2a$10$j8S5d7Sr7.8VTOYNviDPOeWX8KcYILUVJBsYV83Y5NtECayypx9lO");	//NOSONAR
        //anonymousUser.setFirstName("Anonymous");
        //anonymousUser.setLastName("User");
      //  anonymousUser.setEmail("anonymous@localhost");
        anonymousUser.setActiveFlag(true);
       // anonymousUser.setLangKey("en");
        anonymousUser.setCreatedBy(systemUser.getUserName());
        anonymousUser.setCreatedDate(Instant.now());
        mongoTemplate.save(anonymousUser);

        User adminUser = new User();
        adminUser.setId("user-2");
        adminUser.setUserName("admin");
        adminUser.setEncryptedPassword("$2a$10$gSAhZrxMllrbgj/kkK9UceBPpChGWJA7SYIb1Mqo.n5aNLq1/oRrC");	//NOSONAR
        //adminUser.setFirstName("admin");
        //adminUser.setLastName("Administrator");
      // adminUser.setEmail("admin@localhost");
        adminUser.setActiveFlag(true);
       // adminUser.setLangKey("en");
        adminUser.setCreatedBy(systemUser.getUserName());
        adminUser.setCreatedDate(Instant.now());
        adminUser.getAuthorities().add(adminAuthority);
        adminUser.getAuthorities().add(userAuthority);
        mongoTemplate.save(adminUser);

        User userUser = new User();
        userUser.setId("user-3");
        userUser.setUserName("user");
        userUser.setEncryptedPassword("$2a$10$VEjxo0jq2YG9Rbk2HmX9S.k1uZBGYUHdUcid3g/vfiEl7lwWgOH/K");		//NOSONAR
        //userUser.setFirstName("");
        //userUser.setLastName("User");
        //userUser.setEmail("user@localhost");
        userUser.setActiveFlag(true);
       // userUser.setLangKey("en");
        userUser.setCreatedBy(systemUser.getUserName());
        userUser.setCreatedDate(Instant.now());
        userUser.getAuthorities().add(userAuthority);
        mongoTemplate.save(userUser);
    }

}
