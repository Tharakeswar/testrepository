package com.zoylo.gateway.mv;

import java.util.Date;

/***
 * Model View for Audit fields
 * 
 * @author damini.arora
 * 
 * @version 1.0
 *
 */
public class BaseDomainMV {

	private Date createdOn;
	private String createdBy;
	private boolean activeFlag;

	public BaseDomainMV() {
	}

	public BaseDomainMV(Date createdOn, String createdBy, boolean activeFlag) {
		super();
		this.createdOn = createdOn;
		this.createdBy = createdBy;
		this.activeFlag = activeFlag;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public boolean isActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(boolean activeFlag) {

		this.activeFlag = activeFlag;
	}

}
