package com.zoylo.gateway.mv;

import java.util.Date;
import java.util.List;

import com.zoylo.gateway.domain.ProviderTypeData;
import com.zoylo.gateway.domain.SearchCategoryData;
import com.zoylo.gateway.domain.SearchParamater;

/**
 * 
 * @author Devendra.Kumar
 * @version 1.0
 *
 */
public class UserSearchMV extends BaseDomainMV {
	private String id;
	private String searchCategoryId;
	private String searchCategoryCode;
	private List<SearchCategoryData> searchCategoryData;
	private String userId;
	private String userZoyloId;
	private String userMobileNumber;
	private Date searchedOn;
	private String locationGpsLatitude;
	private String locationGpsLongitude;
	private String providerTypeId;
	private String providerTypeCode;
	private List<ProviderTypeData> providerTypeData;
	private List<SearchParamater> searchParamaterList;
	private Boolean systemFlag;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSearchCategoryId() {
		return searchCategoryId;
	}

	public void setSearchCategoryId(String searchCategoryId) {
		this.searchCategoryId = searchCategoryId;
	}

	public String getSearchCategoryCode() {
		return searchCategoryCode;
	}

	public void setSearchCategoryCode(String searchCategoryCode) {
		this.searchCategoryCode = searchCategoryCode;
	}

	public List<SearchCategoryData> getSearchCategoryData() {
		return searchCategoryData;
	}

	public void setSearchCategoryData(List<SearchCategoryData> searchCategoryData) {
		this.searchCategoryData = searchCategoryData;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserZoyloId() {
		return userZoyloId;
	}

	public void setUserZoyloId(String userZoyloId) {
		this.userZoyloId = userZoyloId;
	}

	public String getUserMobileNumber() {
		return userMobileNumber;
	}

	public void setUserMobileNumber(String userMobileNumber) {
		this.userMobileNumber = userMobileNumber;
	}

	public Date getSearchedOn() {
		return searchedOn;
	}

	public void setSearchedOn(Date searchedOn) {
		this.searchedOn = searchedOn;
	}

	public String getLocationGpsLatitude() {
		return locationGpsLatitude;
	}

	public void setLocationGpsLatitude(String locationGpsLatitude) {
		this.locationGpsLatitude = locationGpsLatitude;
	}

	public String getLocationGpsLongitude() {
		return locationGpsLongitude;
	}

	public void setLocationGpsLongitude(String locationGpsLongitude) {
		this.locationGpsLongitude = locationGpsLongitude;
	}

	public String getProviderTypeId() {
		return providerTypeId;
	}

	public void setProviderTypeId(String providerTypeId) {
		this.providerTypeId = providerTypeId;
	}

	public String getProviderTypeCode() {
		return providerTypeCode;
	}

	public void setProviderTypeCode(String providerTypeCode) {
		this.providerTypeCode = providerTypeCode;
	}

	public List<ProviderTypeData> getProviderTypeData() {
		return providerTypeData;
	}

	public void setProviderTypeData(List<ProviderTypeData> providerTypeData) {
		this.providerTypeData = providerTypeData;
	}

	public List<SearchParamater> getSearchParamaterList() {
		return searchParamaterList;
	}

	public void setSearchParamaterList(List<SearchParamater> searchParamaterList) {
		this.searchParamaterList = searchParamaterList;
	}

	public Boolean getSystemFlag() {
		return systemFlag;
	}

	public void setSystemFlag(Boolean systemFlag) {
		this.systemFlag = systemFlag;
	}

}
