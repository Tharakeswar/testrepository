package com.zoylo.gateway.mv;

import java.util.List;

import com.zoylo.gateway.domain.AuthCategoryData;
import com.zoylo.gateway.domain.PermissionData;

public class PermissionMV extends BaseDomainMV {

	private String id;
	private String permissionCode;
	private List<PermissionData> permissionData;

	private String authCategoryId;
	private String authCategoryCode;
	private List<AuthCategoryData> authCategoryData;

	private String authCategoryLandingPageUrl;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPermissionCode() {
		return permissionCode;
	}

	public void setPermissionCode(String permissionCode) {
		this.permissionCode = permissionCode;
	}

	public List<PermissionData> getPermissionData() {
		return permissionData;
	}

	public void setPermissionData(List<PermissionData> permissionData) {
		this.permissionData = permissionData;
	}

	public String getAuthCategoryId() {
		return authCategoryId;
	}

	public void setAuthCategoryId(String authCategoryId) {
		this.authCategoryId = authCategoryId;
	}

	public String getAuthCategoryCode() {
		return authCategoryCode;
	}

	public void setAuthCategoryCode(String authCategoryCode) {
		this.authCategoryCode = authCategoryCode;
	}

	public List<AuthCategoryData> getAuthCategoryData() {
		return authCategoryData;
	}

	public void setAuthCategoryData(List<AuthCategoryData> authCategoryData) {
		this.authCategoryData = authCategoryData;
	}

	public String getAuthCategoryLandingPageUrl() {
		return authCategoryLandingPageUrl;
	}

	public void setAuthCategoryLandingPageUrl(String authCategoryLandingPageUrl) {
		this.authCategoryLandingPageUrl = authCategoryLandingPageUrl;
	}

	public PermissionMV() {

	}

	public PermissionMV(String permissionCode, List<PermissionData> permissiondata, List<AuthCategoryData> moduleInfo,
			boolean systemDefined) {
		super();
		this.permissionCode = permissionCode;
		this.permissionData = permissiondata;
		this.authCategoryData = moduleInfo;
	}

}
