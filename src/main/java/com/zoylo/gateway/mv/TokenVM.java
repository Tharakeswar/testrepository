package com.zoylo.gateway.mv;

public class TokenVM {
	private String chatToken;
	private String ecommId;
	private String ecommToken;

	public String getChatToken() {
		return chatToken;
	}

	public void setChatToken(String chatToken) {
		this.chatToken = chatToken;
	}

	public String getEcommId() {
		return ecommId;
	}

	public void setEcommId(String ecommId) {
		this.ecommId = ecommId;
	}

	public String getEcommToken() {
		return ecommToken;
	}

	public void setEcommToken(String ecommToken) {
		this.ecommToken = ecommToken;
	}
}
