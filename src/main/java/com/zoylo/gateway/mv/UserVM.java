package com.zoylo.gateway.mv;

import java.util.List;

import com.zoylo.gateway.domain.EmailInfo;
import com.zoylo.gateway.domain.PhoneInfo;
import com.zoylo.gateway.model.UserRoleVM;

public class UserVM {

	private String id;
	private boolean activeFlag;
	private boolean loginEnabled;
	private boolean accountLocked;
	private EmailInfo emailInfo;
	private PhoneInfo phoneInfo;
	private String firstName;
	private String lastName;
	private String middleName;
	private Boolean isZoyloEmployee;
	private List<UserRoleVM> userRoleVM;

	public List<UserRoleVM> getUserRoleVM() {
		return userRoleVM;
	}
	public void setUserRoleVM(List<UserRoleVM> userRoleVM) {
		this.userRoleVM = userRoleVM;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public boolean isActiveFlag() {
		return activeFlag;
	}
	public void setActiveFlag(boolean activeFlag) {
		this.activeFlag = activeFlag;
	}
	public boolean isLoginEnabled() {
		return loginEnabled;
	}
	public void setLoginEnabled(boolean loginEnabled) {
		this.loginEnabled = loginEnabled;
	}
	public boolean isAccountLocked() {
		return accountLocked;
	}
	public void setAccountLocked(boolean accountLocked) {
		this.accountLocked = accountLocked;
	}
	public EmailInfo getEmailInfo() {
		return emailInfo;
	}
	public void setEmailInfo(EmailInfo emailInfo) {
		this.emailInfo = emailInfo;
	}
	public PhoneInfo getPhoneInfo() {
		return phoneInfo;
	}
	public void setPhoneInfo(PhoneInfo phoneInfo) {
		this.phoneInfo = phoneInfo;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public Boolean getIsZoyloEmployee() {
		return isZoyloEmployee;
	}
	public void setIsZoyloEmployee(Boolean isZoyloEmployee) {
		this.isZoyloEmployee = isZoyloEmployee;
	}
	
	

}
