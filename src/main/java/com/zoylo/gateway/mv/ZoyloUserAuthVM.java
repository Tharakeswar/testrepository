package com.zoylo.gateway.mv;

import com.zoylo.gateway.domain.ServiceProvidersList;

public class ZoyloUserAuthVM {

	private String id;

	private String userId;

	private String zoyloId;

	private String emailId;
	private String phoneNumber;
	private ServiceProvidersList serviceProvidersList;


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getZoyloId() {
		return zoyloId;
	}

	public void setZoyloId(String zoyloId) {
		this.zoyloId = zoyloId;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public ServiceProvidersList getServiceProvidersList() {
		return serviceProvidersList;
	}

	public void setServiceProvidersList(ServiceProvidersList serviceProvidersList) {
		this.serviceProvidersList = serviceProvidersList;
	}

	

	
}
