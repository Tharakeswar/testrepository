package com.zoylo.gateway.mv;

import com.zoylo.gateway.model.ZoyloAuthVM;

public class AuthVM extends ZoyloAuthVM {

	private String userId;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
