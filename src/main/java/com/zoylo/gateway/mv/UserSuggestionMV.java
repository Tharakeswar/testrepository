package com.zoylo.gateway.mv;

import java.util.Set;

public class UserSuggestionMV {
	
	private Set<String> result;

	public Set<String> getResult() {
		return result;
	}

	public void setResult(Set<String> result) {
		this.result = result;
	}
	
}