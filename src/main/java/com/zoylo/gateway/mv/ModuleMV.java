package com.zoylo.gateway.mv;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.zoylo.gateway.domain.ModuleData;

/**
 * @author damini.arora
 * @version 1.0
 *
 */
@Validated
public class ModuleMV extends BaseDomainMV {

	private String id;
	@JsonIgnore
	private Integer versionId;

	@NotNull
	@NotBlank
	private String moduleCode;

	private int displaySequence;

	private List<ModuleData> moduleData;

	private String imageInformation;

	private boolean activeFlag;

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	public int getDisplaySequence() {
		return displaySequence;
	}

	public void setDisplaySequence(int displaySequence) {
		this.displaySequence = displaySequence;
	}

	public List<ModuleData> getModuleData() {
		return moduleData;
	}

	public void setModuleData(List<ModuleData> moduleData) {
		this.moduleData = moduleData;
	}

	public String getImageInformation() {
		return imageInformation;
	}

	public void setImageInformation(String imageInformation) {
		this.imageInformation = imageInformation;
	}

	public boolean isActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(boolean activeFlag) {
		this.activeFlag = activeFlag;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
