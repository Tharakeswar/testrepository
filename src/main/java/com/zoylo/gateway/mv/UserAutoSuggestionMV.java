package com.zoylo.gateway.mv;

/**
 * 
 * @author Devendra.Kumar
 * @version 1.0
 *
 */
public class UserAutoSuggestionMV {

	private String id;
	private String fullName;
	private String zoyloId;
	private String firstName;
	private String middleName;
	private String lastName;
	private String phoneNumber;
	private String email;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getZoyloId() {
		return zoyloId;
	}

	public void setZoyloId(String zoyloId) {
		this.zoyloId = zoyloId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
