package com.zoylo.gateway.mv;

import java.util.Date;
import java.util.List;

import com.zoylo.gateway.domain.AuthCategoryData;
import com.zoylo.gateway.domain.GrantedPermissionsList;
import com.zoylo.gateway.domain.RoleData;

public class RoleMV extends BaseDomainMV {
	private String id;
	private String roleCode;
	private List<RoleData> roleData;
	private Date activeFromDate;
	private Date activeTillDate;
	private List<GrantedPermissionsList> permissionList;
	private String authCategoryId;
	private String authCategoryCode;
	private List<AuthCategoryData> authCategoryData;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public List<RoleData> getRoleData() {
		return roleData;
	}

	public void setRoleData(List<RoleData> roleData) {
		this.roleData = roleData;
	}

	
	public Date getActiveFromDate() {
		return activeFromDate;
	}

	public void setActiveFromDate(Date activeFromDate) {
		this.activeFromDate = activeFromDate;
	}

	public Date getActiveTillDate() {
		return activeTillDate;
	}

	public void setActiveTillDate(Date activeTillDate) {
		this.activeTillDate = activeTillDate;
	}

	public String getAuthCategoryCode() {
		return authCategoryCode;
	}

	public void setAuthCategoryCode(String authCategoryCode) {
		this.authCategoryCode = authCategoryCode;
	}

	
	public List<GrantedPermissionsList> getPermissionList() {
		return permissionList;
	}

	public void setPermissionList(List<GrantedPermissionsList> permissionList) {
		this.permissionList = permissionList;
	}

	public String getAuthCategoryId() {
		return authCategoryId;
	}

	public void setAuthCategoryId(String authCategoryId) {
		this.authCategoryId = authCategoryId;
	}

	public List<AuthCategoryData> getAuthCategoryData() {
		return authCategoryData;
	}

	public void setAuthCategoryData(List<AuthCategoryData> authCategoryData) {
		this.authCategoryData = authCategoryData;
	}

	public RoleMV() {
	}

	public RoleMV(String roleCode, List<RoleData> roleData, List<GrantedPermissionsList> permissionsList,
			boolean systemDefined) {
		super();
		this.roleCode = roleCode;
		this.roleData = roleData;
		this.permissionList = permissionsList;
	}

}
