package com.zoylo.gateway.web.rest.errors;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartException;


import io.jsonwebtoken.ExpiredJwtException;

/**
 * 
 * Controller advice to translate the server side exceptions to client-friendly
 * json structures.
 * 
 * @author piyush singh
 * @version 1.0
 */
@ControllerAdvice
public class ExceptionTranslator {

	private final Logger log = LoggerFactory.getLogger(ExceptionTranslator.class);

	@ExceptionHandler(ExpiredJwtException.class)
	@ResponseStatus(HttpStatus.UNAUTHORIZED)

	@ResponseBody
	public ParameterizedErrorVM processExpiredJwtException(ExpiredJwtException exception) {
		log.error("-- ExpiredJwtException --- {} " , exception);
		
		return  new ParameterizedErrorVM(
				RegisteredExceptions.UNAUTHORIZED_EXCEPTION.getException(),
				RegisteredExceptions.UNAUTHORIZED_EXCEPTION.getExceptionMessage(), Collections.emptyList());
	}

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
	public ParameterizedErrorVM processValidationError(MethodArgumentNotValidException ex) {
		List<FieldError> fieldErrors = ex.getBindingResult().getFieldErrors();
		List<String> errorCodeList = new ArrayList<>();
		for (FieldError fieldError : fieldErrors) {
			errorCodeList.add(fieldError.getDefaultMessage());
		}
	
		return new ParameterizedErrorVM(
				RegisteredExceptions.CONSTRAINT_VIOLATION_EXCEPTION.getException(),
				RegisteredExceptions.CONSTRAINT_VIOLATION_EXCEPTION.getExceptionMessage(), errorCodeList);
	}
    

	@ExceptionHandler(CustomParameterizedException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ParameterizedErrorVM processParameterizedValidationError(CustomParameterizedException ex) {
		return ex.getErrorVM();
	}

	@ExceptionHandler(AccessDeniedException.class)
	@ResponseStatus(HttpStatus.FORBIDDEN)
	@ResponseBody
	public ErrorVM processAccessDeniedException(AccessDeniedException e) {
		return new ErrorVM(ErrorConstants.ERR_ACCESS_DENIED, e.getMessage());
	}

	@ExceptionHandler(HttpRequestMethodNotSupportedException.class)
	@ResponseBody
	@ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
	public ParameterizedErrorVM processMethodNotSupportedException(HttpRequestMethodNotSupportedException exception) {
		log.error("-- HttpRequestMethodNotSupportedException --- ");
		return new ParameterizedErrorVM(RegisteredExceptions.METHOD_NOT_ALLOWED.getException(),
				RegisteredExceptions.METHOD_NOT_ALLOWED.getExceptionMessage(), Collections.emptyList());
	}

	@ExceptionHandler(MultipartException.class)
	@ResponseBody
	@ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
	public ParameterizedErrorVM processMultipartException(MultipartException exception) {
		log.error("-- MultipartException --- {} " , exception);
		return new ParameterizedErrorVM(RegisteredExceptions.MULTIPART_EXCEPTION.getException(),
				RegisteredExceptions.MULTIPART_EXCEPTION.getExceptionMessage(), Collections.emptyList());
	}

	@ExceptionHandler(Exception.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ParameterizedErrorVM processException(Exception ex) {
		log.error("----Unknown Exception--- {} " ,  ex);
		return new ParameterizedErrorVM(RegisteredExceptions.UNKNOWN_EXCEPTION.getException(),
				ex.getMessage(), Collections.emptyList());
	}
	
	@ExceptionHandler(CustomInvalidTokenException.class)
	@ResponseStatus(HttpStatus.UNAUTHORIZED)
	@ResponseBody
	public ParameterizedErrorVM processJwtException(CustomInvalidTokenException exception) {
		log.error("--- CustomInvalidTokenException : {}" , exception);
		ParameterizedErrorVM errorVm = new ParameterizedErrorVM(
				RegisteredException.UNAUTHORIZED_EXCEPTION.getException(),
				RegisteredException.UNAUTHORIZED_EXCEPTION.getExceptionMessage(), Collections.emptyList());
		return errorVm;
	}
}
