package com.zoylo.gateway.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zoylo.gateway.bll.MailService;
import com.zoylo.gateway.bll.UserBll;
import com.zoylo.gateway.bll.UserService;
import com.zoylo.gateway.config.Constants;
import com.zoylo.gateway.domain.User;
import com.zoylo.gateway.model.PageableVM;
import com.zoylo.gateway.mv.UserVM;
import com.zoylo.gateway.repository.UserRepository;
import com.zoylo.gateway.security.AuthoritiesConstants;
import com.zoylo.gateway.service.dto.UserDTO;
import com.zoylo.gateway.successcode.CustomResponse;
import com.zoylo.gateway.successcode.SuccessCode;
import com.zoylo.gateway.web.rest.util.HeaderUtil;
import com.zoylo.gateway.web.rest.util.PaginationUtil;
import com.zoylo.gateway.web.rest.vm.ManagedUserVM;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing users.
 * <p>
 * This class accesses the User entity, and needs to fetch its collection of
 * authorities.
 * <p>
 * For a normal use-case, it would be better to have an eager relationship
 * between User and Authority, and send everything to the client side: there
 * would be no View Model and DTO, a lot less code, and an outer-join which
 * would be good for performance.
 * <p>
 * We use a View Model and a DTO for 3 reasons:
 * <ul>
 * <li>We want to keep a lazy association between the user and the authorities,
 * because people will quite often do relationships with the user, and we don't
 * want them to get the authorities all the time for nothing (for performance
 * reasons). This is the #1 goal: we should not impact our users' application
 * because of this use-case.</li>
 * <li>Not having an outer join causes n+1 requests to the database. This is not
 * a real issue as we have by default a second-level cache. This means on the
 * first HTTP call we do the n+1 requests, but then all authorities come from
 * the cache, so in fact it's much better than doing an outer join (which will
 * get lots of data from the database, for each HTTP call).</li>
 * <li>As this manages users, for security reasons, we'd rather have a DTO
 * layer.</li>
 * </ul>
 * <p>
 * Another option would be to have a specific JPA entity graph to handle this
 * case.
 */
@RestController
@RequestMapping("/api")
public class UserResource {

	@Autowired
	private UserBll userBll;
	private final Logger log = LoggerFactory.getLogger(UserResource.class);

	private static final String ENTITY_NAME = "userManagement";

	private final UserRepository userRepository;

	private final UserService userService;
	private final MailService mailService;

	public UserResource(UserRepository userRepository, MailService mailService, UserService userService) {

		this.userRepository = userRepository;
		this.userService = userService;
		this.mailService = mailService;
	}

	/**
	 * POST /users : Creates a new user.
	 * <p>
	 * Creates a new user if the login and email are not already used, and sends
	 * an mail with an activation link. The user needs to be activated on
	 * creation.
	 *
	 * @param managedUserVM
	 *            the user to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new user, or with status 400 (Bad Request) if the login or email
	 *         is already in use
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/users")
	@Timed
	@Secured(AuthoritiesConstants.ADMIN)
	public ResponseEntity<User> createUser(@Valid @RequestBody ManagedUserVM managedUserVM) throws URISyntaxException {
		log.debug("REST request to save User : {}", managedUserVM);

		if (managedUserVM.getId() != null) {
			return ResponseEntity.badRequest().headers(
					HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new user cannot already have an ID"))
					.body(null);
			// Lowercase the user login before comparing with database
		} else if (userRepository.findOneByUserName(managedUserVM.getLogin().toLowerCase()).isPresent()) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "userexists", "Login already in use"))
					.body(null);
		} else if (userRepository.findByEmailAddress(managedUserVM.getEmail()).isPresent()) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "emailexists", "Email already in use"))
					.body(null);
		} else {
			User newUser = userService.createUser(managedUserVM);
			return ResponseEntity.created(new URI("/api/users/" + newUser.getPhoneInfo().getPhoneNumber()))
					.headers(HeaderUtil.createAlert(
							"A user is created with identifier " + newUser.getPhoneInfo().getPhoneNumber(),
							newUser.getPhoneInfo().getPhoneNumber()))
					.body(newUser);
		}
	}

	/**
	 * PUT /users : Updates an existing User.
	 *
	 * @param managedUserVM
	 *            the user to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         user, or with status 400 (Bad Request) if the login or email is
	 *         already in use, or with status 500 (Internal Server Error) if the
	 *         user couldn't be updated
	 */
	@PutMapping("/users")
	@Timed
	@Secured(AuthoritiesConstants.ADMIN)
	public ResponseEntity<UserDTO> updateUser(@Valid @RequestBody ManagedUserVM managedUserVM) {
		log.debug("REST request to update User : {}", managedUserVM);
		Optional<User> existingUser = userRepository.findByEmailAddress(managedUserVM.getEmail());
		if (existingUser.isPresent() && (!existingUser.get().getId().equals(managedUserVM.getId()))) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "emailexists", "Email already in use"))
					.body(null);
		}
		existingUser = userRepository.findOneByUserName(managedUserVM.getLogin().toLowerCase());
		if (existingUser.isPresent() && (!existingUser.get().getId().equals(managedUserVM.getId()))) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "userexists", "Login already in use"))
					.body(null);
		}
		Optional<UserDTO> updatedUser = userService.updateUser(managedUserVM);

		return ResponseUtil.wrapOrNotFound(updatedUser, HeaderUtil.createAlert(
				"A user is updated with identifier " + managedUserVM.getLogin(), managedUserVM.getLogin()));
	}

	/**
	 * GET /users : get all users.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and with body all users
	 */
	@GetMapping("/users")
	@Timed
	public ResponseEntity<List<UserDTO>> getAllUsers(@ApiParam Pageable pageable) {
		final Page<UserDTO> page = userService.getAllManagedUsers(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/users");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * @return a string list of the all of the roles
	 */
	@GetMapping("/users/authorities")
	@Timed
	@Secured(AuthoritiesConstants.ADMIN)
	public List<String> getAuthorities() {
		return userService.getAuthorities();
	}

	/**
	 * GET /users/:login : get the "login" user.
	 *
	 * @param login
	 *            the login of the user to find
	 * @return the ResponseEntity with status 200 (OK) and with body the "login"
	 *         user, or with status 404 (Not Found)
	 */
	@GetMapping("/users/{login:" + Constants.LOGIN_REGEX + "}")
	@Timed
	public ResponseEntity<UserDTO> getUser(@PathVariable String login) {
		log.debug("REST request to get User : {}", login);
		return ResponseUtil.wrapOrNotFound(userService.getUserWithAuthoritiesByLogin(login).map(UserDTO::new));
	}

	/**
	 * @author damini.arora
	 * @description To get List of all users
	 * @return Custom Response
	 */
	/*@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping("/get")
	public ResponseEntity<CustomResponse> getAll(Pageable pageable) {
		PageableVM<UserVM> pageableVM = userBll.getAll(pageable);
		List<UserVM> UserResponseVM = pageableVM.getContent();
		HashMap metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.GET_USER_SUCCESSFULLY.getServiceName());
		ObjectMapper mapper = new ObjectMapper();
		HashMap props = mapper.convertValue(pageableVM.getPageInfo(), HashMap.class);
		metaInfo.putAll(props);
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.GET_USER_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.GET_USER_SUCCESSFULLY.getSuccessMessage(), UserResponseVM, metaInfo),
				HttpStatus.OK);
	}*/

	/**
	 * @author damini.arora
	 * @description To update UserProfile
	 * @param userVM
	 * @return Custom Response
	 */
	@PutMapping("/update")
	public ResponseEntity<CustomResponse> update(@RequestBody UserVM userVM) {
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.MODULE_UPDATE.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.MODULE_UPDATE.getSuccessCode(),
						SuccessCode.MODULE_UPDATE.getSuccessMessage(), userBll.update(userVM), metaInfo),
				HttpStatus.OK);

	}
	

	/**
	 * @author Damini.arora
	 * @description To get list of Users
	 * @param value
	 * @return CustomResponse
	 */
	@GetMapping("/auto-suggestion")
	public ResponseEntity<CustomResponse> autoSuggestion(@RequestParam("value") String value) {
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.GET_USER_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.GET_USER_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.GET_USER_SUCCESSFULLY.getSuccessMessage(), userBll.autoSuggestion(value), metaInfo),
				HttpStatus.OK);
	}

				
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping("/get")
	public ResponseEntity<CustomResponse> getAll(Pageable pageable,@RequestParam("search")String searchField) {
		PageableVM<UserVM> pageableVM = userBll.getAll(pageable,searchField);
		List<UserVM> UserResponseVM = pageableVM.getContent();
		HashMap metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.GET_USER_SUCCESSFULLY.getServiceName());
		ObjectMapper mapper = new ObjectMapper();
		HashMap props = mapper.convertValue(pageableVM.getPageInfo(), HashMap.class);
		metaInfo.putAll(props);
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.GET_USER_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.GET_USER_SUCCESSFULLY.getSuccessMessage(), UserResponseVM, metaInfo),
				HttpStatus.OK);
	}

	/**
	 * @author Devendra.Kumar
	 * @description To get list of Users
	 * @param name
	 * @return CustomResponse
	 */
	@GetMapping("/auto-search")
	public ResponseEntity<CustomResponse> getIdAndName(@RequestParam(value = "name",required=true) String value) {
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.GET_USER_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.GET_USER_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.GET_USER_SUCCESSFULLY.getSuccessMessage(), userBll.getIdAndName(value), metaInfo),
				HttpStatus.OK);
	}
}
