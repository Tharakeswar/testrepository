package com.zoylo.gateway.web.rest.errors;

/**
 * The enum which contains all the registered exceptions 
 * 
 * @author Diksha Gupta
 *
 */
public enum RegisteredExceptions {

	
	LOGIN_EXCEPTION("LoginException", "Login failed "),
	REGISTRATION_EXCEPTION("RegistrationException", "Registration failed "),
	ROLE_EXCEPTION("RoleException", "Role setup failed "),
	PERMISSION_EXCEPTION("PermissionException", "Permission Denied "),
	CONSTRAINT_VIOLATION_EXCEPTION("ConstraintViolationException", "Constraint Violation Exception"),
	
	USER_PROFILE_EXCEPTION("UserProfileException", "User Profile registration failed "),
	MODULE_EXCEPTION("ModuleException", "Module Exception"),
	UNKNOWN_EXCEPTION("UnknownException", "Unknown Exception"),
	METHOD_NOT_ALLOWED("HttpRequestMethodNotSupportedException", "Http Request Method Not Supported Exception"),
	MULTIPART_EXCEPTION("MultipartException", "Multipart Exception "),
	EMAIL_VERIFICATION_EXCEPTION("EMail Verification Exception", "EMail Verification Exception "),
	UNAUTHORIZED_EXCEPTION("UnauthorizedException", "Unauthorized Exception "),
	ROLES_DELETION_ERROR("Role Deletion exception", "Role Deletion exception");
	
	
	
private final String exception;
	
	private final String exceptionMessage;
	
	private RegisteredExceptions(final String exception, String exceptionMessage) {
		this.exception = exception;
		this.exceptionMessage = exceptionMessage;
	}
		
	public String getException() {
		return exception;
	}
	
	public String getExceptionMessage() {
		return exceptionMessage;
	}
	
	
}
