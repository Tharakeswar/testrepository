package com.zoylo.gateway.web.rest;

import java.util.HashMap;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zoylo.gateway.bll.SwitchProfileBll;
import com.zoylo.gateway.security.CustomUserDetails;
import com.zoylo.gateway.security.jwt.JWTConfigurer;
import com.zoylo.gateway.successcode.CustomResponse;
import com.zoylo.gateway.successcode.SuccessCode;

/**
 * 
 * @author Ankur Goel
 * @version 1.0
 *
 */
@RestController
@RequestMapping("/api")
public class SwitchProfileRest {
	@Autowired
	private SwitchProfileBll switchProfileBll;

	/***
	 * 
	 * @param response
	 * @param customUserDetails
	 * @param authCategory
	 * @param serviceProviderId
	 * @return
	 */
	@GetMapping(path = "/switch-profile")
	public ResponseEntity<CustomResponse> authorize(HttpServletResponse response,
			@AuthenticationPrincipal CustomUserDetails customUserDetails,
			@RequestParam(name = "authcategory", required = true) String authCategory,
			@RequestParam(name = "providerid", required = false) String serviceProviderId,
			@RequestParam(name = "head-office-id", required = false) String headOfficeId) {
		if (headOfficeId != null && !headOfficeId.isEmpty()) {
			serviceProviderId = headOfficeId;
		}
		String token = switchProfileBll.switchProfile(customUserDetails, authCategory, serviceProviderId);
		response.setHeader(JWTConfigurer.AUTHORIZATION_HEADER, "Bearer " + token);
		response.setHeader("Access-Control-Expose-Headers", "Authorization");
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.SWITCH_PROFILE_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.SWITCH_PROFILE_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.SWITCH_PROFILE_SUCCESSFULLY.getSuccessMessage(), metaInfo),
				HttpStatus.OK);

	}

	/**
	 * 
	 * @param customUserDetails
	 * @param permissioncode
	 * @param serviceProviderId
	 * @return
	 */
	@GetMapping(path = "/is-authorize")
	public ResponseEntity<CustomResponse> isAuthorize(@AuthenticationPrincipal CustomUserDetails customUserDetails,
			@RequestParam(name = "permissioncode", required = true) String permissioncode,
			@RequestParam(name = "providerid", required = false) String serviceProviderId) {

		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.PMS_AUTHRIZATION_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.PMS_AUTHRIZATION_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.PMS_AUTHRIZATION_SUCCESSFULLY.getSuccessMessage(),
						switchProfileBll.isAuthorized(customUserDetails.getId(), serviceProviderId, permissioncode),
						metaInfo),
				HttpStatus.OK);

	}

	/**
	 * Get service provider assign role
	 * 
	 * @param customUserDetails
	 * @param providerTypeCode
	 * @param serviceProviderId
	 * @return
	 */
	@GetMapping(path = "/provider-roles")
	public ResponseEntity<CustomResponse> getServiceProviderRole(
			@AuthenticationPrincipal CustomUserDetails customUserDetails,
			@RequestParam(name = "providertypecode", required = true) String providerTypeCode,
			@RequestParam(name = "providerid", required = false) String serviceProviderId) {	
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.GET_PMS_ROLE_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.GET_PMS_ROLE_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.GET_PMS_ROLE_SUCCESSFULLY.getSuccessMessage(),
						switchProfileBll.getServiceProviderUserRole(customUserDetails.getId(), serviceProviderId,
								providerTypeCode),
						metaInfo),
				HttpStatus.OK);

	}

}
