package com.zoylo.gateway.web.rest;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zoylo.gateway.bll.UserSearchBll;
import com.zoylo.gateway.model.UserSearchData;
import com.zoylo.gateway.security.CustomUserDetails;
import com.zoylo.gateway.successcode.CustomResponse;
import com.zoylo.gateway.successcode.SuccessCode;

/**
 * 
 * @author Devendra.Kumar
 * @version 1.0
 *
 */
@RestController
@RequestMapping("/api/user-search")
public class UserSearchRest {

	@Autowired
	private UserSearchBll userSearchBll;

	/**
	 * @author Devendra.Kumar
	 * @param userSearch
	 */
	@PostMapping("/diagnostic")
	public ResponseEntity<CustomResponse> saveUserSearch(@AuthenticationPrincipal CustomUserDetails customUserDetails,@RequestBody UserSearchData userSearch) {
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.USERSEARCH_DAIGNOSTIC_SAVED_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.USERSEARCH_DAIGNOSTIC_SAVED_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.USERSEARCH_DAIGNOSTIC_SAVED_SUCCESSFULLY.getSuccessMessage(), userSearchBll.saveDiagnosticUserSearchData(customUserDetails,userSearch),
						metaInfo),
				HttpStatus.OK);

	}

	/**
	 * @author Devendra.Kumar
	 * @param userId
	 */
	@GetMapping
	public ResponseEntity<CustomResponse> getTopFive(@RequestParam(value = "userid", required = true) String userId,
			@RequestParam(value = "provider-type", required = true) String providerType) {
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.USERSEARCH_TOP5_GET_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.USERSEARCH_TOP5_GET_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.USERSEARCH_TOP5_GET_SUCCESSFULLY.getSuccessMessage(),
						userSearchBll.getTopFive(userId, providerType), metaInfo),
				HttpStatus.OK);

	}

	/**
	 * @author Devendra.Kumar
	 * @return
	 */
	@GetMapping("/popular-search")
	public ResponseEntity<CustomResponse> getPopularSearch(@RequestParam("provider-type") String providerType) {
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.GET_POPULAR_SEARCH.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.GET_POPULAR_SEARCH.getSuccessCode(),
						SuccessCode.GET_POPULAR_SEARCH.getSuccessMessage(), userSearchBll.getPopularSearch(providerType), metaInfo),
				HttpStatus.OK);

	}
	
	/**
	 * @author Devendra.Kumar
	 * @param userSearch
	 */
	@PostMapping("/doctor")
	public ResponseEntity<CustomResponse> saveDoctorUserSearch(@AuthenticationPrincipal CustomUserDetails customUserDetails,@RequestBody UserSearchData userSearch){
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.USERSEARCH_DOCTOR_SAVED_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.USERSEARCH_DOCTOR_SAVED_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.USERSEARCH_DOCTOR_SAVED_SUCCESSFULLY.getSuccessMessage(), userSearchBll.saveDoctorUserSearchData(customUserDetails,userSearch),
						metaInfo),
				HttpStatus.OK);
	}
}
