package com.zoylo.gateway.web.rest;

/**
 * 
 * @author damini.arora
 * @description This class provides REST APIs to operate on ZoyloUser
 *              collection.
 *
 */
import java.util.HashMap;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.zoylo.gateway.bll.UserBll;
import com.zoylo.gateway.domain.User;
import com.zoylo.gateway.model.GhostUserVM;
import com.zoylo.gateway.successcode.CustomResponse;
import com.zoylo.gateway.successcode.SuccessCode;

@Repository
@RequestMapping("/api/user")
public class UserRest {

	private final Logger logger = LoggerFactory.getLogger(UserRegisterRest.class);

	@Autowired
	private UserBll userBll;

	/**
	 * @author damini.arora
	 * @description This is REST API to add ghost user in zoyloUser collection.
	 * @param ghostUserVM
	 * @return ResponseEntity<CustomResponse>
	 */
	@PostMapping(path = "/ghost-user")
	public ResponseEntity<CustomResponse> save(@Valid @RequestBody GhostUserVM ghostUserVM) {
		logger.info("---In Gateway controller ... before service method");
		GhostUserVM ghostUser = userBll.addGhostUser(ghostUserVM);
		logger.info("---In Gateway controller ... after service method");
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.REGISTRATION_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.REGISTRATION_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.REGISTRATION_SUCCESSFULLY.getSuccessMessage(), ghostUser, metaInfo),
				HttpStatus.OK);
	}

	/**
	 * @author damini.arora
	 * @description This is REST API to get user from zoyloUser collection by
	 *              phoneNumber.
	 * @param phoneNumber
	 * @return ResponseEntity<CustomResponse>
	 */
	@GetMapping("/{phoneNumber}/number")
	public ResponseEntity<CustomResponse> getUserByPhoneNumber(@PathVariable("phoneNumber") String phoneNumber) {
		User user = userBll.getUserByPhoneNumber(phoneNumber);
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.GET_USER_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(new CustomResponse(SuccessCode.GET_USER_SUCCESSFULLY.getSuccessCode(),
				SuccessCode.GET_USER_SUCCESSFULLY.getSuccessMessage(), user, metaInfo), HttpStatus.OK);

	}
}
