package com.zoylo.gateway.web.rest.vm;

import org.hibernate.validator.constraints.NotEmpty;

import com.zoylo.gateway.web.rest.errors.CustomValidationConstant;

/**
 * View Model object for storing a user's credentials.
 */
public class LoginVM {

	@NotEmpty(message = CustomValidationConstant.USER_NAME_NOT_EMPTY)
	private String username;

	private String mobilenumber;

	@NotEmpty(message = CustomValidationConstant.PD_NOT_EMPTY)
	private String password;

	private Boolean rememberMe;

	private String deviceId;

	private String deviceType;
	
	private String roleCategory;
	
	private String ecommAdminToken;
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean isRememberMe() {
		return rememberMe;
	}

	public void setRememberMe(Boolean rememberMe) {
		this.rememberMe = rememberMe;
	}

	public String getMobilenumber() {
		return mobilenumber;
	}

	public void setMobilenumber(String mobilenumber) {
		this.mobilenumber = mobilenumber;
	}

	@Override
	public String toString() {
		return "LoginVM{" + "username='" + username + '\'' + ", rememberMe=" + rememberMe + '}';
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getRoleCategory() {
		return roleCategory;
	}

	public void setRoleCategory(String roleCategory) {
		this.roleCategory = roleCategory;
	}

	public String getEcommAdminToken() {
		return ecommAdminToken;
	}

	public void setEcommAdminToken(String ecommAdminToken) {
		this.ecommAdminToken = ecommAdminToken;
	}
}
