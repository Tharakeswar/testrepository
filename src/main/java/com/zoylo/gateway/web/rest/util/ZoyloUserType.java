package com.zoylo.gateway.web.rest.util;

public enum ZoyloUserType {
	
	RECIPIENT("USER003","recipient","Recipient"),
	DOCTOR("USER004","doctor","I am a Doctor"),
	DIAGONSTIC("USER005","diagnostic","Diagnostic Center");
	private final String userCode;
    private final String userType;
    private final String name;
    
    private ZoyloUserType(final String userCode,final String userType,String name) {
    	this.userCode = userCode;
    	this.userType = userType;
    	this.name = name;
    }
    public String getUserCode() {
		return userCode;
	}
    
    public String getUserType() {
		return userType;
	}
    public String getName() {
		return name;
	}
}
