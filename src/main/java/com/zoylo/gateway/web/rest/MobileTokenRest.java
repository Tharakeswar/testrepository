package com.zoylo.gateway.web.rest;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zoylo.gateway.bll.MobileTokenBll;
import com.zoylo.gateway.model.MobileVerificationVM;
import com.zoylo.gateway.security.jwt.JWTConfigurer;
import com.zoylo.gateway.successcode.CustomResponse;
import com.zoylo.gateway.successcode.SuccessCode;

/**
 * @author Mehraj Malik
 * @version 1.0
 *
 */
@RestController
@RequestMapping("/api/mobile/")
public class MobileTokenRest {

	@Autowired
	private MobileTokenBll mobileTokenBll;
	private static final Double ANDROID_APPVERSION = 2.2;
	private static final Double IOS_APPVERSION = 2.2;
	private static final String ANDROID = "Android";
	private static final String IOS = "iOS";
		
	@GetMapping("/version-verify")
	public ResponseEntity<CustomResponse> versionVerify(@RequestParam("appVersionId") Double appVersionId,@RequestParam("osType") String osType,
			HttpServletResponse response) {
		boolean flag;
		if (osType.equalsIgnoreCase(ANDROID)) {
			if (Double.compare(appVersionId,ANDROID_APPVERSION) >= 0) {
				flag = Boolean.TRUE;
			} else {
				flag = Boolean.FALSE;
			}
		} else if (osType.equalsIgnoreCase(IOS)) {
			if (Double.compare(appVersionId,IOS_APPVERSION) >= 0) {
				flag = Boolean.TRUE;
			} else {
				flag = Boolean.FALSE;
			}
		} else {
			flag = Boolean.TRUE;
		}
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.MOBILE_VERIFY_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<>(new CustomResponse(null, null, flag, null), HttpStatus.OK);
	}
	
	@PostMapping("/verify-otp")
	public ResponseEntity<CustomResponse> verifyMobileOTP(@Valid @RequestBody MobileVerificationVM mobileVerificationVM,
			HttpServletResponse response) {
		Map<String,String> token = mobileTokenBll.getMobileToken(mobileVerificationVM);
		response.setHeader(JWTConfigurer.AUTHORIZATION_HEADER, "Bearer " + token.get("token"));
		response.setHeader("Access-Control-Expose-Headers", "Authorization");
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.MOBILE_VERIFY_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<>(new CustomResponse(SuccessCode.MOBILE_VERIFY_SUCCESSFULLY.getSuccessCode(),
				SuccessCode.MOBILE_VERIFY_SUCCESSFULLY.getSuccessMessage(),token.get("chatToken"), metaInfo), HttpStatus.OK);
	}

}
