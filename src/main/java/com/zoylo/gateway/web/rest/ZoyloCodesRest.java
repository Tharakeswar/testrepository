package com.zoylo.gateway.web.rest;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zoylo.gateway.successcode.CustomResponse;
import com.zoylo.gateway.successcode.SuccessCode;
import com.zoylo.gateway.web.rest.errors.CustomExceptionCode;

/**
 * Rest APIs for codes in Zoylo Gateway
 * 
 * @author Manoj Narwal
 * @version 1.0
 */

@RestController
@RequestMapping("/api/codes")
public class ZoyloCodesRest {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	/**
	 * Get all error codes in Zoylo Gateway
	 * 
	 * @return
	 */
	@GetMapping("/error")
	public ResponseEntity<CustomResponse> getAllErrorCodes() {
		logger.info("In get Zoylo Gateway error codes");

		HashMap<String, String> data = new HashMap<>();
		for (CustomExceptionCode exceptionCode : CustomExceptionCode.values()) {
			data.put(exceptionCode.getErrCode(), exceptionCode.getErrMsg());
		}

		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.ERROR_CODES_FETCH_SUCCESS.getServiceName());

		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.ERROR_CODES_FETCH_SUCCESS.getSuccessCode(),
						SuccessCode.ERROR_CODES_FETCH_SUCCESS.getSuccessMessage(), data,metaInfo), HttpStatus.OK);
	}
	
	/**
	 * Get all success codes in Zoylo Gateway 
	 * 
	 * @return
	 */
	@GetMapping("/success")
	public ResponseEntity<CustomResponse> getAllSuccessCodes() {
		logger.info("In get Zoylo Gateway success codes");

		HashMap<String, String> data = new HashMap<>();
		for (SuccessCode successCode : SuccessCode.values()) {
			data.put(successCode.getSuccessCode(), successCode.getSuccessMessage());
		}

		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.SUCCESS_CODES_FETCH_SUCCESS.getServiceName());

		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.SUCCESS_CODES_FETCH_SUCCESS.getSuccessCode(),
						SuccessCode.SUCCESS_CODES_FETCH_SUCCESS.getSuccessMessage(), data,metaInfo), HttpStatus.OK);
	}

}
