package com.zoylo.gateway.web.rest.errors;

public final class CustomValidationConstant {
	public static final String FIRST_NAME_LENGTH = "VLD0010";
	public static final String LAST_NAME_LENGTH = "VLD0011";
	public static final String EMAIL_NOT_EMPTY = "VLD0012";
	public static final String EMAIL_CORE = "VLD0013";
	public static final String MOBILE_LENGTH = "VLD0014";
	public static final String PD_NOT_EMPTY = "VLD0015";
	public static final String USER_TYPE_NOT_EMPTY = "VLD0016";
	public static final String USER_NAME_NOT_EMPTY = "VLD0017";
	public static final String USER_ID_NOT_EMPTY = "VLD0018";
	public static final String OTP_LENGTH = "VLD0019";
	public static final String LOGIN_TYPE_NOT_EMPTY = "VLD0020";
	public static final String SOCIAL_ID_NOT_EMPTY = "VLD0021";
	public static final String MOBILE_DIGIT = "VLD0022";
	public static final String ROLE_ID = "VLD2001";
	public static final String ROLE_CODE = "VLD2002";
	public static final String PERMISSION_CODE = "VLD2011";
	public static final String PERMISSION_ID = "VLD2010";
	public static final String FIRST_NAME_NOT_BLANK = "VLD0023";
	public static final String LAST_NAME_NOT_BLANK = "VLD0024";
	public static final String TERMS_AND_CONDITION_NOT_SELECT = "VLD0025";
	public static final String USER_TYPE_NOT_VALID = "VLD0026";
	public static final String TOKEN = "VLD0027";
	public static final String OLD_PD = "VLD0028";
	public static final String NEW_PD = "VLD0029";
	public static final String EMAIL_ID = "VLD0026";
	public static final String AUTH_CATEGORY_CODE = "VLD0027";
	
	private CustomValidationConstant() {}

}
