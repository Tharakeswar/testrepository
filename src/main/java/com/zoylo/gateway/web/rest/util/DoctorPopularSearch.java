package com.zoylo.gateway.web.rest.util;

/**
 * 
 * @author Devendra.Kumar
 * @version 1.0
 *
 */
public enum DoctorPopularSearch {

	DENTAL("Dental"),
	DERMATOLOGY("Dermatology"),
	GENERAL_PHYSICIAL("General Physician"),
	PEDIATRICS("Pediatrics"),
	ENT("ENT"),
	PSYCHIARTY("Psychiatry"),
	CARDIOLOGY("Cardiology"),
	DIABETOLOGY("Diabetology"),
	GASTROENTEROLOGY("Gastroenterology"),
	HOMEOPATHY("Homeopathy"),
	AYURVEDA("Ayurveda"),
	GYNECOLOGY("Gynecology"),
	ORTHOPEDIC("Orthopedic"),
	NEUROLOGY("Neurology"),
	NEPHROLOGY("Nephrology"),
	SEXOLOGY("Sexology"),
	OPHTHALMOLOGY("Ophthalmology"),
	OTHER_SPECIALTIES("Other Specialties");

	private final String search;

	private DoctorPopularSearch(final String search) {
		this.search = search;

	}

	public String getSearch() {
		return search;
	}

}
