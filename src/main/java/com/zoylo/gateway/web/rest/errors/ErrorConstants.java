package com.zoylo.gateway.web.rest.errors;

public final class ErrorConstants {

    public static final String ERR_CONCURRENCY_FAILURE = "error.concurrencyFailure";
    public static final String ERR_ACCESS_DENIED = "error.accessDenied";
    public static final String ERR_VALIDATION = "error.validation";
    public static final String ERR_METHOD_NOT_SUPPORTED = "error.methodNotSupported";
    public static final String ERR_INTERNAL_SERVER_ERROR = "error.internalServerError";
    
    public static final String SERACH_CATEGORY_TYPE = "SERACH_CATEGORY_TYPE";
	public static final String USER_SEARCH = "USER_SEARCH";
	public static final String DIAGNOSTIC_CENTER = "DIAGNOSTIC_CENTER";
	public static final String PROVIDER_TYPE = "PROVIDER_TYPE";
	public static final String SEARCH_OPERATOR = "SEARCH_OPERATOR"; 
	public static final String LIKE = "LIKE";
	public static final String DOCTOR_CLINIC = "DOCTOR_CLINIC";
    private ErrorConstants() {
    }

}
