package com.zoylo.gateway.web.rest.errors;

public enum CustomExceptionCode {
	NO_DATA_FOUND("ERR1000","No data found for username: {0} ."),
	PASSWORD_WRONG("ERR1001","Password does not match for username: {0} ."),
	REGISTERATION_FAIL("ERR1002","Registration failed for username: {0}"),
	EMAIL_EXISTS("ERR1003","Email alreday used by another user: {0}"),
	MOBILE_NUMBER_EXISTS("ERR1004","Mobile number already use by other user: {0}"),
	EMAIL_NOT_EXISTS("ERR1005","Email id not found"),
	MOBILE_NUMBER_NOT_EXISTS("ERR1006","Mobile number not found"),
	LOGIN_FAIL("ERR1007","Login failed for username: {0}"),
	DB_ERROR("ERR1008","DataBase Error."),
	USER_NOT_FOUND("ERR1009","User not found.")	,
	ACCOUNT_LOCKED("ERR1010","User account has been locked."),
	MOBILE_NOT_VERIFIED("ERR1011","User mobile number not verified yet."),	
	ACCOUNT_DEACTIVE("ERR1012","User account is not active ."),	
	ACCOUNT_DELETED("ERR1013","User account has been deleted ."),
	ADMIN_ROLE_REQUIRED("ERR1014","Admin role required ."),
	OTP_NOT_MATCH("ERR1015","OTP not match ."),
	FAIL_TO_SAVE_DOCTOR("ERR1016","Doctor not save ."),
	FAIL_TO_SAVE_DIAGONSTIC("ERR1017","Diagnostic not save ."),
	FAIL_TO_CREATE_USER_PROFILE("ERR1018","User profile not save ."),
	OTP_LENGTH("ERR1019","OTP length should be 6."),
	EMAIL_ALREADY_VERIFIED("ERR1020","Email is already verified"),

	OTP_EXPIRED("ERR1022","OTP has been expired."), 
	EMAIL_VERIFIED_SUCCESSFULLY("ERR1021","Email verified sucessfully."),
	PLEASE_SELECT_TERMS_AND_CONDITIONE("ERR1023","Terms and condition fail."),
	USER_TYPE_EXCEPTION("ERR1024","Exception occoured when get user type."),
	OLD_AND_NEW_PASSWORD_SAME_EXCEPTION("ERR7187","Old and New Password is same"),
	RESPONSE_PARSER_ERROR("ERR2070","Unable to parse response json."),
    LOGIN_NOT_ENABLE("ERR2071","Login not enable."),

	

	/**
	 * Created by diksha gupta
	 * for roles api error code
	 */
	ROLES_CREATION_ERROR("ERR2001","Role not created."),
	ROLES_UPDATION_ERROR("ERR2002","Role not updated."),
	ROLES_DELETION_ERROR("ERR2003","Role not deleted."),
	ROLE_CODE_ERROR("ERR2004","Role code must be unique."),
	ROLE_NOT_FOUND_ERROR("ERR2005","Role not found."),
	ROLE_CODE_NOT_FOUND_ERROR("ERR2005","Role not found."),
	/**
	 * Created by diksha gupta
	 * for permission api error code
	 */

	PERMISSIONS_CREATION_ERROR("ERR2051","Permission not created."),
	PERMISSIONS_UPDATION_ERROR("ERR2052","Permission not updated."),
	PERMISSIONS_DELETION_ERROR("ERR2053","Permission not deleted."), 
	PERMISSION_CODE_ERROR("ERR2054","Permission code must be unique."),
	PERMISSION_NOT_FOUND_ERROR("ERR2055","Permission not found."),
	ACCESS_DENIED_ERROR("ERR0010","Access Denied"),
	
	
	CHANGE_PASSWORD_ERROR("ERR7020","Change Password Error"),
	PASSWORD_INCORRECT("ER7021","Password is incorrect"),
	
	/**
	 * @author arvind.rawat
	 * for User profile api error code
	 */
	
	MOBILE_UPDATE_ERROR("ERR2210","Unable to update mobile number."),
	EMAIL_UPDATE_ERROR("ERR2220","Unable to update email."),
	/**
	 * @author Damini Arora
	 * for modules api
	 */
	NO_MODULE_FOUND("ERR4000", "No Module found"),
	MODULE_SAVE_ERROR("ERR6009","Exception occur while saving Module data"),
	MODULE_UPDATE_ERROR("ERR6010","Exception occured while updating the Module"),
	MODULE_NOT_EXIST("ERR6011","Module not exist"),
	MODULE_NOT_DELETE("ERR6012","Module not deleted"),
	MODULE_CODE_ALREADY_EXIST("ERR6013","Module Code Already exist"),
	/**
	 * @author piyush singh
	 * token expire
	 */
	TOKEN_EXPIRE("ERR6013","token expire"),
	UNAUTHORIZED_EXCEPTION("ERR4055","The JWT Token has been expired."),
	PASSWORD_EMPTY("ERR4056","Password empty: {0} ."),

	MOBILE_NUMBER_ALREADY_EXIST_NOT_VERIFIED("ERR4057","mobile number already exist.But not verified "),
	OTP_NOT_FOUND("ERR4058","otp not found"),
	FILE_UPLOAD_FAIL_EXCEPTION("ERR4058", "Fail to upload file"),
	UPLOAD_FILE_MIN_SIZE_EXCEPTION("ERR6091", "upload file size should be more than 10KB"),
	UPLOAD_FILE_MAX_SIZE_EXCEPTION("ERR6092", "upload file size should be less than 10MB"),
	UPLOAD_FILE_REPORT_MAX_SIZE_EXCEPTION("ERR6093", "upload file size should be less than 20MB"),
	FILE_DOWNLOAD_FAIL_EXCEPTION("ERR4059", "Fail to download file"),
	NO_DOWNLOAD_URLS_FOUND("ERR4059", "Downloads urls not found with the given id."),
	INVALID_TOKEN_ERROR("ERR2630", "Token is invalid"),
	DATA_NOT_SAVE("ERR6090","Fail to save data"), 
	AUTH_CATEGORY_ERROR("ERR6191","Fail to get auth category data"),
	SWITCH_PROFILE_ERROR("ERR6192","Fail to switch profile"),
	USER_AUTH_ERROR("ERR6193","Fail to get servive provider role"),
	USER_UPDATE_ERROR("ERR6999","Fail to update user"),
	USER_EMAIL_ALREADY_EXIST("ERR6991","User Email already exist"),
	USER_PHONE_NUMBER_ALREADY_EXIST("ERR6992","User Phone Number already exist"),

	ADMIN_USER_FETCH_EXCEPTION("ERR2631","Exception occor while fetching admin user"),
	AUTHCATEGORY_CODE_ERROR("ERR7000","Authorization code must be unique."),
	AUTHCATEGORY_CODE_NOT_FOUND("ERR7001","Authorization code can not be null"),
	LANDINGPAGE_URL_NOT_FOUND("ERR7002","Landing page url not found"),

	PERMISSION_CODE_NOT_FOUND("ERR2055","Permission code not found "),	

	PERMISSION_DATA_NOT_FOUND("ERR2056","Permission data not found "),
	USER_DATA_FETCH_EXCEPTION("ERR6993", "Failed to fetch organization user data"),
	USER_FIRST_NAME_MANDATORY("ERR6994", "User first name is mandatory"),
	USER_LAST_NAME_MANDATORY("ERR6995", "User last name is mandatory"),
	FAIL_TO_UPDATE_USER_PROFILE("ERR6996", "Failed to update user profile"),
	FAIL_TO_ASSIGN_ROLE("ERR6997", "Failed to assign role to user"),
	ZOYLO_ID_MANDATORY("ERR6998", "Mandatory zoylo id not provided while assigning role"),
	USER_ID_MANDATORY("ERR6703", "Mandatory user id not provided while assigning role"),
	EMAIL_ID_MANDATORY("ERR6700", "Mandatory email id not provided while assigning role"),
	PHONE_NUMBER_MANDATORY("ERR6701", "Mandatory phone number not provided while assigning role"),
	ROLE_CODE_MANDATORY("ERR6702", "Mandatory role code not provided for assigning role"),
	ROLE_CODE_MAPPED_ALREADY("ERR6703", "Role is already mapped for the user"),
	USER_SEARCH_FAILED("ERR6704", "User search failed"),
	EMAIL_TOKEN_EXPIRED("ERR6705","Email is already verified"),
	FAIL_TO_SAVE_DIAGNOSTIC_USER_SEARCH_DATA("ERR7450","Exception occured while saving Diagnostic UserSearch"),
	USERSEARCH_GET_TOP5_ERROR("ERR7451","Exception occured while getting top 5 UserSearch"),
	FAIL_TO_SAVE_DOCTOR_USER_SEARCH_DATA("ERR7452","Exception occured while saving Doctor UserSearch"),
	
	USER_SEARCH_REST_CLIENT_SAVE_ERROR("ERR7740","Fail to save UserSearch data by Gateway Rest Client"),
	USER_GET_BY_ID_ERROR("ERR7741","Exception occured while parse User detail by user id"),
	LOOKUP_ERROR("ERR7742","Exception occured while parse Lookup data by lookup code"),
	USER_SEARCH_SAVE_ERROR("ERR7743","Fail to save UserSearch data"),
	
    USER_ROLE_UPDATE_EXCEPTION("ERR2072","Exception occured while updating the User Auth Exception"),
    IP_ADDRESS_PERMISSION_DENIED("ERR2692", "API Access denied for this machine"),
    USER_IDS_FETECH_EXCEPTION("ERR2693","Exception occured while getting userIds on the basis of user PhoneNumbers"),
	LOOKUP_NOT_FOUND("ERR7164", "Exception occured while fatching the lookup"),
	CHECK_EMAIL_VERIFIED_EXCEPTION("ERR2694", "Exception occured while checking email is verified or not"),
	EXCEPTION_OCCOR_WHILE_VERIFYING_OTP("ERR2695","Login not enable."),
	EXCEPTION_OCCOR_IN_EMAIL_TEMPLTE("ERR0800","Exception ouccurred in email template"),
	ECOMM_LOGIN_API_ACCESS_EXCEPTION("ERR0801","Exception ouccurred while accessing ecommerce login api."),
	ECOMM_SIGNUP_API_ACCESS_EXCEPTION("ERR0803","Exception ouccurred while accessing ecommerce signup api."),
	ECOMM_SIGNUP_EXCEPTION("ERR0804","Exception ouccurred while signup ecommerce."),
	ECOMM_LOGIN_STATUS_EXCEPTION("ERR0805","ecommerce authentication status : failed."),
	ECOMM_SIGNUP_STATUS_EXCEPTION("ERR0806","ecommerce signup status : failed."),
	ECOMM_LOGIN_EXCEPTION("ERR0802","Exception occurred while authenticating user for ecommerce login."),
	ECOMM_INVALID_TOKEN("ERR0803","Ecommerce admin token can not be null or empty.");
	
    private final String errCode;
    private final String errMsg;

    /**
     * @param text
     */
    private CustomExceptionCode(final String errCode,final String errMsg) {
        this.errCode = errCode;
        this.errMsg = errMsg;
    }

    /**
	 * Return a string representation of this status code.
	 */
	@Override
	public String toString() {
		return  errCode + ": " + errMsg;
	}

	/**
	 * Return the enum constant of this type with the specified numeric value.
	 * 
	 * @param statusCode
	 *            the numeric value of the enum to be returned
	 * @return the enum constant with the specified numeric value
	 * @throws IllegalArgumentException
	 *             if this enum has no constant for the specified numeric value
	 */

	public String getErrCode() {
		return errCode;
	}

	public String getErrMsg() {
		return errMsg;
	}
}