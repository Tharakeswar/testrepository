package com.zoylo.gateway.web.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zoylo.gateway.bll.AuthCategoryBll;
import com.zoylo.gateway.model.PageableVM;
import com.zoylo.gateway.model.ZoyloAuthCategoryVM;
import com.zoylo.gateway.mv.ZoyloAuthCategoryMV;
import com.zoylo.gateway.successcode.CustomResponse;
import com.zoylo.gateway.successcode.SuccessCode;

/**
 * 
 * @author Balram Sharma
 * @version 1.0
 * @description authorization category management
 *
 */

@RestController
@RequestMapping("/api/authcategory")
public class AuthCategoryRest {
	
	@Autowired
	private AuthCategoryBll authCategoryBll;
	@Autowired
	ObjectMapper objectMapper;
	private static final String serviceName = "serviceName";
	
	
	/**
	 * @author Balram Sharma
	 * @description To save AuthCategory
	 * @param zoyloAuthCategoryVM
	 * @return CustomResponse
	 */
	@PostMapping
	public ResponseEntity<CustomResponse> saveAuthCategory(@Valid @RequestBody ZoyloAuthCategoryVM zoyloAuthCategoryVM) {
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put(serviceName, SuccessCode.AUTHCATEGORY_SAVE.getServiceName());
		return new ResponseEntity<>(new CustomResponse(SuccessCode.AUTHCATEGORY_SAVE.getSuccessCode(),
				SuccessCode.AUTHCATEGORY_SAVE.getSuccessMessage(), authCategoryBll.saveAuthCategory(zoyloAuthCategoryVM),
				metaInfo), HttpStatus.OK);

	}
	/**
	 * @author Balram Sharma
	 * @description To Update AuthCategory
	 * @param zoyloAuthCategoryVM
	 * @return CustomResponse
	 */
	@PutMapping
	public ResponseEntity<CustomResponse> updateAuthCategory(@RequestBody ZoyloAuthCategoryVM zoyloAuthCategoryVM) {
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put(serviceName, SuccessCode.AUTHCATEGORY_UPDATE.getServiceName());
		return new ResponseEntity<>(new CustomResponse(SuccessCode.AUTHCATEGORY_UPDATE.getSuccessCode(),
				SuccessCode.AUTHCATEGORY_UPDATE.getSuccessMessage(),
				authCategoryBll.updateAuthCategory(zoyloAuthCategoryVM), metaInfo), HttpStatus.OK);

	}
	/**
	 * @author Balram Sharma
	 * @description filtering zoyloAuthCategory by using LanguageCode
	 * @param languageCode
	 * @return CustomResponse
	 */
	@GetMapping("/authcodes")
	public ResponseEntity<CustomResponse> findAllAuthCode(@RequestParam(defaultValue = "en",required = false) String languageCode) {
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put(serviceName, SuccessCode.PERMISSION_FOUND.getServiceName());
		return new ResponseEntity<>(new CustomResponse(SuccessCode.GET_AUTH_CODE_SUCCESSFULLY.getSuccessCode(),
				SuccessCode.GET_AUTH_CODE_SUCCESSFULLY.getSuccessMessage(), authCategoryBll.findAllAuthCode(languageCode),
				metaInfo), HttpStatus.OK);
	}
	
	/**
	 * @author Balram Sharma
	 * @description To getAuthCategory by id
	 * @param id
	 * @return CustomResponse
	 */
	@GetMapping("/{id}")
	public ResponseEntity<CustomResponse> getAuthCategoryById(@Valid @PathVariable("id") String id) {

		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put(serviceName, SuccessCode.AUTH_CATEGORY_FETCHED_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<>(new CustomResponse(SuccessCode.AUTH_CATEGORY_FETCHED_SUCCESSFULLY.getSuccessCode(),
				SuccessCode.AUTH_CATEGORY_FETCHED_SUCCESSFULLY.getSuccessMessage(),
				authCategoryBll.getAuthCategoryById(id), metaInfo), HttpStatus.OK);
	}
	
	/**
	 * @author Balram Sharma
	 * @description search ZoyloAuthCategory by using authCategoryCode & authCategoryName,
	 *              activeFlag.
	 * @param authCategoryCode
	 * @param authCategoryName
	 * @param activeFlag
	 * @param languageCode
	 * @return CustomResponse
	 */
	@GetMapping("/search")
	public ResponseEntity<CustomResponse> searchAuthCategory(Pageable pageable,
			@RequestParam(required = false) String authCategoryCode,
			@RequestParam(required = false) String authCategoryName, @RequestParam(required = true) boolean activeFlag,
			@RequestParam(required = false, defaultValue = "en") String languageCode) {

		PageableVM<ZoyloAuthCategoryMV> pageableVM = authCategoryBll.searchAuthCategory(pageable, authCategoryCode, authCategoryName,
				activeFlag, languageCode);
		List<ZoyloAuthCategoryMV> result = pageableVM.getContent();
		Map<String, Object> metaInfo = new HashMap<String, Object>();
		HashMap<String, String> props = objectMapper.convertValue(pageableVM.getPageInfo(), HashMap.class);
		metaInfo.putAll(props);
		metaInfo.put("serviceName", SuccessCode.AUTH_CATEGORY_FETCHED_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.AUTH_CATEGORY_FETCHED_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.AUTH_CATEGORY_FETCHED_SUCCESSFULLY.getSuccessMessage(), result, metaInfo),
				HttpStatus.OK);
	}


}
