package com.zoylo.gateway.web.rest.util;

/**
 * 
 * @author Ankur
 *
 */
public enum LookUp {

	DOCTOR("ZOYLOAUTHCATEGORY", "DOCTOR_CLINIC"), DIAGONSTIC("ZOYLOAUTHCATEGORY", "DIAGNOSTIC_CENTER"), RECIPIENT(
			"ZOYLOAUTHCATEGORY", "RECIPIENT"), ADMIN("ZOYLOAUTHCATEGORY", "ADMIN_APP"), ROLE_ZOYLO_ADMIN("ROLE",
					"ZOYLO_ADMIN"), ROLE_DOCTOR_CLINIC_ADMIN("ROLE",
							"DOCTOR_CLINIC_ADMIN"), ROLE_DIAGNOSTIC_CENTER_ADMIN("ROLE", "DIAGNOSTIC_CENTER_ADMIN"),
	WELLNESS_CENTER("ZOYLOAUTHCATEGORY", "WELLNESS_CENTER");
	private final String lookUpType;
	private final String lookUpValue;

	private LookUp(final String lookUpType, final String lookUpValue) {
		this.lookUpType = lookUpType;
		this.lookUpValue = lookUpValue;

	}

	public String getLookUpType() {
		return lookUpType;
	}

	public String getLookUpValue() {
		return lookUpValue;
	}

}
