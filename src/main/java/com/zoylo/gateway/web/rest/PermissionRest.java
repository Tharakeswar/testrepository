package com.zoylo.gateway.web.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zoylo.gateway.bll.PermissionBll;
import com.zoylo.gateway.model.PageableVM;
import com.zoylo.gateway.model.PermissionEditVM;
import com.zoylo.gateway.model.PermissionVM;
import com.zoylo.gateway.model.PermissionZoyloVM;
import com.zoylo.gateway.mv.PermissionMV;
import com.zoylo.gateway.successcode.CustomResponse;
import com.zoylo.gateway.successcode.SuccessCode;

/**
 * 
 * @author Diksha Gupta
 * @version 1.0
 * @description User permission management
 *
 */

@RestController
@RequestMapping("/api/permissions")
public class PermissionRest {
	@Autowired
	private PermissionBll permissionBll;
	private static final String serviceName = "serviceName";
	@Autowired
	ObjectMapper objectMapper;

	/**
	 * @author Diksha Gupta
	 * @description To save Permission
	 * @param permissionModel
	 * @return CustomResponse
	 */
	// @PreAuthorize("hasRole('permission_add')")
	@PostMapping
	public ResponseEntity<CustomResponse> savePermission(@Valid @RequestBody PermissionVM permissionModel,
			@RequestHeader("languageCode") String languageCode) {
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put(serviceName, SuccessCode.PERMISSION_SAVE.getServiceName());
		return new ResponseEntity<>(new CustomResponse(SuccessCode.PERMISSION_SAVE.getSuccessCode(),
				SuccessCode.PERMISSION_SAVE.getSuccessMessage(),
				permissionBll.savePermission(permissionModel, languageCode), metaInfo), HttpStatus.OK);

	}

	/**
	 * @author Diksha Gupta
	 * @description To get List of all permission
	 * @return CustomResponse
	 */
	// @PreAuthorize("hasRole('permission_view')")
	@GetMapping
	public ResponseEntity<CustomResponse> getAllPermission() {
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put(serviceName, SuccessCode.PERMISSION_FOUND.getServiceName());
		return new ResponseEntity<>(
				new CustomResponse(SuccessCode.PERMISSION_FOUND.getSuccessCode(),
						SuccessCode.PERMISSION_FOUND.getSuccessMessage(), permissionBll.getAllPermission(), metaInfo),
				HttpStatus.OK);

	}

	/**
	 * @author Diksha Gupta
	 * @description To get module on the basis of permissionCode
	 * @param permissionCode
	 * @return CustomResponse
	 */
	// @PreAuthorize("hasRole('permission_view')")
	@GetMapping("/{permissionCode}")
	public ResponseEntity<CustomResponse> getByPermissionCode(
			@Valid @NotNull @NotBlank @PathVariable("permissionCode") String permissionCode) {
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put(serviceName, SuccessCode.PERMISSION_FOUND.getServiceName());
		return new ResponseEntity<>(
				new CustomResponse(SuccessCode.PERMISSION_FOUND.getSuccessCode(),
						SuccessCode.PERMISSION_FOUND.getSuccessMessage(), permissionBll.get(permissionCode), metaInfo),
				HttpStatus.OK);
	}

	/**
	 * @author Diksha Gupta
	 * @description To update permission
	 * @param permissionModel
	 * @return CustomResponse
	 */
	// @PreAuthorize("hasRole('permission_edit')")
	@PutMapping()
	public ResponseEntity<CustomResponse> updatePermission(@Valid @RequestBody PermissionEditVM permissionModel,
			@RequestHeader("languageCode") String languageCode) {
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put(serviceName, SuccessCode.PERMISSION_UPDATE.getServiceName());
		return new ResponseEntity<>(new CustomResponse(SuccessCode.PERMISSION_UPDATE.getSuccessCode(),
				SuccessCode.PERMISSION_UPDATE.getSuccessMessage(),
				permissionBll.updatePermission(permissionModel, languageCode), metaInfo), HttpStatus.OK);

	}

	/**
	 * @author Diksha Gupta
	 * @description To delete permision
	 * @param id
	 * @return CustomResponse
	 */
	// @PreAuthorize("hasRole('permission_delete')")
	@DeleteMapping("/{id}")
	public ResponseEntity<CustomResponse> deletePermission(@Valid @NotNull @NotBlank @PathVariable("id") String id) {

		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put(serviceName, SuccessCode.PERMISSION_DELETE.getServiceName());
		return new ResponseEntity<>(new CustomResponse(SuccessCode.PERMISSION_UPDATE.getSuccessCode(),
				SuccessCode.PERMISSION_UPDATE.getSuccessMessage(), permissionBll.deletePermission(id), metaInfo),
				HttpStatus.OK);
	}

	/**
	 * @author Diksha Gupta
	 * @description To get module on the basis of permissionCode
	 * @param moduleCode
	 * @return CustomResponse
	 */
	// @PreAuthorize("hasRole('permission_view')")
	@GetMapping(params = "authcode")
	public ResponseEntity<CustomResponse> getPermissionByAuthCode(
			@RequestParam(name = "authcode",required=false) String authCode) {
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put(serviceName, SuccessCode.PERMISSION_FOUND.getServiceName());
		return new ResponseEntity<>(new CustomResponse(SuccessCode.PERMISSION_FOUND.getSuccessCode(),
				SuccessCode.PERMISSION_FOUND.getSuccessMessage(), permissionBll.getPermissionByAuthCode(authCode),
				metaInfo), HttpStatus.OK);
	}
	
	/**
	 * @author Balram Sharma
	 * @description To save Permission
	 * @param PermissionZoyloVM
	 * @return CustomResponse
	 */
	@PostMapping("/permission")
	public ResponseEntity<CustomResponse> savePermissionData(@Valid @RequestBody PermissionZoyloVM permissionZoyloVM) {
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put(serviceName, SuccessCode.PERMISSION_SAVE.getServiceName());
		return new ResponseEntity<>(new CustomResponse(SuccessCode.PERMISSION_SAVE.getSuccessCode(),
				SuccessCode.PERMISSION_SAVE.getSuccessMessage(), permissionBll.savePermissionData(permissionZoyloVM),
				metaInfo), HttpStatus.OK);

	}

	/**
	 * @author Balram Sharma
	 * @description To update existing Permission
	 * @param PermissionZoyloVM
	 * @return CustomResponse
	 */
	@PutMapping("/permission")
	public ResponseEntity<CustomResponse> updatePermissionData(@Valid @RequestBody PermissionZoyloVM permissionZoyloVM) {
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put(serviceName, SuccessCode.PERMISSION_UPDATE.getServiceName());
		return new ResponseEntity<>(new CustomResponse(SuccessCode.PERMISSION_UPDATE.getSuccessCode(),
				SuccessCode.PERMISSION_UPDATE.getSuccessMessage(),
				permissionBll.updatePermissionData(permissionZoyloVM), metaInfo), HttpStatus.OK);

	}

	/**
	 * @author Balram Sharma
	 * @description search Permission by using permissionCode & permissionName,
	 *              activeFlag, authCategoryCode
	 * @param permissionCode
	 * @param permissionName
	 * @param activeFlag
	 * @param authCategory
	 * @param languageCode
	 * @return CustomResponse
	 */
	@GetMapping("/searchpermission")
	public ResponseEntity<CustomResponse> searchPermissionProperties(Pageable pageable,
			@RequestParam(required = false) String permissionCode,
			@RequestParam(required = false) String permissionName, @RequestParam(required = true) boolean activeFlag,
			@RequestParam(required = false) String authCategoryCode,
			@RequestParam(required = false, defaultValue = "en") String languageCode) {

		PageableVM<PermissionMV> pageableVM = permissionBll.searchPermissionProperties(pageable, permissionCode, permissionName,
				activeFlag, authCategoryCode, languageCode);
		List<PermissionMV> result = pageableVM.getContent();
		Map<String, Object> metaInfo = new HashMap<String, Object>();
		HashMap<String, String> props = objectMapper.convertValue(pageableVM.getPageInfo(), HashMap.class);
		metaInfo.putAll(props);
		metaInfo.put("serviceName", SuccessCode.PERMISSION_FETCHED_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.PERMISSION_FETCHED_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.PERMISSION_FETCHED_SUCCESSFULLY.getSuccessMessage(), result, metaInfo),
				HttpStatus.OK);
	}
	/**
	 * @author Balram Sharma
	 * @description To get Permission by id
	 * @param id
	 * @return CustomResponse
	 */
	@GetMapping("/id/{permissionId}")
	public ResponseEntity<CustomResponse> getPermissionById(@Valid @PathVariable("permissionId") String permissionId) {

		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put(serviceName, SuccessCode.PERMISSION_FETCHED_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<>(new CustomResponse(SuccessCode.PERMISSION_FETCHED_SUCCESSFULLY.getSuccessCode(),
				SuccessCode.PERMISSION_FETCHED_SUCCESSFULLY.getSuccessMessage(),
				permissionBll.getPermissionById(permissionId), metaInfo), HttpStatus.OK);
	}


}
