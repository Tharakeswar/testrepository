package com.zoylo.gateway.web.rest.util;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import com.zoylo.gateway.model.PageInfoVM;

public class PageableUtil<T> {

	public static <T> Page<T> getPageAbleList(Pageable pageable, Object pageObject, List<T> queryResults) {
		PagedListHolder<T> holder = new PagedListHolder<>(queryResults);
		holder.setPage(pageable.getPageNumber());
		holder.setPageSize(pageable.getPageSize());

		Page<T> pages = null;
		if (holder.getPageCount() <= pageable.getPageNumber()) {
			pages = new PageImpl<>(new ArrayList<>(), pageable, queryResults.size());
		} else {
			pages = new PageImpl<>(holder.getPageList(), pageable, queryResults.size());
		}
		return pages;
	}
	
	public static PageInfoVM getPageMetaInfo(Pageable pageable, List<?> queryResults, Long totalRecord) {
		PageInfoVM infoVM = new PageInfoVM();
		infoVM.setTotalElements(totalRecord + "");
		infoVM.setSize(pageable.getPageSize() + "");
		if (totalRecord % pageable.getPageSize() == 0) {
			infoVM.setTotalPages(totalRecord / pageable.getPageSize() + "");
		} else {
			infoVM.setTotalPages((totalRecord / pageable.getPageSize() + 1) + "");
		}
		infoVM.setNumberOfElements(queryResults.size() + "");
		infoVM.setNumber((pageable.getPageNumber())+"");
		return infoVM;
	}
}
