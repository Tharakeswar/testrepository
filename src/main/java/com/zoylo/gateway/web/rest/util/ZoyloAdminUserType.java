package com.zoylo.gateway.web.rest.util;

public enum ZoyloAdminUserType {
	ADMIN("USER001","admin","Admin"),
	SUB_ADMIN("USER002","subadmin","Sub Admin");
	private final String userCode;
    private final String userType;
    private final String name;
    
    private ZoyloAdminUserType(final String userCode,final String userType,String name) {
    	this.userCode = userCode;
    	this.userType = userType;
    	this.name = name;
    }
    public String getUserCode() {
		return userCode;
	}
    
    public String getUserType() {
		return userType;
	}
    public String getName() {
		return name;
	}
}
