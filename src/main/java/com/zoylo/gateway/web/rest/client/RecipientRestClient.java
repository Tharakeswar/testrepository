package com.zoylo.gateway.web.rest.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.zoylo.gateway.domain.UserProfile;
import com.zoylo.gateway.model.UserProfileVM;
import com.zoylo.gateway.model.ZoyloUserUpdateVM;

@FeignClient(name = "zoylorecipient")
public interface RecipientRestClient {
	// zoylorecipient-0.0.1-SNAPSHOT/
	@RequestMapping(method = RequestMethod.POST, value = "zoylorecipient-0.0.1-SNAPSHOT/api/user-profile")
	public ResponseEntity<String> setUserProfile(@RequestBody UserProfileVM userProfileVM);

	@RequestMapping(method = RequestMethod.GET, value = "zoylorecipient-0.0.1-SNAPSHOT/api/user-profile/permissions", params = "userid")
	public ResponseEntity<String> getUserPermission(@RequestParam(name = "userid") String userId);

	@RequestMapping(method = RequestMethod.POST, value = "zoylorecipient-0.0.1-SNAPSHOT/api/user-profile/createUserProfile")
	public ResponseEntity<String> createUserProfile(@RequestBody UserProfile UserProfile);

	@RequestMapping(method = RequestMethod.PUT, value = "zoylorecipient-0.0.1-SNAPSHOT/api/user-profile/updateUserProfile")
	public ResponseEntity<String> updateUserProfile(@RequestBody ZoyloUserUpdateVM zoyloUserUpdateVM);
}
