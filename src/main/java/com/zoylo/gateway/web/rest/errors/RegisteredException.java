package com.zoylo.gateway.web.rest.errors;

/**
 * The enum which contains all the registered exceptions 
 * 
 * @author Diksha Gupta
 *
 */
public enum RegisteredException {

	LOGIN_EXCEPTION("LoginException",""),
	REGISTRATION_EXCEPTION("RegistrationException",""),
	ROLE_EXCEPTION("RoleException",""),
	PERMISSION_EXCEPTION("PermissionException",""),
	CONSTRAINT_VIOLATION_EXCEPTION("ConstraintViolationException",""),
	
	USER_PROFILE_EXCEPTION("UserProfileException",""),
	MODULE_EXCEPTION("ModuleException",""),
	UNKNOWN_EXCEPTION("Unknown Exception",""),
	METHOD_NOT_ALLOWED("HttpRequestMethodNotSupportedException",""),
	MULTIPART_EXCEPTION("MultipartException",""),
	EMAIL_VERIFICATION_EXCEPTION("EMail Verification Exception",""),
	ROLES_DELETION_ERROR("Role Deletion exception", "Role Deletion exception"),
	UNAUTHORIZED_EXCEPTION("Unauthorized exception",""),
	FILE_UPLOAD_EXCEPTION("File Upload exception",""),
	USER_FETCH_EXCEPTION("User Fetch Exception","Exception occured while fetching User"),
	FILE_DOWNLOAD_EXCEPTION("File Download exception",""),
	INVALID_TOKEN_EXCEPTION("Token is invalid","Exception occured while validating token"),
	DATA_SAVE_EXCEPTION("Data save exception","Exception occurred while saving data"),
	RESPONSE_PARSER_EXCEPTION("JsonParsingException", "Json Parsing Exception Occurred"),
	SWITCH_PROFILE_EXCEPTION("SwitchProfileException", "Switch Profile Exception Occurred"),
	USER_AUTH_EXCEPTION("UserAuthException", "User auth Exception Occurred"),
	AUTHCATEGORY_EXCEPTION("AuthCategoryException","Auth Category Exception Occurred"),
	USER_CREATION_EXCEPTION("UserCreationException", "Exception Occured while creating new user"),
	USER_ROLE_ASSIGN_EXCEPTION("UserRoleAssignException", "Exception occured while assigning role to user"),
	ZOYLO_ID_MANDATORY("RoleZoyloIdException", "Mandatory zoylo id not provided while assigning role"),
	USER_ID_MANDATORY("RoleUserIdException", "Mandatory user id not provided while assigning role"),
	EMAIL_ID_MANDATORY("RoleEmailIdException", "Mandatory email id not provided while assigning role"),
	PHONE_NUMBER_MANDATORY("RolePhoneNumberException", "Mandatory phone number not provided while assigning role"),
	ROLE_CODE_MANDATORY("RoleUserIdException", "Mandatory role code not provided for assigning role"),
	ROLE_CODE_MAPPED_ALREADY("RoleUserIdException", "Role is already mapped for the user"),
	USER_SEARCH_EXCEPTION("UserSearchException", "UserSearchException"),
	IP_ADDRESS_PERMISSION_ACCESS_REQUEST("IpAddressPermissionAccessRequest", "API Access denied for this machine"),
	LOOKUP_EXCEPTION("LookupException", "Lookup Exception Occurred"), DOCTOR_DETAILS_EXCEPTION("DoctorDetailsException",
			"Doctor Details Exception Occurred"), 
	EMAIL_TEMPLATE_EXCEPTION("Email Template Exception","Email template exception"),	
	ECOMM_SIGNUP_EXCEPTION("EcommerceSignupException","Exception occured while ecommerce signup"),
	ECOMM_LOGIN_EXCEPTION("EcommerceLoginException","Exception occurred while authenticating user for ecommerce login");

	private final String exception;
	private final String exceptionMessage;
	
	private RegisteredException(final String exception, String exceptionMessage) {
		this.exception = exception;
		this.exceptionMessage = exceptionMessage;
	}
		
	public String getException() {
		return exception;
	}
	
	public String getExceptionMessage() {
		return exceptionMessage;
	}
	
}
