package com.zoylo.gateway.web.rest.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zoylo.gateway.service.util.CustomResponseModel;
import com.zoylo.gateway.successcode.CustomResponse;
import com.zoylo.gateway.web.rest.errors.CustomExceptionCode;
import com.zoylo.gateway.web.rest.errors.CustomParameterizedException;
import com.zoylo.gateway.web.rest.errors.RegisteredException;

/***
 * @version 1.0
 * @author arvindrawat
 */
public class CustomJsonParser<T> {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	/***
	 * @author arvindrawat
	 * @description To parse the custom json response
	 * @param jsonObj
	 * @return CustomResponse
	 */
	public CustomResponse parseJsonResponse(ResponseEntity<String> jsonObj) {
		try {
			String body = jsonObj.getBody();
			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
			CustomResponseModel CustomResponseModel = mapper.readValue(body, CustomResponseModel.class);
			CustomResponse customResponse = new CustomResponse(CustomResponseModel.getInfoCode(),
					CustomResponseModel.getInfoMsg(), CustomResponseModel.getData(), CustomResponseModel.getMetaInfo());
			return customResponse;
		} catch (Exception exception) {
			logger.error("Exception occor while parsing Response json{} ", exception.getMessage());
			throw new CustomParameterizedException(RegisteredException.RESPONSE_PARSER_EXCEPTION.getException(),
					CustomExceptionCode.RESPONSE_PARSER_ERROR.getErrCode(), exception,
					CustomExceptionCode.RESPONSE_PARSER_ERROR.getErrMsg());
		}
	}

	/***
	 * @author arvind.rawat
	 * @description It received data value from json object in jsonObj and return
	 *              generic object
	 * @param jsonObj
	 * @param destination
	 * @return T
	 */
	public T parseResponseData(String jsonObj, Class<T> destination) {
		logger.info("In parseStateDataData method to parse data");
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
			T genericResponse = mapper.readValue(jsonObj, destination);
			return genericResponse;
		} catch (Exception exception) {
			logger.error("Exception occor while parsing Response data {}", exception.getMessage());
			throw new CustomParameterizedException(RegisteredException.RESPONSE_PARSER_EXCEPTION.getException(),
					CustomExceptionCode.RESPONSE_PARSER_ERROR.getErrCode(), exception,
					CustomExceptionCode.RESPONSE_PARSER_ERROR.getErrMsg());
		}
	}

}
