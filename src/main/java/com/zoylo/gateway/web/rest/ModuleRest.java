package com.zoylo.gateway.web.rest;

import java.util.HashMap;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zoylo.gateway.bll.ModuleBll;
import com.zoylo.gateway.model.ModuleEditVM;
import com.zoylo.gateway.model.ModuleVM;
import com.zoylo.gateway.successcode.CustomResponse;
import com.zoylo.gateway.successcode.SuccessCode;

/**
 * @author damini.arora
 * @version 1.0
 *
 */
@RestController
@RequestMapping("/api/modules")
public class ModuleRest {

	@Autowired
	private ModuleBll moduleBll;

	/**
	 * @author damini.arora
	 * @description To save Module
	 * @param moduleModel
	 * @return Custom Response
	 */
	@PostMapping
	public ResponseEntity<CustomResponse> save(
			@Valid @RequestHeader(value = "lang", defaultValue = "en") String languageCode,
			@RequestBody ModuleVM moduleModel) {
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.MODULE_SAVE.getServiceName());
		return new ResponseEntity<CustomResponse>(new CustomResponse(SuccessCode.MODULE_SAVE.getSuccessCode(),
				SuccessCode.MODULE_SAVE.getSuccessMessage(), moduleBll.saveModule(moduleModel, languageCode), metaInfo),
				HttpStatus.OK);
	}

	/**
	 * @author damini arora
	 * @description To get List of all modules
	 * @return Custom Response
	 */
	@GetMapping
	public ResponseEntity<CustomResponse> getAll() {
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.FOUND_ALL_MODULE.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.FOUND_ALL_MODULE.getSuccessCode(),
						SuccessCode.FOUND_ALL_MODULE.getSuccessMessage(), moduleBll.getAllModule(), metaInfo),
				HttpStatus.OK);
	}

	/**
	 * @author damini arora
	 * @description To get module on the ba-+sis of moduleCode
	 * @param moduleCode
	 * @return Custom Response
	 */
	@GetMapping("/{moduleCode}")
	public ResponseEntity<CustomResponse> getByCode(
			@Valid @NotNull @NotBlank @PathVariable("moduleCode") String moduleCode) {
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.FOUND_MODULE.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.FOUND_MODULE.getSuccessCode(),
						SuccessCode.FOUND_MODULE.getSuccessMessage(), moduleBll.getByModuleCode(moduleCode), metaInfo),
				HttpStatus.OK);

	}

	/**
	 * @author damini arora
	 * @description To update module
	 * @return Custom Response
	 */
	@PutMapping
	public ResponseEntity<CustomResponse> update(@Valid @RequestBody ModuleEditVM moduleModel) {
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.MODULE_UPDATE.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.MODULE_UPDATE.getSuccessCode(),
						SuccessCode.MODULE_UPDATE.getSuccessMessage(), moduleBll.updateModule(moduleModel), metaInfo),
				HttpStatus.OK);

	}

	/**
	 * @author damini arora
	 * @description To delete module
	 * @param id
	 * @return Custom Response
	 */
	@DeleteMapping("/{id}")
	public ResponseEntity<CustomResponse> delete(@Valid @NotNull @NotBlank @PathVariable("id") String id) {
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.MODULE_DELETE.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.MODULE_DELETE.getSuccessCode(),
						SuccessCode.MODULE_DELETE.getSuccessMessage(), moduleBll.deleteById(id), metaInfo),
				HttpStatus.OK);
	}

}
