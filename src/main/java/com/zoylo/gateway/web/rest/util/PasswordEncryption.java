package com.zoylo.gateway.web.rest.util;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * 
 * 
 * @author Ankur Goel
 * @description SHA256 password encryption
 *
 */
public final class PasswordEncryption {

	private PasswordEncryption() {

	}

	public static String getEncryptedPassword(String password) {
		password = DigestUtils.sha256Hex(password);
		return password;
	}
}
