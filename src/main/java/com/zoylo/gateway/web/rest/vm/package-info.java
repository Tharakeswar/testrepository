/**
 * View Models used by Spring MVC REST controllers.
 */
package com.zoylo.gateway.web.rest.vm;
