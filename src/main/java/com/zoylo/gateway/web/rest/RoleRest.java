package com.zoylo.gateway.web.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zoylo.gateway.bll.RoleBll;
import com.zoylo.gateway.model.PageableVM;
import com.zoylo.gateway.model.RoleEditVM;
import com.zoylo.gateway.model.RoleVM;
import com.zoylo.gateway.model.ZoyloPermissionVM;
import com.zoylo.gateway.model.ZoyloRoleVM;
import com.zoylo.gateway.mv.RoleMV;
import com.zoylo.gateway.successcode.CustomResponse;
import com.zoylo.gateway.successcode.SuccessCode;

/**
 * 
 * @author Diksha Gupta
 * @version 1.0
 * @description Class for User role management
 *
 */
@RestController
@RequestMapping("/api/roles")
public class RoleRest {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private RoleBll roleBll;
	private static final String serviceName = "serviceName";

	/**
	 * @author Diksha Gupta
	 * @description To get all the defined roles.
	 * @param
	 * @return CustomResponse
	 */
	//@PreAuthorize("hasRole('DOC_MY_PROFILE_VIEW')")
	@GetMapping
	public ResponseEntity<CustomResponse> getAllRole() {
		logger.info("Info:-----------Getting all roles--------");
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put(serviceName, SuccessCode.FETCHED_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<>(
				new CustomResponse(SuccessCode.FETCHED_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.FETCHED_SUCCESSFULLY.getSuccessMessage(), roleBll.getall(), metaInfo),
				HttpStatus.OK);
	}

	/**
	 * @author Diksha Gupta
	 * @description To get role by given role code.
	 * @param roleCode
	 * @return CustomResponse
	 */
    // @PreAuthorize("hasRole('role_view')")
	@GetMapping("/code")
	public ResponseEntity<CustomResponse> getRoleByCode(
			@RequestParam(name="rolecode",required = false) String rolecode, @RequestParam(name = "authcategorycode", required = false) String authcategorycode) {
		logger.info("Info:-----------Getting a role by rolecode:  {}");
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put(serviceName, SuccessCode.FETCHED_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<>(
				new CustomResponse(SuccessCode.FETCHED_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.FETCHED_SUCCESSFULLY.getSuccessMessage(), roleBll.getRole(rolecode, authcategorycode), metaInfo),
				HttpStatus.OK);
	}

	/**
	 * @author Diksha Gupta
	 * @description To save role for given data
	 * @param roleModel
	 * @return CustomResponse
	 */
	// @PreAuthorize("hasRole('role_add')")
	@PostMapping()
	public ResponseEntity<CustomResponse> save(@Valid @RequestBody RoleVM roleModel,
			@RequestHeader("languageCode") String languageCode) {
		logger.info("Info:-----------Create a role--------");
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put(serviceName, SuccessCode.SAVED_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<>(new CustomResponse(SuccessCode.SAVED_SUCCESSFULLY.getSuccessCode(),
				SuccessCode.SAVED_SUCCESSFULLY.getSuccessMessage(), roleBll.save(roleModel, languageCode), metaInfo),
				HttpStatus.OK);
	}

	/**
	 * @author Diksha Gupta
	 * @description To update a role for given data
	 * @param roleModel
	 * @return CustomResponse
	 */
	// @PreAuthorize("hasRole('role_edit')")
	@PutMapping()
	public ResponseEntity<CustomResponse> update(@Valid @RequestBody RoleEditVM roleEditModel,
			@RequestHeader("languageCode") String languageCode) {
		logger.info("Info:-----------Update a role for Id: {} " , roleEditModel.getId() );
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put(serviceName, SuccessCode.UPDATED_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<>(new CustomResponse(SuccessCode.UPDATED_SUCCESSFULLY.getSuccessCode(),
				SuccessCode.UPDATED_SUCCESSFULLY.getSuccessMessage(), roleBll.update(roleEditModel, languageCode),
				metaInfo), HttpStatus.OK);
	}

	/**
	 * @author Diksha Gupta
	 * @description To delete a role for given data
	 * @param roleId
	 * @return CustomResponse
	 */
	// @PreAuthorize("hasRole('role_delete')")
	@DeleteMapping("/{roleId}")
	public ResponseEntity<CustomResponse> delete(@Valid @NotNull @NotBlank @PathVariable("roleId") String roleId) {
		logger.info("Info:-----------Delete a role for Id: {}" , roleId );
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put(serviceName, SuccessCode.DELETED_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<>(
				new CustomResponse(SuccessCode.DELETED_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.DELETED_SUCCESSFULLY.getSuccessMessage(), roleBll.delete(roleId), metaInfo),
				HttpStatus.OK);
	}
	
	/**
	 * @author Ramesh
	 * @description To save role for given data
	 * @param roleModel
	 * @return CustomResponse
	 */
	// @PreAuthorize("hasRole('role_add')")
	@PostMapping("/role")
	public ResponseEntity<CustomResponse> saveRole(@Valid @RequestBody ZoyloRoleVM roleModel,
			@RequestHeader("languageCode") String languageCode) {
		logger.info("Info:-----------Create a role--------");
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put(serviceName, SuccessCode.SAVED_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<>(new CustomResponse(SuccessCode.SAVED_SUCCESSFULLY.getSuccessCode(),
				SuccessCode.SAVED_SUCCESSFULLY.getSuccessMessage(), roleBll.saveRole(roleModel, languageCode),
				metaInfo), HttpStatus.OK);
	}
	
	/**
	 * @author Ramesh
	 * @description To save role for given data
	 * @param roleModel
	 * @return CustomResponse
	 */
	// @PreAuthorize("hasRole('role_add')")
	@PostMapping("/permission")
	public ResponseEntity<CustomResponse> savePermission(@Valid @RequestBody ZoyloPermissionVM permissionModel,
			@RequestHeader("languageCode") String languageCode) {
		logger.info("Info:-----------Create a role--------");
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put(serviceName, SuccessCode.SAVED_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<>(new CustomResponse(SuccessCode.SAVED_SUCCESSFULLY.getSuccessCode(),
				SuccessCode.SAVED_SUCCESSFULLY.getSuccessMessage(),
				roleBll.assignPermission(permissionModel, languageCode), metaInfo), HttpStatus.OK);
	}
	/**
	 * @author Ramesh
	 * @param pageable
	 * @param languageCode
	 * @param roleCode
	 * @param roleName
	 * @param authCategoryCode
	 * @param authCategoryName
	 * @param activeFlag
	 * @return
	 */
	@GetMapping("/searchroles")
	public ResponseEntity<CustomResponse> getAllRoles(Pageable pageable,
			@RequestParam(value = "roleCode", required = false) String roleCode,
			@RequestParam(value = "roleName", required = false) String roleName,
			@RequestParam(value = "authCategoryCode", required = false) String authCategoryCode,
			@RequestParam(value = "authCategoryName", required = false) String authCategoryName,
			@RequestParam(value = "activeFlag", required = false) boolean activeFlag,
			
			@RequestParam(value = "languageCode", defaultValue="en",required = false) String languageCode) {
		Map<String, Object> metaInfo = new HashMap<String, Object>();
		PageableVM<RoleMV> pageableVM = roleBll.searchRoles(pageable, roleCode, roleName,
				authCategoryCode, authCategoryName, activeFlag,languageCode);
		List<RoleMV> result = pageableVM.getContent();
		ObjectMapper mapper = new ObjectMapper();
		HashMap<String, String> maps = mapper.convertValue(pageableVM.getPageInfo(), HashMap.class);
		metaInfo.putAll(maps);
		metaInfo.put(serviceName, SuccessCode.FETCHED_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(new CustomResponse(SuccessCode.FETCHED_SUCCESSFULLY.getSuccessCode(),
				SuccessCode.FETCHED_SUCCESSFULLY.getSuccessMessage(), result, metaInfo), HttpStatus.OK);

	}
	/**
	 * @author Ramesh
	 * @description To update a role for given data
	 * @param roleModel
	 * @return CustomResponse
	 */
	// @PreAuthorize("hasRole('role_edit')")
	@PutMapping("/role")
	public ResponseEntity<CustomResponse> updateRole(@Valid @RequestBody ZoyloRoleVM roleModel,
			@RequestHeader("languageCode") String languageCode) {
		logger.info("Info:-----------Update a role for Code: {} ", roleModel.getRoleCode());
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put(serviceName, SuccessCode.UPDATED_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(new CustomResponse(SuccessCode.UPDATED_SUCCESSFULLY.getSuccessCode(),
				SuccessCode.UPDATED_SUCCESSFULLY.getSuccessMessage(), roleBll.updateRole(roleModel, languageCode),
				metaInfo), HttpStatus.OK);
	}
	
	/**
	 * @author Balram Sharma
	 * @description To update permission for given role.
	 * @param roleModel
	 * @return CustomResponse
	 */
	@PutMapping("/rolepermission")
	public ResponseEntity<CustomResponse> updateRolePermission(@Valid @RequestBody ZoyloPermissionVM zoyloPermissionVM) {
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put(serviceName, SuccessCode.UPDATED_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<>(new CustomResponse(SuccessCode.UPDATED_SUCCESSFULLY.getSuccessCode(),
				SuccessCode.UPDATED_SUCCESSFULLY.getSuccessMessage(), roleBll.updateRolePermission(zoyloPermissionVM),
				metaInfo), HttpStatus.OK);
		
	}
	
	/**
	 * @author Balram Sharma
	 * @description To get Role by id
	 * @param RoleMV
	 * @return CustomResponse
	 */
	@GetMapping("/id/{roleId}")
	public ResponseEntity<CustomResponse> getRoleById(@Valid @PathVariable("roleId") String roleId) {

		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put(serviceName, SuccessCode.FETCHED_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<>(new CustomResponse(SuccessCode.FETCHED_SUCCESSFULLY.getSuccessCode(),
				SuccessCode.FETCHED_SUCCESSFULLY.getSuccessMessage(),
				roleBll.getRoleById(roleId), metaInfo), HttpStatus.OK);
	}

}
