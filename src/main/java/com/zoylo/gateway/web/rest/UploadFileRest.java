package com.zoylo.gateway.web.rest;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.zoylo.core.enums.FileType;
import com.zoylo.core.filetransferservice.FileTransferService;
import com.zoylo.core.filetransferservice.model.DownloadFileVM;
import com.zoylo.core.filetransferservice.model.UploadFileVM;
import com.zoylo.core.utils.ObjectsUtil;
import com.zoylo.core.utils.StringsUtil;
import com.zoylo.gateway.successcode.CustomResponse;
import com.zoylo.gateway.successcode.SuccessCode;
import com.zoylo.gateway.web.rest.errors.CustomExceptionCode;
import com.zoylo.gateway.web.rest.errors.CustomParameterizedException;
import com.zoylo.gateway.web.rest.errors.RegisteredException;

/**
 * Rest APIs for upload in Zoylo Gateway
 * 
 * @author sohan.maurya
 * 
 */

@RestController
@RequestMapping("/api")
public class UploadFileRest {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private FileTransferService UploadFileService;
	
	private final long fileMinSize = 10240; // Set file size to 10KB.
	private final long fileMaxSize = 10485760; // Set file size to 10MB.
	private final long fileReportMaxSize = 20971520; // Set file size to 20MB.
	
	@PostMapping("/upload")
	public ResponseEntity<CustomResponse> uploadFile(@RequestParam(name="file", required=true) MultipartFile file, @RequestParam(name="id", required=true) String id,
    		@RequestParam(name="filetype", required=true) String fileType) {
		FileType uploadType = FileType.getFileTypeByCode(fileType);
		if(ObjectsUtil.isNull(file) || StringsUtil.isNullOrEmpty(id) || ObjectsUtil.isNull(uploadType)){
			throw new CustomParameterizedException(RegisteredException.FILE_UPLOAD_EXCEPTION.getException(),
					CustomExceptionCode.FILE_UPLOAD_FAIL_EXCEPTION.getErrMsg(),
					CustomExceptionCode.FILE_UPLOAD_FAIL_EXCEPTION.getErrCode());
		}
		log.info("{}",file.getSize());
		if(file.getSize() < fileMinSize && FileType.SIGNATURES != uploadType){ // Signatures can have file size less than 10KB.
			throw new CustomParameterizedException(RegisteredException.FILE_UPLOAD_EXCEPTION.getException(),
					CustomExceptionCode.UPLOAD_FILE_MIN_SIZE_EXCEPTION.getErrMsg(),
					CustomExceptionCode.UPLOAD_FILE_MIN_SIZE_EXCEPTION.getErrCode());
		}if(FileType.IMAGES == uploadType){
			if(file.getSize() > fileMaxSize){
				throw new CustomParameterizedException(RegisteredException.FILE_UPLOAD_EXCEPTION.getException(),
						CustomExceptionCode.UPLOAD_FILE_MAX_SIZE_EXCEPTION.getErrMsg(),
						CustomExceptionCode.UPLOAD_FILE_MAX_SIZE_EXCEPTION.getErrCode());
			}
		}else{
			if(file.getSize() > fileReportMaxSize){
				throw new CustomParameterizedException(RegisteredException.FILE_UPLOAD_EXCEPTION.getException(),
						CustomExceptionCode.UPLOAD_FILE_REPORT_MAX_SIZE_EXCEPTION.getErrMsg(),
						CustomExceptionCode.UPLOAD_FILE_REPORT_MAX_SIZE_EXCEPTION.getErrCode());
			}
		}
		String uploadedPath = null;
		try {
			uploadedPath = UploadFileService.uploadFile(file, id, FileType.getFileTypeByCode(fileType));
			log.debug("Doc upload completed, Location: {}" , uploadedPath);
		} catch (Exception exception) {
			log.error("Exception in uploadMultipleFileHandler : {} " , exception.getMessage());
			log.debug("{}" , exception);
			throw new CustomParameterizedException(RegisteredException.FILE_UPLOAD_EXCEPTION.getException(),
					CustomExceptionCode.FILE_UPLOAD_FAIL_EXCEPTION.getErrMsg(),exception,
					CustomExceptionCode.FILE_UPLOAD_FAIL_EXCEPTION.getErrCode());
		}
		UploadFileVM data = new UploadFileVM();
		data.setId(id);
		data.setUploadFilePath(uploadedPath);
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.FILE_UPLOAD_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.FILE_UPLOAD_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.FILE_UPLOAD_SUCCESSFULLY.getSuccessMessage(), data, metaInfo),
				HttpStatus.OK);
	}
	
	@GetMapping("/download-files")
	public ResponseEntity<CustomResponse> downloadAllFiles(@RequestParam(value = "id", required = true) String id,
			@RequestParam(value = "filetype", required = true) String fileType) {
		if (StringsUtil.isNullOrEmpty(id) || ObjectsUtil.isNull(FileType.getFileTypeByCode(fileType))) {
			throw new CustomParameterizedException(RegisteredException.FILE_DOWNLOAD_EXCEPTION.getException(),
					CustomExceptionCode.FILE_DOWNLOAD_FAIL_EXCEPTION.getErrMsg(),
					CustomExceptionCode.FILE_DOWNLOAD_FAIL_EXCEPTION.getErrCode());
		}
		DownloadFileVM downloadFileVM = null;
		try {
			downloadFileVM = UploadFileService.downloadAllFilesById(id, FileType.getFileTypeByCode(fileType));
			if(downloadFileVM.getDownloadUrls().size()==0){
				throw new CustomParameterizedException(RegisteredException.FILE_DOWNLOAD_EXCEPTION.getException(),
						CustomExceptionCode.NO_DOWNLOAD_URLS_FOUND.getErrMsg(),
						CustomExceptionCode.NO_DOWNLOAD_URLS_FOUND.getErrCode());
			}
		} catch (AmazonServiceException exception) {
			throw new CustomParameterizedException(RegisteredException.FILE_DOWNLOAD_EXCEPTION.getException(),
					CustomExceptionCode.FILE_DOWNLOAD_FAIL_EXCEPTION.getErrMsg(),exception,
					CustomExceptionCode.FILE_DOWNLOAD_FAIL_EXCEPTION.getErrCode());
		} catch (AmazonClientException exception) {
			throw new CustomParameterizedException(RegisteredException.FILE_DOWNLOAD_EXCEPTION.getException(),
					CustomExceptionCode.FILE_DOWNLOAD_FAIL_EXCEPTION.getErrMsg(),exception,
					CustomExceptionCode.FILE_DOWNLOAD_FAIL_EXCEPTION.getErrCode());
		} catch (InterruptedException exception) {
			throw new CustomParameterizedException(RegisteredException.FILE_DOWNLOAD_EXCEPTION.getException(),
					CustomExceptionCode.FILE_DOWNLOAD_FAIL_EXCEPTION.getErrMsg(),exception,
					CustomExceptionCode.FILE_DOWNLOAD_FAIL_EXCEPTION.getErrCode());
		}

		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.FILES_DOWNLOAD_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.FILES_DOWNLOAD_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.FILES_DOWNLOAD_SUCCESSFULLY.getSuccessMessage(), downloadFileVM, metaInfo),
				HttpStatus.OK);
	}
}
