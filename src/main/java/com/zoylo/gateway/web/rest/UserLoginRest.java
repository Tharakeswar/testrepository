package com.zoylo.gateway.web.rest;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.twilio.Twilio;
import com.twilio.jwt.accesstoken.AccessToken;
import com.twilio.jwt.accesstoken.ChatGrant;
import com.zoylo.core.utils.StringsUtil;
import com.zoylo.gateway.bll.UserBll;
import com.zoylo.gateway.domain.User;
import com.zoylo.gateway.domain.ZoyloUserToken;
import com.zoylo.gateway.model.RecipientVM;
import com.zoylo.gateway.model.WalletVM;
import com.zoylo.gateway.mv.TokenVM;
import com.zoylo.gateway.repository.UserRepository;
import com.zoylo.gateway.repository.ZoyloUserTokenRepository;
import com.zoylo.gateway.security.CustomUserDetails;
import com.zoylo.gateway.security.DomainUserDetailsService;
import com.zoylo.gateway.security.jwt.JWTConfigurer;
import com.zoylo.gateway.security.jwt.TokenProvider;
import com.zoylo.gateway.service.util.Constants;
import com.zoylo.gateway.successcode.CustomResponse;
import com.zoylo.gateway.successcode.SuccessCode;
import com.zoylo.gateway.web.rest.client.AdminRestClient;
import com.zoylo.gateway.web.rest.errors.CustomExceptionCode;
import com.zoylo.gateway.web.rest.errors.CustomParameterizedException;
import com.zoylo.gateway.web.rest.errors.RegisteredException;
import com.zoylo.gateway.web.rest.util.PasswordEncryption;
import com.zoylo.gateway.web.rest.vm.LoginVM;



/**
 * Controller to authenticate users.
 */
@RestController
@RequestMapping("/api")
public class UserLoginRest {

	private final Logger log = LoggerFactory.getLogger(UserLoginRest.class);

	private final TokenProvider tokenProvider;
	@Autowired
	private DomainUserDetailsService domainUserDetailsService;
	@Autowired
	private  UserRepository userRepository;
	@Autowired
	private RestTemplate restTemplate;
	@Autowired
	private UserBll userBll;
	
	@Value("${app.twilioAccountSid}")
	private String twilioAccountSid;
	@Value("${app.twilioApiKey}")
	private String twilioApiKey;
	@Value("${app.twilioApiSecret}")
	private String twilioApiSecret;
	@Value("${app.twilioserviceSid}")
	private String serviceSid;
	@Value("${app.twilioauthenticateToken}")
	private String authenticateToken;
	@Value("${app.twiliottl}")
	private int twilioTtl;
	@Value("${app.ecomm.login.url}")
	private String ecommLoginUrl;
	@Value("${app.ecomm.enabled}")
	private Boolean ecommEnabled;
	
	private final String USER_NAME="username";
	private final String ECOMM_LOGIN_ID="ECOMM_LOGIN_ID";
	private final String ECOMM_LOGIN_TOKEN="ECOMM_LOGIN_TOKEN";
	private final String LOGIN_DATA="login_data";
	private final String TOKEN="token";
	private final String ID="id";
	private final String ERROR = "error";
	private final String CUSTOMER_NOT_FOUND = "Customer not found.";
	private final String EMPTY = " ";
	
	
	@Autowired
	private ZoyloUserTokenRepository zoyloUserTokenRepository;
	
	private final AuthenticationManager authenticationManager;
	
	@Autowired
	private AdminRestClient adminRestClient;

	public UserLoginRest(TokenProvider tokenProvider, AuthenticationManager authenticationManager) {
		this.tokenProvider = tokenProvider;
		this.authenticationManager = authenticationManager;
	}

	/**
	 * @author diksha gupta
	 * @description to authenticate user for zoylo system
	 * @param loginVM
	 * @param response
	 * @return
	 * @throws JSONException 
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 */
	@PostMapping("/authenticate")
	@Timed
	public ResponseEntity<CustomResponse> authorize(@Valid @RequestBody LoginVM loginVM, HttpServletResponse response) throws JSONException, JsonParseException, JsonMappingException, IOException {
		String phone = "";
		if (loginVM.getUsername() != null && !loginVM.getUsername().contains("@")) {
			if (!loginVM.getUsername().startsWith(Constants.INDIA_MOBILE_CODE)) {
				phone = loginVM.getUsername();
				loginVM.setUsername(Constants.INDIA_MOBILE_CODE + loginVM.getUsername());
			} else {
				phone = loginVM.getUsername().substring(3, loginVM.getUsername().length());
			}
		}
		UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
				loginVM.getUsername(), PasswordEncryption.getEncryptedPassword(loginVM.getPassword()));
		try {
			Authentication authentication = this.authenticationManager.authenticate(authenticationToken);
			SecurityContextHolder.getContext().setAuthentication(authentication);
			domainUserDetailsService.checkCredentialsForLogin(loginVM.getUsername(), phone,loginVM.getPassword());
              User user=null;
              CustomUserDetails customUser = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication()
      				.getPrincipal();
      		String userId = customUser.getId();
      		user = userRepository.findOneById(userId);
			if(loginVM.getDeviceId()!=null && loginVM.getDeviceType()!=null)
			{
				user = userRepository.findOneById(userId);
				if(user!=null)
				{
					user.setDeviceId(loginVM.getDeviceId());
					user.setDeviceType(loginVM.getDeviceType());
					userRepository.save(user);
				}
			}
			boolean rememberMe = (loginVM.isRememberMe() == null) ? false : loginVM.isRememberMe();
			String jwt = tokenProvider.createToken(authentication, rememberMe,loginVM.getRoleCategory());
			Set<String> providerTypeCodes=customUser.getAuthCategoryList();
			
			response.addHeader(JWTConfigurer.AUTHORIZATION_HEADER, "Bearer " + jwt);
			response.addHeader("Access-Control-Expose-Headers", "Authorization");

			HashMap<String, Object> metaInfo = new HashMap<>();
			metaInfo.put("serviceName", SuccessCode.LOGIN_SUCCESSFULLY.getServiceName());
			
			// chat
			ChatGrant chatGrant = new ChatGrant();
			chatGrant.setServiceSid(serviceSid);

			// Twilio
			Twilio.init(twilioAccountSid, authenticateToken);
			TokenVM tokenVM=new TokenVM();
			List<ZoyloUserToken> zoyloUserToken = zoyloUserTokenRepository.findByUserId(userId);

			if (zoyloUserToken != null && !zoyloUserToken.isEmpty()) {
				for (ZoyloUserToken zoyloUser : zoyloUserToken) {
					if (zoyloUser.getTokenCode().equals(Constants.TWILIO_CHAT_VALUE)) {
						AccessToken firstChatToken = new AccessToken.Builder(twilioAccountSid, twilioApiKey,
								twilioApiSecret).identity(userId).grant(chatGrant).ttl(twilioTtl).build();
						zoyloUser.setTokenValue(firstChatToken.toJwt());
						tokenVM.setChatToken(firstChatToken.toJwt());
					} 
				}
				zoyloUserTokenRepository.save(zoyloUserToken);
			}
			//for wallet
			if (user != null) {
				// Create Wallet
				WalletVM walletVM = new WalletVM();
				if (user.getEmailInfo() != null) {
					walletVM.setEmail(user.getEmailInfo().getEmailAddress());
				}
				walletVM.setFirstName(user.getFirstName());
				walletVM.setMiddleName(user.getMiddleName());
				walletVM.setLastName(user.getLastName());
				if (user.getPhoneInfo() != null) {
					walletVM.setPhoneNumber(user.getPhoneInfo().getPhoneNumber());
				}
				walletVM.setUserId(user.getId());
				walletVM.setZoyloId(user.getZoyloId());
				walletVM.setBalanceAmount(0.0);
				try {
					adminRestClient.createWallet(jwt,walletVM);
				} catch (Exception exception) {
					log.error("Exception Occured in gateway while creating user Wallet : {}", exception);

				}
			}
			if (ecommEnabled) {
				Map<String, String> ecommLoginResMap = ecommAuthentication(user,loginVM.getUsername(),loginVM.getPassword(),loginVM.getEcommAdminToken());
				if (ecommLoginResMap != null && ecommLoginResMap.size() > 0) {
					tokenVM.setEcommId(ecommLoginResMap.get(ECOMM_LOGIN_ID));
					tokenVM.setEcommToken(ecommLoginResMap.get(ECOMM_LOGIN_TOKEN));

				}
			}
			return new ResponseEntity<CustomResponse>(
					new CustomResponse(SuccessCode.LOGIN_SUCCESSFULLY.getSuccessCode(),
							SuccessCode.LOGIN_SUCCESSFULLY.getSuccessMessage(),tokenVM, metaInfo),
					HttpStatus.OK);
		} catch (CustomParameterizedException customParameterizedException) {
			log.trace("Authentication exception trace: {}", customParameterizedException);
			throw customParameterizedException;

		} catch (BadCredentialsException exception) {
			log.trace("Authentication exception trace: {}", exception);
			throw new CustomParameterizedException(RegisteredException.LOGIN_EXCEPTION.getException(),
					CustomExceptionCode.PASSWORD_WRONG.getErrMsg(), exception,
					CustomExceptionCode.PASSWORD_WRONG.getErrCode());
		} catch (AuthenticationException ae) {
			log.trace("Authentication exception trace: {}", ae);
			throw new CustomParameterizedException(RegisteredException.LOGIN_EXCEPTION.getException(),
					CustomExceptionCode.LOGIN_FAIL.getErrMsg(), ae, CustomExceptionCode.LOGIN_FAIL.getErrCode());
			}
	}

	/**
	 * Object to return as body in JWT Authentication.
	 */
	static class JWTToken {

		private String idToken;

		JWTToken(String idToken) {
			this.idToken = idToken;
		}

		@JsonProperty("id_token")
		String getIdToken() {
			return idToken;
		}

		void setIdToken(String idToken) {
			this.idToken = idToken;
		}
	}
	/**
	 * @author arvind.rawat
	 * @description this method is used for ecommerce authenticatication
	 */
	private Map<String, String> ecommAuthentication(User user,String userName,String password,String ecommAdminToken) {
		log.info("++++++ Entery in ecommAuthentication ++++++");
		log.debug("username getting from front end : {} ", userName);
		Map<String, String> ecommLoginResponseData = new HashMap<>();
		try {
			if (!userName.startsWith(Constants.INDIA_MOBILE_CODE)) {
				userName = Constants.INDIA_MOBILE_CODE + userName;
			}
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			Map<String, String> ecommLoginRequestData = new HashMap<>();
			ecommLoginRequestData.put(USER_NAME, userName);
			HttpEntity<Map<String, String>> httpEntity = new HttpEntity<Map<String, String>>(ecommLoginRequestData,
					headers);
			ResponseEntity<String> responseEntity = restTemplate.exchange(ecommLoginUrl, HttpMethod.POST, httpEntity,
					String.class);
			log.debug("Response is " + responseEntity.getBody());
			String body = responseEntity.getBody();
			JSONObject jObject = new JSONObject(body);
			JSONArray loginDataArray = jObject.getJSONArray(LOGIN_DATA);
			JSONObject jsonObject = loginDataArray.getJSONObject(0);
			String apiStatus = jObject.getString(Constants.SUCCESS);
			if (!apiStatus.equalsIgnoreCase(Constants.TRUE)) {
				log.debug("ecommerce authentication api status : {} ", apiStatus);
				String error = jsonObject.getString(ERROR);
				log.debug("error message is  : {} ", error);
				if (error.equalsIgnoreCase(CUSTOMER_NOT_FOUND)) {
					RecipientVM recipientVM = new RecipientVM();
					if (StringsUtil.isNullOrEmpty(user.getFirstName())) {
						recipientVM.setFirstName(EMPTY);
					} else {
						recipientVM.setFirstName(user.getFirstName());
					}
					if (StringsUtil.isNullOrEmpty(user.getLastName())) {
						recipientVM.setLastName(EMPTY);
					} else {
						recipientVM.setLastName(user.getLastName());
					}
					recipientVM.setEcommAdminToken(ecommAdminToken);
					recipientVM.setEmailAddress(user.getEmailInfo().getEmailAddress());
					recipientVM.setPhoneNumber(user.getPhoneInfo().getPhoneNumber());
					recipientVM.setEncryptedPassword(password);
					Map<String, String> ecommerceSignupResMap = userBll.ecommSignup(recipientVM);
					if (ecommerceSignupResMap != null && ecommerceSignupResMap.size() > 0) {
						ecommLoginResponseData.put(ECOMM_LOGIN_ID, ecommerceSignupResMap.get(ECOMM_LOGIN_ID));
						ecommLoginResponseData.put(ECOMM_LOGIN_TOKEN, ecommerceSignupResMap.get(ECOMM_LOGIN_TOKEN));
						log.debug("response ecommLoginResponseData : {} ", ecommLoginResponseData);
						log.info("------ Exit from ecommAuthentication ------");
						return ecommLoginResponseData;
					}
				} else {
					throw new CustomParameterizedException(RegisteredException.ECOMM_LOGIN_EXCEPTION.getException(),
							CustomExceptionCode.ECOMM_LOGIN_STATUS_EXCEPTION.getErrMsg(),
							CustomExceptionCode.ECOMM_LOGIN_STATUS_EXCEPTION.getErrCode());
				}
			}
			String token = jObject.getString(TOKEN);
			String loginId = jsonObject.getString(ID);
			ecommLoginResponseData.put(ECOMM_LOGIN_ID, loginId);
			ecommLoginResponseData.put(ECOMM_LOGIN_TOKEN, token);
			log.debug("loginId is : {} ", loginId);
			log.debug("response ecommLoginResponseData : {} ", ecommLoginResponseData);
			log.info("------ Exit from ecommAuthentication ------");
			return ecommLoginResponseData;
		} catch (CustomParameterizedException customException) {
			throw customException;
		} catch (HttpStatusCodeException exception) {
			log.error("Exception while accessing ecommerce login api {} ", exception);
			throw new CustomParameterizedException(RegisteredException.ECOMM_LOGIN_EXCEPTION.getException(),
					CustomExceptionCode.ECOMM_LOGIN_API_ACCESS_EXCEPTION.getErrMsg(), exception.getMessage(),
					CustomExceptionCode.ECOMM_LOGIN_API_ACCESS_EXCEPTION.getErrCode());
		} catch (Exception exception) {
			log.error("Exception while authenticating user for ecommerce login {} ", exception.getMessage());
			throw new CustomParameterizedException(RegisteredException.ECOMM_LOGIN_EXCEPTION.getException(),
					CustomExceptionCode.ECOMM_LOGIN_EXCEPTION.getErrMsg(), exception.getMessage(),
					CustomExceptionCode.ECOMM_LOGIN_EXCEPTION.getErrCode());

		}
	}
}
