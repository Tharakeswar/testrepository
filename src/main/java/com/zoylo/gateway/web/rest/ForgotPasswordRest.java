package com.zoylo.gateway.web.rest;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zoylo.gateway.bll.ForgotPasswordBll;
import com.zoylo.gateway.model.ForgotPasswordOtpVM;
import com.zoylo.gateway.model.ForgotPasswordVM;
import com.zoylo.gateway.model.GhostResetPasswordVM;
import com.zoylo.gateway.model.MobileVerificationVM;
import com.zoylo.gateway.successcode.CustomResponse;
import com.zoylo.gateway.successcode.SuccessCode;

/**
 * 
 * @author piyush.singh
 * @version 1.0
 *
 */
@RestController
@RequestMapping("api/forgot-password")
public class ForgotPasswordRest {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private ForgotPasswordBll forgotPasswordBll;
	private static  final String serviceName = "serviceName";

	/**
	 * @author piyuhs.singh
	 * @description forgot password send mail
	 * @return CustomRepsonse
	 */

	@GetMapping
	public ResponseEntity<CustomResponse> sendEmail(@RequestParam("email") String email) {
		forgotPasswordBll.sendForgotPasswordMail(email);
		return new ResponseEntity<CustomResponse>(new CustomResponse(SuccessCode.EMAIL_SEND_SUCCESS.getSuccessCode(),
				SuccessCode.EMAIL_SEND_SUCCESS.getSuccessMessage()), HttpStatus.OK);
	}

	/**
	 * @author piyuhs.singh
	 * @description forgot password verification
	 * @return CustomRepsonse
	 */

	@GetMapping("/activate")
	public ResponseEntity<CustomResponse> tokenVerification(@RequestParam("token") String token) {
		Map<String, String> userVerficationList = forgotPasswordBll.validateToken(token);
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put(serviceName, SuccessCode.TOKEN_CHECK_SUCCESS.getServiceName());
		return new ResponseEntity<CustomResponse>(new CustomResponse(SuccessCode.TOKEN_CHECK_SUCCESS.getSuccessCode(),
				SuccessCode.TOKEN_CHECK_SUCCESS.getSuccessMessage(), userVerficationList,metaInfo), HttpStatus.OK);
	}

	/**
	 * @author piyuhs.singh
	 * @description forgot password : save new password
	 * @return CustomRepsonse
	 */

	@PostMapping(path = "/reset")
	public ResponseEntity<CustomResponse> saveNewPassword(@RequestBody @Valid ForgotPasswordVM forgotPasswordVM) {
		Map<String, String> statusOdnew = forgotPasswordBll.setNewPassword(forgotPasswordVM);
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put(serviceName, SuccessCode.PASSWORD_UPDATE_SUCCESS.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.PASSWORD_UPDATE_SUCCESS.getSuccessCode(),
						SuccessCode.PASSWORD_UPDATE_SUCCESS.getSuccessMessage(), statusOdnew,metaInfo),
				HttpStatus.OK);
	}
	
	
	/**
	 * @author piyuhs.singh
	 * @description forgot password : send otp
	 * @return CustomRepsonse
	 */

	@GetMapping(path = "/send-otp")
	public ResponseEntity<CustomResponse> sendOtp(@RequestParam("mobileNumber") String mobileNumber) {
		String sendId = forgotPasswordBll.sendForgotPasswordOtp(mobileNumber);
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put(serviceName, SuccessCode.OTP_SEND_SUCCESS.getServiceName());
		return new ResponseEntity<CustomResponse>(new CustomResponse(SuccessCode.OTP_SEND_SUCCESS.getSuccessCode(),
				SuccessCode.OTP_SEND_SUCCESS.getSuccessMessage(), sendId, metaInfo), HttpStatus.OK);
	}
	
	
	/**
	 * @author piyuhs.singh
	 * @description forgot password : verify otp
	 * @return CustomRepsonse
	 */

	@PostMapping(path = "/verify-otp")
	public ResponseEntity<CustomResponse> verifyOtp(@RequestBody MobileVerificationVM mobileVerificationVM) {
		logger.info("::::::::::::::::::::");
		Map<String, String> verifyStatus = forgotPasswordBll.verifyOtp(mobileVerificationVM);
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put(serviceName, SuccessCode.OTP_VERIFICATION_SUCCESS.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.OTP_VERIFICATION_SUCCESS.getSuccessCode(),
						SuccessCode.OTP_VERIFICATION_SUCCESS.getSuccessMessage(), verifyStatus, metaInfo),
				HttpStatus.OK);
	}
	
	
	/**
	 * @author piyuhs.singh
	 * @description forgot password : reset password using OTP
	 * @return CustomRepsonse
	 */

	@PostMapping(path = "/set-password")
	public ResponseEntity<CustomResponse> resetPasswordUsingOtp(@RequestBody ForgotPasswordOtpVM forgotPasswordOtpVM) {
		logger.info("::::::::::::::::::::");
		Map<String, String> verifyStatus = forgotPasswordBll.setNewPassworUsingOtp(forgotPasswordOtpVM);
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put(serviceName, SuccessCode.PASSWORD_UPDATE_SUCCESS.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.PASSWORD_UPDATE_SUCCESS.getSuccessCode(),
						SuccessCode.PASSWORD_UPDATE_SUCCESS.getSuccessMessage(), verifyStatus, metaInfo),
				HttpStatus.OK);
	}
	
	

	/**
	 * @author piyuhs.singh
	 * @description forgot password : reset password using OTP
	 * @return CustomRepsonse
	 */

	@PostMapping(path = "/ghost-user/password")
	public ResponseEntity<CustomResponse> resetGhostPassword(@RequestBody GhostResetPasswordVM ghostResetPasswordVM) {
		Map<String, String> verifyStatus = forgotPasswordBll.setGhostNewPassword(ghostResetPasswordVM);
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put(serviceName, SuccessCode.PASSWORD_UPDATE_SUCCESS.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.PASSWORD_UPDATE_SUCCESS.getSuccessCode(),
						SuccessCode.PASSWORD_UPDATE_SUCCESS.getSuccessMessage(), verifyStatus,metaInfo),
				HttpStatus.OK);
	}

}
