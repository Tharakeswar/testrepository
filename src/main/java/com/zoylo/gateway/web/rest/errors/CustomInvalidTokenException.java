package com.zoylo.gateway.web.rest.errors;

import java.util.ArrayList;
import java.util.List;

public class CustomInvalidTokenException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private final String exception;

	private final String exceptionMessage;

	private final List<String> paramList = new ArrayList<>();

	public CustomInvalidTokenException(String exception, String exceptionMessage, String... params) {
		super(exception);
		this.exception = exception;
		this.exceptionMessage = exceptionMessage;
		if (params != null && params.length > 0) {
			for (int i = 0; i < params.length; i++) {
				paramList.add(params[i]);
			}
		}
	}
	
	public CustomInvalidTokenException(String exception, String exceptionMessage,Exception detailException, String... params) {
		super(exception);
		this.exception = exception;
		this.exceptionMessage = exceptionMessage;
		if (params != null && params.length > 0) {
			for (int i = 0; i < params.length; i++) {
				paramList.add(params[i]);
			}
		}
	}

	public CustomInvalidTokenException(String exception, String exceptionMessage, List<String> paramList) {
		super(exception);
		this.exception = exception;
		this.exceptionMessage = exceptionMessage;
		this.paramList.addAll(paramList);
	}

	public ParameterizedErrorVM getErrorVM() {
		return new ParameterizedErrorVM(exception, exceptionMessage, paramList);
	}
}
