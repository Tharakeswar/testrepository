package com.zoylo.gateway.web.rest.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.zoylo.gateway.model.WalletVM;
import com.zoylo.gateway.model.ZoyloLeadGenerationVM;
import com.zoylo.gateway.model.ZoyloNotificationVM;

@FeignClient(name = "zoyloadmin")
public interface AdminRestClient {
	@RequestMapping(method = RequestMethod.POST, value = "/zoyloadmin-0.0.1-SNAPSHOT/api/lead")
	public ResponseEntity<String> saveLead(@RequestBody ZoyloLeadGenerationVM zoyloLeadGenerationVM);
	@RequestMapping(method = RequestMethod.GET, value = "/zoyloadmin-0.0.1-SNAPSHOT/api/provider-resources/{userid}")
	public ResponseEntity<String> getServiceProviderResourceId(@PathVariable("userid") String userId);
	
	@RequestMapping(method = RequestMethod.POST, value = "/zoyloadmin-0.0.1-SNAPSHOT/api/notification")
	public ResponseEntity<String> save(@RequestBody ZoyloNotificationVM zoyloNotificationVM);
	
	@RequestMapping(method = RequestMethod.GET, value = "/zoyloadmin-0.0.1-SNAPSHOT/api/lookups/{param}")
	public ResponseEntity<String> getLookupByParam(@PathVariable("param") String param);
	
	@RequestMapping(method = RequestMethod.POST, value = "/zoyloadmin-0.0.1-SNAPSHOT/api/wallet")
	public ResponseEntity<String> createWallet(@RequestHeader("Authorization") String token,@RequestBody WalletVM walletVM);
	
	@RequestMapping(method = RequestMethod.GET, value = "zoyloadmin-0.0.1-SNAPSHOT/api/doctor-registration/gender", params = "userId")
	public ResponseEntity<String> getGenderByUserId(@RequestParam(name = "userId") String userId);

}
