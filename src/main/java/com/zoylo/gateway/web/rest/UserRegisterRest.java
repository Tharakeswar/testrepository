package com.zoylo.gateway.web.rest;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zoylo.core.mobilepush.SendPushVM;
import com.zoylo.gateway.bll.UserAuthBll;
import com.zoylo.gateway.bll.UserBll;
import com.zoylo.gateway.bll.UserProfileBll;
import com.zoylo.gateway.bll.UserService;
import com.zoylo.gateway.domain.User;
import com.zoylo.gateway.domain.ZoyloUserAuth;
import com.zoylo.gateway.model.ChangeMobileEmailVM;
import com.zoylo.gateway.model.ChangePasswordVM;
import com.zoylo.gateway.model.CustomerAttribute;
import com.zoylo.gateway.model.DiagnosticVM;
import com.zoylo.gateway.model.DoctorRoleDataVM;
import com.zoylo.gateway.model.DoctorVM;
import com.zoylo.gateway.model.EcommCustomer;
import com.zoylo.gateway.model.EcommUserRegistrationVM;
import com.zoylo.gateway.model.GhostUserFamilyMemberVM;
import com.zoylo.gateway.model.GhostUserVM;
import com.zoylo.gateway.model.MobileVerificationVM;
import com.zoylo.gateway.model.OTPDetailVM;
import com.zoylo.gateway.model.OrganizationUserDataVM;
import com.zoylo.gateway.model.PMSUserRoleMV;
import com.zoylo.gateway.model.PMSUserRoleMVList;
import com.zoylo.gateway.model.PageableVM;
import com.zoylo.gateway.model.PasswordUpdateVM;
import com.zoylo.gateway.model.RecipientVM;
import com.zoylo.gateway.model.RegisterVM;
import com.zoylo.gateway.model.ResendOTPVM;
import com.zoylo.gateway.model.UpdateRegisterVM;
import com.zoylo.gateway.model.UserDataVM;
import com.zoylo.gateway.model.UserDetailVM;
import com.zoylo.gateway.model.UserPMSDataVM;
import com.zoylo.gateway.model.UserProfileDataVM;
import com.zoylo.gateway.model.UserResponseVM;
import com.zoylo.gateway.model.UserRoleDataVM;
import com.zoylo.gateway.model.UserTypeVM;
import com.zoylo.gateway.model.UserUpdateResponceVM;
import com.zoylo.gateway.model.UserUpdateVM;
import com.zoylo.gateway.model.ZoyloUserCampaignVM;
import com.zoylo.gateway.model.ZoyloUserVM;
import com.zoylo.gateway.mv.UserVM;
import com.zoylo.gateway.repository.UserRepository;
import com.zoylo.gateway.security.CustomUserDetails;
import com.zoylo.gateway.security.SecurityUtils;
import com.zoylo.gateway.service.dto.UserDTO;
import com.zoylo.gateway.successcode.CustomResponse;
import com.zoylo.gateway.successcode.SuccessCode;
import com.zoylo.gateway.web.rest.errors.CustomExceptionCode;
import com.zoylo.gateway.web.rest.errors.CustomParameterizedException;
import com.zoylo.gateway.web.rest.errors.RegisteredException;
import com.zoylo.gateway.web.rest.util.HeaderUtil;
import com.zoylo.gateway.web.rest.util.ZoyloUserType;
import com.zoylo.gateway.web.rest.vm.KeyAndPasswordVM;
import com.zoylo.gateway.web.rest.vm.ManagedUserVM;


/**
 * @author Ankur
 * @version 1.0 REST controller for managing the current user's account.
 */
@RestController
@RequestMapping("/api")
public class UserRegisterRest {

	private final Logger logger = LoggerFactory.getLogger(UserRegisterRest.class);

	private final UserRepository userRepository;

	private final UserService userService;

	@Autowired
	private UserProfileBll userProfileBll;

	@Autowired
	private UserAuthBll userAuthBll;

	@Autowired
	private UserBll userBll;

	private static final String CHECK_ERROR_MESSAGE = "Incorrect password";

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private ObjectMapper mapper;

	public UserRegisterRest(UserRepository userRepository, UserService userService) {
		this.userRepository = userRepository;
		this.userService = userService;
	}

	/**
	 * POST /register : register the user.
	 *
	 * @param managedUserVM
	 *            the managed user View Model
	 * @return the ResponseEntity with status 201 (Created) if the user is
	 *         registered or 400 (Bad Request) if the login or email is already in
	 *         use
	 */
	@PostMapping(path = "/recipient/register")
	@Timed
	public ResponseEntity<CustomResponse> registerAccount(@Valid @RequestBody RecipientVM registerModel) {
		RegisterVM registerVM = modelMapper.map(registerModel, RegisterVM.class);
		User user = userBll.registerUser(registerVM);
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.REGISTRATION_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.REGISTRATION_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.REGISTRATION_SUCCESSFULLY.getSuccessMessage(), user, metaInfo),
				HttpStatus.OK);
	}
	
	/**
	 * POST /register : register the user.
	 *
	 * @param managedUserVM
	 *            the managed user View Model
	 * @description This method is used only for registering user for Campaign
	 * @return the ResponseEntity with status 201 (Created) if the user is
	 *         registered or 400 (Bad Request) if the login or email is already in
	 *         use
	 */
	@PostMapping(path = "/campaign/register")
	@Timed
	public ResponseEntity<CustomResponse> registerUserIfNotPresentForCampaign(@Valid @RequestBody RecipientVM registerModel) {
		RegisterVM registerVM = modelMapper.map(registerModel, RegisterVM.class);
		User user = userBll.registerUserIfNotPresentForCampaign(registerVM);
		ZoyloUserCampaignVM result = modelMapper.map(user, ZoyloUserCampaignVM.class);
		if(user.getEmailInfo() != null){
		result.setEmailId(user.getEmailInfo().getEmailAddress());
		}
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.REGISTRATION_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.REGISTRATION_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.REGISTRATION_SUCCESSFULLY.getSuccessMessage(),result, metaInfo),
				HttpStatus.OK);
	}
	
	/**
	 * @author nagaraju
	 * @description update User
	 * @param zoyloUserVM, input data to update user
	 * @return CustomResponse
	 */
	@PutMapping(path = "/campaign/updateuser")
	public ResponseEntity<CustomResponse> updateUserForCampaign(@RequestBody OTPDetailVM otpDetail) {
		 User user = userBll.updateUserForCampaign(otpDetail);
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.USER_UPDATED_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.USER_UPDATED_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.USER_UPDATED_SUCCESSFULLY.getSuccessMessage(), user, metaInfo),
				HttpStatus.OK);
	}
	


	/**
	 * GET /activate : activate the registered user.
	 *
	 * @param key
	 *            the activation key
	 * @return the ResponseEntity with status 200 (OK) and the activated user in
	 *         body, or status 500 (Internal Server Error) if the user couldn't be
	 *         activated
	 */
	@GetMapping("/activate")
	@Timed
	public ResponseEntity<String> activateAccount(@RequestParam(value = "key") String key) {
		return userService.activateRegistration(key).map(user -> new ResponseEntity<String>(HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
	}

	/**
	 * GET /authenticate : check if the user is authenticated, and return its login.
	 *
	 * @param request
	 *            the HTTP request
	 * @return the login if the user is authenticated
	 */
	@GetMapping("/authenticate")
	@Timed
	public String isAuthenticated(HttpServletRequest request) {
		logger.debug("REST request to check if the current user is authenticated");
		return request.getRemoteUser();
	}

	/**
	 * GET /account : get the current user.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the current user in body,
	 *         or status 500 (Internal Server Error) if the user couldn't be
	 *         returned
	 */
	@GetMapping("/account")
	@Timed
	public ResponseEntity<UserDTO> getAccount() {
		/*
		 * return Optional.ofNullable(userService.getUserWithAuthorities()) .map(user ->
		 * new ResponseEntity<>(new UserDTO(user), HttpStatus.OK)) .orElse(new
		 * ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
		 */
		UserDTO userDto = new UserDTO();
		Set<String> auth = new HashSet<String>();
		auth.add("ROLE_ADMIN");
		userDto.setAuthorities(auth);
		return new ResponseEntity<>(userDto, HttpStatus.OK);
	}

	/**
	 * POST /account : update the current user information.
	 *
	 * @param userDTO
	 *            the current user information
	 * @return the ResponseEntity with status 200 (OK), or status 400 (Bad Request)
	 *         or 500 (Internal Server Error) if the user couldn't be updated
	 */
	@PostMapping("/account")
	@Timed
	public ResponseEntity saveAccount(@Valid @RequestBody UserDTO userDTO) {
		final String userLogin = SecurityUtils.getCurrentUserLogin();
		Optional<User> existingUser = userRepository.findByEmailAddress(userDTO.getEmail());
		if (existingUser.isPresent() && (!existingUser.get().getUserName().equalsIgnoreCase(userLogin))) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("user-management", "emailexists", "Email already in use"))
					.body(null);
		}
		return userRepository.findOneByUserName(userLogin).map(u -> {
			userService.updateUser(userDTO.getFirstName(), userDTO.getLastName(), userDTO.getEmail(),
					userDTO.getLangKey(), userDTO.getImageUrl());
			return new ResponseEntity(HttpStatus.OK);
		}).orElseGet(() -> new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
	}

	/**
	 * POST /account/reset_password/init : Send an email to reset the password of
	 * the user
	 *
	 * @param mail
	 *            the mail of the user
	 * @return the ResponseEntity with status 200 (OK) if the email was sent, or
	 *         status 400 (Bad Request) if the email address is not registered
	 */
	/*
	 * @PostMapping(path = "/account/reset_password/init", produces =
	 * MediaType.TEXT_PLAIN_VALUE)
	 * 
	 * @Timed public ResponseEntity requestPasswordReset(@RequestBody String mail) {
	 * return userService.requestPasswordReset(mail) .map(user -> {
	 * mailService.sendPasswordResetMail(user); return new ResponseEntity<>(
	 * "email was sent", HttpStatus.OK); }).orElse(new ResponseEntity<>(
	 * "email address not registered", HttpStatus.BAD_REQUEST)); }
	 */

	/**
	 * POST /account/reset_password/finish : Finish to reset the password of the
	 * user
	 *
	 * @param keyAndPassword
	 *            the generated key and the new password
	 * @return the ResponseEntity with status 200 (OK) if the password has been
	 *         reset, or status 400 (Bad Request) or 500 (Internal Server Error) if
	 *         the password could not be reset
	 */
	@PostMapping(path = "/account/reset_password/finish", produces = MediaType.TEXT_PLAIN_VALUE)
	@Timed
	public ResponseEntity<String> finishPasswordReset(@RequestBody KeyAndPasswordVM keyAndPassword) {
		if (!checkPasswordLength(keyAndPassword.getNewPassword())) {
			return new ResponseEntity<>(CHECK_ERROR_MESSAGE, HttpStatus.BAD_REQUEST);
		}
		return userService.completePasswordReset(keyAndPassword.getNewPassword()/* , keyAndPassword.getKey() */)
				.map(user -> new ResponseEntity<String>(HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
	}

	private boolean checkPasswordLength(String password) {
		return !StringUtils.isEmpty(password) && password.length() >= ManagedUserVM.PASSWORD_MIN_LENGTH
				&& password.length() <= ManagedUserVM.PASSWORD_MAX_LENGTH;
	}

	/***
	 * @description Get User Type
	 * @return User type list
	 */

	@GetMapping("/recipient/user-types")
	public ResponseEntity<CustomResponse> getUserType() {
		List<UserTypeVM> userTypeVMs = userBll.getRecipentUserType();
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.GET_USER_TYPE.getServiceName());
		return new ResponseEntity<CustomResponse>(new CustomResponse(SuccessCode.GET_USER_TYPE.getSuccessCode(),
				SuccessCode.GET_USER_TYPE.getSuccessMessage(), userTypeVMs, metaInfo), HttpStatus.OK);

	}

	/***
	 * @description Delete user
	 * @return the ResponseEntity with status 201 (deleted)
	 */

	@DeleteMapping("/admin/{id}")
	public ResponseEntity<CustomResponse> deleteUser(@PathVariable("id") String userId) {
		userBll.deleteUser(userId);
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.DELETE_USER_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.DELETE_USER_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.DELETE_USER_SUCCESSFULLY.getSuccessMessage(), metaInfo),
				HttpStatus.OK);
	}

	/***
	 * OPTExpirationMinutes
	 * 
	 * @description Update user
	 * @return the ResponseEntity with status 201 (deleted)
	 */

	@PutMapping("/admin")
	public ResponseEntity<CustomResponse> updateUser(@Valid @RequestBody UpdateRegisterVM registerModel) {
		User user = userBll.updateUser(registerModel);
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.UPDATE_USER_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.UPDATE_USER_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.UPDATE_USER_SUCCESSFULLY.getSuccessMessage(), user, metaInfo),
				HttpStatus.OK);

	}

	/**
	 * update zoylo user email and phone number.
	 * 
	 * @param registerVM
	 * @return
	 */
	@PutMapping("/userdetails/email-mobile")
	public ResponseEntity<CustomResponse> updateUserEmailAndMobileIfNotExist(@RequestBody RegisterVM registerVM) {
		UserUpdateResponceVM userUpdateResponceVM = userBll.updateUserEmailAndMobile(registerVM);
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.UPDATE_USER_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.UPDATE_USER_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.UPDATE_USER_SUCCESSFULLY.getSuccessMessage(), userUpdateResponceVM, metaInfo),
				HttpStatus.OK);
	}
	
	/**
	 * update zoylo user email,phone number and userType.
	 * @param registerVM
	 * @return
	 */
	@PutMapping("/userdetails/update/email-mobile-usertype")
	public ResponseEntity<CustomResponse> updateUserEmailAndMobile(@RequestBody RegisterVM registerVM) {
		UserUpdateResponceVM userUpdateResponceVM = userBll.updateUserEmailAndMobileAndUserType(registerVM);
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.UPDATE_USER_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.UPDATE_USER_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.UPDATE_USER_SUCCESSFULLY.getSuccessMessage(), userUpdateResponceVM, metaInfo),
				HttpStatus.OK);
	}

	/***
	 * @description Get Admin User Type
	 * @return the ResponseEntity with status 201 and User type list
	 */

	@GetMapping("/admin/user-types")
	public ResponseEntity<CustomResponse> getAdminType() {
		List<UserTypeVM> userTypeVMs = userBll.getAdminUserType();
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.GET_USER_TYPE.getServiceName());
		return new ResponseEntity<CustomResponse>(new CustomResponse(SuccessCode.GET_USER_TYPE.getSuccessCode(),
				SuccessCode.GET_USER_TYPE.getSuccessMessage(), userTypeVMs, metaInfo), HttpStatus.OK);

	}

	/***
	 * GET /user get all user
	 * 
	 * @description Get All User page wise
	 * @return the ResponseEntity with status 201 and User Detail list {List
	 *         <UserDetailVM>}
	 */

	@GetMapping("/admin")
	public ResponseEntity<CustomResponse> getAllUser(Pageable pageable) {
		PageableVM<UserDetailVM> pageableVM = userBll.getAllByPage(pageable);
		List<UserDetailVM> users = pageableVM.getContent();
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.GET_USER_SUCCESSFULLY.getServiceName());

		ObjectMapper mapper = new ObjectMapper();
		HashMap<String, String> props = mapper.convertValue(pageableVM.getPageInfo(), HashMap.class);
		metaInfo.putAll(props);

		return new ResponseEntity<CustomResponse>(new CustomResponse(SuccessCode.GET_USER_SUCCESSFULLY.getSuccessCode(),
				SuccessCode.GET_USER_SUCCESSFULLY.getSuccessMessage(), users, metaInfo), HttpStatus.OK);

	}

	/**
	 * POST /social/setMobileEmail : Set mobile number and Email address for social
	 * media registration.
	 * 
	 * @return the ResponseEntity with status 201 (Created) if the user is
	 *         registered or 400 (Bad Request) if the login or email is already in
	 *         use
	 */
	@PostMapping(path = "/recipient/social/set-mobile-email")
	@Timed
	public ResponseEntity<CustomResponse> setMobileAndEmailId(
			@Valid @RequestBody ChangeMobileEmailVM changeMobileEmailVM) {
		User user = userBll.setMobileAndEmailId(changeMobileEmailVM);
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.SOCIAL_MOBILE_AND_EMAIL_UPDATE_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.SOCIAL_MOBILE_AND_EMAIL_UPDATE_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.SOCIAL_MOBILE_AND_EMAIL_UPDATE_SUCCESSFULLY.getSuccessMessage(), user.getId(),
						metaInfo),
				HttpStatus.OK);
	}

	/**
	 * POST /verifyMobile : Verify mobile number
	 * 
	 * @return the ResponseEntity with status 201 (Created) if the user is
	 *         registered or 400 (Bad Request) if OTP match
	 */
	@PostMapping(path = "/verify-mobile")
	@Timed
	public ResponseEntity<CustomResponse> verifyMobileNumber(
			@Valid @RequestBody MobileVerificationVM mobileVerificationVM) {
		userBll.mobileVerification(mobileVerificationVM);
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.MOBILE_VERIFY_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.MOBILE_VERIFY_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.MOBILE_VERIFY_SUCCESSFULLY.getSuccessMessage(), metaInfo),
				HttpStatus.OK);
	}

	@PostMapping(path = "/resend-otp")
	@Timed
	public ResponseEntity<CustomResponse> resentOTP(@Valid @RequestBody ResendOTPVM resentOTPVM) {
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.MOBILE_VERIFY_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.MOBILE_VERIFY_SUCCESSFULLY, userBll.resendOTP(resentOTPVM), metaInfo),
				HttpStatus.OK);
	}

	/**
	 * POST /admin/register : register the admin user.
	 *
	 * @param registerModel
	 *            the managed user View Model
	 * @return the ResponseEntity with status 201 (Created) if the user is
	 *         registered or 400 (Bad Request) if the login or email is already in
	 *         use
	 */
	@PostMapping(path = "/admin")
	@Timed
	public ResponseEntity<CustomResponse> registerAdminAccount(@Valid @RequestBody RegisterVM registerModel) {
		User user = userBll.registerUser(registerModel);
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.ADMIN_REGISTRATION_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.ADMIN_REGISTRATION_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.ADMIN_REGISTRATION_SUCCESSFULLY.getSuccessMessage(), user.getId(), metaInfo),
				HttpStatus.OK);
	}

	@PostMapping(path = "/register-doctor")
	@Timed
	public ResponseEntity<CustomResponse> registerPIDcotorAccount(@RequestBody RegisterVM registerModel) {
		User user = userBll.registerUser(registerModel);
		UserResponseVM responseVM = modelMapper.map(user, UserResponseVM.class);
		responseVM.setUserType(ZoyloUserType.RECIPIENT.getUserType());
		responseVM.setIsUserExist(false);
		responseVM.setRegisterType("new");
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.ADMIN_REGISTRATION_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.ADMIN_REGISTRATION_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.ADMIN_REGISTRATION_SUCCESSFULLY.getSuccessMessage(), responseVM, metaInfo),
				HttpStatus.OK);
	}

	/**
	 * POST /doctor/save : save doctor detail.
	 *
	 * @param doctorVM
	 *            the managed doctor View Model
	 * @return the ResponseEntity with status 201 (Created)
	 */
	@PostMapping(path = "/recipient/doctor")
	@Timed
	public ResponseEntity<CustomResponse> saveDoctorDetail(@Valid @RequestBody DoctorVM doctorVM) {
		userBll.saveDoctorDetail(doctorVM);
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.DOCTOR_SAVE_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.DOCTOR_SAVE_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.DOCTOR_SAVE_SUCCESSFULLY.getSuccessMessage(), metaInfo),
				HttpStatus.OK);
	}

	/**
	 * POST /diagnostic/save : save diagnostic detail.
	 *
	 * @param diagonsticVM
	 *            the managed diagnostic View Model
	 * @return the ResponseEntity with status 201 (Created)
	 */
	@PostMapping(path = "/recipient/diagnostic")
	@Timed
	public ResponseEntity<CustomResponse> saveDiagnosticDetail(@Valid @RequestBody DiagnosticVM diagonsticVM) {
		userBll.saveDiagnosticDetail(diagonsticVM);
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.DIANOSTIC_SAVE_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.DIANOSTIC_SAVE_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.DIANOSTIC_SAVE_SUCCESSFULLY.getSuccessMessage(), metaInfo),
				HttpStatus.OK);
	}

	/**
	 * Activate the user registered Email
	 * 
	 * @author Mehraj Malik
	 */
	@GetMapping("/email/activate")
	public ResponseEntity<CustomResponse> tokenVerification(@RequestParam("token") String token) {
		boolean isVerified = userBll.validateToken(token);
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.EMAIL_VERIFICATION.getServiceName());
		return new ResponseEntity<CustomResponse>(new CustomResponse(SuccessCode.EMAIL_VERIFICATION.getSuccessCode(),
				SuccessCode.EMAIL_VERIFICATION.getSuccessMessage(), isVerified, metaInfo), HttpStatus.OK);
	}

	/**
	 * POST /register : register the user.
	 *
	 * @param managedUserVM
	 *            the managed user View Model
	 * @return the ResponseEntity with status 201 (Created) if the user is
	 *         registered or 400 (Bad Request) if the login or email is already in
	 *         use
	 */
	@PostMapping(path = "/ghost-user")
	public ResponseEntity<CustomResponse> save(@Valid @RequestBody GhostUserVM ghostUserVM) {
		logger.info("---In Gateway controller ... before service method");
		GhostUserVM ghostUser = userBll.addGhostUser(ghostUserVM);
		logger.info("---In Gateway controller ... after service method");
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.REGISTRATION_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.REGISTRATION_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.REGISTRATION_SUCCESSFULLY.getSuccessMessage(), ghostUser, metaInfo),
				HttpStatus.OK);
	}

	/***
	 * GET /user get all user
	 * 
	 * @description Get All User page wise
	 * @return the ResponseEntity with status 201 and User Detail list
	 *         {List<UserDetailVM>}
	 */

	@GetMapping("/users/{id}")
	public ResponseEntity<CustomResponse> getUser(@PathVariable("id") String id) {
		logger.info("Get user in gateway while appointment.");
		UserProfileDataVM userProfileDataVM = userBll.getUser(id);
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.GET_USER_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.GET_USER_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.GET_USER_SUCCESSFULLY.getSuccessMessage(), userProfileDataVM, metaInfo),
				HttpStatus.OK);

	}

	/***
	 * GET /user get all user
	 * 
	 * @description Get All User page wise
	 * @return the ResponseEntity with status 201 and User Detail list
	 *         {List<UserDetailVM>}
	 */

	@GetMapping("/user-data/search")
	public ResponseEntity<CustomResponse> getUserData(@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "mobile", required = false) String mobile) {
		UserProfileDataVM userProfileDataVM = userBll.getUserData(email, mobile);
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.GET_USER_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.GET_USER_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.GET_USER_SUCCESSFULLY.getSuccessMessage(), userProfileDataVM, metaInfo),
				HttpStatus.OK);

	}

	/***
	 * 
	 * @param RegisterVM
	 * @description Get All User page wise
	 * @return the ResponseEntity with status 201 and User Detail list
	 *         {List<UserDetailVM>}
	 */

	@PostMapping(path = "/userdetail")
	public ResponseEntity<CustomResponse> getUserByMobile(@RequestBody RegisterVM registerModel) {
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.ADMIN_REGISTRATION_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.ADMIN_REGISTRATION_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.ADMIN_REGISTRATION_SUCCESSFULLY.getSuccessMessage(),
						userBll.getUserByMobile(registerModel), metaInfo),
				HttpStatus.OK);
	}

	/**
	 * POST /register : register the user.
	 * 
	 * @author Devendra.Kumar
	 * @description To change User Password
	 * @param customUser
	 * @param changePasswordVM
	 */
	@PostMapping("/account/change-password")
	@Timed
	public ResponseEntity<CustomResponse> changePassword(@AuthenticationPrincipal CustomUserDetails customUser,
			@Valid @RequestBody ChangePasswordVM changePasswordVM) {
		logger.info("Change User Password of UserResource controller ");

		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.PASSWORD_UPDATE_SUCCESS.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.PASSWORD_UPDATE_SUCCESS.getSuccessCode(),
						SuccessCode.PASSWORD_UPDATE_SUCCESS.getSuccessMessage(),
						userProfileBll.changePassword(customUser.getId(), changePasswordVM), metaInfo),
				HttpStatus.OK);
	}

	/**
	 * @author Diksha Gupta To add a ghost user
	 * @param userDataVM
	 *            the managed user View Model
	 * @return the ResponseEntity with status 201 (Created) if the user is
	 *         registered or 400 (Bad Request) if the login or email is already in
	 *         use
	 */
	@PostMapping(path = "/pms-users")
	public ResponseEntity<CustomResponse> savePMSUser(@Valid @RequestBody UserDataVM userDataVM) {
		UserPMSDataVM userProfileDataVM = userBll.savePMSUser(userDataVM);
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.REGISTRATION_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.REGISTRATION_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.REGISTRATION_SUCCESSFULLY.getSuccessMessage(), userProfileDataVM, metaInfo),
				HttpStatus.OK);
	}

	/**
	 * @author Iti Gupta To add a ghost user family member
	 *
	 * @param managedUserVM
	 *            the managed user View Model
	 * @return the ResponseEntity with status 201 (Created) if the user is
	 *         registered or 400 (Bad Request) if the login or email is already in
	 *         use
	 */
	@PostMapping(path = "/ghost-user-family-member")
	public ResponseEntity<CustomResponse> saveGhostUser(
			@Valid @RequestBody GhostUserFamilyMemberVM ghostUserFamilyMemberVM) {
		GhostUserFamilyMemberVM ghostUserFamilyMember = userBll.addGhostUserFamilyMember(ghostUserFamilyMemberVM);
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.REGISTRATION_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.REGISTRATION_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.REGISTRATION_SUCCESSFULLY.getSuccessMessage(), ghostUserFamilyMember, metaInfo),
				HttpStatus.OK);
	}

	/***
	 * GET /user get all user
	 * 
	 * @description Get All User page wise
	 * @return the ResponseEntity with status 201 and User Detail list
	 *         {List<UserDetailVM>}
	 */

	@GetMapping("/user-data")
	public ResponseEntity<CustomResponse> getWalkinUserData(@RequestParam("search") String search) {
		UserProfileDataVM userProfileDataVM = userBll.getWalkinUserData(search);
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.GET_USER_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.GET_USER_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.GET_USER_SUCCESSFULLY.getSuccessMessage(), userProfileDataVM, metaInfo),
				HttpStatus.OK);

	}

	/**
	 * Provides a collection of User's accroding to provided device type
	 * 
	 * @author Mehraj Malik
	 * @param deviceType
	 *            Type of device needs to be found
	 * @param pageable
	 *            Pageable Object, if not provided 20 records at a time
	 */
	@GetMapping("/device-type/{devicetype}")
	public ResponseEntity<CustomResponse> searchByDevicetype(@PathVariable("devicetype") String deviceType,
			Pageable pageable) {
		PageableVM<User> users = userBll.getByDeviceType(deviceType, pageable);
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.GET_USER_SUCCESSFULLY.getSuccessMessage());
		metaInfo.put("pagination", users.getPageInfo());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.GET_USER_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.GET_USER_SUCCESSFULLY.getSuccessMessage(), users.getContent(), metaInfo),
				HttpStatus.OK);
	}

	/**
	 * @author Devendra.Kumar
	 * @param name
	 * @param pageable
	 * @return
	 */
	@GetMapping(params = "name", value = "/admin")
	public ResponseEntity<CustomResponse> searchByName(@RequestParam("name") String name, Pageable pageable) {
		PageableVM<UserDetailVM> users = userBll.getAllByName(pageable, name);
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.GET_USER_SUCCESSFULLY.getSuccessMessage());
		metaInfo.put("pagination", users.getPageInfo());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.GET_USER_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.GET_USER_SUCCESSFULLY.getSuccessMessage(), users.getContent(), metaInfo),
				HttpStatus.OK);
	}

	@PostMapping(path = "/push-notification")
	public ResponseEntity<CustomResponse> pushNotification(@RequestBody SendPushVM sendPushVM) {
		userBll.sendNotifiation(sendPushVM);
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.ADMIN_REGISTRATION_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.ADMIN_REGISTRATION_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.ADMIN_REGISTRATION_SUCCESSFULLY.getSuccessMessage(), null, metaInfo),
				HttpStatus.OK);
	}

	/***
	 * GET /user get all user
	 * 
	 * @description Get All User page wise
	 * @return the ResponseEntity with status 201 and User Detail list
	 *         {List<UserDetailVM>}
	 */

	@PostMapping("/user-data/password")
	public ResponseEntity<CustomResponse> updatePassword(@RequestBody PasswordUpdateVM passwordUpdateVM) {
		userBll.updatePassword(passwordUpdateVM);
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.PASSWORD_UPDATE_SUCCESS.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.PASSWORD_UPDATE_SUCCESS.getSuccessCode(),
						SuccessCode.PASSWORD_UPDATE_SUCCESS.getSuccessMessage(), metaInfo),
				HttpStatus.OK);

	}

	/***
	 * POST /set role
	 * 
	 * @description set PMS user role
	 * @return the ResponseEntity with status 201
	 */

	@PostMapping("/set-roles")
	public ResponseEntity<CustomResponse> setRole(@RequestBody DoctorRoleDataVM doctorRoleDataVM) {
		userBll.setDoctorRole(doctorRoleDataVM);
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.SET_PMS_ROLE_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.SET_PMS_ROLE_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.SET_PMS_ROLE_SUCCESSFULLY.getSuccessMessage(), metaInfo),
				HttpStatus.OK);

	}

	/***
	 * GET /user get pms user role
	 * 
	 * @description Get All User page wise
	 * @return the ResponseEntity with status 201 and User Detail list
	 *         {List<UserDetailVM>}
	 */

	@PostMapping("/pmsuser-role")
	public ResponseEntity<CustomResponse> getPmsUserRole(@RequestBody PMSUserRoleMVList pMSUserRoleMVList) {
		PMSUserRoleMVList userProfileDataVM = userBll.getPMSUserRole(pMSUserRoleMVList);
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.GET_PMS_USER_ROLE_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.GET_PMS_USER_ROLE_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.GET_PMS_USER_ROLE_SUCCESSFULLY.getSuccessMessage(), userProfileDataVM, metaInfo),
				HttpStatus.OK);

	}

	/***
	 * GET /user get pms user
	 * 
	 * @description Get All User page wise
	 * @return the ResponseEntity with status 201 and User Detail list
	 *         {List<UserDetailVM>}
	 */

	@GetMapping("/provider-admin")
	public ResponseEntity<CustomResponse> getProviderAdmin(
			@RequestParam(name = "providerid", required = true) String serviceProviderId,
			@RequestParam(name = "providertypecode", required = true) String providerTypeCode,
			@RequestParam(name = "rolecode", required = true) String roleCode) {
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.GET_USER_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.GET_USER_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.GET_USER_SUCCESSFULLY.getSuccessMessage(),
						userBll.getProviderUserDetail(serviceProviderId, providerTypeCode, roleCode), metaInfo),
				HttpStatus.OK);

	}
	
	/***
	 * GET /user get pms user
	 * 
	 * @description Get All User page wise
	 * @return the ResponseEntity with status 201 and User Detail list
	 *         {List<UserDetailVM>}
	 */

	@GetMapping("/pms-users/roles")
	public ResponseEntity<CustomResponse> getProviderByRole(
			@RequestParam(name = "providerid", required = true) String serviceProviderId,
			@RequestParam(name = "rolecode", required = true) String roleCode) {
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.GET_USER_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.GET_USER_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.GET_USER_SUCCESSFULLY.getSuccessMessage(),
						userBll.getProviderUserDetail(serviceProviderId, roleCode), metaInfo),
				HttpStatus.OK);

	}
	/***
	 * @author Iti Gupta
	 * @param token
	 * @param userUpdateVM
	 * @return
	 */
	@PostMapping("/update-user-profiles")
	public ResponseEntity<CustomResponse> updateUserProfile(@RequestHeader("Authorization") String token,@RequestBody UserUpdateVM userUpdateVM)
	{
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.UPDATE_USER_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<>(
				new CustomResponse(SuccessCode.UPDATE_USER_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.UPDATE_USER_SUCCESSFULLY.getSuccessMessage(),
						userBll.updateUser(userUpdateVM),metaInfo),
				HttpStatus.OK);
		
	}
	
	/***
	 * GET /user get user
	 * 
	 * @description Get User data
	 * @return the ResponseEntity with status 201 and User Detail
	 *         {OrganizationUserDataVM}
	 */

	@GetMapping("/users/organizationuserdata/{id}")
	public ResponseEntity<CustomResponse> fetchOrganizationUserData(@PathVariable("id") String id) {
		logger.info("Get user in gateway for organization users.");
		OrganizationUserDataVM orgUserDataVM = userBll.fetchOrganizationUserData(id);
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.GET_USER_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.GET_USER_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.GET_USER_SUCCESSFULLY.getSuccessMessage(), orgUserDataVM, metaInfo),
				HttpStatus.OK);

	}
	
	/**
	 * @author nagaraju
	 * @description Create User
	 * @param zoyloUserVM, input data to create user
	 * @return CustomResponse
	 */
	@PostMapping(path = "/createuser")
	public ResponseEntity<CustomResponse> createUser( @RequestBody ZoyloUserVM zoyloUserVM) {
		UserVM newUser = userBll.createUser(zoyloUserVM);
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.USER_CREATED_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.USER_CREATED_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.USER_CREATED_SUCCESSFULLY.getSuccessMessage(), newUser, metaInfo),
				HttpStatus.OK);
	}

	/**
	 * @author nagaraju
	 * @description update User
	 * @param zoyloUserVM, input data to update user
	 * @return CustomResponse
	 */
	@PutMapping(path = "/updateuser")
	public ResponseEntity<CustomResponse> updateUser(@RequestBody ZoyloUserVM zoyloUserVM) {
		UserVM updatedUser = userBll.updateUser(zoyloUserVM);
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.USER_UPDATED_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.USER_UPDATED_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.USER_UPDATED_SUCCESSFULLY.getSuccessMessage(), updatedUser, metaInfo),
				HttpStatus.OK);
	}

	/**
	 * @author nagaraju
	 * @description assign role to a User
	 * @param userRoleDataVM, input data to assign role
	 * @return CustomResponse
	 */
	@PostMapping("/addrole")
	public ResponseEntity<CustomResponse> addRole(@RequestBody UserRoleDataVM userRoleDataVM) {
		ZoyloUserAuth zoyloUserAuth = userAuthBll.addRole(userRoleDataVM);
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.ADD_USER_ROLE.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.ADD_USER_ROLE.getSuccessCode(),
						SuccessCode.ADD_USER_ROLE.getSuccessMessage(), zoyloUserAuth, metaInfo),
				HttpStatus.OK);
	}

	/**
	 * @author nagaraju
	 * @description un assign role to a User
	 * @param userRoleDataVM, input data to un assign role
	 * @return CustomResponse
	 */
	@PutMapping("/updaterole")
	public ResponseEntity<CustomResponse> updateRole(@RequestBody UserRoleDataVM userRoleDataVM) {
		ZoyloUserAuth zoyloUserAuth = userAuthBll.updateRole(userRoleDataVM);
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.UPDATE_USER_ROLE.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.UPDATE_USER_ROLE.getSuccessCode(),
						SuccessCode.UPDATE_USER_ROLE.getSuccessMessage(), zoyloUserAuth, metaInfo),
				HttpStatus.OK);
	}

	/**
	 * @author nagaraju
	 * @description search users
	 * @param activeFlag, status of user
	 * @param isZoyloEmployee, flag to denote if the user being searched is a zoylo employee
	 * @param pageable
	 * @return CustomResponse
	 */
	@GetMapping("/search")
	public ResponseEntity<CustomResponse> search(Pageable pageable, @RequestParam(name = "activeFlag", required = false) boolean activeFlag,
			@RequestParam(name = "zoyloEmployee", required = false) boolean isZoyloEmployee,
			@RequestParam(name = "firstName", required = false)String firstName, 
			@RequestParam(name = "lastName", required = false)String lastName, 
			@RequestParam(name = "emailId", required = false)String emailId, 
			@RequestParam(name = "mobileNumber", required = false)String mobileNumber) {
		PageableVM<ZoyloUserVM> pageableVM = userBll.search(pageable, activeFlag, isZoyloEmployee, firstName, lastName, emailId, mobileNumber);
		List<ZoyloUserVM> result = pageableVM.getContent();
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.SEARCH_USER_ROLE.getServiceName());
	    HashMap<String, String> props = mapper.convertValue(pageableVM.getPageInfo(), HashMap.class);
		metaInfo.putAll(props);
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.SEARCH_USER_ROLE.getSuccessCode(),
						SuccessCode.SEARCH_USER_ROLE.getSuccessMessage(), result, metaInfo),
				HttpStatus.OK);
	}
	
	/**
	 * @author RakeshH
	 * @description Get user, user role information for user management
	 * @return CustomResponse
	 */
	@GetMapping("{id}/userwithrole")
	public ResponseEntity<CustomResponse> userDetailsWithRole(@PathVariable("id") String id){
		ZoyloUserVM userVm = userBll.fetchUserRoleDetail(id);
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.GET_USER_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.GET_USER_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.GET_USER_SUCCESSFULLY.getSuccessMessage(), userVm, metaInfo),
				HttpStatus.OK);
	}
	
	
	/**
	 * @author RakeshH
	 * @description Get user, user role information for user management
	 * @return CustomResponse
	 */
	@GetMapping("userList")
	public ResponseEntity<CustomResponse> userList(@RequestParam(value = "activeFlag", defaultValue = "true")Boolean activeFlag, 
			@RequestParam(value = "zoyloEmployee", defaultValue = "true") Boolean zoyloEmployee) {
		List<User> userList = userBll.fetchUserList(activeFlag, zoyloEmployee);
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.GET_USER_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.GET_USER_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.GET_USER_SUCCESSFULLY.getSuccessMessage(), userList, metaInfo),
				HttpStatus.OK);
	}
	
	/**
	 * @author RakeshH
	 * @param verificationToken
	 * @param emailAddress
	 * @return
	 */
	@GetMapping("/verifyemailtoken")
	public ResponseEntity<CustomResponse> verfiyEmail (@RequestParam (value = "verToken") String verificationToken,
			@RequestParam (value = "emailid") String emailAddress){
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.EMAIL_VERIFIED_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.EMAIL_VERIFIED_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.EMAIL_VERIFIED_SUCCESSFULLY.getSuccessMessage(), userBll.verifyEmailToken(verificationToken, emailAddress), metaInfo),
				HttpStatus.OK);
	}
	
	
	/**
	 * @author RakeshH
	 * @param emailAddress
	 * @return
	 */
	@PostMapping("/generateemailtoken")
	public ResponseEntity<CustomResponse> generateEmailToken (
			@RequestParam (value = "emailid") String emailAddress){
		userBll.generateEmailToken(emailAddress);
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.EMAIL_VERFICATION_TOKEN_GENERATED_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.EMAIL_VERFICATION_TOKEN_GENERATED_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.EMAIL_VERFICATION_TOKEN_GENERATED_SUCCESSFULLY.getSuccessMessage(), "Generated", metaInfo),
				HttpStatus.OK);
	}
	
	/***
	 * GET /user get pms user role
	 * 
	 * @description Get All User page wise
	 * @return the ResponseEntity with status 201 and User Detail list
	 *         {List<UserDetailVM>}
	 */

	@GetMapping("/resource-provider-role")
	public ResponseEntity<CustomResponse> getPracticeLocationRole(
			@RequestParam(name = "serviceproviderid", required = true) String serviceProviderId,
			@RequestParam(name = "userid", required = true) String userId) {
		PMSUserRoleMV userProfileDataVM = userBll.getPracticeLocationRole(serviceProviderId,userId);
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.GET_PMS_USER_ROLE_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.GET_PMS_USER_ROLE_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.GET_PMS_USER_ROLE_SUCCESSFULLY.getSuccessMessage(), userProfileDataVM, metaInfo),
				HttpStatus.OK);
	}
	

	@GetMapping("/{phoneNumber}/number")
	public ResponseEntity<CustomResponse> getUserByPhoneNumber(@RequestHeader("Authorization") String token,@PathVariable("phoneNumber")String phoneNumber) {
		User user = userBll.getUserByPhoneNumber(phoneNumber);
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.GET_USER_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(new CustomResponse(SuccessCode.GET_USER_SUCCESSFULLY.getSuccessCode(),
				SuccessCode.GET_USER_SUCCESSFULLY.getSuccessMessage(), user, metaInfo), HttpStatus.OK);

	}
	
	@GetMapping("/{email}/email")
	public ResponseEntity<CustomResponse> getUserByEmailId(@RequestHeader("Authorization") String token,@PathVariable("email")String email) {
		User user = userBll.getUserByEmail(email);
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.GET_USER_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(new CustomResponse(SuccessCode.GET_USER_SUCCESSFULLY.getSuccessCode(),
				SuccessCode.GET_USER_SUCCESSFULLY.getSuccessMessage(), user, metaInfo), HttpStatus.OK);

	}
	
	/**
	 * @author Devendra.Kumar
	 * @param serviceProviderId
	 * @param userId
	 * @return
	 */
	@PostMapping("/users/roles")
	public ResponseEntity<CustomResponse> getUserRoleByProviderId(
			@RequestParam(name = "providerid", required = true) String serviceProviderId,
			@RequestBody List<String> userIds) {
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.GET_USER_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(new CustomResponse(SuccessCode.GET_USER_SUCCESSFULLY.getSuccessCode(),
				SuccessCode.GET_USER_SUCCESSFULLY.getSuccessMessage(),
				userBll.getUserRoleByProviderId(serviceProviderId, userIds), metaInfo), HttpStatus.OK);

	}
	
	/**
	 * @author Devendra.Kumar
	 * @param serviceProviderId
	 * @param userId
	 * @return
	 */
	@PutMapping("/users/roles")
	public ResponseEntity<CustomResponse> updateRoles(
			@RequestParam(name = "userId", required = true) String userId,
			@RequestParam(name = "providerId", required = true) String providerId,
			@RequestParam(name = "role-code", required = true) String roleCode) {
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.UPDATE_USER_ROLE.getServiceName());
		return new ResponseEntity<CustomResponse>(new CustomResponse(SuccessCode.UPDATE_USER_ROLE.getSuccessCode(),
				SuccessCode.UPDATE_USER_ROLE.getSuccessMessage(),
				userBll.updateRoles(userId,providerId,roleCode), metaInfo), HttpStatus.OK);

	}
	
	/**
	 * @author Devendra.Kumar
	 * @description Get Map<userPhone,userId> on the basis of given phoneNumbers
	 * @param phoneNumbers
	 */
	@PostMapping("/user/phones")
	public ResponseEntity<CustomResponse> getUserIdByPhoneNumber(@RequestBody ArrayList<String> phoneNumbers) {
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.USER_IDS_FETCHED.getServiceName());
		return new ResponseEntity<CustomResponse>(new CustomResponse(SuccessCode.USER_IDS_FETCHED.getSuccessCode(),
				SuccessCode.USER_IDS_FETCHED.getSuccessMessage(),
				userBll.getUserIdByPhoneNumber(phoneNumbers), metaInfo), HttpStatus.OK);
	}	

	@PostMapping("/ecommerce/updateuser")
	public ResponseEntity<CustomResponse> populateUserToEcomm(@RequestParam(value = "start-date", required = false) @DateTimeFormat(pattern = "YYYY-MM-dd") Date startDate,
			@RequestParam(value = "end-date", required = false) @DateTimeFormat(pattern = "YYYY-MM-dd") Date endDate){
		
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.UPDATE_ECOMM_USER.getServiceName());
		return new ResponseEntity<CustomResponse>(new CustomResponse(SuccessCode.UPDATE_ECOMM_USER.getSuccessCode(),
				SuccessCode.UPDATE_ECOMM_USER.getSuccessMessage(),
				userBll.populateUserToEcomm(startDate, endDate), metaInfo), HttpStatus.OK);
	}
	
	@GetMapping("/user/check-email-verified")
	public ResponseEntity<CustomResponse> checkEmailVerified(@RequestParam("userId") String userId) {
		Boolean emailVerifiedStatus = userBll.checkEmailVerified(userId);
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.GET_USER_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(new CustomResponse(SuccessCode.GET_USER_SUCCESSFULLY.getSuccessCode(),
				SuccessCode.GET_USER_SUCCESSFULLY.getSuccessMessage(), emailVerifiedStatus, metaInfo), HttpStatus.OK);

	}
	
	@PostMapping(path = "/diagnostic/ghost-user")
	public ResponseEntity<CustomResponse> saveGhostUser(@Valid @RequestBody GhostUserVM ghostUserVM) {
		logger.info("---In Gateway controller ... before service method");
		GhostUserVM ghostUser = userBll.addGhostUserInDiagnostics(ghostUserVM);
		logger.info("---In Gateway controller ... after service method");
		HashMap<String, Object> metaInfo = new HashMap<>();
		metaInfo.put("serviceName", SuccessCode.REGISTRATION_SUCCESSFULLY.getServiceName());
		return new ResponseEntity<CustomResponse>(
				new CustomResponse(SuccessCode.REGISTRATION_SUCCESSFULLY.getSuccessCode(),
						SuccessCode.REGISTRATION_SUCCESSFULLY.getSuccessMessage(), ghostUser, metaInfo),
				HttpStatus.OK);
	}
	
}
