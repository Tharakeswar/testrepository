package com.zoylo.gateway.web.rest.util;

public enum DiagnosticPopularSearch {

	COMPLATE_BLOOD_PICTURE("Complete Blood Picture (CBP)"),
	BLOOD_UREA("Blood Urea"),
	CREATININE("Creatinine"),
	FASTING_BLOOD_SUGAR("Fasting Blood Sugar"),
	POST_PRANDIAL_BLOOD_SUGER("Post Prandial Blood Sugar"),
	FBS_PLBS("FBS & PLBS"),
	SERUM_ELECTROLYTES("Serum Electrolytes"),
	BILIRUBIN_TOTAL_AND_DIRECT("Bilirubin Total And Direct"),
	COMPLETE_URINE_EXAMINATION("Complete Urine Examination (C U E)"),
	BLOOD_GROUPING("Blood Grouping"),;
	
	private final String search;

	private DiagnosticPopularSearch(final String search) {
		this.search = search;

	}

	public String getSearch() {
		return search;
	}
}
