package com.zoylo.gateway.util;

import java.util.ArrayList;
import java.util.List;

import com.zoylo.gateway.domain.Permission;
import com.zoylo.gateway.domain.PermissionData;

public class PermissionUtil {

/**
 * @description mock object for role to save	
 * @return
 */
public static List<Permission> mockPermission(){
	
	String languageId = "59a56089a92bccfa399d09fa";
	String languageCode = "en";
	String permissionDisplayName = "read";
	String permisisonBriefDescription = "view data";
	List<Permission> permissions= new ArrayList<>();	
	List<PermissionData> permissionDatas = new ArrayList<>();
	PermissionData permissionData = new PermissionData(languageId, languageCode, permissionDisplayName, permisisonBriefDescription);
	permissionDatas.add(permissionData);

	Permission permission1 = new Permission("doctor_read",permissionDatas, true);
	Permission permission2 = new Permission("admin_read",permissionDatas, true);
	Permission permission3 = new Permission("superadmin_read",permissionDatas, true);
	Permission permission4 = new Permission("diagnotic_read",permissionDatas, true);
	Permission permission5 = new Permission("user_read",permissionDatas, true);
	permissions.add(permission1);
	permissions.add(permission2);
	permissions.add(permission3);
	permissions.add(permission4);
	permissions.add(permission5);
		return permissions;
		
	}

}
