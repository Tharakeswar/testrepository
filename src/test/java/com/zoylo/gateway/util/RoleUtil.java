package com.zoylo.gateway.util;

import java.util.ArrayList;
import java.util.List;

import com.zoylo.gateway.domain.Role;
import com.zoylo.gateway.domain.RoleData;

public class RoleUtil {

	/**
	 * @description mock object for role to save
	 * @return
	 */
	public static List<Role> mockRole() {

		String languageId = "59a56089a92bccfa399d09fa";
		String languageCode = "en";
		String roleDisplayName = "read";
		String roleBriefDescription = "view data";
		String permissionId = "59a56089a92bccfa399d09fb";
		String permissionCode = "ROLE_role_read";
		List<Role> roles = new ArrayList<Role>();
		List<RoleData> roleDatas = new ArrayList<RoleData>();
		RoleData roleData = new RoleData(languageId, languageCode, roleDisplayName, roleBriefDescription);
		roleDatas.add(roleData);
		Role role1 = new Role("admin", roleDatas, null, true);
		Role role2 = new Role("subadmin", roleDatas, null, true);
		Role role3 = new Role("superadmin", roleDatas, null, true);
		Role role4 = new Role("admin1", roleDatas, null, true);
		Role role5 = new Role("admin2", roleDatas, null, true);
		roles.add(role1);
		roles.add(role2);
		roles.add(role3);
		roles.add(role4);
		roles.add(role5);
		return roles;

	}

}
